import { addDecorator, addParameters } from "@storybook/react";
import { create } from "@storybook/theming";
import Logo from "assets/images/icons/logo_white.png";
import { wrapWithProviders } from "utils/testUtils";

import "antd-mobile/dist/antd-mobile.css";
import "antd/dist/antd.css";
import "style/index.scss";
import "assets/icon/style.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/core/lib/css/blueprint.css";

const params = {
  options: {
    theme: create({
      base: "dark",
      fontBase: '"Roboto", Impact, Charcoal, sans-serif',
      brandTitle: "CoinFLEX",
      brandUrl: "https://coinflex.com",
      brandImage: Logo
    })
  }
};

addParameters(params);
addDecorator(wrapWithProviders);
