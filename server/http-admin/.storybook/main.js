const override = require("../config-overrides");

module.exports = {
  stories: ["../src/**/stories.js"],
  addons: [
    "@storybook/preset-create-react-app",
    "@storybook/addon-actions",
    "@storybook/addon-links"
  ],
  webpackFinal: override.webpack
};
