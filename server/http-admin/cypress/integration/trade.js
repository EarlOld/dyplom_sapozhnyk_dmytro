const CONFIG = require("../config/config").CONFIG;
const ENVIRONMENT = Cypress.env("ENVIRONMENT");
const { EMAIL, PASSWORD, URL } = CONFIG[ENVIRONMENT];

describe("E2E Tests", () => {
  console.log("CONFIG[ENVIRONMENT]", CONFIG[ENVIRONMENT]);
  it("Should login successfully", () => {
    cy.visit(URL);

    // Add email to email input
    cy.get(".login")
      .find("#normal_login_email")
      .focus()
      .type(EMAIL)
      .should("have.value", EMAIL);

    // Add password to password input
    cy.get(".login")
      .find("#normal_login_password")
      .focus()
      .type(PASSWORD)
      .should("have.value", PASSWORD);

    // Submit the form
    cy.get(".login-form").submit();

    // Add 2fa details
    cy.wait(3000);
    cy.get(".login")
      .find("#normal_login_twofactor_token")
      .focus()
      .type("this account has got 2fa disabled");

    // Submit the form again
    cy.get(".login-form").submit();

    // If successfully logged in the user will now see the leverage-slider component
    cy.get(".leverage-sliders").should("exist");

    // And XBT/USDT for the current month should be in view
    cy.get(".trade-ticket")
      .find(".header")
      .contains("XBT/USDT");

    // Let's change the asset to ETH/USDT (index 4)
    cy.get(".market-container")
      .find(".market-card")
      .eq(3)
      .click();

    // Check that the market changed:
    cy.get(".trade-ticket")
      .find(".header")
      .contains("ETH/USDT");

    // Open the leverage sliders
    // cy.get(".trade-ticket")
    //   .find(".leverage-sliders")
    //   .find("input[type=checkbox]")
    //   .first()
    //   .click();
    //
    // //Repay all loans
    // cy.get(".trade-ticket")
    //   .find(".leverage-sliders")
    //   .find(".repay-all")
    //   .click();

    // Take some USDT leverage
    // cy.get(".trade-ticket")
    //   .find(".ant-slider")
    //   .eq(1)
    //   .click(1, 0);
    //
    // // Take some ETH leverage
    // cy.get(".trade-ticket")
    //   .find(".ant-slider")
    //   .first()
    //   .click(1, 0);

    // Set the trade amount
    cy.get(".ticket-form")
      .find(".input-wrapper")
      .find("button")
      .first()
      .click();

    // // Submit the form
    // cy.get(".trade-ticket")
    //   .find(".place-btn")
    //   .click();
    // cy.wait(3000);
    //
    // // Navigate to My History in the blotter
    // cy.wait(3000);
    // cy.get(".blotter")
    //   .find(".blotter-radio")
    //   .find(".ant-radio-button-wrapper")
    //   .eq(2)
    //   .click();
    // cy.get(".blotter")
    //   .find(".blotter-radio")
    //   .find(".ant-radio-button-wrapper")
    //   .last()
    //   .click();
    //
    // // View My History
    // cy.get(".blotter")
    //   .find("table")
    //   .find("tr.ant-table-row")
    //   .find("td")
    //   .first()
    //   .contains("ETH/USDT");
    //
    // cy.get(".blotter")
    //   .find("table")
    //   .find("tr.ant-table-row")
    //   .find("td")
    //   .eq(1)
    //   .contains("BUY");
    //
    // // Now reverse the trade
    // cy.get(".ticket-form")
    //   .find(".ant-radio-group")
    //   .find(".ant-radio-button-wrapper")
    //   .eq(1)
    //   .click();
    //
    // // Set the trade amount
    // cy.get(".ticket-form")
    //   .find(".input-wrapper")
    //   .find("button")
    //   .last()
    //   .click();
    //
    // // Submit the form
    // cy.get(".trade-ticket")
    //   .find(".place-btn")
    //   .click();
    //
    // // Navigate to My History in the blotter
    // cy.wait(3000);
    // cy.get(".blotter")
    //   .find(".blotter-radio")
    //   .find(".ant-radio-button-wrapper")
    //   .eq(2)
    //   .click();
    // cy.get(".blotter")
    //   .find(".blotter-radio")
    //   .find(".ant-radio-button-wrapper")
    //   .last()
    //   .click();
    //
    // // View My History
    // cy.get(".blotter")
    //   .find("table")
    //   .find("tr.ant-table-row")
    //   .find("td")
    //   .first()
    //   .contains("ETH/USDT");
    //
    // cy.get(".blotter")
    //   .find("table")
    //   .find("tr.ant-table-row")
    //   .find("td")
    //   .eq(1)
    //   .contains("SELL");

    // Logged out functionality
    // Check that assets have a current price
    // Change the language
    // Check that orders are being placed
    // Check shrunken Navigation mode
    // Check Coinflex TV

    // Login
    // Place an XBTUSD market order
    // Check if the trade appears in Spot Balances in the blotter
    // Check if it appears in my history
    // Check if it appears in RecentTrades

    // Place an FLEXUSDTJAN limit order
    // Check if the trade appears in My Futures Positions in the blotter
    // Check if it appears in my orders
    // Check that it doesn't appear in RecentTrades if not filled

    // Chart functionality
    // Check expanded functionality
    // Check modified granularity
    // Check that it doesn't appear in RecentTrades if not filled
  });
});
