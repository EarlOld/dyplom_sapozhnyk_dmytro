const {
  override,
  addBabelPlugin,
  removeModuleScopePlugin
} = require("customize-cra");

module.exports = {
  webpack: override(
    removeModuleScopePlugin(),
    addBabelPlugin([
      "import",
      {
        libraryName: "antd",
        libraryDirectory: "es",
        style: false
      },
      "antd"
    ]),
    addBabelPlugin([
      "import",
      {
        libraryName: "lodash",
        customName: name => `lodash/${name.match(/(?!_).*/)[0]}`
      },
      "lodash"
    ]),
    addBabelPlugin([
      "react-intl-auto",
      {
        removePrefix: true
      }
    ]),
    addBabelPlugin([
      "react-intl",
      {
        messagesDir: "i18n",
        extractFromFormatMessageCall: true
      }
    ])
  )
};
