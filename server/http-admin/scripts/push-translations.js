require("dotenv").config();

const API_TOKEN = "d10758a6b05fe891b1f4b9beda805aaf";
const APP_ID = 302933;
const exec = require("child_process").exec;

(async () => {
  try {
    await new Promise(resolve =>
      exec(
        "react-intl-po json2pot 'i18n/src/**/*.json' -o i18n/mcs-public.pot",
        resolve
      )
    );

    await new Promise((resolve, reject) => {
      exec(
        `
        curl -X POST https://api.poeditor.com/v2/projects/upload \
             -F api_token="${API_TOKEN}" \
             -F id="${APP_ID}" \
             -F updating="terms" \
             -F file=@"i18n/mcs-public.pot" \
             -F tags="{\"obsolete\":\"removed-strings\"}"
        `,
        (err, stdout) => {
          const response = JSON.parse(stdout);

          if (err) return reject(err);

          if (response.response.status === "fail")
            return reject(response.response.message);

          resolve(response.result);
        }
      );
    });
  } catch (error) {
    console.error(error);
  }
})();
