require("dotenv").config();

const API_TOKEN = "d10758a6b05fe891b1f4b9beda805aaf";
const APP_ID = 302933;
const exec = require("child_process").exec;

let outputPath = "public/i18n";
const outputArgIndex = process.argv.findIndex(arg => arg === "-o");
if (outputArgIndex !== -1) {
  outputPath = process.argv[outputArgIndex + 1];
}

(async () => {
  try {
    await Promise.all([
      new Promise(resolve =>
        exec(`rm -rf ${outputPath} && mkdir ${outputPath}`, resolve)
      ),
      new Promise(resolve => exec(`mkdir -p i18n`, resolve))
    ]);

    let languages = await new Promise((resolve, reject) => {
      exec(
        `curl -X POST https://api.poeditor.com/v2/languages/list -d api_token="${API_TOKEN}" -d id="${APP_ID}"`,
        (err, stdout) => {
          const response = JSON.parse(stdout);

          if (err) return reject(err);

          if (response.response.status === "fail")
            return reject(response.response.message);

          resolve(response.result);
        }
      );
    });

    const languageCodes = languages.languages.map(one => one.code);

    await new Promise(resolve =>
      exec(
        `echo '${JSON.stringify(
          languageCodes
        )}' > ${outputPath}/languages.json`,
        resolve
      )
    );

    languages = await Promise.all(
      languages.languages.map(language => {
        return new Promise((resolve, reject) => {
          exec(
            `curl -X POST https://api.poeditor.com/v2/projects/export -d api_token="${API_TOKEN}" -d id="${APP_ID}" -d language="${language.code}" -d type="po"`,
            (err, stdout) => {
              if (err) return reject(err);

              language.url = JSON.parse(stdout).result.url;

              resolve(language);
            }
          );
        });
      })
    );

    await Promise.all(
      languages.map(language => {
        return new Promise((resolve, reject) => {
          exec(
            `curl ${language.url} --output i18n/${language.code}.po`,
            (err, stdout) => {
              if (err) return reject(err);

              resolve();
            }
          );
        });
      })
    );

    await new Promise(resolve =>
      exec(
        `react-intl-po po2json 'i18n/*.po' -m 'i18n/**/*.json' -o '${outputPath}'`,
        resolve
      )
    );

    await new Promise(resolve => exec(`rm -rf i18n/*.po`, resolve));
  } catch (error) {
    console.error(error);
  }
})();
