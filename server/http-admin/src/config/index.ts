export interface IEndpoint {
  WALLET_API_URL: string;
  REST_API_URL: string;
  WS_API_URL: string;
  WEB_APP_URL: string;
}
export interface IEndpoints {
  LIVE: IEndpoint;
  STAGE: IEndpoint;
  KIWI: IEndpoint;
  ORANGE: IEndpoint;
}
export type IEnvironment = "LIVE" | "STAGE" | "KIWI" | "ORANGE";
export const PRODUCTION_ENVIRONMENT = ((process.env
  .REACT_APP_ENVIRONMENT as IEnvironment) || "LIVE") as IEnvironment;
export const ALL_ENDPOINTS: IEndpoints = {
  LIVE: {
    WALLET_API_URL: "https://walletapi.coinflex.com/",
    REST_API_URL: "https://webapi.coinflex.com/",
    WS_API_URL: "wss://api.coinflex.com/v1",
    WEB_APP_URL: "https://iot.coinflex.com/"
  },
  STAGE: {
    WALLET_API_URL: "https://stgwalletapi.coinflex.com/",
    REST_API_URL: "https://stgwebapi.coinflex.com/",
    WS_API_URL: "wss://stgapi.coinflex.com/v1",
    WEB_APP_URL: "https://stgiot.coinflex.com/"
  },
  ORANGE: {
    WALLET_API_URL: "https://orangewalletapi.coinflex.com/",
    REST_API_URL: "https://orangewebapi.coinflex.com/",
    WS_API_URL: "wss://orangeapi.coinflex.com/v1",
    WEB_APP_URL: "https://orange.coinflex.com/"
  },
  KIWI: {
    WALLET_API_URL: "https://kiwiwalletapi.coinflex.com/",
    REST_API_URL: "https://kiwiwebapi.coinflex.com/",
    WS_API_URL: "wss://kiwiapi.coinflex.com/v1",
    WEB_APP_URL: "https://kiwi.coinflex.com/"
  }
};
export const LIVE_CHAT = process.env.REACT_APP_WITH_PIT;
export const PUBLIC_URL = process.env.PUBLIC_URL || "";
export const ENDPOINTS = ALL_ENDPOINTS[PRODUCTION_ENVIRONMENT] as IEndpoint;
export const REST_API_URL = ENDPOINTS.REST_API_URL;
export const WALLET_API_URL = ENDPOINTS.WALLET_API_URL;
export const WS_API_URL = ENDPOINTS.WS_API_URL;
export const WEB_APP_URL = ENDPOINTS.WEB_APP_URL;
export const POEDITOR_API_TOKEN = "d10758a6b05fe891b1f4b9beda805aaf";
export const POEDITOR_APP_ID = 302933;
export const YOUTUBE_CHANNEL_ID = "UC_gDvpLbi6YB5d2wyKhMe6A";
export const YOUTUBE_MEDIA_PLAYLIST_ID = "PL-zVLtwOgnLuYVFYpBbpWjekBeUkOeh0E";
export const YOUTUBE_DEVELOPER_KEY = "AIzaSyCv6UV6MKPOKa5db9eNAXypvgCj3vi96PY";
export const CHAT_KEY = "COINFLEX_CHAT";
export const CHAT_PASSWORD = "FLEX_TO_THE_MOON";
export const SIMPLEWEBRTC_API_KEY = "df8b3bbac064ae88d8d5a39a";
export const BUCKETS_DOMAIN = "https://marketapi.coinflex.com/";
export const REST_ROUTES = {
  ASSETS: "assets/",
  TICKERS: "tickers/",
  MARKETS: "markets/",
  DEPTH: "depth/",
  ORDERS: "orders/",
  BALANCES: "balances/",
  BORROW_LIST: "borrower/offers/",
  BORROW_LOANS: "borrower/loans/",
  BORROW_CONVERSION: "borrower/conversion/",
  BORROW_COLLATERAL: "borrower/collateral/",
  BORROW_CONVERTED_TOTALS: "borrower/converted_totals/",
  BORROW_EVENTS: "borrower/events",
  TRADE_HISTORY: "trades/",
  POSITIONS: "positions/"
};
export const BUCKET_ROUTES = {
  CHANGE: "buckets/change",
  BUCKETS_BATCH: "buckets/batch",
  BUCKETS: "buckets"
};
export const WALLET_API_ROUTES = {
  SIGN_IN: "api/v1/sign_in",
  TFA: "api/v1/sign_in/tfa",
  USER: "api/v1/user",
  WITHDRAWAL_ADDRESS: "api/v1/withdrawal/address",
  COMPETITIONS: "api/v1/info/competitions.json",
  WITHDRAWAL: "api/v1/withdrawal/",
  PATCH_BALANCE: "api/v1/user/available_balances",
  DEPOSIT_ADDRESS: "api/v1/deposit/address/",
  USER_ASSETS: "api/v1/user/assets",
  DEPOSIT_WITHDRAWALS_HISTORY: "api/v1/user/deposit_withdrawals"
};
export const WEBAPP_ROUTES = {
  SIGN_OUT: "logout",
  SIGN_UP: "users/sign_up",
  TOKEN: "api/auth",
  LOGIN: "auth/login",
  TFA: "auth/tfa",
  CREDENTIALS: "auth/credentials",
  FORGOTTEN_PASSWORD: "resetpassword"
};
export const YOUTUBE_DOMAIN = "https://www.googleapis.com/youtube/v3/";
export const I18N = PUBLIC_URL + "/i18n/";
