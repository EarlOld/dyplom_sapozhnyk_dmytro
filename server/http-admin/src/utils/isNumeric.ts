export default (n: string): boolean =>
  !isNaN(parseFloat(n)) && isFinite(parseFloat(n));
