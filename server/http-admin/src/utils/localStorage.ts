export const removeItemFromLocalStorageState = (name: string) => {
  if (window.localStorage) {
    try {
      const state = JSON.parse(window.localStorage.getItem("state") || "");
      delete state[name];
      window.localStorage.setItem("state", JSON.stringify(state));
    } catch (e) {
      console.log(e);
    }
  }
};
