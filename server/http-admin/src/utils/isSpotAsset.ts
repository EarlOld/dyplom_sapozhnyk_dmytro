import _has from "lodash/has";
export const isSpotAsset = (asset: IAsset): boolean => !_has(asset, "spot_id");
