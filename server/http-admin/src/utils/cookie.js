export function setCookie(cname, cvalue, exdays = 30) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function setCookieOnCoinflexDomain(cname, cvalue, exdays = 30) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie =
    cname + "=" + cvalue + ";" + expires + ";path=/; domain=.coinflex.com";
}

export function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

export const deleteCookie = name => {
  document.cookie =
    name +
    "=; Path=/; Domain=.coinflex.com; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
};

export const moveAccesTokenToSessionStorageAndRemove = () => {
  const { sessionStorage } = window;
  const accessTokenFromCookie = getCookie("access_token");
  if (sessionStorage && accessTokenFromCookie) {
    const parsedBase64 = window.atob(accessTokenFromCookie);
    const credentials = JSON.parse(parsedBase64);
    if (credentials && credentials.token) {
      sessionStorage.setItem(
        "state",
        JSON.stringify({ authToken: credentials })
      );
      deleteCookie("access_token");
    }
  }
};
