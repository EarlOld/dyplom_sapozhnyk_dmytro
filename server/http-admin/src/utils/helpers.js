import _get from "lodash/get";
import _isNan from "lodash/isNaN";
import _parseInt from "lodash/parseInt";

import { WEB_APP_URL } from "config";
import moment from "moment";
const log = console.log.bind(console);
export default log;

export function uuid() {
  let uuid = "";
  for (let i = 0; i < 32; i++) {
    let random = (Math.random() * 16) | 0;
    if (i === 8 || i === 12 || i === 16 || i === 20) {
      uuid += "-";
    }
    uuid += (i === 12 ? 4 : i === 16 ? (random && 3) || 8 : random).toString(
      16
    );
  }
  return uuid;
}

export function store(namespace, data) {
  if (data) return (localStorage[namespace] = JSON.stringify(data));

  let store = localStorage[namespace];
  return (store && JSON.parse(store)) || [];
}

export function delay(ms) {
  return new Promise(r => setTimeout(r, ms));
}

export function exec(url, route, opts) {
  let reg = /(?:\?([^#]*))?(#.*)?$/,
    c = url.match(reg),
    matches = {},
    ret;
  if (c && c[1]) {
    let p = c[1].split("&");
    for (let i = 0; i < p.length; i++) {
      let r = p[i].split("=");
      matches[decodeURIComponent(r[0])] = decodeURIComponent(
        r.slice(1).join("=")
      );
    }
  }
  url = segmentize(url.replace(reg, ""));
  route = segmentize(route || "");
  let max = Math.max(url.length, route.length);
  for (let i = 0; i < max; i++) {
    if (route[i] && route[i].charAt(0) === ":") {
      let param = route[i].replace(/(^:|[+*?]+$)/g, ""),
        flags = (route[i].match(/[+*?]+$/) || {})[0] || "",
        plus = ~flags.indexOf("+"),
        star = ~flags.indexOf("*"),
        val = url[i] || "";
      if (!val && !star && (flags.indexOf("?") < 0 || plus)) {
        ret = false;
        break;
      }
      matches[param] = decodeURIComponent(val);
      if (plus || star) {
        matches[param] = url
          .slice(i)
          .map(decodeURIComponent)
          .join("/");
        break;
      }
    } else if (route[i] !== url[i]) {
      ret = false;
      break;
    }
  }
  if (opts.default !== true && ret === false) return false;
  return matches;
}

export function segmentize(url) {
  return url.replace(/(^\/+|\/+$)/g, "").split("/");
}

/**
 * Returns a random value between -1 and 1.
 *
 * @return Random value between -1 and 1.
 */
export function randomClamped() {
  return Math.random() * 2 - 1;
}

/**
 * Logistic activation function.
 *
 * @param {a} Input value.
 * @return Logistic function output.
 */
export function activation(a) {
  const ap = -a / 1;
  return 1 / (1 + Math.exp(ap));
}

export function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

export const pick = (obj, keys) => {
  const r = {};
  keys.forEach(key => {
    r[key] = obj[key];
  });
  return r;
};

export const transformRequest = data => {
  let ret = "";
  for (let it in data) {
    ret += encodeURIComponent(it) + "=" + encodeURIComponent(data[it]) + "&";
  }
  return ret;
};

export const computeAuthToken = (coreId, apiKey, prvKey) => {
  let jsonCredentials = {
    random: Math.random() * 255,
    core_id: coreId.toString(),
    api_key: apiKey,
    prv_key: prvKey
  };
  let plainCredentials = JSON.stringify(jsonCredentials);
  let base64Credentials = btoa(unescape(encodeURIComponent(plainCredentials)));
  return base64Credentials;
};

export function getCurrentUrl(path) {
  return WEB_APP_URL + path;
}
export const localStore = {
  set(name, data) {
    window.localStorage.setItem(name, JSON.stringify(data));
  },
  get(name) {
    return JSON.parse(window.localStorage.getItem(name));
  }
};

export const sorterFunction = (a, b, field) => {
  const one = _get(a, [field]);
  const two = _get(b, [field]);
  const areNumbers = [one, two].every(v => !_isNan(_parseInt(v)));
  return areNumbers ? _parseInt(one) - _parseInt(two) : one.localeCompare(two);
};

export const upperCaseFirstLetter = line =>
  line.charAt(0).toUpperCase() + line.slice(1);

export const getCurrentMonth = () => {
  const currentData = moment(new Date());
  currentData.locale("en");
  const day = Number(currentData.format("D"));
  if (day >= 26) currentData.add({ month: 1 });
  return currentData.format("MMM").toUpperCase();
};
