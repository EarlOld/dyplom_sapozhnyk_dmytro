import { getCookie } from "utils/cookie";

export const getCredentialsFromCookie = (): IAuthToken | null => {
  const accessTokenFromCookie = getCookie("access_token");
  if (accessTokenFromCookie) {
    const parsedBase64 = window.atob(accessTokenFromCookie);
    return JSON.parse(parsedBase64);
  }
  return null;
};
