import React from "react";
import { Provider } from "react-redux";
import { render } from "@testing-library/react";
import { initStore } from "store";
import mock from "store/state/mock";
import { CookiesProvider } from "react-cookie";
import { IntlProvider } from "react-intl";
import { BrowserRouter } from "react-router-dom";

export const wrapWithProviders = Component => (
  <CookiesProvider>
    <BrowserRouter>
      <IntlProvider locale="en">
        <Component />
      </IntlProvider>
    </BrowserRouter>
  </CookiesProvider>
);

export const renderWithProviders = (ui, options) => {
  const wrapper = ({ children }) => wrapWithProviders(() => children);
  return render(ui, { wrapper, ...options });
};

export { renderWithProviders as render };

export const wrapWithRedux = initialState => Component => {
  const store = initStore(initialState);
  const wrapped = wrapWithProviders(Component);
  return <Provider store={store}>{wrapped}</Provider>;
};

export const renderWithRedux = (ui, { initialState, ...options } = {}) => {
  const wrapper = ({ children }) => wrapWithRedux(initialState)(() => children);
  return render(ui, { wrapper, ...options });
};

export const wrapWithMockRedux = initialState =>
  wrapWithRedux({
    ...mock,
    ...initialState
  });

export const wrapWithEmptyRedux = () => wrapWithRedux({});

export const renderWithMockRedux = (ui, { initialState, ...options } = {}) =>
  renderWithRedux(ui, {
    initialState: {
      ...mock,
      ...initialState
    },
    ...options
  });
export const renderWithEmptyRedux = (ui, { initialState, ...options } = {}) =>
  renderWithRedux(ui, {
    initialState: {},
    ...options
  });
