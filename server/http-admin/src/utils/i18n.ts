export const localesList: ILocale[] = [
  {
    code: "en",
    momentCode: "en",
    chartCode: "en",
    title: "English",
    nativeTitle: "EN"
  }
];

export const localesMap: { [key: string]: ILocale } = localesList.reduce(
  (res, one) => ({
    ...res,
    [one.code]: one
  }),
  {}
);

export function getPreferedLocale(locale: string) {
  if (localesMap[locale]) return locale;
  let clientLocale;
  if (window.navigator.languages) {
    clientLocale = window.navigator.languages[0];
  } else {
    clientLocale = window.navigator.language;
  }
  return localesMap[clientLocale] ? clientLocale : localesList[0].code;
}
