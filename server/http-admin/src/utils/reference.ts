interface IDecimal {
  [key: string]: {
    price: number;
    balance: number;
  };
}

interface IContractCode {
  [key: string]: string;
}
interface IMinimumTradeAmount {
  [key: string]: number;
}
export const decimals: IDecimal = {
  FLEX: {
    price: 2,
    balance: 4
  },
  ETH: {
    price: 2,
    balance: 4
  },
  XBT: {
    price: 2,
    balance: 4
  },
  USDC: {
    price: 2,
    balance: 2
  },
  USDT: {
    price: 2,
    balance: 2
  },
  BCH: {
    price: 2,
    balance: 4
  }
};
export const minimumTradeAmount: IMinimumTradeAmount = {
  FLEX: 1,
  ETH: 0.01,
  XBT: 0.001,
  USDC: 1,
  USDT: 1,
  BCH: 0.01
};
export const contractCode: IContractCode = {
  F: "Jan",
  G: "Feb",
  H: "Mar",
  J: "Apr",
  K: "May",
  M: "Jun",
  N: "Jul",
  Q: "Aug",
  U: "Sep",
  V: "Oct",
  X: "Nov",
  Z: "Dec"
};
