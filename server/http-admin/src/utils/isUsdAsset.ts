export const isUsdAsset = (id: number): boolean => [65283, 65282].includes(id);
export const isUSDT = (id: number): boolean => id === 65283;
export const isUSDByName = (assetName: string) =>
  ["USDT", "USDC"].some(str => assetName.toUpperCase().includes(str));
