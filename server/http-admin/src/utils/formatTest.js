/*
 * @Author: Victor
 */
//password format test
export function pwdFormat(password) {
  let pwdRegex = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
  let formatMatch = true;
  if (!pwdRegex.test(password)) {
    formatMatch = false;
    return formatMatch;
  } else {
    return formatMatch;
  }
}

//email format test
export function emailFormat(email) {
  let emailRegex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  let formatMatch = true;
  if (!emailRegex.test(email)) {
    formatMatch = false;
    return formatMatch;
  } else {
    return formatMatch;
  }
}
