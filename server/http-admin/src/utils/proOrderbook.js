let orderbook_orders = {};
let orderbook_bids = {};
let orderbook_asks = {};
let lastTime = 0;

export const handleOrderbookData = data => {
  if (data.error_code === 0 && data.tag !== undefined) {
    let current_orderbook = {};

    orderbook_orders = {};
    orderbook_bids = {};
    orderbook_asks = {};

    current_orderbook["bids"] = [];
    current_orderbook["asks"] = [];
    if (data["orders"] && data["orders"].length > 0) {
      let _lastTime = data.orders.sort((a, b) => b.time - a.time)[0].time;
      if (_lastTime === lastTime) return;
      lastTime = _lastTime;
      for (let item of data.orders) {
        let price = +item.price;
        let size = +item.quantity;
        orderbook_orders[item.id] = {
          price: price,
          size: size
        };
        if (size > 0) {
          if (orderbook_bids[price] === undefined) {
            orderbook_bids[price] = 0.0;
          }
          orderbook_bids[price] += size;
        } else {
          if (orderbook_asks[price] === undefined) {
            orderbook_asks[price] = 0.0;
          }
          orderbook_asks[price] += size;
        }
      }
      for (let price in orderbook_bids) {
        let size = orderbook_bids[price];
        current_orderbook.bids.push([+price, size]);
      }
      for (let price in orderbook_asks) {
        let size = orderbook_asks[price];
        current_orderbook.asks.push([+price, Math.abs(size)]);
      }
      current_orderbook.bids.sort((a, b) => b[0] - a[0]);
      current_orderbook.asks.sort((a, b) => b[0] - a[0]);
    }
    return [current_orderbook["asks"], current_orderbook["bids"]];
  } else if (data.notice === "OrderOpened") {
    let _lastTime = data.time;
    if (_lastTime === lastTime) return;
    lastTime = _lastTime;

    let price = +data.price;
    let size = +data.quantity;
    orderbook_orders[data.id] = {
      price: price,
      size: size
    };
    if (size > 0) {
      if (orderbook_bids[price] === undefined) {
        orderbook_bids[price] = 0.0;
      }
      orderbook_bids[price] += size;
    } else {
      if (orderbook_asks[price] === undefined) {
        orderbook_asks[price] = 0.0;
      }
      orderbook_asks[price] += size;
    }
    let current_orderbook = {};
    current_orderbook["bids"] = [];
    current_orderbook["asks"] = [];
    for (let price in orderbook_bids) {
      let size = orderbook_bids[price];
      current_orderbook.bids.push([+price, size]);
    }
    for (let price in orderbook_asks) {
      let size = orderbook_asks[price];

      current_orderbook.asks.push([+price, Math.abs(size)]);
    }

    current_orderbook.bids.sort((a, b) => b[0] - a[0]);
    current_orderbook.asks.sort((a, b) => b[0] - a[0]);
    if (
      current_orderbook.bids.length <= 0 ||
      current_orderbook.asks.length <= 0 ||
      current_orderbook.bids[0][0] >= current_orderbook.asks[0][0]
    ) {
      return;
    }
    return [current_orderbook["asks"], current_orderbook["bids"]];
  } else if (data.notice === "OrderModified") {
    let _lastTime = data.time;
    if (_lastTime === lastTime) return;
    lastTime = _lastTime;

    if (orderbook_orders[data.id] === undefined) {
      return;
    }

    let orderTmp = orderbook_orders[data.id];
    if (orderTmp.size > 0) {
      orderbook_bids[orderTmp.price] -= orderTmp.size;

      if (Math.abs(orderbook_bids[orderTmp.price]) <= 0) {
        delete orderbook_bids[orderTmp.price];
      }
    } else {
      orderbook_asks[orderTmp.price] -= orderTmp.size;

      if (Math.abs(orderbook_asks[orderTmp.price]) <= 0) {
        delete orderbook_asks[orderTmp.price];
      }
    }

    let price = +data.price;
    let size = +data.quantity;
    orderbook_orders[data.id] = {
      price: price,
      size: size
    };
    //console.log("77",orderbook_bids, orderbook_asks )

    if (size > 0) {
      if (orderbook_bids[price] === undefined) {
        orderbook_bids[price] = 0.0;
      }
      orderbook_bids[price] += size;
    } else {
      if (orderbook_asks[price] === undefined) {
        orderbook_asks[price] = 0.0;
      }
      orderbook_asks[price] += size;
    }
    let current_orderbook = {};
    current_orderbook["bids"] = [];
    current_orderbook["asks"] = [];
    for (let price in orderbook_bids) {
      let size = orderbook_bids[price];
      current_orderbook.bids.push([+price, size]);
    }
    for (let price in orderbook_asks) {
      let size = orderbook_asks[price];

      current_orderbook.asks.push([+price, Math.abs(size)]);
    }

    current_orderbook.bids.sort((a, b) => b[0] - a[0]);
    current_orderbook.asks.sort((a, b) => b[0] - a[0]);

    if (
      current_orderbook.bids.length <= 0 ||
      current_orderbook.asks.length <= 0 ||
      current_orderbook.bids[0][0] >= current_orderbook.asks[0][0]
    ) {
      return;
    }
    return [current_orderbook["asks"], current_orderbook["bids"]];
  } else if (data.notice === "OrderClosed") {
    if (orderbook_orders[data.id] === undefined) {
      return;
    }

    let price = +data.price;
    let size = orderbook_orders[data.id]["size"];
    delete orderbook_orders[data.id];

    if (size > 0) {
      orderbook_bids[price] -= size;

      if (Math.abs(orderbook_bids[price]) <= 0) {
        delete orderbook_bids[price];
      }
    } else {
      orderbook_asks[price] -= size;

      if (Math.abs(orderbook_asks[price]) <= 0) {
        delete orderbook_asks[price];
      }
    }
    let current_orderbook = {};
    current_orderbook["bids"] = [];
    current_orderbook["asks"] = [];
    for (let price in orderbook_bids) {
      let size = orderbook_bids[price];
      current_orderbook.bids.push([+price, size]);
    }
    for (let price in orderbook_asks) {
      let size = orderbook_asks[price];
      current_orderbook.asks.push([+price, Math.abs(size)]);
    }
    current_orderbook.bids.sort((a, b) => b[0] - a[0]);
    current_orderbook.asks.sort((a, b) => b[0] - a[0]);

    if (
      current_orderbook.bids.length <= 0 ||
      current_orderbook.asks.length <= 0 ||
      current_orderbook.bids[0][0] >= current_orderbook.asks[0][0]
    ) {
      return;
    }
    return [current_orderbook["asks"], current_orderbook["bids"]];
  } else if (data.notice === "OrdersMatched") {
    let _lastTime = data.time;
    if (_lastTime === lastTime) return;
    lastTime = _lastTime;

    if (orderbook_orders[data.bid] !== undefined) {
      let bid_price = orderbook_orders[data.bid].price;
      let bid_diff = orderbook_orders[data.bid].size - parseFloat(data.bid_rem);

      if (Math.abs(parseFloat(data.bid_rem)) <= 0) {
        delete orderbook_orders[data.bid];
      } else {
        orderbook_orders[data.bid]["size"] = parseFloat(data.bid_rem);
      }
      orderbook_bids[bid_price] -= bid_diff;
      if (Math.abs(orderbook_bids[bid_price]) <= 0) {
        delete orderbook_bids[bid_price];
      }
    }

    if (orderbook_orders[data.ask] !== undefined) {
      let ask_price = orderbook_orders[data.ask].price;
      let ask_diff = orderbook_orders[data.ask].size + parseFloat(data.ask_rem);
      if (Math.abs(parseFloat(data.ask_rem)) <= 0) {
        delete orderbook_orders[data.ask];
      } else {
        orderbook_orders[data.ask]["size"] = -parseFloat(data.ask_rem);
      }
      orderbook_asks[ask_price] -= ask_diff;
      if (Math.abs(orderbook_asks[ask_price]) <= 0) {
        delete orderbook_asks[ask_price];
      }
    }
    let current_orderbook = {};
    current_orderbook["bids"] = [];
    current_orderbook["asks"] = [];
    for (let price in orderbook_bids) {
      let size = orderbook_bids[price];
      // console.log("++++++++++++++++++++++++++++++++++", size);
      current_orderbook.bids.push([+price, size]);
    }
    for (let price in orderbook_asks) {
      let size = orderbook_asks[price];
      // console.log("-----------------------------------", size);
      current_orderbook.asks.push([+price, Math.abs(size)]);
    }
    current_orderbook.bids.sort((a, b) => b[0] - a[0]);
    current_orderbook.asks.sort((a, b) => b[0] - a[0]);
    if (
      current_orderbook.bids.length <= 0 ||
      current_orderbook.asks.length <= 0 ||
      current_orderbook.bids[0][0] >= current_orderbook.asks[0][0]
    ) {
      return;
    }
    return [current_orderbook["asks"], current_orderbook["bids"]];
  }
};
