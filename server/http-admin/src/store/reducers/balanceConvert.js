import { handleActions } from "redux-actions";

import userDefaultState from "../state/userState.js";
import * as actions from "../actions";

export default handleActions(
  {
    [actions.balanceConvert.TRIGGER]: (state, action) => ({
      ...state,
      [action.payload.asset_from]: {
        ...action.payload,
        pending: true,
        error: false,
        errorCode: undefined,
        errorMessage: undefined,
        success: false
      }
    }),
    [actions.balanceConvert.FAILURE]: (state, action) => ({
      ...state,
      [action.payload.asset_from]: {
        ...action.payload,
        pending: false,
        error: true,
        success: false
      }
    }),
    [actions.balanceConvert.SUCCESS]: (state, action) => ({
      ...state,
      [action.payload.asset_from]: {
        ...action.payload,
        pending: false,
        error: false,
        errorCode: undefined,
        errorMessage: undefined,
        success: true
      }
    }),
    [actions.balanceConvert.FULFILL]: (state, action) => {
      const { asset_from } = action.payload;
      const entryState = state[asset_from] || {};

      return {
        ...state,
        [asset_from]: {
          ...entryState,
          ...action.payload,
          pending: false
        }
      };
    }
  },
  userDefaultState.balancesConversionsState
);
