import { handleActions } from "redux-actions";

import userDefaultState from "../state/userState.js";
import * as actions from "../actions";

export default handleActions(
  {
    [actions.getAddress.TRIGGER]: (state, action) => ({
      ...state,
      ...action.payload,
      pending: true,
      error: false,
      errorCode: undefined,
      errorMessage: undefined,
      success: false
    }),
    [actions.getAddress.FAILURE]: (state, action) => ({
      ...state,
      ...action.payload,
      pending: false,
      error: true,
      success: false
    }),
    [actions.getAddress.SUCCESS]: (state, action) => ({
      ...state,
      ...action.payload,
      pending: false,
      error: false,
      errorCode: undefined,
      errorMessage: undefined,
      success: true
    }),
    [actions.getAddress.FULFILL]: (state, action) => {
      return {
        ...state,
        ...action.payload,
        pending: false
      };
    }
  },
  userDefaultState.getAddressState
);
