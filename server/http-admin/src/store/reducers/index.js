import { combineReducers } from "redux";
import _isEqual from "lodash/isEqual";
import defaultState from "../state/publicState.js";
import userDefaultState from "../state/userState.js";
import { handleAction, handleActions } from "redux-actions";

import * as actions from "../actions";

import getAddressState from "./getAddress";
import balancesConversionsState from "./balanceConvert";
import balanceWithdraw from "./balanceWithdraw";
import uploadBalanceWithdrawalAddress from "./uploadBalanceWithdrawalAddress";

const defaultReducer = (state, action) => action.payload;

const isMobile = handleAction(
  actions.setMobilePage,
  defaultReducer,
  defaultState.isMobile
);

const authToken = handleAction(
  actions.setAuthToken,
  defaultReducer,
  defaultState.authToken
);

const currentSymbol = handleAction(
  actions.setCurrentSymbol,
  defaultReducer,
  defaultState.currentSymbol
);

const changeList = handleAction(
  actions.setChangeList,
  defaultReducer,
  defaultState.changeList
);

const marketList = handleAction(
  actions.setMarketList,
  defaultReducer,
  defaultState.marketList
);

const assetsList = handleAction(
  actions.setAssetsList,
  defaultReducer,
  defaultState.assetsList
);

const tickersList = handleAction(
  actions.setTickersList,
  defaultReducer,
  defaultState.tickersList
);

const orderBookList = handleAction(
  actions.setOrderBookList,
  defaultReducer,
  defaultState.orderBookList
);

const tvVideosList = handleAction(
  actions.setTVVideosList,
  (state, action) => {
    const { payload } = action;
    const itemsExtendedInfo = payload.itemsInfo.items || [];
    const getItemInfo = (item, videoId) => {
      const { snippet } = item;
      const extendedInfo = itemsExtendedInfo.filter(
        item => item.id === videoId
      )[0];

      return {
        title: snippet.title,
        thumbnail: item.snippet.thumbnails.medium.url,
        duration: extendedInfo && extendedInfo.contentDetails.duration,
        viewCount: extendedInfo && extendedInfo.statistics.viewCount
      };
    };

    return payload.mediaItems
      .map(item => ({
        id: item.snippet.resourceId.videoId,
        group: "media",
        ...getItemInfo(item, item.snippet.resourceId.videoId)
      }))
      .concat(
        payload.educationItems.map(item => ({
          id: item.id.videoId,
          group: "education",
          ...getItemInfo(item, item.id.videoId)
        }))
      );
  },
  defaultState.tvVideosList
);

const lastTrade = handleAction(
  actions.setLastTrade,
  (state, action) => ({
    ...state,
    [action.payload.base]: action.payload
  }),
  defaultState.lastTrade
);

const lastTicker = handleAction(
  actions.setLastTicker,
  defaultReducer,
  defaultState.lastTicker
);

const bidPrice = handleAction(
  actions.setBidPrice,
  defaultReducer,
  defaultState.bidPrice
);

const askPrice = handleAction(
  actions.setAskPrice,
  defaultReducer,
  defaultState.askPrice
);

const positionList = handleAction(
  actions.setPositionList,
  defaultReducer,
  defaultState.positionList
);

const bucketList = handleAction(
  actions.setBucketList,
  defaultReducer,
  defaultState.bucketList
);

const allBalances = handleAction(
  actions.setBalances,
  defaultReducer,
  userDefaultState.allBalances
);

const borrowOffers = handleActions(
  {
    [actions.setBorrowOffers]: defaultReducer,
    [actions.setBorrowOffer]: (state, { payload }) => {
      const offerIndex = state.findIndex(offer => offer.id === payload.id);
      if (offerIndex >= 0) {
        return state.map((offer, index) =>
          index === offerIndex ? payload : offer
        );
      }
      return [...state, payload];
    },
    [actions.removeBorrowOffer]: (state, { payload }) => {
      return state.filter(offer => offer.id !== payload);
    }
  },
  userDefaultState.borrowOffers
);

const borrowLoans = handleActions(
  {
    [actions.setBorrowLoans]: defaultReducer,
    [actions.setBorrowLoan]: (state, { payload }) => {
      const loanIndex = state.findIndex(offer => offer.id === payload.id);

      if (loanIndex >= 0) {
        return state.map((loan, index) =>
          index === loanIndex ? payload : loan
        );
      }
      return [...state, payload];
    },
    [actions.removeBorrowLoan]: (state, { payload }) => {
      return state.filter(loan => loan.id !== payload);
    },
    [actions.repayBorrowLoan]: (state, { payload }) => {
      return state.map(loan =>
        loan.id !== payload.id
          ? loan
          : {
              ...loan,
              principal: payload.newPrincipal
            }
      );
    }
  },
  userDefaultState.borrowLoans
);

const borrowCollaterals = handleAction(
  actions.setBorrowCollaterals,
  (state, action) => {
    return state
      ? action.payload.map((item, i) =>
          _isEqual(state[i], item) ? state[i] : item
        )
      : action.payload;
  },
  userDefaultState.borrowCollaterals
);

const lockedLeverageSlidersValues = handleActions(
  {
    [actions.lockLeverageSlider]: (state, action) => ({
      ...state,
      [action.payload.assetId]: action.payload.value
    }),
    [actions.unlockLeverageSlider]: (state, action) => {
      const { [action.payload]: value, ...withoutTarget } = state;
      return withoutTarget;
    }
  },
  userDefaultState.lockedLeverageSlidersValues
);

const positions = handleAction(
  actions.setPositions,
  defaultReducer,
  userDefaultState.positions
);

const orderList = handleAction(
  actions.setOrders,
  defaultReducer,
  userDefaultState.orderList
);

const isLogin = handleAction(
  actions.setLogin,
  defaultReducer,
  defaultState.isLogin
);

const currentBalance = handleAction(
  actions.setCurrentBalance,
  defaultReducer,
  userDefaultState.currentBalance
);

const panelConfirm = handleAction(
  actions.setPanelConfirm,
  defaultReducer,
  defaultState.panelConfirm
);

const topTabsType = handleAction(
  actions.setTopTabs,
  defaultReducer,
  defaultState.topTabsType
);

const i18n = handleAction(actions.setI18n, defaultReducer, defaultState.i18n);

const serverNonce = handleAction(
  actions.setServerNonce,
  defaultReducer,
  defaultState.serverNonce
);

const historyTrade = handleAction(
  actions.setHistoryTrade,
  defaultReducer,
  defaultState.historyTrade
);

const lastPosition = handleAction(
  actions.setLastPosition,
  defaultReducer,
  defaultState.lastPosition
);

const askData = handleAction(
  actions.setAskData,
  defaultReducer,
  defaultState.askData
);

const bidData = handleAction(
  actions.setBidData,
  defaultReducer,
  defaultState.bidData
);

const selectedMarket = handleAction(
  actions.setSelectedMarket,
  defaultReducer,
  defaultState.selectedMarket
);

const amountFromOrderBook = handleAction(
  actions.setAmountFromOrderBook,
  defaultReducer,
  defaultState.amountFromOrderBook
);

const orderConfirmResult = handleAction(
  actions.setOrderConfirmResult,
  defaultReducer,
  defaultState.orderConfirmResult
);

const websocketAuthParams = handleAction(
  actions.setWebsocketAuthParams,
  defaultReducer,
  defaultState.websocketAuthParams
);

const borrowConversions = handleAction(
  actions.setBorrowConversions,
  defaultReducer,
  userDefaultState.borrowConversions
);

const borrowConvertedTotals = handleAction(
  actions.setBorrowConvertedTotals,
  defaultReducer,
  userDefaultState.borrowConvertedTotals
);

const user = handleAction(
  actions.setUser,
  defaultReducer,
  userDefaultState.user
);

const competitions = handleAction(
  actions.setCompetitions,
  defaultReducer,
  defaultState.competitions
);

const depositAddress = handleAction(
  actions.setDepositAddress,
  (state, { payload }) => payload,
  userDefaultState.depositAddress
);

const depositWithdrawalsHistory = handleAction(
  actions.setDepositWithdrawalsHistory,
  (state, { payload }) => {
    if (payload.length !== state.length) {
      return payload;
    }

    let hasChanges = false;
    for (let i = 0; i < payload.length; i++) {
      if (!_isEqual(payload[i], state[i])) {
        hasChanges = true;
        break;
      }
    }

    return hasChanges ? payload : state;
  },
  userDefaultState.depositWithdrawalsHistory
);

const notifications = handleAction(
  actions.pushNotification,
  (state, action) => [...state, action.payload],
  defaultState.notifications
);

const userAssets = handleAction(
  actions.setUserAssets,
  defaultReducer,
  userDefaultState.userAssets
);

export default combineReducers({
  authToken,
  isMobile,
  currentSymbol,
  bucketList,
  changeList,
  depositAddress,
  depositWithdrawalsHistory,
  marketList,
  assetsList,
  tickersList,
  orderBookList,
  tvVideosList,
  lastTrade,
  lastTicker,
  positionList,
  askPrice,
  bidPrice,
  isLogin,
  allBalances,
  borrowOffers,
  borrowLoans,
  borrowCollaterals,
  borrowConversions,
  borrowConvertedTotals,
  balancesConversionsState,
  getAddressState,
  balanceWithdraw,
  uploadBalanceWithdrawalAddress,
  lockedLeverageSlidersValues,
  positions,
  orderList,
  currentBalance,
  panelConfirm,
  topTabsType,
  i18n,
  serverNonce,
  historyTrade,
  lastPosition,
  askData,
  bidData,
  selectedMarket,
  orderConfirmResult,
  websocketAuthParams,
  user,
  competitions,
  notifications,
  userAssets,
  amountFromOrderBook
});
