import { createSelector } from "reselect";
export const selectAuthParams = createSelector<
  IPublicState,
  IUserAuthentificateParams,
  IAuthParams
>(
  state => state.websocketAuthParams,
  ({ core_id, api_key, priv_key }) => ({
    auth: {
      username: `${core_id}/${api_key}`,
      password: priv_key
    }
  })
);

export const selectAuthToken = createSelector<IUserState, IAuthToken, string>(
  state => state.authToken,
  (props: IAuthToken) => {
    return props && props.token;
  }
);

export const selectUserAssets = createSelector<
  IUserState,
  IUserAsset[],
  IUserAsset[]
>(
  state => state.userAssets,
  (userAssets: IUserAsset[]) => userAssets
);
