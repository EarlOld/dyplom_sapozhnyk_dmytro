import { getPreferedLocale } from "utils/i18n";

export default {
  topTabsType: "CHART",
  isMobile: false,
  currentSymbol: {
    base: {},
    quote: {}
  },
  quoteInfo: {},
  changeList: [],
  marketList: [],
  bucketList: [],
  assetsList: [],
  amountFromOrderBook: 0,
  tickersList: [],
  tvVideosList: [],
  orderbook_orders: {},
  orderbook_bids: {},
  orderbook_asks: {},
  orderBookList: {},
  lastTrade: {},
  lastTicker: {},
  positionList: [],
  bidPrice: 0,
  askPrice: 0,
  isLogin: false,
  panelConfirm: { panelType: "login", tradeData: {} },
  i18n: {
    locale: getPreferedLocale(),
    languages: [],
    messages: null
  },
  serverNonce: null,
  historyTrade: [],
  lastPosition: {},
  askData: null,
  bidData: null,
  selectedMarket: null,
  orderConfirmResult: null,
  websocketAuthParams: null,
  competitions: [],
  notifications: [],
  authToken: null
};
