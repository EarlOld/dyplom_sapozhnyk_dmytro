export default {
  allBalances: [],
  borrowOffers: null,
  borrowLoans: null,
  positions: {},
  orderList: null,
  currentBalance: 0,
  borrowCollaterals: null,
  borrowConvertedTotals: [],
  borrowConversions: [],
  getAddressState: {},
  balancesConversionsState: {},
  uploadBalanceWithdrawalAddress: {},
  balanceWithdraw: {},
  lockedLeverageSlidersValues: {},
  user: null,
  depositAddress: null,
  depositWithdrawalsHistory: [],
  userAssets: null
};
