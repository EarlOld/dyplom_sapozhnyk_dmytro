import { applyMiddleware, createStore, compose } from "redux";
import createSagaMiddleware from "redux-saga";
import _isEmpty from "lodash/isEmpty";
import _throttle from "lodash/throttle";
import _pick from "lodash/pick";
import reducers from "store/reducers";
import rootSaga from "./sagas";

const loadState = () => {
  let stateFromLocalStorage = localStorage.getItem("state");
  let stateFromSessionStorage = sessionStorage.getItem("state");

  stateFromLocalStorage = stateFromLocalStorage
    ? JSON.parse(stateFromLocalStorage)
    : undefined;

  stateFromSessionStorage = stateFromSessionStorage
    ? JSON.parse(stateFromSessionStorage)
    : undefined;

  const state = { ...stateFromLocalStorage, ...stateFromSessionStorage };
  return !_isEmpty(state) ? state : undefined;
};

const saveStateToLocalStorage = state =>
  localStorage.setItem("state", JSON.stringify(state));
const saveStateToSessionStorage = state =>
  sessionStorage.setItem("state", JSON.stringify(state));

const sagaMiddleware = createSagaMiddleware();

const middlewares = compose(
  applyMiddleware(sagaMiddleware),
  !!window.__REDUX_DEVTOOLS_EXTENSION__
    ? window.__REDUX_DEVTOOLS_EXTENSION__()
    : f => f
);

export const initStore = initialState =>
  createStore(reducers, initialState, middlewares);

const store = initStore(loadState());

store.subscribe(
  _throttle(() => {
    const stateToSaveToLocalStorage = _pick(store.getState(), [
      "i18n",
      "authToken"
    ]);
    const stateToSaveToSessionStorage = _pick(store.getState(), []);
    saveStateToLocalStorage(stateToSaveToLocalStorage);
    saveStateToSessionStorage(stateToSaveToSessionStorage);
  }, 1000)
);

sagaMiddleware.run(rootSaga);

export default store;
