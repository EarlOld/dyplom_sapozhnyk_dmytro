import { takeLatest, put, call, select } from "redux-saga/effects";

import * as http from "services/http/http";
import { getBorrowLoans, setBorrowLoans } from "store/actions";
import { selectAuthParams } from "store/selectors";

function* fetchBorrowLoansSaga() {
  const authParams = yield select(selectAuthParams);
  const data: IBorrowLoan[] = yield call(http.getBorrowLoans, authParams);
  yield put(setBorrowLoans(data));
}

export function* borrowLoansSaga() {
  yield takeLatest(getBorrowLoans.toString(), fetchBorrowLoansSaga);
}
