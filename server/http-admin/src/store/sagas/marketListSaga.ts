import { takeLatest, put, call } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getMarketList, setMarketList } from "store/actions";

function* fetchMarketListSaga() {
  const data: IMarket[] = yield call(httpService.getCoinFlexMarkets);
  yield put(setMarketList(data));
}

export function* marketListSaga() {
  yield takeLatest(getMarketList.toString(), fetchMarketListSaga);
}
