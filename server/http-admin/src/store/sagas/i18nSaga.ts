import { takeLatest, put, call } from "redux-saga/effects";
import moment from "moment";

import * as httpService from "services/http/http";
import { getI18n, setI18n, TGetI18nAction } from "store/actions";
import { localesMap } from "utils/i18n";

function* fetchI18nSaga(action: TGetI18nAction) {
  const locale = action.payload;
  const languages = yield call(httpService.getI18nLanguages);
  let messages = null;
  if (languages.includes(locale)) {
    messages = yield call(httpService.getI18nMessages, locale);
  }
  yield put(setI18n({ locale, languages, messages }));

  moment.locale(localesMap[locale]?.momentCode);
}

export function* i18nSaga() {
  yield takeLatest(getI18n.toString(), fetchI18nSaga);
}
