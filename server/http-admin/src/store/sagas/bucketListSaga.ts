import { takeLatest, put, call } from "redux-saga/effects";

import * as httpService from "services/http/http";
import {
  getBucketList,
  setBucketList,
  TGetBucketListAction
} from "store/actions";

function* fetchBucketListSaga(action: TGetBucketListAction) {
  try {
    const bucketData: IBucketListParam = {
      since: new Date().getTime() - 24 * 60 * 60 * 1000,
      interval: "1h",
      assets: action.payload.map(datum => ({
        base: datum.base,
        counter: datum.counter
      }))
    };
    const data: ITickerChange = yield call(
      httpService.getBucketBatch,
      bucketData
    );
    yield put(setBucketList(data));
  } catch (e) {
    // error handling
  }
}

export function* bucketListSaga() {
  yield takeLatest(getBucketList.toString(), fetchBucketListSaga);
}
