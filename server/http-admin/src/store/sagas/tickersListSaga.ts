import { takeLatest, put, call } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getTickersList, setTickersList } from "store/actions";

function* fetchTickersListSaga() {
  const data: ITicker[] = yield call(httpService.getCoinFlexTickers);
  yield put(setTickersList(data));
}

export function* tickersListSaga() {
  yield takeLatest(getTickersList.toString(), fetchTickersListSaga);
}
