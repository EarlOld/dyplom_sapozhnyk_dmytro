import { takeLatest, put, all, delay } from "redux-saga/effects";

import {
  setOrderBookData,
  TSetOrderBookDataAction,
  setAskData,
  setBidData
} from "store/actions";
import { handleOrderbookData } from "utils/proOrderbook";

function* watchOrderBookDataSaga(action: TSetOrderBookDataAction) {
  const [asks, bids] = handleOrderbookData(action.payload) || [];
  yield delay(250);

  if (asks && asks.length) {
    yield all([put(setAskData(asks)), put(setBidData(bids))]);
  }
}

export function* orderBookDataSaga() {
  yield takeLatest(setOrderBookData.toString(), watchOrderBookDataSaga);
}
