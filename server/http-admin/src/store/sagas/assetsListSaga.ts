import { takeLatest, put, call } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getAssetsList, setAssetsList } from "store/actions";

function* fetchAssetsListSaga() {
  const data: IAsset[] = yield call(httpService.getCoinFlexAssets);
  yield put(setAssetsList(data));
}

export function* assetsListSaga() {
  yield takeLatest(getAssetsList.toString(), fetchAssetsListSaga);
}
