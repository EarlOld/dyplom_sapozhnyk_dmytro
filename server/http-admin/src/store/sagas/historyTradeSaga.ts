import { takeLatest, put, call, select } from "redux-saga/effects";

import * as http from "services/http/http";
import { getHistoryTrade, setHistoryTrade } from "store/actions";
import { selectAuthParams } from "store/selectors";

function* fetchHistoryTradeSaga() {
  try {
    const authParams = yield select(selectAuthParams);
    const data: IAsset[] = yield call(http.getHistoryTrade, authParams);
    yield put(setHistoryTrade(data));
  } catch (e) {
    // getHistoryTrade error handling
  }
}

export function* historyTradeSaga() {
  yield takeLatest(getHistoryTrade.toString(), fetchHistoryTradeSaga);
}
