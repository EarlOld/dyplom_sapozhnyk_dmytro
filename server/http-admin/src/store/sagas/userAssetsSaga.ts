import { takeLatest, put, call, select } from "redux-saga/effects";
import _isEqual from "lodash/isEqual";
import * as httpService from "services/http/http";
import { getUserAssets, setUserAssets } from "store/actions";
import { selectAuthToken, selectUserAssets } from "../selectors";

function* fetchUserAssetsSaga() {
  const authToken = yield select(selectAuthToken);
  const userAssets = yield select(selectUserAssets);
  try {
    const assets: any = yield call(httpService.getUserAssets, authToken);
    if (assets?.user_assets && !_isEqual(userAssets, assets.user_assets)) {
      yield put(setUserAssets(assets.user_assets));
    }
  } catch (e) {}
}

export function* userAssetsSaga() {
  yield takeLatest(getUserAssets.toString(), fetchUserAssetsSaga);
}
