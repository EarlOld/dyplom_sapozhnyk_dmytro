import { takeLatest, put, call, select } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getOrders, setOrders } from "store/actions";
import { selectAuthParams } from "store/selectors";

function* fetchOrdersSaga() {
  const authParams = yield select(selectAuthParams);
  const data: IOrder[] = yield call(httpService.getOpenOrder, authParams);
  yield put(setOrders(data));
}

export function* ordersSaga() {
  yield takeLatest(getOrders.toString(), fetchOrdersSaga);
}
