import { Action } from "redux";
import { eventChannel, channel, buffers, Channel } from "redux-saga";
import { all, take, put, call, fork, delay, spawn } from "redux-saga/effects";
import { createActions } from "redux-actions";

import coinflexWebsocket from "services/websocket";

import {
  setServerNonce,
  setLastTicker,
  setOrderBookData,
  setLastTrade,
  setLastPosition,
  setOrderConfirmResult,
  getBalances,
  getOrders,
  getPositions,
  getHistoryTrade,
  getBorrowOffers,
  getBorrowLoans,
  getBorrowCollaterals
} from "store/actions";

const THROTTLE_TIME = 250;

const { balanceChanged, orderChanged } = createActions(
  "BALANCE_CHANGED",
  "ORDER_CHANGED"
);

function createSocketChannel() {
  return eventChannel(emit => {
    coinflexWebsocket.subscribe(emit);
    return () => coinflexWebsocket.unsubscribe(emit);
  });
}

function* balanceChangedSaga(balanceChangedChannel: Channel<Action>) {
  while (true) {
    yield take(balanceChangedChannel);
    yield fork(function*() {
      yield all([
        put(getBalances()),
        put(getOrders()),
        put(getPositions()),
        put(getBorrowOffers()),
        put(getBorrowLoans()),
        put(getBorrowCollaterals())
      ]);
      yield put(getHistoryTrade());
    });
    yield delay(THROTTLE_TIME);
  }
}

function* orderChangedSaga(orderChangedChannel: Channel<Action>) {
  while (true) {
    const { payload: data } = yield take(orderChangedChannel);
    yield fork(function*() {
      yield put(setOrderBookData(data));
    });
  }
}

export function* webSocketSaga() {
  const socketChannel = yield call(createSocketChannel);
  const balanceChangedChannel = yield channel(buffers.sliding(1));
  const orderChangedChannel = yield channel(buffers.sliding(1));

  yield spawn(balanceChangedSaga, balanceChangedChannel);
  yield spawn(orderChangedSaga, orderChangedChannel);

  while (true) {
    const data = yield take(socketChannel);
    switch (data.notice) {
      case "Welcome":
        yield put(setServerNonce(data.nonce));
        break;
      case "TickerChanged":
        yield put(setLastTicker(data));
        break;
      case "OrdersMatched":
        yield put(orderChangedChannel, orderChanged(data));
        yield put(setLastTrade(data));
        if (data.ask_tonce || data.bid_tonce) {
          yield put(setLastPosition(data));
        }
        if (data.base && data.bid) {
          yield put(setOrderConfirmResult(data));
        }
        break;
      case "OrderClosed":
      case "OrderOpened":
      case "OrderModified":
        yield put(orderChangedChannel, orderChanged(data));
        break;
      case "BalanceChanged":
        yield put(balanceChangedChannel, balanceChanged());
        break;
      default:
        break;
    }
  }
}
