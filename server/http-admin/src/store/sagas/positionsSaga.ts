import { takeLatest, put, call, select } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getPositions, setPositions } from "store/actions";
import { selectAuthParams } from "store/selectors";

function* fetchPositionsSaga() {
  const authParams = yield select(selectAuthParams);
  const data: IPositions[] = yield call(httpService.getPositions, authParams);
  const list: any = {};
  data.forEach((item: any) => {
    list[item.asset] = item;
  });
  yield put(setPositions(list));
}

export function* positionsSaga() {
  yield takeLatest(getPositions.toString(), fetchPositionsSaga);
}
