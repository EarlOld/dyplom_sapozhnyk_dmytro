import { takeLatest, put, call, select } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getCompetitions, setCompetitions } from "store/actions";
import { selectAuthParams } from "store/selectors";

function* fetchCompetitionsSaga() {
  const authParams = yield select(selectAuthParams);
  const data: IBalance[] = yield call(httpService.getCompetitions, authParams);
  yield put(setCompetitions(data));
}

export function* competitionsSaga() {
  yield takeLatest(getCompetitions.toString(), fetchCompetitionsSaga);
}
