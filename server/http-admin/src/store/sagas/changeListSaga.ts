import { takeLatest, put, call } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getChangeList, setChangeList } from "store/actions";

function* fetchChangeListSaga() {
  const data: ITickerChange = yield call(httpService.getTickers24hAgo);
  yield put(setChangeList(data));
}

export function* changeListSaga() {
  yield takeLatest(getChangeList.toString(), fetchChangeListSaga);
}
