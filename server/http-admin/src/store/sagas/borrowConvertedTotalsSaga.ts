import { takeLatest, put, call, select } from "redux-saga/effects";
import {
  getBorrowConvertedTotals,
  setBorrowConvertedTotals
} from "store/actions";
import { selectAuthParams } from "store/selectors";
import * as httpService from "../../services/http/http";

function* getBorrowConvertedTotalsSaga() {
  const authParams = yield select(selectAuthParams);
  const data: IBorrowCollateral[] = yield call(
    httpService.getBorrowConvertedTotals,
    authParams
  );
  yield put(setBorrowConvertedTotals(data));
}

export function* borrowConvertedTotalsSaga() {
  yield takeLatest(
    getBorrowConvertedTotals.toString(),
    getBorrowConvertedTotalsSaga
  );
}
