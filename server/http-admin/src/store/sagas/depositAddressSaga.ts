import { takeLatest, put, call, select } from "redux-saga/effects";

import * as httpService from "services/http/http";
import {
  getDepositAddress,
  setDepositAddress,
  getAddress
} from "store/actions";
import { selectAuthToken } from "../selectors";
import _get from "lodash/get";

interface FetchDepositAddressActionType {
  payload: { name: string };
  type: string;
}

function* getDepositAddressSaga(action: FetchDepositAddressActionType) {
  yield put(getAddress.trigger(action.payload));
  const authToken = yield select(selectAuthToken);

  try {
    let asset = null;
    const assets: any = yield call(httpService.getUserAssets, authToken);
    if (assets?.user_assets) {
      asset = assets.user_assets.find(
        (item: any) => item.asset.name === action.payload.name
      );
      if (asset.deposit_address) {
        yield put(getAddress.success(asset));
        yield put(setDepositAddress(asset));
      } else {
        const data: any = yield call(
          httpService.getDepositAddress,
          {
            asset_iso: action.payload.name
          },
          authToken
        );
        yield put(getAddress.success(data));
        yield put(setDepositAddress(data));
      }
    } else {
      const data: any = yield call(
        httpService.getDepositAddress,
        {
          asset_iso: action.payload.name
        },
        authToken
      );
      yield put(getAddress.success(data));
      yield put(setDepositAddress(data));
    }
  } catch (e) {
    yield put(
      getAddress.failure({
        ...action.payload,
        errorCode: _get(e, "data.errors[0].code") || 0,
        errorMessage:
          _get(e, "data.errors[0].message") || "Error requested an address"
      })
    );
  }
}

export function* depositAddressSaga() {
  yield takeLatest(getDepositAddress, getDepositAddressSaga);
}
