import { takeLatest, put, call } from "redux-saga/effects";

import * as http from "services/http/http";
import { getBorrowConversions, setBorrowConversions } from "store/actions";

function* fetchBorrowConversionsSaga() {
  const data: IBorrowConversion[] = yield call(http.getBorrowConversions);
  const reversedConcatedData = data.concat(
    data.map(datum => ({
      asset_from: datum.asset_to,
      asset_to: datum.asset_from
    }))
  );
  yield put(setBorrowConversions(reversedConcatedData));
}

export function* borrowConversionsSaga() {
  yield takeLatest(getBorrowConversions.toString(), fetchBorrowConversionsSaga);
}
