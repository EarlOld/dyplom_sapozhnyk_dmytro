import { takeLatest, put, call, select } from "redux-saga/effects";

import * as httpService from "services/http/http";
import {
  balanceConvert,
  balanceWithdraw,
  uploadBalanceWithdrawalAddress,
  getBalances,
  getBorrowConvertedTotals,
  setBalances,
  setBorrowLoan
} from "store/actions";
import { selectAuthParams, selectAuthToken } from "store/selectors";
import * as http from "../../services/http/http";

function* fetchBalancesSaga() {
  const authParams = yield select(selectAuthParams);
  const data: IBalance[] = yield call(httpService.allAvailablesApi, authParams);
  yield put(setBalances(data));
}

function* balanceConvertSaga(action: {
  payload: IConvertAndDoBorrowTwiceData;
}) {
  const authParams = yield select(selectAuthParams);
  try {
    const { payload } = action;
    const convertData = {
      asset_from: payload.asset_from,
      asset_to: payload.asset_to,
      amount: payload.amount
    };

    yield call(http.convert, convertData, authParams);
    yield put(getBorrowConvertedTotals());

    if (payload.borrow_offer_id && payload.borrow_amount) {
      const doBorrowData = {
        offer_id: payload.borrow_offer_id,
        amount: payload.borrow_amount
      };

      const responseData = yield call(http.doBorrow, doBorrowData, authParams);
      responseData.principal = payload.borrow_amount;
      yield put(setBorrowLoan(responseData));
    }
    yield put(balanceConvert.success(action.payload));
    if (payload.counter_borrow_offer_id && payload.counter_borrow_amount) {
      const doBorrowData = {
        offer_id: payload.counter_borrow_offer_id,
        amount: payload.counter_borrow_amount
      };

      const responseData = yield call(http.doBorrow, doBorrowData, authParams);
      responseData.principal = payload.counter_borrow_amount;
      yield put(setBorrowLoan(responseData));
    }
    yield put(balanceConvert.success(action.payload));
  } catch (e) {
    yield put(
      balanceConvert.failure({
        ...action.payload,
        errorCode: (e.data && e.data.error_code) || 0,
        errorMessage:
          e.data && (typeof e.data === "string" ? e.data : e.data.error_msg)
      })
    );
  } finally {
    yield put(balanceConvert.fulfill(action.payload));
  }
}

function* uploadBalanceWithdrawalAddressSaga(action: {
  payload: IWithdrawalAddressData;
}) {
  const authToken = yield select(selectAuthToken);
  try {
    const { payload } = action;

    yield call(http.uploadWithdrawalAddress, payload, authToken);
    yield put(uploadBalanceWithdrawalAddress.success(action.payload));
  } catch (e) {
    const error = e.data && e.data.errors && e.data.errors[0];

    yield put(
      uploadBalanceWithdrawalAddress.failure({
        ...action.payload,
        errorCode: (error && error.code) || 0,
        errorMessage: (error && error.message) || ""
      })
    );
  } finally {
    yield put(uploadBalanceWithdrawalAddress.fulfill(action.payload));
  }
}

function* balanceWithdrawalSaga(action: { payload: IWithdrawalData }) {
  const authToken = yield select(selectAuthToken);

  try {
    const { payload } = action;

    yield call(http.withdrawal, payload, authToken);
    yield put(balanceWithdraw.success(action.payload));
  } catch (e) {
    const error = e.data && e.data.errors && e.data.errors[0];

    yield put(
      balanceWithdraw.failure({
        ...action.payload,
        errorCode: (error && error.code) || 0,
        errorMessage: (error && error.message) || ""
      })
    );
  } finally {
    yield put(balanceWithdraw.fulfill(action.payload));
  }
}

export function* balancesSaga() {
  yield takeLatest(getBalances.toString(), fetchBalancesSaga);
  yield takeLatest(balanceConvert, balanceConvertSaga);
  yield takeLatest(balanceWithdraw, balanceWithdrawalSaga);
  yield takeLatest(
    uploadBalanceWithdrawalAddress,
    uploadBalanceWithdrawalAddressSaga
  );
}
