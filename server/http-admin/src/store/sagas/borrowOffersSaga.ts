import { takeLatest, put, call, select } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getBorrowOffers, setBorrowOffers } from "store/actions";
import { selectAuthParams } from "store/selectors";

function* fetchBorrowOffersSaga() {
  const authParams = yield select(selectAuthParams);
  const data: IBorrowOffer[] = yield call(
    httpService.getBorrowList,
    authParams
  );
  yield put(setBorrowOffers(data));
}

export function* borrowOffersSaga() {
  yield takeLatest(getBorrowOffers.toString(), fetchBorrowOffersSaga);
}
