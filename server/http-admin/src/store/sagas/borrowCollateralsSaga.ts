import { takeLatest, put, call, select } from "redux-saga/effects";

import * as httpService from "services/http/http";
import { getBorrowCollaterals, setBorrowCollaterals } from "store/actions";
import { selectAuthParams } from "store/selectors";

function* fetchBorrowCollateralsSaga() {
  const authParams = yield select(selectAuthParams);
  const data: IBorrowCollateral[] = yield call(
    httpService.getBorrowCollateral,
    authParams
  );
  yield put(setBorrowCollaterals(data));
}

export function* borrowCollateralsSaga() {
  yield takeLatest(getBorrowCollaterals.toString(), fetchBorrowCollateralsSaga);
}
