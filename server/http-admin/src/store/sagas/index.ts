import { all, fork } from "redux-saga/effects";
import { webSocketSaga } from "./webSocketSaga";
import { marketListSaga } from "./marketListSaga";
import { assetsListSaga } from "./assetsListSaga";
import { tickersListSaga } from "./tickersListSaga";
import { changeListSaga } from "./changeListSaga";
import { bucketListSaga } from "./bucketListSaga";
import { i18nSaga } from "./i18nSaga";
import { historyTradeSaga } from "./historyTradeSaga";
import { orderBookDataSaga } from "./orderBookDataSaga";
import { positionsSaga } from "./positionsSaga";
import { balancesSaga } from "./balancesSaga";
import { ordersSaga } from "./ordersSaga";
import { borrowConversionsSaga } from "./borrowConversionsSaga";
import { borrowCollateralsSaga } from "./borrowCollateralsSaga";
import { borrowConvertedTotalsSaga } from "./borrowConvertedTotalsSaga";
import { borrowOffersSaga } from "./borrowOffersSaga";
import { borrowLoansSaga } from "./borrowLoansSaga";
import { competitionsSaga } from "./competitionsSaga";
import { depositAddressSaga } from "./depositAddressSaga";
import { depositWithdrawalsHistorySaga } from "./depositWithdawalsHistorySaga";
import { userAssetsSaga } from "./userAssetsSaga";

export default function* rootSaga() {
  yield all([
    fork(webSocketSaga),
    fork(marketListSaga),
    fork(assetsListSaga),
    fork(tickersListSaga),
    fork(changeListSaga),
    fork(bucketListSaga),
    fork(depositAddressSaga),
    fork(depositWithdrawalsHistorySaga),
    fork(userAssetsSaga),
    // fork(i18nSaga),
    fork(historyTradeSaga),
    fork(orderBookDataSaga),
    fork(positionsSaga),
    fork(balancesSaga),
    fork(ordersSaga),
    fork(borrowConversionsSaga),
    fork(borrowCollateralsSaga),
    fork(borrowConvertedTotalsSaga),
    fork(borrowOffersSaga),
    fork(competitionsSaga),
    fork(borrowLoansSaga)
  ]);
}
