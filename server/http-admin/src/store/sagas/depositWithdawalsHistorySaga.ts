import { takeLatest, call, select, put } from "redux-saga/effects";

import * as httpService from "services/http/http";
import {
  getDepositWithdrawalsHistory,
  setDepositWithdrawalsHistory
} from "store/actions";
import { selectAuthToken } from "store/selectors";

function* fetchDepositWithdrawalsHistorySaga() {
  const authToken = yield select(selectAuthToken);
  const data: {
    deposit_withdrawals: IDepositWithdrawalsHistoryItem[];
  } = yield call(httpService.getDepositWithdrawalHistory, authToken);
  yield put(setDepositWithdrawalsHistory(data.deposit_withdrawals));
}

export function* depositWithdrawalsHistorySaga() {
  yield takeLatest(
    getDepositWithdrawalsHistory.toString(),
    fetchDepositWithdrawalsHistorySaga
  );
}
