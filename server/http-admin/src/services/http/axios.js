import axios from "axios";
import { Toast } from "antd-mobile";

const CancelToken = axios.CancelToken;
axios.defaults.timeout = 15000;
axios.defaults.headers.post["Content-Type"] =
  "application/x-www-form-urlencoded; charset=utf-8";
axios.defaults.headers.get["Content-Type"] = "application/json; charset=utf-8";
axios.defaults.headers.put["Content-Type"] = "application/json; charset=utf-8";
axios.defaults.withCredentials = false;

axios.interceptors.request.use(
  config => {
    const requestName =
      config.method === "get" || config.method === "delete"
        ? config.params
          ? config.params.requestName
          : null
        : config.data.requestName;
    if (requestName) {
      if (axios[requestName] && axios[requestName].cancel) {
        axios[requestName].cancel();
      }
      config.cancelToken = new CancelToken(c => {
        axios[requestName] = {};
        axios[requestName].cancel = c;
      });
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  response => {
    const res = response.data;
    if (res.success) {
      return res;
    }
    if (res.code !== undefined) {
      switch (res.code) {
        case 0:
          return res;
        default:
          Toast.info(res.message);
          return Promise.reject();
      }
    } else {
      return res;
    }
  },
  error => {
    return Promise.reject(error.response);
  }
);

/**
 * GET
 * @param url
 * @param params
 * @param callback
 * @param config
 */
export function fetch(url, params = {}, callback = res => {}, config = {}) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, { params, ...config })
      .then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          resolve(response);
        }
      })
      .catch(reject)
      .then(callback);
  });
}

/**
 * POST
 * @param url
 * @param data
 * @param callback
 * @param config
 */

export function post(url, data = {}, callback = () => {}, config = {}) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, data, config)
      .then(response => {
        if (response.data) {
          resolve(response.data);
        } else {
          resolve(response);
        }
      })
      .catch(reject)
      .then(callback);
  });
}

/**
 * PUT
 * @param url
 * @param data
 */

export function put(url, data = {}, callback = () => {}) {
  return new Promise((resolve, reject) => {
    axios
      .put(url, data)
      .then(response => {
        resolve(response.data);
      })
      .catch(reject)
      .then(callback);
  });
}

/**
 * DELETE
 * @param url
 * @param params
 * @param callback
 * @param config
 */

export function del(url, params = {}, callback = () => {}, config = {}) {
  return new Promise((resolve, reject) => {
    axios
      .delete(url, { params, ...config })
      .then(response => {
        resolve(response.data);
      })
      .catch(reject)
      .then(callback);
  });
}

export function uploadFile(url, data = {}, callback = () => {}) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, data, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then(response => {
        resolve(response.data);
      })
      .catch(reject)
      .then(callback);
  });
}

/**
 * POST method request form data
 * @param url
 * @param data
 * @param callback
 * @param config
 */

export function postForm(url, data = {}, callback = () => {}, config = {}) {
  return post(url, data, callback, {
    transformRequest: [
      function(data) {
        let ret = "";
        for (const it in data) {
          ret +=
            encodeURIComponent(it) + "=" + encodeURIComponent(data[it]) + "&";
        }
        return ret;
      }
    ],
    headers: {
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
    },
    withCredentials: true,
    ...config
  });
}
