import { fetch, post, del, postForm } from "./axios";
import axios from "axios";
import {
  REST_ROUTES,
  BUCKETS_DOMAIN,
  YOUTUBE_DOMAIN,
  I18N,
  BUCKET_ROUTES,
  REST_API_URL,
  WALLET_API_ROUTES,
  WALLET_API_URL,
  WEBAPP_ROUTES
} from "config";
import moment from "moment";

type TCallback = () => void;

/**
 * get all assets
 */
export function getCoinFlexAssets(callback?: TCallback) {
  return fetch(REST_API_URL + REST_ROUTES.ASSETS, {}, callback);
}

/**
 * get the latest tickers of all active markets
 */
export function getCoinFlexTickers() {
  return fetch(REST_API_URL + REST_ROUTES.TICKERS);
}

/**
 * get all active markets
 */
export function getCoinFlexMarkets() {
  return fetch(REST_API_URL + REST_ROUTES.MARKETS);
}

/**
 * get the depth data
 */
export function getDepthData(data: string) {
  return fetch(REST_API_URL + REST_ROUTES.DEPTH + data);
}

/**
 * place limit/market order
 */
export function placeOrder(data: IPlaceOrderData, callback?: TCallback) {
  return postForm(REST_API_URL + REST_ROUTES.ORDERS, data, callback);
}

/**
 * limit, market Returns the user's open limit orders.
 */
export function getOpenOrder(authParams: IAuthParams) {
  return fetch(REST_API_URL + REST_ROUTES.ORDERS, {}, () => {}, authParams);
}

/**
 * get the available balance of the asset witch asset_id is id
 */
export function availableApi(id: number, callback?: TCallback) {
  return fetch(REST_API_URL + REST_ROUTES.BALANCES + id, callback);
}

/**
 * get availables of all assets
 */
export function allAvailablesApi(authParams: IAuthParams) {
  return fetch(REST_API_URL + REST_ROUTES.BALANCES, {}, () => {}, authParams);
}

export function getBorrowList(authParams: IAuthParams) {
  return fetch(
    REST_API_URL + REST_ROUTES.BORROW_LIST,
    {},
    () => {},
    authParams
  );
}

export function getBorrowConversions() {
  return fetch(REST_API_URL + REST_ROUTES.BORROW_CONVERSION);
}

export function getBorrowConvertedTotals(authParams: IAuthParams) {
  return fetch(
    REST_API_URL + REST_ROUTES.BORROW_CONVERTED_TOTALS,
    {},
    () => {},
    authParams
  );
}

export function convert(data: IConvertData, authParams: IAuthParams) {
  return postForm(
    REST_API_URL + REST_ROUTES.BORROW_CONVERSION,
    data,
    () => {},
    {
      ...authParams,
      withCredentials: false
    }
  );
}

export function getBorrowLoans(authParams: IAuthParams) {
  return fetch(
    REST_API_URL + REST_ROUTES.BORROW_LOANS,
    {},
    () => {},
    authParams
  );
}

// export function getBorrowEvents(authParams: IAuthParams) {
//   return fetch(
//     REST_API_URL + REST_ROUTES.BORROW_EVENTS,
//     {},
//     () => {},
//     { ...authParams, keepAlive: true }
//   );
// }

export function clearLoans(
  id: number,
  authParams: IAuthParams,
  callback?: TCallback
) {
  return del(
    REST_API_URL + REST_ROUTES.BORROW_LOANS + id,
    {},
    callback,
    authParams
  );
}

export function repayLoans(
  id: number,
  params: { amount: number },
  authParams: IAuthParams,
  callback?: TCallback
) {
  return postForm(
    REST_API_URL + REST_ROUTES.BORROW_LOANS + id,
    params,
    callback,
    {
      ...authParams,
      withCredentials: false
    }
  );
}

export function getBorrowCollateral(authParams: IAuthParams) {
  return fetch(
    REST_API_URL + REST_ROUTES.BORROW_COLLATERAL,
    {},
    () => {},
    authParams
  );
}

export function doBorrow(
  data: IDoBorrowData,
  authParams: IAuthParams,
  callback?: TCallback
) {
  return postForm(REST_API_URL + REST_ROUTES.BORROW_LOANS, data, callback, {
    ...authParams,
    withCredentials: false
  });
}

/**
 * Returns historical records of the user's past trades.
 */
export function getHistoryTrade(authParams: IAuthParams) {
  return fetch(REST_API_URL + REST_ROUTES.TRADE_HISTORY, {}, () => {}, {
    ...authParams,
    keepAlive: true
  });
}

/**
 * Returns the user's traded net position in all assets.
 */
export function getPositions(authParams: IAuthParams) {
  return fetch(REST_API_URL + REST_ROUTES.POSITIONS, {}, () => {}, authParams);
}

/**
 *
 * @param {base, counter, interval, since} data
 * @param {*} callback
 */
export function getChartHistory(
  data: IChartHistoryRequest,
  callback?: TCallback
) {
  return post(BUCKETS_DOMAIN + BUCKET_ROUTES.BUCKETS, data, callback);
}

/**
 *
 * @param {base, counter, interval, since} data
 * @param {*} callback
 */
export function getBucketBatch(data: IBucketListParam, callback?: TCallback) {
  return post(BUCKETS_DOMAIN + BUCKET_ROUTES.BUCKETS_BATCH, data, callback);
}

/**
 *
 * @param {*} data
 * @param {*} callback
 */
export function getTickers24hAgo(callback?: TCallback) {
  return fetch(BUCKETS_DOMAIN + BUCKET_ROUTES.CHANGE, {}, callback);
}

/**
 *
 * @param {*} data
 * @param {*} callback
 */
export function getYoutubePlaylistVideos(
  data: IYoutubePlaylistItemsRequest,
  callback?: TCallback
) {
  return fetch(`${YOUTUBE_DOMAIN}playlistItems`, data, callback, {
    withCredentials: true
  });
}

/**
 *
 * @param {*} data
 * @param {*} callback
 */
export function getYoutubeVideos(
  data: IYoutubeVideosRequest,
  callback?: TCallback
) {
  return fetch(`${YOUTUBE_DOMAIN}videos`, data, callback, {
    withCredentials: true
  });
}

export function getI18nLanguages() {
  return fetch(`${I18N}languages.json`);
}

export function getI18nMessages(locale: String) {
  return fetch(`${I18N}${locale}.json`);
}

/**
 *
 * @param {*} data
 */
export function oldSignIn(data: ILoginRequest) {
  return postForm(WEBAPP_ROUTES.LOGIN, data);
}

/**
 *
 * @param {*} data
 */
export function oldDoTFA(data: I2FARequest) {
  return postForm(WEBAPP_ROUTES.TFA, data);
}

/**
 *
 * @param {*} data
 * @param {*} callback
 */
export function signIn(data: ILoginRequestV2, callback?: TCallback) {
  return post(WALLET_API_URL + WALLET_API_ROUTES.SIGN_IN, data, callback, {
    headers: {
      "Referer-App": "iot"
    }
  });
}

export const signOut = async (authToken: IAuthToken) => {
  return axios.delete(WALLET_API_URL + WALLET_API_ROUTES.SIGN_IN, {
    headers: {
      Token: authToken.token,
      "Referer-App": "iot"
    }
  });
};

export const getAuthTokenWithPatch = async (authToken: IAuthToken) => {
  return axios.patch(
    WALLET_API_URL + WALLET_API_ROUTES.SIGN_IN,
    {
      refresh_token: authToken.refresh_token
    },
    {
      headers: {
        Token: authToken.token,
        "Referer-App": "iot"
      }
    }
  );
};

export const patchBalances = async (token: string) => {
  return axios.patch(
    WALLET_API_URL + WALLET_API_ROUTES.PATCH_BALANCE,
    {},
    {
      headers: {
        Token: token,
        "Referer-App": "iot"
      }
    }
  );
};

/**
 *
 * @param {*} authToken
 */
export const getUser = async (authToken: IAuthToken) => {
  const expirationDateUTC = moment.utc(authToken.expiration_date).valueOf();
  const nowUTC = moment(new Date()).valueOf();
  const tokenIsValid = expirationDateUTC > nowUTC;
  const returnUser = async (aT: IAuthToken) => {
    return fetch(WALLET_API_URL + WALLET_API_ROUTES.USER, {}, () => {}, {
      headers: {
        Token: aT.token,
        "Referer-App": "iot"
      }
    });
  };

  if (!tokenIsValid) {
    return Promise.reject({ err: 5, errMessage: "TOKEN_EXPIRED" });
  }
  return returnUser(authToken);
};

export function getCompetitions(token: string) {
  return fetch(WALLET_API_URL + WALLET_API_ROUTES.COMPETITIONS, {}, () => {}, {
    headers: {
      Token: token,
      "Referer-App": "iot"
    }
  });
}

export function getUserAssets(token: string) {
  return fetch(WALLET_API_URL + WALLET_API_ROUTES.USER_ASSETS, {}, () => {}, {
    headers: {
      Token: token,
      "Referer-App": "iot"
    }
  });
}

/**
 *
 * @param {*} data
 * @param {*} token
 * @param {*} callback
 */
export function doTFA(data: I2FARequestV2, token: string) {
  return post(WALLET_API_URL + WALLET_API_ROUTES.TFA, data, () => {}, {
    headers: {
      Token: token,
      "Referer-App": "iot"
    }
  });
}

/**
 *
 * @param {*} data
 * @param {*} token
 * @param {*} callback
 */
export const withdrawal = async (data: IWithdrawalData, token: string) => {
  return patchBalances(token).then(() => {
    return post(WALLET_API_URL + WALLET_API_ROUTES.WITHDRAWAL, data, () => {}, {
      headers: {
        Token: token,
        "Referer-App": "iot"
      }
    });
  });
};

/**
 *
 * @param {*} data
 * @param {*} token
 * @param {*} callback
 */
export function uploadWithdrawalAddress(
  data: IWithdrawalAddressData,
  token: string
) {
  return post(
    WALLET_API_URL + WALLET_API_ROUTES.WITHDRAWAL_ADDRESS,
    data,
    () => {},
    {
      headers: {
        Token: token,
        "Referer-App": "iot"
      }
    }
  );
}

export function getDepositWithdrawalHistory(token: string) {
  return fetch(
    WALLET_API_URL + WALLET_API_ROUTES.DEPOSIT_WITHDRAWALS_HISTORY,
    {},
    () => {},
    {
      headers: {
        Token: token,
        "Referer-App": "iot"
      }
    }
  );
}

/**
 *
 * @param {*} data
 * @param {*} callback
 */
export function getYoutubeChannelVideos(
  data: IYoutubeChannelItemsRequest,
  callback?: TCallback
) {
  return fetch(`${YOUTUBE_DOMAIN}search`, data, callback, {
    withCredentials: true
  });
}

export function getDepositAddress(data: object, token: string) {
  return post(
    WALLET_API_URL + WALLET_API_ROUTES.DEPOSIT_ADDRESS,
    data,
    () => {},
    {
      headers: {
        Token: token,
        "Referer-App": "iot"
      }
    }
  );
}
