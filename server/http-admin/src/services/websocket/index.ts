import { WS_API_URL } from "config";
import { moveAccesTokenToSessionStorageAndRemove } from "../../utils/cookie";

class CoinFlexWebsocket {
  private websocket: WebSocket | null = null;
  private globalCallback: any = {};
  private reConnect: boolean = false;
  private reConnectCount: number = 10;
  private subscribers: Set<(arg0: any) => void> = new Set();

  constructor() {
    this.init();
  }

  open() {
    this.reConnect = false;
    this.reConnectCount = 10;
  }

  init() {
    moveAccesTokenToSessionStorageAndRemove();
    this.websocket = new WebSocket(WS_API_URL);
    this.websocket.onmessage = (e: WebSocketMessageEvent) => this.onMessage(e);
    this.websocket.onclose = this.onClose;
    this.websocket.onopen = this.open;
    this.websocket.onerror = e => console.log("Websocket error", e);
  }

  onClose() {
    this.reConnect = true;
    const that = this;
    let timer = setInterval(() => {
      if (this.reConnectCount > 0 && this.reConnect) {
        that.init();
      } else {
        clearInterval(timer);
      }
      this.reConnectCount--;
    }, 1000);
  }

  send(agentData: { tag?: any; channel?: any }, callback: any) {
    agentData.tag && (this.globalCallback[agentData.tag] = callback);
    agentData.channel && (this.globalCallback[agentData.channel] = callback);
    if (this.websocket && this.websocket.readyState === this.websocket.OPEN) {
      this.wsSend(agentData);
    } else if (
      this.websocket &&
      this.websocket.readyState === this.websocket.CONNECTING
    ) {
      setTimeout(() => {
        this.send(agentData, callback);
      }, 1000);
    } else {
      setTimeout(() => {
        this.send(agentData, callback);
      }, 1000);
    }
  }

  wsSend(agentData: any) {
    this.websocket && this.websocket.send(JSON.stringify(agentData));
  }

  onMessage(e: WebSocketMessageEvent) {
    let data = JSON.parse(e.data);
    if (data.tag === 1 || data.error_code) return;
    data.tag && this.globalCallback[data.tag](data);
    data.channel && this.globalCallback[data.channel](data);
    this.subscribers.forEach(subscriber => subscriber(data));
  }

  subscribe(subscriber: (arg0: any) => void) {
    this.subscribers.add(subscriber);
  }

  unsubscribe(subscriber: (arg0: any) => void) {
    this.subscribers.delete(subscriber);
  }
}

export const CoinFlexWebSocketInstance = new CoinFlexWebsocket();
export default CoinFlexWebSocketInstance;
