import { REST_ROUTES, REST_API_URL } from "config";
import store from "store";

import {
  removeBorrowLoan,
  removeBorrowOffer,
  setBorrowCollaterals,
  setBorrowLoan,
  setBorrowLoans,
  setBorrowOffer,
  setBorrowOffers
} from "store/actions";

interface ISourceEvent {
  data: string;
}

class CoinFlexEventstream {
  private eventstream: EventSource | null = null;
  private authParams: IAuthParams | undefined;

  init(_authParams: IAuthParams) {
    this.authParams = _authParams;
    const bit = btoa(
      [this.authParams.auth.username, this.authParams.auth.password].join(":")
    );
    const URL = REST_API_URL + REST_ROUTES.BORROW_EVENTS + `?auth=${bit}`;
    if (!this.eventstream) {
      this.eventstream = new EventSource(URL);
      this.setEventHandlers();
    }
  }

  setEventHandlers() {
    this.setEventHandler("Collateral", data => setBorrowCollaterals(data));
    this.setEventHandler("Offers", data => setBorrowOffers(data));
    this.setEventHandler("OfferOpened", data => setBorrowOffer(data));
    this.setEventHandler("OfferUpdated", data => setBorrowOffer(data));
    this.setEventHandler("OfferClosed", data => removeBorrowOffer(data.id));
    this.setEventHandler("Loans", data => setBorrowLoans(data));
    this.setEventHandler("LoanInitiated", data => setBorrowLoan(data));
    this.setEventHandler("LoanUpdated", data => setBorrowLoan(data));
    this.setEventHandler("LoanTerminated", data => removeBorrowLoan(data.id));
  }

  setEventHandler(eventName: string, handler: (data: any) => void) {
    this.eventstream!.addEventListener(eventName, event => {
      const data = JSON.parse(((event as unknown) as ISourceEvent).data);

      store.dispatch(handler(data));
    });
  }
}

export const CoinFlexEventSourceInstance = new CoinFlexEventstream();
export default CoinFlexEventSourceInstance;
