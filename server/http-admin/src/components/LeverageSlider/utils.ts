import store from "store/index";
import getAuthParams from "../Home/utils/auth/authParams";
import _round from "lodash/round";
import {
  removeBorrowLoan,
  setBorrowLoan,
  repayBorrowLoan
} from "store/actions";
import _isEqual from "lodash/isEqual";
import { decimals } from "../../utils/reference";
import { SCALED } from "../TicketForm/utils/config";
import * as httpService from "services/http/http";

interface IRepayAction {
  fullRepay: boolean;
  loanId: number;
  amount?: number;
  newPrincipal?: number;
}

interface ISliderData {
  empty: boolean;
  offerId: number;
  total: number;
  available: number;
  availableForRepay: number;
  minLoanAmount: number;
  loanQuantity: number;
  ratio: number;
  viewBorrowed: number;
  viewAvailable: number;
  viewMax: number;
  viewMin: number;
  decimalPlaces: number;
}

export const SLIDER_STEPS_COUNT = 40;
export const REPAYMENT_LESS_THAN_MIN_ERROR =
  "Requested repayment would lower outstanding amount below minimum allowed. Consider a full repayment using DELETE.";

const _getAuthParams = () =>
  getAuthParams(store.getState().websocketAuthParams);

export const createLoan = (offerId: number, amount: number) => {
  return httpService
    .doBorrow(
      {
        offer_id: offerId,
        amount: Math.floor(amount)
      },
      _getAuthParams()
    )
    .then(responseData => {
      responseData.principal = Math.floor(amount); //TODO: backend returns 0

      store.dispatch(setBorrowLoan(responseData));
      return Promise.resolve();
    })
    .catch(e => {
      return Promise.reject(e);
    });
};

export const repayLoans = (amount: number, assetId: number) => {
  const allLoans = store.getState().borrowLoans;

  const loans = (allLoans || []).filter(
    (loan: IBorrowLoan) => loan.asset_id === assetId
  );
  let repayAmount = amount;
  const actions: IRepayAction[] = [];

  for (let i = loans.length; i--; ) {
    const loan = loans[i];

    if (repayAmount <= 0) {
      break;
    }

    if (loan.principal <= repayAmount) {
      actions.push({
        fullRepay: true,
        loanId: loan.id
      });
      repayAmount -= loan.principal;
    } else {
      actions.push({
        fullRepay: false,
        loanId: loan.id,
        amount: repayAmount,
        newPrincipal: loan.principal - repayAmount
      });
      break;
    }
  }

  return doRepayActions(actions);
};

const doRepayActions = (actions: IRepayAction[]) => {
  const authParams = _getAuthParams();

  return Promise.all(
    actions.map(action =>
      action.fullRepay
        ? httpService
            .clearLoans(action.loanId, authParams)
            .then(() => {
              store.dispatch(removeBorrowLoan(action.loanId));
              return Promise.resolve();
            })
            .catch(e => {
              return Promise.reject(e);
            })
        : httpService
            .repayLoans(action.loanId, { amount: action.amount! }, authParams)
            .then(() => {
              store.dispatch(
                repayBorrowLoan({
                  id: action.loanId,
                  amount: action.amount!,
                  newPrincipal: action.newPrincipal!
                })
              );
              return Promise.resolve();
            })
            .catch(e => {
              return Promise.reject(e);
            })
    )
  );
};

export const repayAllLoans = (assetId?: number) => {
  let loans: IBorrowLoan[] = store.getState().borrowLoans;
  const authParams = _getAuthParams();

  if (assetId) {
    loans = loans.filter(loan => loan.asset_id === assetId);
  }

  return Promise.all(
    loans.map(loan =>
      httpService
        .clearLoans(loan.id, authParams)
        .then(() => {
          store.dispatch(removeBorrowLoan(loan.id));
          return Promise.resolve();
        })
        .catch(e => {
          return Promise.reject(e);
        })
    )
  );
};

export const getLeverageSliderData = (
  state: IAppState,
  assetId: number,
  baseCounter?: IBaseCounter
): ISliderData => {
  const { borrowOffers, borrowCollaterals, borrowLoans, allBalances } = state;
  const asset =
    baseCounter &&
    state.marketList.find(({ base, counter }) => {
      return _isEqual({ base, counter }, baseCounter);
    });

  const isFuturesAsset = !!asset && !!asset.expires;

  const loans = borrowLoans
    ? borrowLoans.filter(loan => loan.asset_id === assetId)
    : [];

  const loanQuantity = loans.reduce((sum, loan) => {
    sum += loan.principal;
    return sum;
  }, 0);

  const emptyData = {
    ...getEmptySliderData(),
    loanQuantity
  };

  if (baseCounter && (!asset || !isFuturesAsset)) {
    return emptyData;
  }
  if (!borrowCollaterals || !borrowOffers) {
    return emptyData;
  }

  const offer = borrowOffers.find(offer => offer.asset_id === assetId);
  const collateral = borrowCollaterals.find(
    collateral => collateral.asset_id === assetId
  );
  const balance = allBalances.find(balance => balance.id === assetId);

  if (!(offer && collateral)) {
    return emptyData;
  }

  const maxAmount = Math.min(offer.max_amount, offer.amount);

  const total = Math.min(
    collateral.total,
    Math.floor(maxAmount / SLIDER_STEPS_COUNT)
  );
  const available = Math.min(
    collateral.available,
    Math.floor((maxAmount - loanQuantity) / SLIDER_STEPS_COUNT)
  );

  const spotName = asset && asset.spot_name.split("/")[0];
  const decimalPlaces =
    (spotName && decimals[spotName] && decimals[spotName].price) || 2;

  const availableForRepay = balance ? balance.available : loanQuantity;
  const minLoanValue = Math.max(
    offer.min_amount,
    loanQuantity - availableForRepay,
    0
  );

  return {
    empty: false,
    offerId: offer.id,
    total,
    available,
    availableForRepay: availableForRepay,
    minLoanAmount: offer.min_amount,
    loanQuantity,
    ratio: offer.initial_margin,
    viewBorrowed: _round(loanQuantity / SCALED, decimalPlaces),
    viewAvailable: _round(
      collateral.available / offer.initial_margin,
      decimalPlaces
    ),
    viewMax: _round(collateral.total / offer.initial_margin, decimalPlaces),
    viewMin: _round(minLoanValue / SCALED, decimalPlaces),
    decimalPlaces: decimalPlaces
  };
};

const getEmptySliderData = () => ({
  empty: true,
  offerId: 0,
  total: 0,
  available: 0,
  availableForRepay: 0,
  minLoanAmount: 0,
  loanQuantity: 0,
  ratio: 0,
  decimalPlaces: 0,
  viewBorrowed: 0,
  viewAvailable: 0,
  viewMax: 0,
  viewMin: 0
});
