import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { SliderValue } from "antd/es/slider";
import { Toast } from "antd-mobile";
import { message } from "antd";
import _round from "lodash/round";
import _flowRight from "lodash/flowRight";
import { injectIntl, WrappedComponentProps } from "react-intl";

import { delay } from "../../utils/helpers";
import isNumeric from "../../utils/isNumeric";

import { SCALED } from "../TicketForm/utils/config";

import {
  createLoan,
  repayLoans,
  repayAllLoans,
  getLeverageSliderData,
  SLIDER_STEPS_COUNT,
  REPAYMENT_LESS_THAN_MIN_ERROR
} from "./utils";
import { ILeverageSliderViewProps, IBaseCounter } from "./types";

interface IOwnProps {
  isLocked: boolean;
  isDisabled?: boolean;
  assetId: number;
  name: string;
  balanceName?: string;
  baseCounter: IBaseCounter;
  onDataInit?: (isEmpty: boolean) => void;
  onValueChange?: (value: number) => void;
  viewComponent: (
    props: ILeverageSliderViewProps
  ) => React.ComponentElement<ILeverageSliderViewProps, any>;
}

type TStateProps = ReturnType<typeof mapStateToProps>;

type TProps = IOwnProps & TStateProps & WrappedComponentProps;

interface IState {
  sliderValue: number;
  inputBorrowedValue: string;
  inputBorrowedError: string;
  isInputBorrowedFocused: boolean;
  isLocked: boolean;
}

class LeverageSlider extends PureComponent<TProps, IState> {
  constructor(props: TProps) {
    super(props);

    const { sliderValue } = this;

    this.state = {
      sliderValue: sliderValue,
      inputBorrowedValue: "" + props.viewBorrowed,
      inputBorrowedError: "",
      isInputBorrowedFocused: false,
      isLocked: false
    };

    this.onSliderChange = this.onSliderChange.bind(this);
    this.onSliderAfterChange = this.onSliderAfterChange.bind(this);
    this.onInputBorrowedChange = this.onInputBorrowedChange.bind(this);
    this.onInputBorrowedRelease = this.onInputBorrowedRelease.bind(this);
    this.onInputBorrowedFocus = this.onInputBorrowedFocus.bind(this);
    this.onInputBorrowedBlur = this.onInputBorrowedBlur.bind(this);
    this.repayAll = this.repayAll.bind(this);
  }

  get borrowed() {
    const { total, ratio, decimalPlaces } = this.props;

    return _round(
      (this.state.sliderValue * total) / (SLIDER_STEPS_COUNT * ratio) || 0,
      decimalPlaces
    );
  }

  get available() {
    const { total, available, ratio, loanQuantity, decimalPlaces } = this.props;

    return _round(
      (available +
        (loanQuantity - this.state.sliderValue * total) / SLIDER_STEPS_COUNT) /
        ratio,
      decimalPlaces
    );
  }

  get max() {
    const { total, ratio, decimalPlaces } = this.props;

    return _round(total / ratio, decimalPlaces);
  }

  get maxSliderValue() {
    const { total, available, loanQuantity } = this.props;

    return total
      ? Math.floor(
          (SLIDER_STEPS_COUNT *
            (available + loanQuantity / SLIDER_STEPS_COUNT)) /
            total
        )
      : 0;
  }

  get minSliderValue() {
    const { total, availableForRepay, loanQuantity } = this.props;
    const notAvailable = loanQuantity - availableForRepay;

    return notAvailable > 0 && total !== 0
      ? Math.floor(notAvailable / total)
      : 0;
  }

  get sliderValue() {
    const { total, loanQuantity } = this.props;

    return total ? Math.floor(loanQuantity / total) : 0;
  }

  componentDidMount() {
    const { empty, onDataInit, onValueChange } = this.props;

    onDataInit && onDataInit(empty);
    onValueChange && onValueChange(this.state.sliderValue);
  }

  componentDidUpdate(prevProps: TProps) {
    const { props, sliderValue } = this;
    const { isLocked } = this.state;

    if (!prevProps.empty) {
      if (prevProps.total !== props.total) {
        if (!props.isLocked) {
          this.setSliderValue(sliderValue);
        }
      } else if (prevProps.loanQuantity !== props.loanQuantity && !isLocked) {
        this.setSliderValue(sliderValue);
      }
    }

    if (prevProps.empty !== props.empty) {
      const { onDataInit } = props;

      onDataInit && onDataInit(props.empty);
    }
  }

  onSliderChange(value: SliderValue) {
    const newValue = Math.max(
      this.minSliderValue,
      Math.min(value as number, this.maxSliderValue)
    );

    this.setSliderValue(newValue);
  }

  onSliderAfterChange(value: SliderValue) {
    const newValue = Math.min(value as number, this.maxSliderValue);
    const { total } = this.props;

    this.updateLoans(newValue * total);
  }

  onInputBorrowedChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      inputBorrowedValue: e.target.value
    });
  }

  onInputBorrowedFocus() {
    this.setState({
      isInputBorrowedFocused: true
    });
  }

  onInputBorrowedBlur() {
    this.setState({
      isInputBorrowedFocused: false
    });
  }

  onInputBorrowedRelease() {
    const inputValueLine = this.state.inputBorrowedValue;
    const inputValue = +inputValueLine;
    let error = "";

    const { viewAvailable, viewBorrowed, viewMin, intl } = this.props;

    if (!isNumeric(inputValueLine)) {
      error = intl.formatMessage({
        id: "not-a-number-error",
        defaultMessage: "Please enter a number"
      });
    } else if (inputValue > viewBorrowed + viewAvailable) {
      error = `${intl.formatMessage({
        id: "max-value-error",
        defaultMessage: "Maximum value is"
      })}  ${viewBorrowed + viewAvailable}`;
    } else if (inputValue && inputValue < viewMin) {
      error = `${intl.formatMessage({
        id: "min-value-error",
        defaultMessage: "Minimum value is"
      })}  ${viewMin}`;
    }

    if (error) {
      setTimeout(() => {
        this.setState({
          isInputBorrowedFocused: true
        });
      }, 0);
    }

    this.setState({
      inputBorrowedError: error
    });

    if (!error) {
      this.updateLoans(inputValue * SCALED);
    }
  }

  updateLoans(newAmount: number, withoutNotice?: boolean) {
    const loanDelta = newAmount - this.props.loanQuantity;

    if (loanDelta !== 0) {
      this.startLoanChange(() => {
        if (loanDelta > 0) {
          this.increaseLoan(loanDelta, withoutNotice);
        } else if (loanDelta < 0) {
          this.repayLoans(-loanDelta, withoutNotice);
        }
      });
    }
  }

  setSliderValue(newValue: number) {
    const { onValueChange } = this.props;

    this.setState({
      sliderValue: newValue
    });

    if (!this.state.isInputBorrowedFocused) {
      this.setState({
        inputBorrowedValue: "" + this.borrowed,
        inputBorrowedError: ""
      });
    }

    onValueChange && onValueChange(newValue);
  }

  increaseLoan(
    amount: number,
    withoutNotice?: boolean,
    isNotFullRepayment?: boolean
  ) {
    const { offerId, minLoanAmount, loanQuantity } = this.props;

    if (amount < minLoanAmount) {
      if (loanQuantity + amount > minLoanAmount) {
        this.repayAllAndCreateNewLoan(
          loanQuantity + amount,
          false,
          withoutNotice
        );
        return;
      } else {
        this.onLoanSubmitError({
          data: "Requested amount is less than minimum allowed."
        });
        this.onLoanChanged();
        return;
      }
    }

    createLoan(offerId, amount)
      .then((e: any) => {
        !withoutNotice && this.onLoanSubmitted(e, isNotFullRepayment);
      })
      .catch((e: any) => {
        !withoutNotice && this.onLoanSubmitError(e);
      })
      .finally(() => {
        this.onLoanChanged();
      });
  }

  repayLoans(amount: number, withoutNotice?: boolean) {
    const {
      assetId,
      loanQuantity,
      minLoanAmount,
      availableForRepay
    } = this.props;
    let _amount = amount;

    const notAvailablePart = amount - availableForRepay;
    let isNotFullRepay = false;

    if (notAvailablePart > 0) {
      _amount -= notAvailablePart;
      isNotFullRepay = true;
    }

    const newValue = loanQuantity - _amount;
    const diffWithMin = newValue ? newValue - minLoanAmount : 0;

    if (diffWithMin < 0) {
      _amount += diffWithMin;
      isNotFullRepay = true;
    }

    if (!_amount) {
      this.onLoanChanged();
      return;
    }

    repayLoans(_amount, assetId)
      .then(r => !withoutNotice && this.onLoanSubmitted(r, isNotFullRepay))
      .catch(e => {
        if (e && e.data === REPAYMENT_LESS_THAN_MIN_ERROR) {
          this.repayAllAndCreateNewLoan(minLoanAmount, true, withoutNotice);
        } else {
          !withoutNotice && this.onLoanSubmitError(e);
        }
      })
      .finally(() => this.onLoanChanged());
  }

  repayAll() {
    this.repayLoans(this.props.loanQuantity);
  }

  repayAllAndCreateNewLoan(
    newLoanAmount: number,
    isNotFullRepayment?: boolean,
    withoutNotice?: boolean
  ) {
    const { assetId, loanQuantity, availableForRepay } = this.props;

    if (loanQuantity - availableForRepay > 0) {
      this.onLoanSubmitError({
        data: "Requested amount is less than minimum allowed."
      });
      this.onLoanChanged();
      return;
    }

    this.startLoanChange(() => {
      repayAllLoans(assetId)
        .then(() => {
          this.increaseLoan(newLoanAmount, withoutNotice, isNotFullRepayment);
        })
        .catch((e: any) => {
          !withoutNotice && this.onLoanSubmitError(e);
          this.onLoanChanged();
        });
    });
  }

  onLoanSubmitted(e: any, isNotFullRepayment?: boolean) {
    const msg = isNotFullRepayment
      ? this.props.intl.formatMessage({
          id: "not-full-repayment-success",
          defaultMessage:
            "Partial repayment successful. Your loan could not be repaid in full"
        })
      : (e && e.data) ||
        this.props.intl.formatMessage({
          id: "loan-success",
          defaultMessage: "Leverage updated"
        });
    (this.props.isMobile ? Toast : message).success(msg);
  }

  onLoanSubmitError(e: any) {
    const msg =
      (e && e.data) ||
      this.props.intl.formatMessage({
        id: "loan-fail",
        defaultMessage: "Failed to update your leverage"
      });
    if (this.props.isMobile) {
      Toast.fail(msg);
    } else {
      message.error(msg);
    }
  }

  startLoanChange(callback?: () => void) {
    this.setState(
      {
        isInputBorrowedFocused: false,
        isLocked: true
      },
      callback
    );
  }

  onLoanChanged() {
    const { viewBorrowed } = this.props;

    delay(0).then(() => {
      this.setState({
        isLocked: false,
        sliderValue: this.sliderValue,
        inputBorrowedValue: "" + viewBorrowed
      });
    });
  }

  render() {
    const {
      sliderValue,
      inputBorrowedValue,
      inputBorrowedError,
      isInputBorrowedFocused,
      isLocked
    } = this.state;
    const {
      empty,
      assetId,
      viewComponent,
      name,
      balanceName,
      isDisabled,
      viewBorrowed,
      viewAvailable,
      viewMax
    } = this.props;

    const { maxSliderValue, minSliderValue } = this;

    const View = viewComponent;

    const viewValues = {
      borrowed: viewBorrowed,
      available: viewAvailable,
      max: viewMax
    };

    return (
      <View
        id={assetId}
        empty={empty}
        stepsCount={SLIDER_STEPS_COUNT}
        name={name}
        balanceName={balanceName}
        value={sliderValue}
        minValue={minSliderValue}
        maxValue={maxSliderValue}
        inputBorrowedValue={inputBorrowedValue}
        viewValues={viewValues}
        onSliderChange={this.onSliderChange}
        onSliderRelease={this.onSliderAfterChange}
        onInputBorrowedChange={this.onInputBorrowedChange}
        onInputBorrowedFocus={this.onInputBorrowedFocus}
        onInputBorrowedBlur={this.onInputBorrowedBlur}
        isInputBorrowedFocused={isInputBorrowedFocused}
        onInputBorrowedRelease={this.onInputBorrowedRelease}
        inputBorrowedError={inputBorrowedError}
        repayAll={this.repayAll}
        disabled={empty || isLocked || !!isDisabled}
      />
    );
  }
}

const mapStateToProps = (state: IAppState, props: IOwnProps) => {
  return {
    ...getLeverageSliderData(state, props.assetId, props.baseCounter),
    isMobile: state.isMobile
  };
};

export default _flowRight(connect(mapStateToProps), injectIntl)(LeverageSlider);
