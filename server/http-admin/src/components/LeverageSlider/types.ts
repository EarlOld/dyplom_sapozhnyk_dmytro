import { SliderValue } from "antd/es/slider";
import React from "react";

export interface ILeverageSliderViewProps {
  id: number;
  empty?: boolean;
  stepsCount: number;
  name: string;
  balanceName?: string;
  value: number;
  maxValue?: number;
  minValue?: number;
  inputBorrowedValue: string;
  onSliderChange: (newValue: SliderValue) => void;
  onSliderRelease: (newValue: SliderValue) => void;
  onInputBorrowedChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onInputBorrowedFocus: () => void;
  onInputBorrowedBlur: () => void;
  isInputBorrowedFocused: boolean;
  onInputBorrowedRelease: () => void;
  inputBorrowedError: string;
  repayAll: () => void;
  disabled: boolean;
  viewValues: {
    borrowed: number;
    available: number;
    max: number;
  };
}

export interface IBaseCounter {
  base: number;
  counter: number;
}
