import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "components/Button";
import DraggableModal from "./DraggableModal";

export default {
  title: "DraggableModal"
};

export const Default = () => (
  <DraggableModal
    visible={true}
    title="Basic Draggable Modal"
    footer={[
      <Button key={"fail"} link tertiary onClick={action("Cancelled!")}>
        Cancel
      </Button>,
      <Button key={"win"} tertiary onClick={action("Win!")}>
        Done
      </Button>
    ]}
  >
    <p>Content</p>
  </DraggableModal>
);

Default.story = {
  name: "default"
};
