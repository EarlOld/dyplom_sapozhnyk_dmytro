import React from "react";
import { fireEvent } from "@testing-library/react";
import { render } from "utils/testUtils";
import Button from "components/Button/Button";
import DraggableModal from "./DraggableModal";

describe("DraggableModal", () => {
  const handleClose = jest.fn();
  const handleWin = jest.fn();

  const TestDraggableModal = () => (
    <DraggableModal
      visible={true}
      title="Basic Draggable Modal"
      footer={[
        <Button key={"fail"} link tertiary onClick={handleClose}>
          Cancel
        </Button>,
        <Button key={"win"} tertiary onClick={handleWin}>
          Done
        </Button>
      ]}
    >
      <p>Content</p>
    </DraggableModal>
  );

  it("Should match snapshot", () => {
    const { getByRole } = render(<TestDraggableModal />);
    const modal = getByRole("dialog");
    expect(modal).toMatchSnapshot();
  });

  it("Should fire the success callback when done is clicked", () => {
    const { getByText } = render(<TestDraggableModal />);
    const doneButton = getByText("Done");
    expect(doneButton).toBeInTheDocument();
    fireEvent.click(doneButton);
    expect(handleWin).toHaveBeenCalled();
  });
});
