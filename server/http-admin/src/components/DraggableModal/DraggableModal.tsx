import React, { Component } from "react";
import { Modal } from "antd";
import { ModalProps } from "antd/es/modal";

let modalIndex = 0;

export class DraggableModal extends Component<ModalProps> {
  private node?: Element;
  private mouseOffset: {
    x: number;
    y: number;
  };
  private modalIndex: number;

  constructor(props: ModalProps) {
    super(props);
    this.mouseOffset = {
      x: 0,
      y: 0
    };

    this.modalIndex = modalIndex;
    modalIndex++;
  }

  componentDidMount() {
    document.addEventListener("mouseup", this.stopDrag);
  }

  componentDidUpdate(prevProps: ModalProps) {
    if (this.props.visible && !prevProps.visible && !this.node) {
      setTimeout(() => {
        const targetNodes = document.getElementsByClassName(
          `draggable-modal-${this.modalIndex}`
        );
        this.node = targetNodes && targetNodes[0];

        if (this.node) {
          this.node.addEventListener("mousedown", this.initDrag);
        }
      }, 0);
    }
  }

  componentWillUnmount() {
    this.stopDrag();
    if (this.node) {
      this.node.removeEventListener("mousedown", this.initDrag);
    }
    document.removeEventListener("mouseup", this.stopDrag);
  }

  initDrag = (event: Event) => {
    const nodeRect = this.node!.getBoundingClientRect();

    this.mouseOffset = {
      x: (event as MouseEvent).clientX - nodeRect.x,
      y: (event as MouseEvent).clientY - nodeRect.y
    };

    document.addEventListener("mousemove", this.move);
  };

  move = (event: MouseEvent) => {
    const { style } = this.node! as HTMLElement;
    const { mouseOffset } = this;

    style.position = "absolute";
    style.left = `${event.pageX - mouseOffset.x}px`;
    style.top = `${event.pageY - mouseOffset.y}px`;
  };

  stopDrag = () => {
    document.removeEventListener("mousemove", this.move);
  };

  render() {
    const { props } = this;

    return (
      <Modal
        {...this.props}
        className={`${props.className || ""} draggable-modal-${
          this.modalIndex
        }`}
      />
    );
  }
}

export default DraggableModal;
