import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "./Button";

export default {
  title: "Button"
};

export const primary = () => (
  <Button primary onClick={action("Clicked!")}>
    My Button
  </Button>
);

export const secondary = () => (
  <Button secondary onClick={action("Clicked!")}>
    My Button
  </Button>
);

export const tertiary = () => (
  <Button tertiary onClick={action("Clicked!")}>
    My Button
  </Button>
);

export const linkPrimary = () => (
  <Button link primary onClick={action("Clicked!")}>
    My Button
  </Button>
);

linkPrimary.story = {
  name: "link primary"
};

export const linkSecondary = () => (
  <Button link secondary onClick={action("Clicked!")}>
    My Button
  </Button>
);

linkSecondary.story = {
  name: "link secondary"
};

export const linkTertiary = () => (
  <Button link tertiary onClick={action("Clicked!")}>
    My Button
  </Button>
);

linkTertiary.story = {
  name: "link tertiary"
};
