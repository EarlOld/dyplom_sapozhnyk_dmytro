import React from "react";
import { fireEvent } from "@testing-library/react";
import { render } from "utils/testUtils";
import Button from "./Button";

describe("Button", () => {
  const handleWin = jest.fn();

  const TestButton = () => (
    <Button primary onClick={handleWin}>
      My Button
    </Button>
  );

  it("Should match snapshot", () => {
    const { container } = render(<TestButton />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should fire the success callback when Button is clicked", () => {
    const { getByText } = render(<TestButton />);
    const doneButton = getByText("My Button");
    expect(doneButton).toBeInTheDocument();
    fireEvent.click(doneButton);
    expect(handleWin).toHaveBeenCalled();
  });
});
