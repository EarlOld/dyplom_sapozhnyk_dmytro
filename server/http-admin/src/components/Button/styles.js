import styled, { css } from "styled-components";
export const StyledButton = styled.button`
  height: 53px;
  line-height: 53px;
  width: 100%;
  border-radius: 5px;
  border: none;
  font-size: 15px;
  font-weight: 500;
  color: #ffffff;
  box-shadow: none;
  outline: 0;
  user-select: none;
  text-decoration: none;
  text-align: center;
  cursor: pointer;

  ${props =>
    props.primary &&
    css`
      background-color: #00d3b8;
      color: white;
    `}

  ${props =>
    props.secondary &&
    css`
      background-color: #ff2366;
      color: white;
    `}

  ${props =>
    props.tertiary &&
    css`
      background: rgb(135, 72, 227);
      background: linear-gradient(
        90deg,
        rgba(135, 72, 227, 1) 12%,
        rgba(65, 86, 215, 1) 100%
      );
      color: white;
      &:hover {
        background: rgb(135, 72, 227);
      }
    `}
  
  ${props =>
    props.link &&
    props.primary &&
    css`
      border: 1px solid #00d3b8;
      background: transparent;
    `}

  ${props =>
    props.link &&
    props.secondary &&
    css`
      border: 1px solid #ff2366;

      background: transparent;
    `}

  ${props =>
    props.link &&
    props.tertiary &&
    css`
      border: 1px solid rgb(135, 72, 227);
      background: transparent;
      color: white;
      &:hover {
        background: rgb(135, 72, 227);
      }
    `}


  &:after {
    opacity: 0;
  }
  &:before {
    border: none;
  }
  &:active {
    opacity: 0.8;
  }
`;
export default StyledButton;
