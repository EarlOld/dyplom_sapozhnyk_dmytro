import React, { Component } from "react";
import { connect } from "react-redux";
import { IntlProvider } from "react-intl";

class ConnectedIntlProvider extends Component {
  render() {
    const {
      children,
      i18n: { locale, messages }
    } = this.props;
    return (
      <IntlProvider key={locale} locale={locale || "en"} messages={messages}>
        {children}
      </IntlProvider>
    );
  }
}

const mapStateToProps = state => {
  return {
    i18n: state.i18n
  };
};

export default connect(mapStateToProps)(ConnectedIntlProvider);
