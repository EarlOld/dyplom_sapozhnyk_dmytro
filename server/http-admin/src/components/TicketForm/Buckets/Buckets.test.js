import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import Buckets from "./Buckets";

describe("Buckets tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Buckets />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
