import styled from "styled-components";
import { ButtonGroup, Button } from "@blueprintjs/core";
export const StyledButtonGroup = styled(ButtonGroup)`
  border: rgba(255, 255, 255, 0.3) 1px solid;
  border-radius: 4px;
  width: 100%;
  display: flex;
`;

export const StyledButton = styled(Button)`
  flex: 1 !important;
  border-left: rgba(255, 255, 255, 0.3) 1px solid;
  border-right: rgba(255, 255, 255, 0.3) 1px solid;
  font-size: 12px;
  color: rgba(255, 255, 255, 0.3);
  background: transparent;
  border-radius: 0;
  > span {
    font-size: 12px;
    color: rgba(255, 255, 255, 0.3);
  }
  &:hover {
    border-color: #00ccff;
    background: #00ccff !important;
    > span {
      color: #ffffff;
    }
  }
  &:focus {
    outline: 0;
  }
  &:active {
    opacity: 0.8;
  }
  &:first-child {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
  }
  &:last-child {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
  }
`;
