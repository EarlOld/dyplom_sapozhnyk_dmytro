import React, { memo } from "react";
import { StyledButtonGroup, StyledButton } from "./styles";

type TProps = { onClick: (num: number) => void };

export const Buckets = memo(({ onClick }: TProps) => {
  return (
    <StyledButtonGroup minimal={true}>
      <StyledButton onClick={() => onClick(0.25)} small>
        25%
      </StyledButton>
      <StyledButton onClick={() => onClick(0.5)} small>
        50%
      </StyledButton>
      <StyledButton onClick={() => onClick(0.75)} small>
        75%
      </StyledButton>
      <StyledButton onClick={() => onClick(1)} small>
        100%
      </StyledButton>
    </StyledButtonGroup>
  );
});

export default Buckets;
