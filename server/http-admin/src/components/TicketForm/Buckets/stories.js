import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Buckets from "./Buckets";
import { action } from "@storybook/addon-actions";

export default {
  title: "Buckets",
  decorators: [wrapWithMockRedux()]
};

const onChangeHandler = action("Changed value");

export const Default = () => <Buckets onClick={onChangeHandler} />;

Default.story = {
  name: "default"
};
