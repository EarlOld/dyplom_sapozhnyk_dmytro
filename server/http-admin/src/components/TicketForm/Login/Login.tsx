import React from "react";
import { Input, Form, Button, message } from "antd";
import { FormComponentProps } from "antd/es/form";
import { connect } from "react-redux";
import {
  setLogin,
  setWebsocketAuthParams,
  setAuthToken,
  setUser
} from "store/actions";
import { getCurrentUrl } from "utils/helpers";
import { signIn, doTFA, oldSignIn, oldDoTFA } from "services/http/http";
import { Toast } from "antd-mobile";
import "./style/index.scss";
import _isEmpty from "lodash/isEmpty";
import _get from "lodash/get";
import _flowRight from "lodash/flowRight";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import { AxiosResponse } from "axios";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TProps = TStateProps &
  TDispatchProps &
  FormComponentProps &
  WrappedComponentProps;

interface IState {
  step: number;
  userName: string;
  password: string;
  tfa: string;
  loading: boolean;
}

interface ILoginFormValues {
  email: string;
  password: string;
}

class Login extends React.Component<TProps, IState> {
  constructor(props: TProps) {
    super(props);
    this.state = {
      step: 1,
      userName: "",
      password: "",
      tfa: "",
      loading: false
    };
  }

  _isMounted = false;

  jumpToRegister = () => {
    window.open(getCurrentUrl("users/sign_up"));
  };
  changeUserName = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      userName: e.target.value
    });
  };
  changePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      password: e.target.value
    });
  };
  change2FA = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      tfa: e.target.value
    });
  };

  initForm = () => {
    this.setState({
      userName: "",
      password: "",
      tfa: "",
      loading: false
    });
  };

  fail = (e: AxiosResponse) => {
    if (!!this._isMounted) {
      const msg: string = _get(
        e,
        "data.message",
        _get(
          e,
          "data.errors[0].message",
          this.props.intl.formatMessage({
            defaultMessage: "Login failed, please try again later",
            id: "fail-2fa"
          })
        )
      );
      if (this.props.isMobile) {
        Toast.info(msg, 2, () => {}, false);
      } else {
        message.error(msg);
      }
      this.setState({ loading: false });
    }
  };

  handleLogin = (formEvent?: React.FormEvent<HTMLFormElement>) => {
    const { form, isLogin } = this.props;
    const formWasSubmitted = !!formEvent;
    const automaticLoginAttemptOnMount = !formWasSubmitted;
    const isCurrentlyLoggedIn = !!isLogin;
    const isCurrentlyLoggedOut = !isCurrentlyLoggedIn;
    const hasPasswordInState = !_isEmpty(this.state.password);
    const hasUserNameInState = !_isEmpty(this.state.userName);
    const hasCredentialsInState = hasPasswordInState && hasUserNameInState;
    const isStillMounted = !!this._isMounted;
    const notLoggedInAndStillMounted = !isCurrentlyLoggedIn && isStillMounted;

    const win = () => {
      if (notLoggedInAndStillMounted) {
        this.setState({
          loading: false
        });
      }
    };

    const loginHandler = (loginData: ILoginRequest) => {
      const signInDataV2 = {
        email: loginData["user[email]"],
        password: loginData["user[password]"]
      };

      signIn(signInDataV2, win)
        .then(res => {
          this.props.setAuthToken(res);
          if (notLoggedInAndStillMounted) {
            this.setState({ step: 2 });
          }
        })
        .catch(this.fail);
      oldSignIn(loginData);
    };

    if (
      automaticLoginAttemptOnMount &&
      isCurrentlyLoggedOut &&
      hasCredentialsInState
    ) {
      loginHandler({
        "user[email]": this.state.userName,
        "user[password]": this.state.password
      });
    } else if (formWasSubmitted) {
      formEvent!.preventDefault();
      form.validateFields((e, values: ILoginFormValues) => {
        if (!e) {
          this.setState({ loading: true });
          loginHandler({
            "user[email]": values.email,
            "user[password]": values.password
          });
        }
      });
    }
  };

  handle2FA = (e: React.FormEvent<HTMLFormElement>) => {
    const {
      authToken,
      setWebsocketAuthParams,
      setLogin,
      setUser,
      form: { validateFields }
    } = this.props;
    e.preventDefault();
    validateFields((err, values: I2FARequest) => {
      if (!err) {
        this.setState({ loading: true });
        const valuesV2: I2FARequestV2 = { tfa_code: values.twofactor_token };
        doTFA(valuesV2, authToken.token)
          .then(res => {
            const { core_id, api_key, priv_key } = res;
            setWebsocketAuthParams({ core_id, api_key, priv_key });
            setUser(res);
            setLogin(true);
            this.initForm();
            this.setState({ loading: false });
          })
          .catch(this.fail);
        oldDoTFA(values);
      }
    });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    this.handleLogin();
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { step, loading } = this.state;
    const { intl } = this.props;
    return (
      <div className="login">
        <form style={{ display: "none" }}>
          <input type="password" />
        </form>
        <input
          type="password"
          style={{ width: 0, height: 0, float: "left", visibility: "hidden" }}
        />
        <h1 className="title">
          <FormattedMessage defaultMessage="Sign-in to start iot" />
        </h1>
        {step === 1 ? (
          <Form onSubmit={this.handleLogin} className="login-form">
            <Form.Item>
              {getFieldDecorator("email", {
                rules: [
                  {
                    required: true,
                    message: intl.formatMessage({
                      defaultMessage: "Please enter your Username!"
                    })
                  }
                ]
              })(
                <Input
                  placeholder={intl.formatMessage({
                    defaultMessage: "Username"
                  })}
                  onChange={this.changeUserName}
                  autoComplete="off"
                />
              )}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator("password", {
                rules: [
                  {
                    required: true,
                    message: intl.formatMessage({
                      defaultMessage: "Please enter your Password!"
                    })
                  }
                ]
              })(
                <Input
                  type="password"
                  placeholder={intl.formatMessage({
                    defaultMessage: "Password"
                  })}
                  onChange={this.changePassword}
                  autoComplete="new-password"
                />
              )}
            </Form.Item>
            <Form.Item className="form-btns">
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                loading={loading}
                disabled={loading}
              >
                <FormattedMessage defaultMessage="Sign in" />
              </Button>
              <p>
                <FormattedMessage defaultMessage="Don't have an account?" />
              </p>
              <Button type="link" onClick={this.jumpToRegister}>
                <FormattedMessage defaultMessage="Register now" />
              </Button>
            </Form.Item>
          </Form>
        ) : (
          <Form onSubmit={this.handle2FA} className="login-form">
            <Form.Item>
              <p>
                <FormattedMessage defaultMessage="Enter your two-factor authentication (2FA) token" />
              </p>
              {getFieldDecorator("twofactor_token", {
                rules: [
                  {
                    required: true,
                    message: intl.formatMessage({
                      defaultMessage: "Please enter your 2FA token!"
                    })
                  }
                ]
              })(
                <Input
                  placeholder="Authy/YubiKey/GoogleAuth"
                  autoComplete="off"
                  onChange={this.change2FA}
                />
              )}
            </Form.Item>
            <Form.Item className="form-btns">
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                loading={loading}
                disabled={loading}
              >
                <FormattedMessage defaultMessage="Submit 2FA code" />
              </Button>
            </Form.Item>
          </Form>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IAppState) => {
  return {
    isMobile: state.isMobile,
    authToken: state.authToken,
    isLogin: state.isLogin
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setLogin(data: boolean) {
      dispatch(setLogin(data));
    },
    setWebsocketAuthParams(data: IUserAuthentificateParams) {
      dispatch(setWebsocketAuthParams(data));
    },
    setAuthToken(data: IAuthToken) {
      dispatch(setAuthToken(data));
    },
    setUser(data: any) {
      dispatch(setUser(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  Form.create({ name: "normal_login" }),
  injectIntl
)(Login);
