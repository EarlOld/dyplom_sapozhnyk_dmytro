import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import Login from "./Login";

describe("Login", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Login />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
