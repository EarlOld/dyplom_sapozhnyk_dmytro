import React, { useState } from "react";
import { Icon, Tab, Tabs } from "@blueprintjs/core";
import "./index.scss";
import ControlsGroup from "../../Dashboard/ControlsGroup/ControlsGroup";

type TOwnProps = {
  onChange: (num: number) => void;
  defaultSelectedTabId?: string;
};

export const MaxLeverage = ({
  onChange = () => {},
  defaultSelectedTabId
}: TOwnProps) => {
  const [selectedTabId, setSelectedTabId] = useState<string>(
    defaultSelectedTabId || "5"
  );

  const onChangeHandler = (val: string): void => {
    if (selectedTabId !== val) {
      onChange(parseInt(val));
    }
    setSelectedTabId(val);
  };

  return (
    <ControlsGroup>
      <Tabs
        animate={false}
        onChange={onChangeHandler}
        selectedTabId={selectedTabId}
        className="trade-ticked-max-leverage"
      >
        <Tab id={"5"} title="5x" />
        <Tab id={"10"} title="10x" />
        <Tab id={"20"} title="20x" />
        <Tab id={"0"}>
          <Icon icon={"delete"} />
        </Tab>
      </Tabs>
    </ControlsGroup>
  );
};

export default MaxLeverage;
