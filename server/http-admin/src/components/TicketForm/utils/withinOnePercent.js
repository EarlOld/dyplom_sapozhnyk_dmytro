export const withinOnePercent = (numberOne, numberTwo) => {
  return (
    Math.abs(numberOne - numberTwo) / Math.max(numberOne, numberTwo) > 0.01
  );
};
