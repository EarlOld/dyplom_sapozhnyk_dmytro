import React, { Component } from "react";
import { Tabs, Button, Toast } from "antd-mobile";
import { Radio, Input, message } from "antd";
import { RadioChangeEvent } from "antd/es/radio";
import { connect } from "react-redux";
import BScroll from "better-scroll";
import Calc from "utils/math.calc.js";
import Buckets from "./Buckets";
import coinflexWebsocket from "services/websocket";
import { decimals, minimumTradeAmount } from "utils/reference";
import { getCurrentUrl, localStore } from "utils/helpers";
import {
  setBidPrice,
  setAskPrice,
  setPanelConfirm,
  setCurrentBalance,
  setAmountFromOrderBook
} from "store/actions";
import Login from "./Login";
import clsx from "clsx";
import TradeConfirmPanel from "./TradeConfirmPanel";
import TradeConfirmResult from "./TradeConfirmResult";
import LeverageSliders from "./LeverageSliders";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import _get from "lodash/get";
import _round from "lodash/round";
import _flowRight from "lodash/flowRight";
import { SCALED } from "./utils/config";
import _isEqual from "lodash/isEqual";
import "assets/icon/style.css";
import "./style/index.scss";
import isEqual from "lodash/isEqual";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

interface IOwnProps {
  direction: TDirection;
}

type TProps = TStateProps & TDispatchProps & IOwnProps & WrappedComponentProps;

interface IState {
  positionLeft: number | string;
  baseName: string;
  timer?: number;
  scaled: number;
  // trade side - '1':'buy','2':'sell'
  direction: TDirection;
  // trade method - '1':'market','2':'limit'
  tradeType: "1" | "2";
  // trade coin - '1':'XBT','2':'USDT'
  tradeCoin: "1" | "2";
  // stop type - '1':'REGULAR','2':'TRAILING'
  stopType: "1" | "2";
  // Expiry type - '1':'Until','2':'TRAILING'
  expiryType: "1" | "2";
  // true: Take-profit is opened
  profitChecked: false;
  // true: Stop-loss is opened
  lossChecked: false;
  // true: Expiry is opened
  expiryChecked: boolean;
  disabledPlace: boolean;
  // true available enuph
  enoughMargin: boolean;
  // input trade price
  priceString: string; // for formating
  price: number;
  // input trade amount
  amount: string | number;
  // compute trade price away
  priceAway: number;
  // input take profit price
  profitPrice: number;
  // compute profit
  profit: number;
  // input stop loss price
  lossPrice: number;
  // compute loss
  loss: number;
  // expiry time
  expiry: number;
  // compute required margin
  requiredMargin: number;
  // show confirmation
  isConfirm: boolean;
  placeLoading: boolean;
  relevantBalance: number;
  order: {
    id: number;
    price: number;
    quantity: number;
  };
}

interface IPlaceOrderSocketRequest extends IPlaceOrderData {
  tag?: number;
  method?: string;
  tonce?: number;
}
interface IStorage {
  [type: string]: string;
}
export class TicketForm extends Component<TProps, IState> {
  private baseId = 0;
  private quoteId = 0;
  private assetId = 0;
  private quoteName = "";
  private minTick: string | number = "0";
  private buyPrice = 0;
  private sellPrice = 0;
  private lastTradeAmount: IStorage = localStore.get("lastTradeAmount")
    ? localStore.get("lastTradeAmount")
    : "";
  private decimalPrice = 4;
  private decimalBalance = 2;
  private repayInfo = {
    asset_id: 0,
    amount: 0,
    repay: false
  };
  private isInitLastPrice = false;
  private isInitBalance = false;

  constructor(props: TProps) {
    super(props);

    this.state = {
      positionLeft: 0,
      scaled: SCALED,
      direction: this.props.direction,
      tradeType: "1",
      tradeCoin: "2", // Buy with the counter
      stopType: "1",
      expiryType: "2",
      profitChecked: false,
      lossChecked: false,
      expiryChecked: false,
      disabledPlace: true,
      enoughMargin: true,
      price: 0,
      priceString: "",
      amount: "",
      priceAway: 0,
      profitPrice: 0,
      profit: 0,
      lossPrice: 0,
      loss: 0,
      expiry: 0,
      requiredMargin: 0,
      relevantBalance: 0,
      isConfirm: false,
      placeLoading: false,
      baseName: "-",
      order: {
        id: 118616,
        price: 92700000,
        quantity: 100
      }
    };
  }

  /**
   * 切换交易类型（'1':market, '2':limit）
   */
  onChangeTradeType = (tab: { key?: string }) => {
    this.setState(
      {
        tradeType: tab.key! as "1" | "2",
        tradeCoin: tab.key === "2" ? "1" : this.state.tradeCoin
      },
      this.computeMargin
    );
  };

  /**
   * 切换交易方向（'1':buy, '2':sell)
   */
  onChangeDirection = (e: RadioChangeEvent) => {
    this.setState(
      {
        direction: e.target.value,
        tradeCoin: e.target.value === "1" ? "2" : "1",
        amount: 0
      },
      () => {
        this.computeMargin();
        this.assetId =
          this.state.direction === "1" ? this.quoteId : this.baseId;
      }
    );
  };

  /**
   * 切换交易币种（'1':XBT, '2':USDT)
   */
  onChangeCoin = (e: RadioChangeEvent) => {
    this.setState(
      {
        tradeCoin: e.target.value
      },
      this.computeMargin
    );
  };

  /**
   * Input price
   */
  onChangePrice = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(
      {
        priceString: e.target.value,
        price: +e.target.value
      },
      this.computeMargin
    );
  };

  /**
   * Input amount
   */
  onChangeAmount = (e: React.ChangeEvent<HTMLInputElement> | any) => {
    this.setState(
      {
        amount: +e.target.value
      },
      this.computeMargin
    );
  };

  onQuickSelectAmount = (ratio: number) => {
    const { allBalances } = this.props;
    const { direction } = this.state;
    const relevantAssetId = direction === "1" ? this.quoteId : this.baseId;
    const relevantTransferPrice = direction === "1" ? this.props.askPrice : 1;
    const relevantBalance = allBalances.find(
      balance => balance.id === relevantAssetId
    );
    const relevantBalanceAmount = _round(
      (relevantBalance ? relevantBalance.available : 0) / SCALED,
      4
    );

    this.setState(
      {
        amount: _round(
          (relevantBalanceAmount * ratio) / relevantTransferPrice,
          4
        )
      },
      () => {
        this.computeMargin();
      }
    );
  };

  computeMargin = () => {
    const { amount, direction, tradeType } = this.state;

    const {
      allBalances,
      currentSymbol: { quote, base }
    } = this.props;
    const assetId = direction === "1" ? quote.id : base.id;
    const relevantBalance = allBalances.find(balance => balance.id === assetId);
    const relevantBalanceAmount =
      (relevantBalance ? relevantBalance.available : 0) / SCALED;

    const totalAmount =
      tradeType === "1"
        ? _round(direction === "1" ? +amount * this.props.bidPrice : +amount, 4)
        : 0;

    const enoughMargin = totalAmount <= relevantBalanceAmount;

    this.setState({
      relevantBalance: relevantBalanceAmount,
      requiredMargin: totalAmount,
      enoughMargin,
      disabledPlace: !enoughMargin || !(totalAmount > 0)
    });
  };

  placeOrder = (data: IPlaceOrderSocketRequest, callback: () => void) => {
    const { intl } = this.props;
    const { price, baseName } = this.state;
    this.setState({
      placeLoading: true
    });
    // websocket
    data.tag = 4;
    data.method = "PlaceOrder";
    data.tonce = new Date().getTime();
    coinflexWebsocket.send(data, (event: IPlaceOrder) => {
      if (event.error_code === 0) {
        const messageText = intl.formatMessage({
          defaultMessage: "Success!"
        });
        this.localStorage(baseName, price + "");
        if (this.props.isMobile) {
          Toast.success(messageText);
        } else {
          message.success(messageText);
        }
        this.computeMargin();
        callback();
      } else {
        const messageText =
          event.error_msg ||
          intl.formatMessage({
            defaultMessage: "Sorry, you can not trade this contract now!"
          });
        if (this.props.isMobile) {
          //TODO: typescript: Toast.error method does not exist
          // Toast.error("Sorry, you can not trade this contract now!");
          Toast.fail(messageText);
        } else {
          message.error(messageText);
        }
      }
    });
  };

  checkForm = () => {
    const data: IPlaceOrderData = {};
    data.method = "PlaceOrder";
    data.base = this.baseId;
    data.counter = this.quoteId;
    let amount = Math.round(Math.abs(+this.state.amount * this.state.scaled));
    if (this.state.direction === "2") {
      amount = -amount;
    }
    data.quantity = amount;

    if (this.state.tradeType === "2") {
      data.price = Math.round(+this.state.price * this.state.scaled);
    }

    this.setState({
      placeLoading: true
    });
    const callback = () => {
      this.setState({
        placeLoading: false
      });
      this.computeMargin();
    };

    this.placeOrder(data, callback);
  };

  initMarket = () => {};

  updateLastPrice = (e: ILastTradeData) => {
    let lastPrice = 0;
    if (e.last) lastPrice = e.last / this.state.scaled;
    if (e.price) lastPrice = e.price / this.state.scaled;
    this.buyPrice = lastPrice;
    this.sellPrice = lastPrice;

    if (!this.isInitLastPrice) {
      this.isInitLastPrice = true;
      setTimeout(this.computeMargin, 0);
    }
  };

  jumpToDeposit = () => window.open(getCurrentUrl("deposit"));

  adjustSize = () => {
    const { direction, tradeType, price, tradeCoin } = this.state;
    const { bidPrice, askPrice } = this.props;
    let size =
      tradeType === "1"
        ? this.state.relevantBalance / (direction === "1" ? bidPrice : askPrice)
        : this.state.relevantBalance / +price;
    if (tradeCoin === "2") {
      size = this.state.relevantBalance;
    }
    this.setState({
      amount: (size / this.state.scaled).toFixed(4)
    });
  };

  handleAskData = (data: TDepthData) => {
    data.sort((a, b) => a[0] - b[0]);
    data.length > 0 && this.props.setAskPrice(+(data[0][0] / 10000));
  };
  handleBidData = (data: TDepthData) => {
    data.sort((a, b) => b[0] - a[0]);
    data.length > 0 && this.props.setBidPrice(+(data[0][0] / 10000));
  };

  repaymentInfo = (data: IOrdersMatched) => {
    let base = data.base;
    let counter = data.counter;
    if (data.notice === "OrderClosed") {
      this.repayInfo.asset_id = base;
      if (data.quantity > 0) {
        this.repayInfo.asset_id = counter;
      }
      this.repayInfo.amount = Math.abs(data.quantity);
      this.repayInfo.repay = true;
    } else {
      let position = this.props.positions[base];
      if (position) {
        let side = position.position > 0 ? "buy" : "sell";
        if (side === "buy" && data.ask_tonce) {
          this.repayInfo.asset_id = counter;
          this.repayInfo.amount = Math.abs(data.total);
          this.repayInfo.repay = true;
        } else if (side === "sell" && data.bid_tonce) {
          this.repayInfo.asset_id = base;
          this.repayInfo.amount = Math.abs(data.quantity);
          this.repayInfo.repay = true;
        }
      }
    }
  };
  localStorage(type: string, storagePrices: string) {
    if (localStore.get("lastTradeAmount")) {
      this.lastTradeAmount[type] = storagePrices;
      localStore.set(this.lastTradeAmount);
    } else {
      localStore.set({ [type]: storagePrices });
    }
  }
  clearPrice = (data: { name: string }) => {
    const baseNameClear = data.name.split("/")[0];
    const _clear = this.lastTradeAmount[baseNameClear];
    if (typeof _clear !== "undefined") {
      this.setState({
        price: +_clear
      });
    } else {
      this.setState({ price: 0, amount: minimumTradeAmount[baseNameClear] });
    }
  };
  handleSetAmountFromOrderBook = (amount: number) => {
    this.setState(
      {
        tradeType: "2",
        direction: amount > 0 ? "1" : "2",
        tradeCoin: amount > 0 ? "2" : "1"
      },
      () => {
        this.setState(
          {
            price: Math.abs(amount),
            priceString: String(Math.abs(amount))
          },
          this.computeMargin
        );
        this.props.setAmountFromOrderBook(0);
      }
    );
  };

  componentDidMount() {
    const wrapper = document.querySelector("#ticket-scroll");

    if (wrapper) {
      new BScroll(wrapper, {
        click: true, // better-scroll 默认会阻止浏览器的原生 click 事件
        mouseWheel: true
      });
    }
  }

  shouldComponentUpdate(
    nextProps: Readonly<TProps>,
    nextState: Readonly<IState>
  ) {
    if (
      !isEqual(this.props.isMobile, nextProps.isMobile) ||
      !isEqual(this.props.currentSymbol, nextProps.currentSymbol) ||
      !isEqual(this.props.lastPosition, nextProps.lastPosition) ||
      !isEqual(this.props.bidData, nextProps.bidData) ||
      !isEqual(this.props.askData, nextProps.askData) ||
      !isEqual(this.props.askPrice, nextProps.askPrice) ||
      !isEqual(this.props.bidPrice, nextProps.bidPrice) ||
      !isEqual(this.props.currentSymbol, nextProps.currentSymbol) ||
      !isEqual(this.props.allBalances, nextProps.allBalances) ||
      !isEqual(this.props.amountFromOrderBook, nextProps.amountFromOrderBook) ||
      !isEqual(this.state, nextState)
    ) {
      return true;
    }
    return false;
  }

  componentDidUpdate(prevProps: TProps, prevState: IState) {
    const {
      lastPosition,
      askData,
      bidData,
      currentSymbol,
      allBalances,
      amountFromOrderBook
    } = this.props;
    if (!_isEqual(allBalances, prevProps.allBalances)) {
      this.computeMargin();
    }
    if (lastPosition !== prevProps.lastPosition) {
      this.repaymentInfo(lastPosition);
    }
    if (askData && askData !== prevProps.askData) {
      this.handleAskData(askData);
    }
    if (bidData && bidData !== prevProps.bidData) {
      this.handleBidData(bidData);
    }
    if (currentSymbol !== prevProps.currentSymbol) {
      this.clearPrice({ name: currentSymbol.symbolTitle });

      this.isInitBalance = false;
      this.isInitLastPrice = false;
      this.setState({
        baseName: currentSymbol.base.spot_name + ""
      });
    }
    if (
      amountFromOrderBook !== 0 &&
      amountFromOrderBook !== prevProps.amountFromOrderBook &&
      prevProps.amountFromOrderBook === 0
    ) {
      this.handleSetAmountFromOrderBook(amountFromOrderBook);
    }
  }
  render() {
    const {
      lastTrade,
      panelConfirm: { panelType },
      bidPrice,
      askPrice,
      currentSymbol,
      currentSymbol: { symbolTitle, base },
      isLogin,
      intl
    } = this.props;
    let {
      direction,
      tradeType,
      tradeCoin,
      price,
      priceString,
      amount,
      enoughMargin,
      disabledPlace,
      placeLoading,
      baseName
    } = this.state;
    const tabs = [
      { title: intl.formatMessage({ defaultMessage: "Market" }), key: "1" },
      { title: intl.formatMessage({ defaultMessage: "Limit" }), key: "2" }
    ];
    let decimalPrice = 0;
    if (base && base.spot_name) {
      decimalPrice = decimals[base.spot_name]
        ? decimals[base.spot_name].price
        : decimalPrice;
    }

    if (!isLogin) return <Login />;

    if (panelType === "panel_trade_confirm") {
      return (
        <div className="trade-ticket">
          <div className="header">{symbolTitle}</div>
          <TradeConfirmPanel />
        </div>
      );
    } else if (panelType === "panel_trade_confirm_result") {
      return (
        <div className="trade-ticket">
          <div className="header">{symbolTitle}</div>
          <TradeConfirmResult />
        </div>
      );
    }

    this.baseId = _get(currentSymbol, "base.id", 0);
    this.quoteId = _get(currentSymbol, "quote.id", 0);
    this.quoteName = "";
    if (currentSymbol.symbolTitle) {
      this.quoteName = currentSymbol.symbolTitle.split(" ")[0].split("/")[1];
    }
    this.minTick = currentSymbol.min || 0;

    if (symbolTitle && lastTrade[base.id]) {
      this.updateLastPrice(lastTrade[base.id] as ILastTradeData);
    }

    return (
      <div className="trade-ticket">
        <div className="header">{symbolTitle}</div>
        <div className="ticket-form">
          <div className="fix-content">
            <div id="ticket-scroll" className="scroll-wrapper">
              <div className="container">
                <Tabs
                  tabs={tabs}
                  page={tradeType}
                  tabBarBackgroundColor="none"
                  tabBarUnderlineStyle={{
                    zIndex: 2,
                    borderColor: "#4C8CB8",
                    width: 18,
                    marginLeft: "calc(25% - 9px)",
                    boxShadow: "0 0 8px 5px rgba(255, 255, 255, 0.2)"
                  }}
                  tabBarInactiveTextColor="#474748"
                  tabBarActiveTextColor="#ffffff"
                  onChange={this.onChangeTradeType}
                />
                <Radio.Group
                  onChange={this.onChangeDirection}
                  value={direction}
                  buttonStyle="solid"
                  className="trade-type-radio"
                >
                  <Radio.Button value="1">
                    <span className="type">
                      <FormattedMessage defaultMessage="BID" />
                    </span>
                    <span className="price">
                      {Calc.toFixeds(bidPrice, decimalPrice)}
                    </span>
                  </Radio.Button>
                  <Radio.Button value="2">
                    <span className="price">
                      {Calc.toFixeds(askPrice, decimalPrice)}
                    </span>
                    <span className="type">
                      <FormattedMessage defaultMessage="ASK" />
                    </span>
                  </Radio.Button>
                </Radio.Group>
                <div className="input-group">
                  {tradeType === "2" && (
                    <>
                      <div className="input-wrapper">
                        <h1 className="input-title">
                          <FormattedMessage defaultMessage="Limit price" />
                        </h1>
                        <Input
                          placeholder=""
                          type="number"
                          value={price || priceString}
                          onChange={this.onChangePrice}
                          addonAfter={
                            <span className={"counter-total"}>
                              {this.quoteName}
                            </span>
                          }
                        />
                        <p className="placeholder">
                          {direction === "1" ? (
                            <FormattedMessage
                              defaultMessage={`Enter the price you wish to BUY at`}
                            />
                          ) : (
                            <FormattedMessage
                              defaultMessage={`Enter the price you wish to SELL at`}
                            />
                          )}
                        </p>
                      </div>
                      <br />
                    </>
                  )}
                  <div className="input-wrapper">
                    <h1 className="input-title">
                      <FormattedMessage defaultMessage="Amount" />
                    </h1>
                    <Input
                      type="number"
                      value={_round(+amount, 4) || amount}
                      onChange={this.onChangeAmount}
                      addonBefore={
                        <span
                          className={"base-total"}
                          style={{ color: "white" }}
                        >
                          &nbsp;
                        </span>
                      }
                      addonAfter={
                        <span className={"counter-total"}>
                          {baseName} (
                          {_round(
                            tradeCoin === "1"
                              ? +amount * askPrice
                              : +amount * bidPrice,
                            4
                          )}{" "}
                          {this.quoteName})
                        </span>
                      }
                    />
                    <p className="placeholder">
                      <FormattedMessage defaultMessage="Enter the amount you wish to trade or select from below" />
                    </p>
                    <br />
                    <Buckets onClick={this.onQuickSelectAmount} />
                  </div>
                </div>
                <LeverageSliders />
              </div>
            </div>
          </div>
          <div className="flex flex-column flex-content-between bottom-info">
            <ul className="flex flex-content-between margin">
              <li className="margin-item" style={{ textAlign: "left" }}>
                <p>
                  <FormattedMessage defaultMessage="Available To Trade" />
                </p>
                <span className="green">
                  {direction === "1" ? "$T" : baseName + " "}
                  {this.state.relevantBalance}
                </span>
              </li>
            </ul>
            {enoughMargin ? (
              <Button
                disabled={disabledPlace}
                className={clsx(
                  direction === "1" ? "place-btn bgGreen" : "place-btn bgRed"
                )}
                onClick={this.checkForm}
                loading={placeLoading}
              >
                {direction === "1" ? (
                  <FormattedMessage defaultMessage="PLACE BUY ORDER" />
                ) : (
                  <FormattedMessage defaultMessage="PLACE SELL ORDER" />
                )}
              </Button>
            ) : (
              <div className="insufficient-funds">
                <p className="tip">
                  <FormattedMessage defaultMessage="INSUFFICIENT FUNDS" />
                </p>
                <div className="flex flex-content-between">
                  <Button onClick={this.jumpToDeposit}>
                    <FormattedMessage defaultMessage="ADD FUNDS" />
                  </Button>
                  <Button onClick={this.adjustSize}>
                    <FormattedMessage defaultMessage="ADJUST SIZE" />
                  </Button>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IAppState) => {
  return {
    lastTrade: state.lastTrade,
    currentSymbol: state.currentSymbol,
    marketList: state.marketList,
    allBalances: state.allBalances,
    isMobile: state.isMobile,
    bidPrice: state.bidPrice,
    askPrice: state.askPrice,
    isLogin: state.isLogin,
    currentBalance: state.currentBalance,
    positions: state.positions,
    panelConfirm: state.panelConfirm,
    lastPosition: state.lastPosition,
    askData: state.askData,
    bidData: state.bidData,
    amountFromOrderBook: state.amountFromOrderBook,
    assetsList: state.assetsList
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setAskPrice(data: number) {
      dispatch(setAskPrice(data));
    },
    setBidPrice(data: number) {
      dispatch(setBidPrice(data));
    },
    setCurrentBalance(data: number) {
      dispatch(setCurrentBalance(data));
    },
    setPanelConfirm(data: IPanelConfirm) {
      dispatch(setPanelConfirm(data));
    },
    setAmountFromOrderBook(data: number) {
      dispatch(setAmountFromOrderBook(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(TicketForm);
