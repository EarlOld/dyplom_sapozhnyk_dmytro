import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import TicketForm from "./TicketForm";
import NewTicketForm from "./NewTicketForm";

export default {
  title: "TicketForm"
};

export const notLoggedIn = () => <TicketForm />;

notLoggedIn.story = {
  name: "Not logged in",
  decorators: [wrapWithMockRedux({ isLogin: false })]
};

export const loggedIn = () => <TicketForm />;

loggedIn.story = {
  name: "Logged in",
  decorators: [wrapWithMockRedux()]
};

export const NewForm = () => <NewTicketForm />;

NewForm.story = {
  name: "NewTicketForm",
  decorators: [wrapWithMockRedux()]
};
