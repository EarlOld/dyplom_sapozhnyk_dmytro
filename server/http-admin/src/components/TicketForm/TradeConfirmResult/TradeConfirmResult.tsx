import React, { Component } from "react";
import { Button } from "antd-mobile";
import { connect } from "react-redux";
import { setPanelConfirm, setTopTabs } from "store/actions";
import "./style/index.scss";
import clsx from "clsx";
import { SCALED } from "../utils/config";
import { FormattedMessage } from "react-intl";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TProps = TStateProps &
  TDispatchProps & {
    askPrice?: number; //TODO: typescript: property askPrice should be in mapStateToProps?
  };

interface IState {
  orderConfirmMatchedData: {
    [id: number]: IOrdersMatched[];
  };
  scaled: number;
}

class TradeConfirmResult extends Component<TProps, IState> {
  private baseName?: string; //TODO: typescript: value is not set?
  private quoteName?: string; //TODO: typescript: value is not set?

  constructor(props: TProps) {
    super(props);
    this.state = {
      orderConfirmMatchedData: {},
      scaled: SCALED
    };
  }

  componentDidUpdate(prevProps: TProps) {
    if (this.props.orderConfirmResult !== prevProps.orderConfirmResult) {
      this.orderConfirmMatched(this.props.orderConfirmResult);
    }
  }

  orderConfirmMatched = (data: IOrdersMatched) => {
    const { panelConfirm } = this.props;
    let { orderConfirmMatchedData } = this.state;

    //TODO: typescript: panelConfirm.tradeData may be an empty object
    const tradeData = panelConfirm.tradeData as ITradeData;

    if (!tradeData) return;

    if (
      tradeData &&
      tradeData.base &&
      typeof orderConfirmMatchedData[tradeData.base] == "undefined"
    ) {
      orderConfirmMatchedData[tradeData.base] = [];
    }
    if (orderConfirmMatchedData[tradeData.base]) {
      if (
        tradeData &&
        tradeData.base &&
        data.base &&
        tradeData.base === data.base
      ) {
        orderConfirmMatchedData[tradeData.base].push(data);
      }
    }
    this.setState({ orderConfirmMatchedData });
  };

  confirmDone = () => {
    const { setPanelConfirm, setTopTabs } = this.props;
    setTopTabs("POSITIONS");
    setPanelConfirm({ panelType: "panel_trade", tradeData: {} });
  };

  render() {
    const {
      askPrice,
      bidPrice,
      currentSymbol: { symbolTitle },
      panelConfirm
    } = this.props;

    //TODO: typescript: panelConfirm.tradeData may be an undefined or empty object
    const tradeData = panelConfirm.tradeData as ITradeData;

    if (!tradeData) {
      return null;
    }

    const { scaled, orderConfirmMatchedData } = this.state;
    let orderId: string | number = "";
    let quantity = 0;
    let total = 0;
    let profitAndLoss = 0;
    const entryPrice = tradeData.entryPrice;
    const orderData = orderConfirmMatchedData[tradeData.base];
    if (orderData) {
      orderData.forEach(item => {
        orderId = item.bid;
        quantity += item.quantity;
        total += item.total;
        //  (bidPrice(or askPrice) - entryPrice) * amount
        if (item.quantity > 0) {
          profitAndLoss +=
            ((askPrice || 0) - tradeData.entryPrice) * item.quantity;
        } else {
          profitAndLoss += (bidPrice - entryPrice) * item.quantity;
        }
      });
    }

    return (
      <div className="confirm-wrapper">
        <div className="scroll-wrapper">
          <div className="flex flex-column flex-align-center">
            <h1 className="confirm-title">
              <FormattedMessage defaultMessage="Order confirmation" />
              <br />
              <span>
                <FormattedMessage
                  defaultMessage="OrderID {id}"
                  values={{ id: orderId }}
                />
              </span>
            </h1>
            <h2>
              {quantity > 0 ? (
                <FormattedMessage defaultMessage="You bought" />
              ) : (
                <FormattedMessage defaultMessage="You sold" />
              )}
            </h2>
            <p>{symbolTitle}</p>
            {quantity ? (
              <>
                <h2>
                  <FormattedMessage defaultMessage="Amount" />
                </h2>
                <p>
                  {quantity ? Math.abs(quantity / scaled) : ""} {this.baseName}
                </p>
              </>
            ) : (
              <>
                <h2>
                  <FormattedMessage defaultMessage="Amount" />
                </h2>
                <p>
                  {total ? Math.abs(total / scaled) : ""} {this.quoteName}
                </p>
              </>
            )}

            <h2>
              <FormattedMessage defaultMessage="at closing average price" />
            </h2>
            <p>{entryPrice ? entryPrice.toFixed(4) : ""}</p>
            <h2>
              <FormattedMessage defaultMessage="Closing Profit/loss" />
            </h2>
            <p>
              <span
                className={clsx(
                  profitAndLoss > 0 && "fontGreen",
                  profitAndLoss <= 0 && "fontRed"
                )}
              >
                {profitAndLoss ? profitAndLoss.toFixed(4) : ""}
              </span>
            </p>
          </div>
        </div>
        <div className="flex flex-column flex-content-between bottom-done">
          <Button onClick={this.confirmDone}>
            <FormattedMessage defaultMessage="Done" />
          </Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    currentSymbol: state.currentSymbol,
    marketList: state.marketList,
    isMobile: state.isMobile,
    bidPrice: state.bidPrice,
    isLogin: state.isLogin,
    panelConfirm: state.panelConfirm,
    orderConfirmResult: state.orderConfirmResult
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setPanelConfirm(data: IPanelConfirm) {
      dispatch(setPanelConfirm(data));
    },
    setTopTabs(data: TTopTabType) {
      dispatch(setTopTabs(data));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TradeConfirmResult);
