import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import TradeConfirmResult from "./TradeConfirmResult";

describe("TradeConfirmResult", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<TradeConfirmResult />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
