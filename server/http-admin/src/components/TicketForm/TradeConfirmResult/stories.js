import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import TradeConfirmResult from "./TradeConfirmResult";

export default {
  title: "TradeConfirmResult",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <TradeConfirmResult />;

Default.story = {
  name: "default"
};
