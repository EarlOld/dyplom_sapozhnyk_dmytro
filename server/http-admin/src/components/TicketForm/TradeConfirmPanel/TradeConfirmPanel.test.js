import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import TradeConfirmPanel from "./TradeConfirmPanel";

describe("TradeConfirmPanel", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<TradeConfirmPanel />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
