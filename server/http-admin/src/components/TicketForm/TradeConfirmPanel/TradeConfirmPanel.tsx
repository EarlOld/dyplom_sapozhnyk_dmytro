import React, { Component } from "react";
import { Button, Toast } from "antd-mobile";
import { Radio, Input, message } from "antd";
import { connect } from "react-redux";
import coinflexWebsocket from "services/websocket";
import {
  setBidPrice,
  setAskPrice,
  setPanelConfirm,
  setTopTabs,
  getPositions
} from "store/actions";
import { SCALED } from "../utils/config";
import "./style/index.scss";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import _flowRight from "lodash/flowRight";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TProps = TStateProps & TDispatchProps & WrappedComponentProps;

interface IState {
  baseName: string;
  confirmTradeCoin: string;
  confirmPlaceLoading: boolean;
  confirmDisabledPlace: boolean;
  positionBtnText: string;
  scaled: number;
}

class TradeConfirmPanel extends Component<TProps, IState> {
  cancelBtn = () => {
    const { setPanelConfirm, setTopTabs } = this.props;
    setTopTabs("POSITIONS");
    setPanelConfirm({
      panelType: "panel_trade",
      tradeData: {},
      tradeResultData: [] //TODO: typescript: not used?
    });
  };

  closePosition = () => {
    const { panelConfirm, isMobile, intl } = this.props;

    this.setState({
      confirmDisabledPlace: true,
      confirmPlaceLoading: true
    });

    //TODO: typescript: panelConfirm.tradeData may be undefined or empty object
    const tradeData = panelConfirm.tradeData as ITradeData;

    if (tradeData.quantity === 0) return;

    const data = {
      tag: 4,
      method: "PlaceOrder",
      tonce: new Date().getTime(),
      quantity: tradeData.confirmAmount * (tradeData.side === "bid" ? -1 : 1),
      base: tradeData.base,
      counter: tradeData.quote
    };
    // websocket
    coinflexWebsocket.send(data, (res: IPlaceOrder) => {
      if (res.error_code === 0) {
        this.props.setPanelConfirm({
          panelType: "panel_trade_confirm_result",
          tradeData
        });
        (isMobile ? Toast : message).success(
          intl.formatMessage({ defaultMessage: "Order Successfully Placed" })
        );
        this.props.getPositions();
      } else {
        const errorText =
          res.error_msg ||
          intl.formatMessage({
            defaultMessage: "Error"
          });
        isMobile ? Toast.fail(errorText) : message.error(errorText);
      }
    });
  };

  /**
   * Input confirm amount
   */
  confirmOnChangeAmount = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      panelConfirm: { tradeData },
      setPanelConfirm,
      intl
    } = this.props;
    const position = tradeData ? (tradeData as ITradeData).position : 0;
    // Reduce position
    let positionBtnText = this.state.positionBtnText;
    let confirmDisabledPlace = false;
    if (parseInt(e.target.value) > Math.abs(position)) {
      confirmDisabledPlace = true;
    }

    if (parseInt(e.target.value) < Math.abs(position)) {
      positionBtnText = intl.formatMessage({
        defaultMessage: "Reduce position"
      });
    } else {
      positionBtnText = intl.formatMessage({
        defaultMessage: "Close Position"
      });
    }
    setPanelConfirm({
      panelType: "panel_trade_confirm",
      tradeData: { ...tradeData, confirmAmount: +e.target.value }
    });
    this.setState({ confirmDisabledPlace, positionBtnText });
  };

  componentWillMount() {
    const { intl } = this.props;
    this.setState({
      baseName: "",
      confirmTradeCoin: intl.formatMessage({ defaultMessage: "Amount" }),
      confirmPlaceLoading: false,
      confirmDisabledPlace: false,
      positionBtnText: intl.formatMessage({ defaultMessage: "Close Position" }),
      scaled: SCALED
    });
  }

  render() {
    const {
      bidPrice,
      askPrice,
      panelConfirm,
      currentSymbol: { symbolTitle }
    } = this.props;
    let {
      confirmTradeCoin,
      confirmDisabledPlace,
      confirmPlaceLoading,
      positionBtnText
    } = this.state;

    //TODO: typescript: panelConfirm.tradeData may be an empty object
    const tradeData = panelConfirm.tradeData as ITradeData;

    const confirmDirection = tradeData && tradeData.position <= 0 ? "1" : "2";
    const buyAndSellBtn =
      confirmDirection === "1"
        ? "btn-confirm buy-button "
        : "btn-confirm sell-button";
    const baseName = symbolTitle ? symbolTitle.split("/")[0] : "";

    let displayAmount = 0;
    if (tradeData && tradeData.confirmAmount) {
      displayAmount = tradeData.confirmAmount / this.state.scaled;
      // displayAmount *= tradeData.side === "bid" ? -1 : 1;
    }

    //  Reduce position
    return (
      <div className="ticket-form">
        <div className="fix-content">
          <div className="scroll-wrapper">
            <div className="container">
              <Radio.Group
                defaultValue={confirmDirection}
                value={confirmDirection}
                buttonStyle="solid"
                className="trade-type-radio"
              >
                <Radio.Button value="1">
                  <span className="type">
                    <FormattedMessage defaultMessage="BID" />
                  </span>
                  <span className="price">{bidPrice}</span>
                </Radio.Button>
                <Radio.Button value="2">
                  <span className="price">{askPrice}</span>
                  <span className="type">
                    <FormattedMessage defaultMessage="ASK" />
                  </span>
                </Radio.Button>
              </Radio.Group>
              <div className="input-group">
                <div className="input-wrapper">
                  <h1 className="input-title">{confirmTradeCoin}</h1>
                  <Input
                    disabled
                    type="number"
                    value={displayAmount ? displayAmount : ""}
                    onChange={this.confirmOnChangeAmount}
                    addonAfter={
                      baseName ? (
                        <Radio.Group
                          value={confirmTradeCoin}
                          buttonStyle="solid"
                          className="trade-coin-radio"
                        >
                          <Radio.Button value="Amount">{baseName}</Radio.Button>
                        </Radio.Group>
                      ) : (
                        ""
                      )
                    }
                  />
                  <p className="placeholder">
                    <FormattedMessage
                      defaultMessage={`The quantity entered cannot be greater than the currentposition`}
                    />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-column flex-content-between bottom-info">
          <div className="confirm-bottom">
            <div className="flex flex-content-between">
              <Button
                className={buyAndSellBtn}
                onClick={this.closePosition}
                disabled={confirmDisabledPlace}
                loading={confirmPlaceLoading}
              >
                {positionBtnText}
              </Button>
              <Button className="btn-cancel" onClick={this.cancelBtn}>
                <FormattedMessage defaultMessage="Cancel" />
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    currentSymbol: state.currentSymbol,
    bidPrice: state.bidPrice,
    askPrice: state.askPrice,
    isMobile: state.isMobile,
    isLogin: state.isLogin,
    panelConfirm: state.panelConfirm
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setAskPrice(data: number) {
      dispatch(setAskPrice(data));
    },
    setBidPrice(data: number) {
      dispatch(setBidPrice(data));
    },
    setPanelConfirm(data: IPanelConfirm) {
      dispatch(setPanelConfirm(data));
    },
    setTopTabs(data: TTopTabType) {
      dispatch(setTopTabs(data));
    },
    getPositions() {
      dispatch(getPositions());
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(TradeConfirmPanel);
