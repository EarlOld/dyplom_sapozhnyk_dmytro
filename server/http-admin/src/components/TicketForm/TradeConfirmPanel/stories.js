import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import TradeConfirmPanel from "./TradeConfirmPanel";

export default {
  title: "TradeConfirmPanel",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <TradeConfirmPanel />;

Default.story = {
  name: "default"
};
