import React, { Component } from "react";
import { Tabs, Button, Toast } from "antd-mobile";
import { Input, message } from "antd";
import { Checkbox } from "@blueprintjs/core";
import { connect } from "react-redux";
import BScroll from "better-scroll";
import Buckets from "./Buckets";
import coinflexWebsocket from "services/websocket";
import { localStore } from "utils/helpers";
import {
  setBidPrice,
  setAskPrice,
  setPanelConfirm,
  setCurrentBalance,
  setAmountFromOrderBook
} from "store/actions";
import Login from "./Login";
import TradeConfirmPanel from "./TradeConfirmPanel";
import TradeConfirmResult from "./TradeConfirmResult";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import _get from "lodash/get";
import _round from "lodash/round";
import _flowRight from "lodash/flowRight";
import { SCALED } from "./utils/config";
import _isEqual from "lodash/isEqual";
import "assets/icon/style.css";
import "./style/index.scss";
import isEqual from "lodash/isEqual";
import walletPng from "assets/images/icons/wallet.png";
import { MaxLeverage } from "./MaxLeverage";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

interface IOwnProps {
  direction: TDirection;
}

type TProps = TStateProps & TDispatchProps & IOwnProps & WrappedComponentProps;

interface IState {
  positionLeft: number | string;
  baseName: string;
  timer?: number;
  scaled: number;
  // trade method - '1':'market','2':'limit'
  tradeType: "1" | "2";
  // trade coin - '1':'XBT','2':'USDT'
  tradeCoin: "1" | "2";
  // stop type - '1':'REGULAR','2':'TRAILING'
  stopType: "1" | "2";
  // Expiry type - '1':'Until','2':'TRAILING'
  expiryType: "1" | "2";
  // true: Take-profit is opened
  profitChecked: false;
  // true: Stop-loss is opened
  lossChecked: false;
  // true: Expiry is opened
  expiryChecked: boolean;
  disabledPlace: boolean;
  // true available enuph
  enoughAskMargin: boolean;
  enoughBidMargin: boolean;
  // input trade price
  askPriceString: string; // for formating
  bidPriceString: string; // for formating
  askPrice: number;
  bidPrice: number;

  askLeverage: undefined | number;
  bidLeverage: undefined | number;
  // input trade amount
  bidAmount: string | number;
  askAmount: string | number;
  // compute trade price away
  priceAway: number;
  // input take profit price
  profitPrice: number;
  // compute profit
  profit: number;
  // input stop loss price
  lossPrice: number;
  // compute loss
  loss: number;
  // expiry time
  expiry: number;
  // compute required margin
  requiredAskMargin: number;
  requiredBidMargin: number;
  // show confirmation
  isConfirm: boolean;
  placeLoading: boolean;
  relevantBidBalance: number;
  relevantAskBalance: number;
  placeAskLoading: boolean;
  placeBidLoading: boolean;
  order: {
    id: number;
    price: number;
    quantity: number;
  };
}

interface IPlaceOrderSocketRequest extends IPlaceOrderData {
  tag?: number;
  method?: string;
  tonce?: number;
}

interface IStorage {
  [type: string]: string;
}

export class TicketForm extends Component<TProps, IState> {
  private baseId = 0;
  private quoteId = 0;
  private quoteName = "";
  private minTick: string | number = "0";
  private buyPrice = 0;
  private sellPrice = 0;
  private lastTradeAmount: IStorage = localStore.get("lastTradeAmount")
    ? localStore.get("lastTradeAmount")
    : "";
  private decimalPrice = 4;
  private decimalBalance = 2;
  private repayInfo = {
    asset_id: 0,
    amount: 0,
    repay: false
  };
  private isInitLastPrice = false;
  private isInitBalance = false;

  constructor(props: TProps) {
    super(props);

    this.state = {
      positionLeft: 0,
      scaled: SCALED,
      tradeType: "2",
      tradeCoin: "2", // Buy with the counter
      stopType: "1",
      expiryType: "2",
      profitChecked: false,
      lossChecked: false,
      expiryChecked: false,
      disabledPlace: true,
      enoughAskMargin: true,
      enoughBidMargin: true,
      askPrice: 0,
      bidPrice: 0,
      askLeverage: 0,
      bidLeverage: 0,
      askPriceString: "",
      bidPriceString: "",
      bidAmount: "",
      askAmount: "",
      priceAway: 0,
      profitPrice: 0,
      profit: 0,
      lossPrice: 0,
      loss: 0,
      expiry: 0,
      requiredAskMargin: 0,
      requiredBidMargin: 0,
      relevantBidBalance: 0,
      relevantAskBalance: 0,
      placeAskLoading: false,
      placeBidLoading: false,
      isConfirm: false,
      placeLoading: false,
      baseName: "XBT",
      order: {
        id: 118616,
        price: 92700000,
        quantity: 100
      }
    };
  }

  setAskLeverage = (value: number) => {
    this.setState({
      askLeverage: value
    });
  };

  setBidLeverage = (value: number) => {
    this.setState({
      bidLeverage: value
    });
  };

  onChangeTradeType = (tab: { key?: string }) => {
    this.setState(
      {
        tradeType: tab.key! as "1" | "2",
        tradeCoin: tab.key === "2" ? "1" : this.state.tradeCoin
      },
      this.computeMargin
    );
  };

  onChangeAskPrice = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(
      {
        askPriceString: e.target.value,
        askPrice: +e.target.value
      },
      this.computeMargin
    );
  };

  onChangeBidPrice = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(
      {
        bidPriceString: e.target.value,
        bidPrice: +e.target.value
      },
      this.computeMargin
    );
  };

  onChangeBidAmount = (e: React.ChangeEvent<HTMLInputElement> | any) => {
    this.setState(
      {
        bidAmount: +e.target.value
      },
      this.computeMargin
    );
  };

  onChangeAskAmount = (e: React.ChangeEvent<HTMLInputElement> | any) => {
    this.setState(
      {
        askAmount: +e.target.value
      },
      this.computeMargin
    );
  };

  onQuickSelectBidAmount = (ratio: number) => {
    const { allBalances } = this.props;
    const relevantAssetId = this.baseId;
    const relevantBalance = allBalances.find(
      balance => balance.id === relevantAssetId
    );
    const relevantBalanceAmount = _round(
      (relevantBalance ? relevantBalance.available : 0) / SCALED,
      4
    );

    this.setState(
      {
        bidAmount: _round(relevantBalanceAmount * ratio, 4)
      },
      () => {
        this.computeMargin();
      }
    );
  };

  onQuickSelectAskAmount = (ratio: number) => {
    const { allBalances } = this.props;
    const relevantAssetId = this.quoteId;
    const relevantTransferPrice = this.props.askPrice;
    const relevantBalance = allBalances.find(
      balance => balance.id === relevantAssetId
    );
    const relevantBalanceAmount = _round(
      (relevantBalance ? relevantBalance.available : 0) / SCALED,
      4
    );

    this.setState(
      {
        askAmount: _round(
          (relevantBalanceAmount * ratio) / relevantTransferPrice,
          4
        )
      },
      () => {
        this.computeMargin();
      }
    );
  };

  computeMargin = () => {
    const { askAmount, bidAmount, tradeType } = this.state;

    const {
      allBalances,
      currentSymbol: { quote, base }
    } = this.props;
    const assetAskId = quote.id;
    const assetBidId = base.id;
    const relevantAskBalance = allBalances.find(
      balance => balance.id === assetAskId
    );
    const relevantBidBalance = allBalances.find(
      balance => balance.id === assetBidId
    );
    const relevantBalanceAskAmount =
      (relevantAskBalance ? relevantAskBalance.available : 0) / SCALED;
    const relevantBalanceBidAmount =
      (relevantBidBalance ? relevantBidBalance.available : 0) / SCALED;

    const totalAskAmount = tradeType === "1" ? _round(+askAmount, 4) : 0;
    const totalBidAmount =
      tradeType === "1" ? _round(+bidAmount * this.props.bidPrice, 4) : 0;

    const enoughAskMargin = totalAskAmount <= relevantBalanceAskAmount;
    const enoughBidMargin = totalBidAmount <= relevantBalanceBidAmount;

    this.setState({
      relevantAskBalance: relevantBalanceAskAmount,
      relevantBidBalance: relevantBalanceBidAmount,
      requiredAskMargin: totalAskAmount,
      requiredBidMargin: totalBidAmount,
      enoughAskMargin,
      enoughBidMargin
    });
  };

  placeOrder = (data: IPlaceOrderSocketRequest, callback: () => void) => {
    const { intl } = this.props;
    const { askPrice, bidPrice, baseName } = this.state;
    // websocket
    data.tag = 4;
    data.method = "PlaceOrder";
    data.tonce = new Date().getTime();
    coinflexWebsocket.send(data, (event: IPlaceOrder) => {
      if (event.error_code === 0) {
        const messageText = intl.formatMessage({
          defaultMessage: "Success!"
        });
        this.localStorage(baseName, askPrice + "");
        this.localStorage(baseName, bidPrice + "");
        if (this.props.isMobile) {
          Toast.success(messageText);
        } else {
          message.success(messageText);
        }
        this.computeMargin();
        callback();
      } else {
        const messageText =
          event.error_msg ||
          intl.formatMessage({
            defaultMessage: "Sorry, you can not trade this contract now!"
          });
        if (this.props.isMobile) {
          //TODO: typescript: Toast.error method does not exist
          // Toast.error("Sorry, you can not trade this contract now!");
          Toast.fail(messageText);
        } else {
          message.error(messageText);
        }
      }
    });
  };

  checkAskForm = () => {
    const data: IPlaceOrderData = {};
    data.method = "PlaceOrder";
    data.base = this.baseId;
    data.counter = this.quoteId;
    let amount = Math.round(
      Math.abs(+this.state.askAmount * this.state.scaled)
    );
    data.quantity = -amount;

    if (this.state.tradeType === "2") {
      data.price = Math.round(+this.state.askPrice * this.state.scaled);
    }

    this.setState({
      placeLoading: true
    });
    const callback = () => {
      this.setState({
        placeAskLoading: false
      });
      this.computeMargin();
    };
    this.setState({
      placeAskLoading: true
    });
    this.placeOrder(data, callback);
  };

  checkBidForm = () => {
    const data: IPlaceOrderData = {};
    data.method = "PlaceOrder";
    data.base = this.baseId;
    data.counter = this.quoteId;
    const amount = Math.round(
      Math.abs(+this.state.bidAmount * this.state.scaled)
    );
    data.quantity = amount;

    if (this.state.tradeType === "2") {
      data.price = Math.round(+this.state.bidPrice * this.state.scaled);
    }

    this.setState({
      placeLoading: true
    });
    const callback = () => {
      this.setState({
        placeBidLoading: false
      });
      this.computeMargin();
    };
    this.setState({
      placeBidLoading: true
    });
    this.placeOrder(data, callback);
  };

  updateLastPrice = (e: ILastTradeData) => {
    let lastPrice = 0;
    if (e.last) lastPrice = e.last / this.state.scaled;
    if (e.price) lastPrice = e.price / this.state.scaled;
    this.buyPrice = lastPrice;
    this.sellPrice = lastPrice;

    if (!this.isInitLastPrice) {
      this.isInitLastPrice = true;
      setTimeout(this.computeMargin, 0);
    }
  };

  handleAskData = (data: TDepthData) => {
    data.sort((a, b) => a[0] - b[0]);
    data.length > 0 && this.props.setAskPrice(+(data[0][0] / 10000));
  };
  handleBidData = (data: TDepthData) => {
    data.sort((a, b) => b[0] - a[0]);
    data.length > 0 && this.props.setBidPrice(+(data[0][0] / 10000));
  };

  repaymentInfo = (data: IOrdersMatched) => {
    let base = data.base;
    let counter = data.counter;
    if (data.notice === "OrderClosed") {
      this.repayInfo.asset_id = base;
      if (data.quantity > 0) {
        this.repayInfo.asset_id = counter;
      }
      this.repayInfo.amount = Math.abs(data.quantity);
      this.repayInfo.repay = true;
    } else {
      let position = this.props.positions[base];
      if (position) {
        let side = position.position > 0 ? "buy" : "sell";
        if (side === "buy" && data.ask_tonce) {
          this.repayInfo.asset_id = counter;
          this.repayInfo.amount = Math.abs(data.total);
          this.repayInfo.repay = true;
        } else if (side === "sell" && data.bid_tonce) {
          this.repayInfo.asset_id = base;
          this.repayInfo.amount = Math.abs(data.quantity);
          this.repayInfo.repay = true;
        }
      }
    }
  };

  localStorage(type: string, storagePrices: string) {
    if (localStore.get("lastTradeAmount")) {
      this.lastTradeAmount[type] = storagePrices;
      localStore.set(this.lastTradeAmount);
    } else {
      localStore.set({ [type]: storagePrices });
    }
  }

  clearPrice = () => {
    this.setState({
      askPrice: 0,
      bidPrice: 0
    });
  };

  handleSetAmountFromOrderBook = (amount: number) => {
    this.setState(
      {
        tradeType: "2",
        tradeCoin: amount > 0 ? "2" : "1"
      },
      () => {
        if (amount > 0) {
          this.setState(
            {
              bidPrice: Math.abs(amount),
              bidPriceString: String(Math.abs(amount))
            },
            this.computeMargin
          );
        } else {
          this.setState(
            {
              askPrice: Math.abs(amount),
              askPriceString: String(Math.abs(amount))
            },
            this.computeMargin
          );
        }
        this.props.setAmountFromOrderBook(0);
      }
    );
  };

  componentDidMount() {
    const wrapper = document.querySelector("#ticket-scroll");

    if (wrapper) {
      new BScroll(wrapper, {
        click: true, // better-scroll 默认会阻止浏览器的原生 click 事件
        mouseWheel: true
      });
    }
  }

  shouldComponentUpdate(
    nextProps: Readonly<TProps>,
    nextState: Readonly<IState>
  ) {
    if (
      !isEqual(this.props.isMobile, nextProps.isMobile) ||
      !isEqual(this.props.currentSymbol, nextProps.currentSymbol) ||
      !isEqual(this.props.lastPosition, nextProps.lastPosition) ||
      !isEqual(this.props.bidData, nextProps.bidData) ||
      !isEqual(this.props.askData, nextProps.askData) ||
      !isEqual(this.props.askPrice, nextProps.askPrice) ||
      !isEqual(this.props.bidPrice, nextProps.bidPrice) ||
      !isEqual(this.props.currentSymbol, nextProps.currentSymbol) ||
      !isEqual(this.props.allBalances, nextProps.allBalances) ||
      !isEqual(this.props.amountFromOrderBook, nextProps.amountFromOrderBook) ||
      !isEqual(this.state, nextState)
    ) {
      return true;
    }
    return false;
  }

  componentDidUpdate(prevProps: TProps, prevState: IState) {
    const {
      lastPosition,
      askData,
      bidData,
      currentSymbol,
      allBalances,
      amountFromOrderBook
    } = this.props;
    if (!_isEqual(allBalances, prevProps.allBalances)) {
      this.computeMargin();
    }
    if (lastPosition !== prevProps.lastPosition) {
      this.repaymentInfo(lastPosition);
    }
    if (askData && askData !== prevProps.askData) {
      this.handleAskData(askData);
    }
    if (bidData && bidData !== prevProps.bidData) {
      this.handleBidData(bidData);
    }
    if (currentSymbol !== prevProps.currentSymbol) {
      this.clearPrice();

      this.isInitBalance = false;
      this.isInitLastPrice = false;
      this.setState({
        baseName: currentSymbol.base.spot_name + ""
      });
    }
    if (
      amountFromOrderBook !== 0 &&
      amountFromOrderBook !== prevProps.amountFromOrderBook &&
      prevProps.amountFromOrderBook === 0
    ) {
      this.handleSetAmountFromOrderBook(amountFromOrderBook);
    }
  }

  render() {
    const {
      lastTrade,
      panelConfirm: { panelType },
      currentSymbol,
      currentSymbol: { symbolTitle, base },
      isLogin,
      intl
    } = this.props;
    let {
      tradeType,
      relevantAskBalance,
      relevantBidBalance,
      askPrice,
      bidPrice,
      askPriceString,
      bidPriceString,
      bidAmount,
      askAmount,
      enoughAskMargin,
      enoughBidMargin,
      placeAskLoading,
      placeBidLoading,
      baseName
    } = this.state;
    const tabs = [
      { title: intl.formatMessage({ defaultMessage: "Market" }), key: "1" },
      { title: intl.formatMessage({ defaultMessage: "Limit" }), key: "2" }
    ];

    if (!isLogin) return <Login />;

    if (panelType === "panel_trade_confirm") {
      return (
        <div className="trade-ticket">
          <div className="header">{symbolTitle}</div>
          <TradeConfirmPanel />
        </div>
      );
    } else if (panelType === "panel_trade_confirm_result") {
      return (
        <div className="trade-ticket">
          <div className="header">{symbolTitle}</div>
          <TradeConfirmResult />
        </div>
      );
    }

    this.baseId = _get(currentSymbol, "base.id", 0);
    this.quoteId = _get(currentSymbol, "quote.id", 0);
    this.quoteName = "";
    if (currentSymbol.symbolTitle) {
      this.quoteName = currentSymbol.symbolTitle.split(" ")[0].split("/")[1];
    }
    this.minTick = currentSymbol.min || 0;

    if (symbolTitle && lastTrade[base.id]) {
      this.updateLastPrice(lastTrade[base.id] as ILastTradeData);
    }

    return (
      <div className="new-trade-ticket">
        <div className="header">
          <span style={{ marginTop: 5 }}>{symbolTitle}</span>
          <Tabs
            tabs={tabs}
            page={tradeType}
            animated={false}
            tabBarBackgroundColor="none"
            tabBarUnderlineStyle={{
              zIndex: 2,
              borderColor: "#4C8CB8",
              width: 18,
              marginLeft: "calc(25% - 9px)",
              boxShadow: "0 0 8px 5px rgba(255, 255, 255, 0.2)"
            }}
            tabBarInactiveTextColor="#474748"
            tabBarActiveTextColor="#ffffff"
            onChange={this.onChangeTradeType}
          />
          <div className="new-trade-ticket-header-wallet">
            <span style={{ color: "white" }}>0.00</span> USDT
            <img src={walletPng} alt={"wallet"} />
          </div>
        </div>
        <div className="checkbox-container">
          <Checkbox label="Post-Only" />
        </div>
        <div className="ticket-form-container">
          <div className="ticket-form">
            <div id="ticket-scroll" className="scroll-wrapper">
              <div className="container">
                <div className="input-group">
                  {tradeType === "2" && (
                    <>
                      <div className="input-wrapper">
                        <Input
                          placeholder=""
                          type="number"
                          value={bidPrice || bidPriceString}
                          onChange={this.onChangeBidPrice}
                          addonAfter={
                            <span className={"counter-total"}>
                              {this.quoteName}
                            </span>
                          }
                          addonBefore={
                            <span className="input-title">Price</span>
                          }
                        />
                      </div>
                    </>
                  )}
                  <div className="input-wrapper">
                    <Input
                      type="number"
                      value={_round(+bidAmount, 4) || bidAmount}
                      onChange={this.onChangeBidAmount}
                      addonBefore={<span className="input-title">Amount</span>}
                      addonAfter={
                        <span className={"counter-total"}>{baseName}</span>
                      }
                    />
                    <Buckets onClick={this.onQuickSelectBidAmount} />
                    <MaxLeverage onChange={this.setBidLeverage} />
                    <div
                      className="new-ticket-form-info"
                      style={{ marginTop: 20 }}
                    >
                      <div className="new-ticket-form-info-name">Cost</div>
                      <div className="new-ticket-form-info-value">
                        <span style={{ color: "white" }}>
                          {_round(+bidAmount * this.props.bidPrice, 4)}
                        </span>{" "}
                        {this.quoteName}
                      </div>
                    </div>
                    <div className="new-ticket-form-info">
                      <div className="new-ticket-form-info-name">Max Buy</div>
                      <div className="new-ticket-form-info-value">
                        <span style={{ color: "white" }}>
                          {relevantBidBalance}
                        </span>{" "}
                        {baseName}
                      </div>
                    </div>
                    <Button
                      disabled={enoughBidMargin}
                      className="place-bid-btn"
                      onClick={this.checkBidForm}
                      loading={placeBidLoading}
                    >
                      <FormattedMessage defaultMessage="Place BUY Trade" />
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="ticket-form">
            <div id="ticket-scroll" className="scroll-wrapper">
              <div className="container">
                <div className="input-group">
                  {tradeType === "2" && (
                    <div className="input-wrapper">
                      <Input
                        addonBefore={<span className="input-title">Price</span>}
                        placeholder=""
                        type="number"
                        value={askPrice || askPriceString}
                        onChange={this.onChangeAskPrice}
                        addonAfter={
                          <span className={"counter-total"}>
                            {this.quoteName}
                          </span>
                        }
                      />
                    </div>
                  )}
                  <div className="input-wrapper">
                    <Input
                      type="number"
                      value={_round(+askAmount, 4) || askAmount}
                      onChange={this.onChangeAskAmount}
                      addonBefore={<span className="input-title">Amount</span>}
                      addonAfter={
                        <span className={"counter-total"}>{baseName}</span>
                      }
                    />
                    <Buckets onClick={this.onQuickSelectAskAmount} />
                    <MaxLeverage onChange={this.setAskLeverage} />
                    <div
                      className="new-ticket-form-info"
                      style={{ marginTop: 20 }}
                    >
                      <div className="new-ticket-form-info-name">Cost</div>
                      <div className="new-ticket-form-info-value">
                        <span style={{ color: "white" }}>
                          {_round(+askAmount * this.props.askPrice, 4)}
                        </span>{" "}
                        {this.quoteName}
                      </div>
                    </div>
                    <div className="new-ticket-form-info">
                      <div className="new-ticket-form-info-name">Max Sell</div>
                      <div className="new-ticket-form-info-value">
                        <span style={{ color: "white" }}>
                          {_round(relevantAskBalance / this.props.askPrice, 4)}
                        </span>{" "}
                        {baseName}
                      </div>
                    </div>
                    <Button
                      disabled={enoughAskMargin}
                      className="place-ask-btn"
                      onClick={this.checkAskForm}
                      loading={placeAskLoading}
                    >
                      <FormattedMessage defaultMessage="Place SELL Trade" />
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IAppState) => {
  return {
    lastTrade: state.lastTrade,
    currentSymbol: state.currentSymbol,
    marketList: state.marketList,
    allBalances: state.allBalances,
    isMobile: state.isMobile,
    bidPrice: state.bidPrice,
    askPrice: state.askPrice,
    isLogin: state.isLogin,
    currentBalance: state.currentBalance,
    positions: state.positions,
    panelConfirm: state.panelConfirm,
    lastPosition: state.lastPosition,
    askData: state.askData,
    bidData: state.bidData,
    amountFromOrderBook: state.amountFromOrderBook,
    assetsList: state.assetsList
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setAskPrice(data: number) {
      dispatch(setAskPrice(data));
    },
    setBidPrice(data: number) {
      dispatch(setBidPrice(data));
    },
    setCurrentBalance(data: number) {
      dispatch(setCurrentBalance(data));
    },
    setPanelConfirm(data: IPanelConfirm) {
      dispatch(setPanelConfirm(data));
    },
    setAmountFromOrderBook(data: number) {
      dispatch(setAmountFromOrderBook(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(TicketForm);
