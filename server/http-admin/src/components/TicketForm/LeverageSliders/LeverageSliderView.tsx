import React, { memo, useState } from "react";
import { Slider } from "antd";
import "./style/index.scss";
import { ILeverageSliderViewProps } from "../../LeverageSlider/types";

export default memo((props: ILeverageSliderViewProps) => {
  if (props.empty) {
    return null;
  }

  const [container, setContainer] = useState<HTMLDivElement>();

  const handleContainerRendered = (element: HTMLDivElement) =>
    setContainer(element);

  return (
    <div className="leverage-slider" ref={handleContainerRendered}>
      <div className="fields">
        <div>{props.name}</div>
        <div>{props.viewValues.borrowed}</div>
        <div>
          {props.viewValues.available}
          <span className="max"> /{props.viewValues.max}</span>
        </div>
      </div>
      {container ? (
        <Slider
          min={0}
          max={props.stepsCount}
          step={1}
          value={props.value}
          defaultValue={0}
          onChange={props.onSliderChange}
          onAfterChange={props.onSliderRelease}
          tooltipVisible={false}
          tooltipPlacement="bottom"
          getTooltipPopupContainer={() => container}
          tipFormatter={value => `${value}x`}
          disabled={props.disabled}
        />
      ) : null}
    </div>
  );
});
