import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import LeverageSliders from "./LeverageSliders";

export default {
  title: "LeverageSliders",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <LeverageSliders />;

Default.story = {
  name: "default"
};
