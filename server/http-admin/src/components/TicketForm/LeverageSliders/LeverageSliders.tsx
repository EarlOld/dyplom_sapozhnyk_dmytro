import React, { memo, useState, useEffect } from "react";
import { connect } from "react-redux";
import { Switch } from "antd-mobile";
import { Button } from "antd";
import _isEqual from "lodash/isEqual";
import { FormattedMessage } from "react-intl";

import LeverageSlider from "../../LeverageSlider/LeverageSlider";
import "./style/index.scss";
import LeverageSliderView from "./LeverageSliderView";
import { IBaseCounter } from "../../LeverageSlider/types";
import { repayAllLoans } from "../../LeverageSlider/utils";

type TStateProps = ReturnType<typeof mapStateToProps>;

const getAssetId = (currentSymbol: ICurrentSymbol, side: TBuySellSide) =>
  currentSymbol[side === "BUY" ? "base" : "quote"].id;

const getBaseCounter = (currentSymbol: ICurrentSymbol) => {
  const {
    base: { id: base },
    quote: { id: counter }
  } = currentSymbol;

  return { base, counter };
};

const getSliderName = (currentSymbol: ICurrentSymbol, side: TBuySellSide) => {
  const { symbolTitle } = currentSymbol;
  const titleParts = symbolTitle ? symbolTitle.split(" ") : [];
  let name = titleParts[0]
    ? titleParts[0].split("/")[side === "BUY" ? 0 : 1] + " "
    : "";

  titleParts.splice(0, 1);
  name += titleParts.join(" ");

  return name;
};

let prevBaseCounter: IBaseCounter;

const LeverageSliders = memo(({ marketList, currentSymbol }: TStateProps) => {
  const buyAssetId = getAssetId(currentSymbol, "BUY");
  const buySliderName = getSliderName(currentSymbol, "BUY");
  const sellAssetId = getAssetId(currentSymbol, "SELL");
  const sellSliderName = getSliderName(currentSymbol, "SELL");
  const baseCounter = getBaseCounter(currentSymbol);

  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [isLocked, setIsLocked] = useState<boolean>(false);
  const [isDisabled, setIsDisabled] = useState<boolean>(false);

  const handleOpenSwitch = (open: boolean) => setIsOpen(open);

  const repayAll = () => {
    setIsDisabled(true);
    repayAllLoans().finally(() => {
      setIsDisabled(false);
    });
  };

  useEffect(() => {
    if (!_isEqual(baseCounter, prevBaseCounter)) {
      setIsLocked(false);

      prevBaseCounter = baseCounter;
    }
  }, [baseCounter, setIsLocked]);

  const market =
    baseCounter &&
    marketList.find(({ base, counter }) => {
      return _isEqual({ base, counter }, baseCounter);
    });

  const isFuturesMarket = !!market && !!market.expires;

  return isFuturesMarket ? (
    <div className="leverage-sliders">
      <div className="title">
        Leverage
        <Switch color="#86BEBE" checked={isOpen} onChange={handleOpenSwitch} />
      </div>
      {isOpen && (
        <div>
          <div className="fields titles">
            <div>Contract</div>
            <div>Borrowed</div>
            <div>
              Available<span className="max">/Max</span>
            </div>
          </div>
          <LeverageSlider
            assetId={buyAssetId}
            baseCounter={baseCounter}
            name={buySliderName}
            isLocked={isLocked}
            isDisabled={isDisabled}
            viewComponent={LeverageSliderView}
          />
          <LeverageSlider
            assetId={sellAssetId}
            baseCounter={baseCounter}
            name={sellSliderName}
            isLocked={isLocked}
            isDisabled={isDisabled}
            viewComponent={LeverageSliderView}
          />
          <div className="controls">
            <Button
              type="link"
              onClick={repayAll}
              className="repay-all"
              disabled={isDisabled}
            >
              <FormattedMessage defaultMessage="Repay All" />
            </Button>
          </div>
        </div>
      )}
    </div>
  ) : null;
});

const mapStateToProps = (state: IAppState) => {
  return {
    currentSymbol: state.currentSymbol,
    marketList: state.marketList
  };
};

export default connect(mapStateToProps)(LeverageSliders);
