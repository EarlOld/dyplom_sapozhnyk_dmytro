import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import LeverageSliders from "./LeverageSliders";

describe("LeverageSliders", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<LeverageSliders />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
