import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import TicketForm from "./TicketForm";

describe("TicketForm", () => {
  it("Should match snapshot when not logged in", () => {
    const { container } = renderWithMockRedux(<TicketForm />, {
      initialState: { isLogin: false }
    });
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should match snapshot when logged in", () => {
    const { container, rerender } = renderWithMockRedux(
      <TicketForm direction="1" />
    );
    expect(container.firstChild).toMatchSnapshot();
    rerender(<TicketForm direction="2" />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
