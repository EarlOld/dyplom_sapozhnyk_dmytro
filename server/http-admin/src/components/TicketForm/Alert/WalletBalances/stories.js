import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import WalletBalances from "./WalletBalances";

export default {
  title: "WalletBalances"
};

export const Default = () => <WalletBalances visible />;

Default.story = {
  name: "default",
  decorators: [wrapWithMockRedux()]
};
