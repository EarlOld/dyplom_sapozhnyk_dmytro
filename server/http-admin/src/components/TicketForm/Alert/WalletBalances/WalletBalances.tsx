import React, { memo } from "react";
import StyledWalletBalance from "./styles";
import Button from "components/Button";
import { IBalancesProps } from "components/Blotter/Balances/types";
import { connect } from "react-redux";
import WalletBalancesTable, {
  IWalletBalancesTableProps
} from "./WalletBalancesTable";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import _flowRight from "lodash/flowRight";

export interface IWalletBalancesProps extends IWalletBalancesTableProps {
  visible?: boolean;
}

export const WalletBalances = memo(
  (props: IWalletBalancesProps & WrappedComponentProps) => {
    const { visible, allBalances, assetsList, positionList, intl } = props;
    return (
      <StyledWalletBalance
        visible={visible}
        title={intl.formatMessage({ defaultMessage: "Wallet Balances" })}
        onCancel={() => {}}
        footer={[
          <Button key={"fail"} link tertiary onClick={() => {}}>
            <FormattedMessage defaultMessage="Convert" />
          </Button>,
          <Button key={"win"} tertiary onClick={() => {}}>
            <FormattedMessage defaultMessage="Deposit" />
          </Button>
        ]}
      >
        <WalletBalancesTable
          allBalances={allBalances}
          assetsList={assetsList}
          positionList={positionList}
        />
      </StyledWalletBalance>
    );
  }
);

const mapStateToProps = (state: IBalancesProps) => ({
  allBalances: state.allBalances,
  assetsList: state.assetsList,
  positionList: state.positionList
});

export default _flowRight(connect(mapStateToProps), injectIntl)(WalletBalances);
