import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import WalletBalances from "./WalletBalances";

describe("WalletBalances table", () => {
  it("Should match snapshot", () => {
    const { getByRole } = renderWithMockRedux(<WalletBalances visible />);
    const modal = getByRole("dialog");
    expect(modal).toMatchSnapshot();
  });
});
