import React from "react";
import { SCALED } from "../../../utils/config";
import { isSpotAsset } from "utils/isSpotAsset";
import StyledWalletBalancesTable from "./styles";
import { isUsdAsset } from "utils/isUsdAsset";
import _get from "lodash/get";

export interface IWalletBalancesTableProps {
  allBalances: IBalance[];
  assetsList: IAsset[];
  positionList: IPositionListItem[];
}

export const WalletBalancesTable = ({
  allBalances = [],
  assetsList = [],
  positionList = []
}: IWalletBalancesTableProps) => {
  let totalTradeableAmount = 0;

  const tickers = positionList.reduce(
    (mem: Partial<IPositionListItem>, position: IPositionListItem) => {
      return { ...mem, [position.base]: position };
    },
    {} as Partial<IPositionListItem>
  );

  const dataSource: IBalance[] = allBalances
    .filter((balance: IBalance): boolean => {
      const matchingAsset: IAsset | undefined = assetsList.find(
        asset => asset.id === balance.id
      );
      const isSpot = matchingAsset ? isSpotAsset(matchingAsset) : false;
      const nonZeroBalance = !!balance.available || !!balance.reserved;
      const valid = nonZeroBalance && isSpot;
      if (valid) {
        const total = balance.available + balance.reserved;
        const isUsd = isUsdAsset(balance.id);
        const lastPrice = isUsd
          ? 1
          : _get(tickers, [balance.id, "priceAgo"], 0);
        totalTradeableAmount += lastPrice * (total / SCALED);
      }
      return valid;
    })
    .map(res => ({ ...res, id_name: _get(tickers, [res.id, "title"]) }));

  const columns = [
    {
      dataIndex: "id_name"
    },
    {
      dataIndex: "available",
      render: (available: number) =>
        available ? `${(available / SCALED).toLocaleString()}` : 0
    }
  ];

  return (
    <StyledWalletBalancesTable
      showHeader={false}
      columns={columns}
      dataSource={dataSource}
      pagination={false}
      rowKey="id"
      data-testid="summary-table"
      footer={() => (
        <div className={"tr flex"}>
          <div className={"td flex-1"}>Available to trade</div>
          <div className={"td flex-1"}>
            {[
              "$T ",
              parseFloat(totalTradeableAmount.toFixed(2)).toLocaleString()
            ].join("")}
          </div>
        </div>
      )}
    />
  );
};

export default WalletBalancesTable;
