import Table from "components/Table";
import styled from "styled-components";
export const StyledSummaryTable = styled(Table)`
  .ant-table {
    .ant-table-tbody {
      & > tr {
        & > td {
          background: black;
        }
      }
    }
    .ant-table-row {
      cursor: default;
    }
    .ant-table-footer {
      background: black;
      border-top: 1px solid #5e5e5e;
      border-bottom: 1px solid #5e5e5e;
      padding: 0;
      > .tr {
        > .td {
          padding: 6px 10px;
          &:first-child {
            color: #5e5e5e;
          }
          &:last-child {
            color: #00d3b8;
          }
        }
      }
      &:before {
        display: none;
      }
    }
  }
`;

export default StyledSummaryTable;
