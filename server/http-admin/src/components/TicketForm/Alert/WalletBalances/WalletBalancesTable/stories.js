import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import WalletBalancesTable from "./WalletBalancesTable";

export default {
  title: "WalletBalancesTable",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <WalletBalancesTable />;

Default.story = {
  name: "default"
};
