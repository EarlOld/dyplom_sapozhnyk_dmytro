import React from "react";
import { WalletBalancesTable } from "./WalletBalancesTable";
import { renderWithMockRedux } from "utils/testUtils";

describe("WalletBalancesTable", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<WalletBalancesTable />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should have No Data", () => {
    const { getByText } = renderWithMockRedux(<WalletBalancesTable />);
    const asset = getByText("No Data");
    expect(asset).toBeInTheDocument();
  });

  it("Should have Available to trade in the table", () => {
    const { getByText } = renderWithMockRedux(<WalletBalancesTable />);
    const valueApprox = getByText("Available to trade");
    expect(valueApprox).toBeInTheDocument();
  });
});
