import React, { Component } from "react";
import ReactEcharts from "./chart/charts";
import { connect } from "react-redux";
import { ECharts } from "echarts";
import "./style/index.scss";

type TStateProps = ReturnType<typeof mapStateToProps>;

interface IOwnProps {
  passValue: boolean;
  asset: IPositionListItem;
  color: string;
}

type TProps = TStateProps & IOwnProps;

interface IState {
  echartsInstance?: ECharts;
}

class Chart extends Component<TProps, IState> {
  private echartsReact?: ReactEcharts;
  private isInit = false;
  public state: IState = {};

  constructor(props: TProps) {
    super(props);
    this.onReactEchartsRef = this.onReactEchartsRef.bind(this);
  }

  getChartData = () => {
    const { asset, bucketList } = this.props;

    const matchedBucket = bucketList.find(bucket => {
      return (
        asset.counter === bucket.assetPair.counter &&
        asset.base === bucket.assetPair.base
      );
    });

    return matchedBucket
      ? matchedBucket.ohlcList.map(({ close }: { close: number }) => close)
      : [];
  };
  getOption = (list: number[]) => ({
    grid: {
      top: 0,
      left: -5,
      right: -5,
      bottom: 5
    },
    xAxis: {
      type: "category",
      show: false
    },
    yAxis: {
      show: false,
      type: "value",
      min: "dataMin",
      max: "dataMax"
    },
    series: [
      {
        symbol: "none",
        data: list,
        type: "line",
        areaStyle: {
          color: {
            type: "linear",
            x: 0,
            y: 0,
            x2: 0,
            y2: 1,
            colorStops: [
              {
                offset: 0,
                color:
                  this.props.color === "green"
                    ? "rgba(0, 211, 184, 1)"
                    : "rgba(255, 35, 102, 1)"
              },
              {
                offset: 1,
                color:
                  this.props.color === "green"
                    ? "rgba(0, 211, 184, 0)"
                    : "rgba(255, 35, 102, 0)"
              }
            ],
            global: false // 缺省为 false
          }
        },
        itemStyle: {
          normal: {
            color: "#00D3B8",
            lineStyle: {
              color:
                this.props.color === "green"
                  ? "rgba(0, 211, 184, 1)"
                  : "rgba(255, 35, 102, 1)",
              width: 1
            }
          }
        }
      }
    ]
  });

  init() {
    const { echartsInstance } = this.state;

    if (!this.isInit && echartsInstance) {
      echartsInstance.setOption({
        series: [
          {
            data: []
          }
        ]
      });
      this.isInit = true;
    }
  }

  onReactEchartsRef(element: ReactEcharts) {
    if (!this.echartsReact && element) {
      this.echartsReact = element;
      this.setState({
        echartsInstance: element.getEchartsInstance()
      });
    }
  }

  componentDidMount() {
    this.getChartData();
    this.init();
  }

  componentDidUpdate(prevProps: TProps, prevState: IState) {
    this.init();
  }

  render() {
    const dataChart = this.getChartData();
    return (
      <div className="chart">
        <ReactEcharts
          ref={this.onReactEchartsRef}
          option={this.getOption(dataChart)}
          style={{ height: "100%", width: "100%" }}
          className="line"
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState, ownProps: IOwnProps) => {
  return {
    bucketList: state.bucketList
  };
};

export default connect(mapStateToProps)(Chart);
