import echarts from "echarts";
import EchartsReactCore from "./core";
import { IChartProps } from "../types";

// export the Component the echarts Object.
export default class EchartsReact extends EchartsReactCore {
  constructor(props: IChartProps) {
    super(props);
    this.echartsLib = echarts;
  }
}
