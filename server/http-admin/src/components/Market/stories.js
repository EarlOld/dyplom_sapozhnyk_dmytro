import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Market from "./Market";

export default {
  title: "Market"
};

export const Default = () => <Market />;

Default.story = {
  name: "default",
  decorators: [wrapWithMockRedux()]
};

export const mobile = () => <Market />;

mobile.story = {
  decorators: [wrapWithMockRedux({ isMobile: true })]
};
