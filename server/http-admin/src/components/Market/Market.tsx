import React, { Component } from "react";

import Chart from "./Chart";
import { Icon, Button } from "antd";
import { Modal } from "antd-mobile";
import BScroll from "better-scroll";
import { decimals, contractCode } from "utils/reference";
import { getCurrentMonth, getCurrentUrl } from "utils/helpers";
import history from "utils/history";
import "./style/index.scss";
import { PUBLIC_URL } from "config";
import _isEqual from "lodash/isEqual";
import { connect } from "react-redux";
import {
  setSelectedMarket,
  getBucketList,
  setPositionList
} from "store/actions";
import coinflexWebsocket from "services/websocket";
import clsx from "clsx";
import { FormattedMessage } from "react-intl";
import isEqual from "lodash/isEqual";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TProps = TStateProps & TDispatchProps;

interface IState {
  isUp: boolean;
  showMore: boolean;
}

interface IHasSubTickers {
  [base: number]: boolean;
}

export class Market extends Component<TProps, IState> {
  private allAssets: IPositionListItem[] = [];
  private isShow = false;
  private scroll: BScroll | null = null;
  private hasSubTickers: IHasSubTickers = {};

  constructor(props: TProps) {
    super(props);
    this.state = {
      isUp: true,
      showMore: false
    };
  }

  changeDirection = () => {
    this.setState({
      isUp: !this.state.isUp
    });
  };

  getCurrentMonthTicker = () => {
    const { changeList, tickersList, marketList } = this.props;
    const list: IPositionListItem[] = [];
    let flex = null;
    marketList.forEach(item => {
      if (!this.hasSubTickers[item.base]) {
        this.subTicker(item.base, item.counter, true);
      }
      const decimal = decimals[item.spot_name.split("/")[0]]
        ? decimals[item.spot_name.split("/")[0]].price
        : 4;
      const tickerAgo = changeList.filter(
        e => e.base === item.base && e.counter === item.counter
      )[0];
      const ticker = tickersList.filter(
        e => e.base === item.base && e.counter === item.counter
      )[0];
      const priceAgo = tickerAgo ? tickerAgo.price : 0;
      const price = (ticker ? (ticker.last || 0) / 10000 : priceAgo).toFixed(
        decimal
      );
      if (item.name === "FLEX/USDT") {
        flex = {
          base: item.base,
          counter: item.counter,
          title: item.name,
          name: item.name,
          priceAgo,
          decimal,
          price: price,
          change: ((+price - priceAgo) / priceAgo) * 100
        };
        return;
      }

      list.push({
        base: item.base,
        counter: item.counter,
        title: item.name,
        name: item.tenor
          ? item.spot_name +
            " " +
            contractCode[item.tenor.substring(0, 1)] +
            " " +
            item.tenor.substring(1)
          : item.spot_name,
        priceAgo,
        decimal,
        price: price,
        change: priceAgo === 0 ? 0 : ((+price - priceAgo) / priceAgo) * 100
      });
    });
    const currentMonth = getCurrentMonth();
    list.sort(a => (a.title.indexOf(`XBT${currentMonth}`) !== -1 ? -1 : 1));
    // put FLEX/USDT after XBT/USDT contract
    if (flex) {
      list.splice(1, 0, flex);
    }
    this.allAssets = [...list];
    this.isShow = true;
    this.props.setPositionList(list);
    this.props.getBucketList(list);
  };

  showMarketList = () => {
    this.setState({
      showMore: true
    });
  };

  hideMarketList = () => {
    this.setState({
      showMore: false
    });
  };

  selectMarket = (market: IPositionListItem) => {
    const { currentSymbol } = this.props;
    if (
      currentSymbol.base.id !== market.base ||
      currentSymbol.quote.id !== market.counter
    ) {
      history.push(
        `${PUBLIC_URL}/markets/` + market.title.replace("/", "_").toLowerCase()
      );
      this.props.setSelectedMarket(market);
    }
    this.setState({
      showMore: false
    });
  };

  initScroll = () => {
    const { isMobile } = this.props;
    const scrollbarOptions = {
      fade: false,
      interactive: true
    };

    if (!isMobile && !this.scroll) {
      const wrapper = document.querySelector(".wrapper");
      if (wrapper) {
        this.scroll = new BScroll(wrapper, {
          scrollX: true, //开启横向滚动
          click: true, // better-scroll 默认会阻止浏览器的原生 click 事件
          scrollY: false, //关闭竖向滚动
          scrollbar: scrollbarOptions
        });
      }
    }
    if (isMobile && this.scroll) {
      this.scroll = null;
    }
  };

  subTicker = (base: number, counter: number, watch: boolean) => {
    const data = {
      tag: 7,
      method: "WatchTicker",
      base: base,
      counter: counter,
      watch: watch
    };
    coinflexWebsocket.send(data, () => {
      this.hasSubTickers[base] = watch;
    });
  };

  updateCurrentPrice = () => {
    const { lastTicker } = this.props;
    if (this.allAssets.length > 0) {
      this.allAssets.map(item => {
        const assetId = item.base;
        if (lastTicker.base !== assetId) {
          return item;
        } else {
          if (lastTicker.last) {
            item.price = (lastTicker.last / 10000).toFixed(lastTicker.decimal);
            item.change =
              item.priceAgo === 0
                ? 0
                : ((lastTicker.last / 10000 - item.priceAgo) / item.priceAgo) *
                  100;
          }
        }
        return item;
      });
      this.setState({ isUp: true });
    }
  };

  componentDidUpdate(prevProps: TProps) {
    if (this.props.lastTicker !== prevProps.lastTicker) {
      this.updateCurrentPrice();
    }
  }

  shouldComponentUpdate(
    nextProps: Readonly<TProps>,
    nextState: Readonly<IState>
  ): boolean {
    if (
      !isEqual(this.props.isMobile, nextProps.isMobile) ||
      !isEqual(this.props.currentSymbol, nextProps.currentSymbol) ||
      !isEqual(this.props.changeList, nextProps.changeList) ||
      !isEqual(this.props.marketList, nextProps.marketList) ||
      !isEqual(this.props.tickersList, nextProps.tickersList) ||
      !isEqual(this.props.bucketList, nextProps.bucketList) ||
      !isEqual(this.state.isUp, nextState.isUp) ||
      !isEqual(this.state.showMore, nextState.showMore)
    ) {
      return true;
    }
    return false;
  }

  getVolume(item: IPositionListItem) {
    const { bucketList } = this.props;

    let volume = 0;
    const bucketListHasLength = !!bucketList.length;
    if (bucketListHasLength) {
      const matchingBucketList = bucketList.find(bucket =>
        _isEqual(bucket.assetPair, {
          base: item.base,
          counter: item.counter
        })
      );
      if (matchingBucketList) {
        volume = matchingBucketList.ohlcList.reduce(
          (mem, ohlc) => mem + ohlc.volume * ohlc.close,
          0
        );
      }
    }
    return Math.round(volume).toLocaleString();
  }

  render() {
    const { isUp, showMore } = this.state;
    const {
      isMobile,
      currentSymbol,
      changeList,
      marketList,
      tickersList
    } = this.props;
    this.initScroll();
    changeList.length &&
      tickersList.length &&
      marketList.length &&
      this.allAssets.length === 0 &&
      this.getCurrentMonthTicker();

    return (
      <div className="market-wrapper">
        {!isMobile ? (
          <div className="market-container flex flex-content-between">
            <div
              className={clsx(
                isUp && "slider-wrap",
                !isUp && "slider-wrap-short",
                "wrapper"
              )}
            >
              <div
                className="swiper-scroll"
                style={{ width: `${this.allAssets.length * 206}px` }}
              >
                {this.allAssets.map((item, key) => {
                  return (
                    <div
                      className={clsx(
                        isUp && "market-card",
                        !isUp && "market-card-short",
                        currentSymbol.symbolTitle &&
                          currentSymbol.base.id === item.base &&
                          "current-market"
                      )}
                      key={key}
                      onClick={() => this.selectMarket(item)}
                    >
                      <div
                        className={clsx(
                          item.change < 0 && "card-info down",
                          item.change >= 0 && "card-info up"
                        )}
                      >
                        <h1>{item.name}</h1>
                        {isUp ? (
                          <p>
                            {(item.change > 0 ? "+" : "") +
                              item.change.toFixed(2)}
                            % Today
                          </p>
                        ) : (
                          ""
                        )}
                        <span>{item.price}</span>
                        <div className={"volume-info"}>
                          <FormattedMessage
                            defaultMessage="24H Vol $T {volume}"
                            values={{
                              volume: this.getVolume(item)
                            }}
                          />
                        </div>
                      </div>
                      {isUp ? (
                        <Chart
                          passValue={this.state.isUp}
                          asset={item}
                          color={item.change < 0 ? "red" : "green"}
                        />
                      ) : (
                        ""
                      )}
                    </div>
                  );
                })}
              </div>
            </div>
            <div
              className="flex flex-row flex-align-center thumbnail"
              onClick={() => this.changeDirection()}
            >
              {isUp ? <Icon type="up" /> : <Icon type="down" />}
            </div>
          </div>
        ) : (
          <div className="flex flex-content-between market-container market-container-mobile">
            {currentSymbol.symbolTitle &&
              this.allAssets
                .filter(
                  e =>
                    e.base === currentSymbol.base.id &&
                    e.counter === currentSymbol.quote.id
                )
                .map((item, key) => {
                  return (
                    <div
                      className="market-card"
                      key={key}
                      onClick={this.showMarketList}
                    >
                      <div
                        className={clsx(
                          "flex",
                          "flex-content-between",
                          "flex-align-center",
                          item.change < 0 && "card-info down",
                          item.change >= 0 && "card-info up"
                        )}
                      >
                        <div>
                          <h1>{item.name}</h1>
                          <p>
                            <FormattedMessage
                              defaultMessage="{change}% Today"
                              values={{
                                change:
                                  (item.change > 0 ? "+" : "") +
                                  item.change.toFixed(2)
                              }}
                            />
                          </p>
                        </div>
                        <div>
                          <h1>{item.price}</h1>
                          <p>
                            <FormattedMessage
                              defaultMessage="24H Vol $T {volume}"
                              values={{
                                volume: this.getVolume(item)
                              }}
                            />
                          </p>
                        </div>
                      </div>
                      <Chart
                        passValue={this.state.isUp}
                        asset={item}
                        color={item.change < 0 ? "red" : "green"}
                      />
                    </div>
                  );
                })}
            {currentSymbol.symbolTitle && (
              <Button
                type="link"
                className="more"
                href={getCurrentUrl("show")}
                target="_blank"
              >
                <i className="icon-menu" />
              </Button>
            )}
            <Modal
              visible={showMore}
              closable={false}
              popup={false}
              animationType="slide"
              title={
                <h1 className="modal-head">
                  <Button type="link" onClick={this.hideMarketList}>
                    <i className="icon-left" />
                  </Button>
                  <FormattedMessage defaultMessage="Select contract" />
                </h1>
              }
              wrapClassName="marketList"
            >
              <div className="market-list-scroll">
                <div className="market-list-wrapper">
                  {this.allAssets.map((item, key) => {
                    return (
                      <div
                        className={
                          currentSymbol.symbolTitle &&
                          currentSymbol.base.id === item.base
                            ? "market-card current-market"
                            : "market-card"
                        }
                        key={key}
                        onClick={() => this.selectMarket(item)}
                      >
                        <div
                          className={
                            item.change < 0
                              ? "card-info flex flex-content-between flex-align-center down"
                              : "card-info flex flex-content-between flex-align-center up"
                          }
                        >
                          <div>
                            <h1>{item.name}</h1>
                            <p>
                              <FormattedMessage
                                defaultMessage="{change}% Today"
                                values={{
                                  change:
                                    (item.change > 0 ? "+" : "") +
                                    item.change.toFixed(2)
                                }}
                              />
                            </p>
                          </div>
                          <span>{item.price}</span>
                        </div>
                        <Chart
                          passValue={this.state.isUp}
                          asset={item}
                          color={item.change < 0 ? "red" : "green"}
                        />
                      </div>
                    );
                  })}
                </div>
              </div>
            </Modal>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    currentSymbol: state.currentSymbol,
    isMobile: state.isMobile,
    changeList: state.changeList,
    tickersList: state.tickersList,
    marketList: state.marketList,
    lastTicker: state.lastTicker,
    bucketList: state.bucketList
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setPositionList(data: IPositionListItem[]) {
      dispatch(setPositionList(data));
    },
    setSelectedMarket(data: IPositionListItem | IMarket) {
      dispatch(setSelectedMarket(data));
    },
    getBucketList(data: IPositionListItem[]) {
      dispatch(getBucketList(data));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Market);
