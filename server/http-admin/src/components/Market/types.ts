export type TChartsSize = number | null | undefined | "auto";

export type TChartsTheme = string | object;

export type TChartsElement = HTMLDivElement | HTMLCanvasElement;

export interface IChartsOpts {
  devicePixelRatio: number;
  renderer: "canvas" | "svg";
}

export type TChartsInstance = any;

export interface IECharts {
  init: (
    element: TChartsElement,
    theme?: TChartsTheme,
    opts?: IChartsOpts
  ) => void;
  getInstanceByDom: (element: TChartsElement) => any;
  dispose: (element: TChartsElement) => void;
}

export type TChartsEventHandler = (
  param: string,
  inctance: TChartsInstance
) => void;

export interface IChartsEvents {
  [name: string]: TChartsEventHandler;
}

export interface IChartProps {
  option: object;
  echarts?: IECharts;
  notMerge?: boolean;
  lazyUpdate?: boolean;
  style: object;
  className: string;
  theme?: string | object;
  onChartReady?: (instance: TChartsInstance) => void;
  showLoading?: boolean;
  loadingOption?: object;
  onEvents?: IChartsEvents;
  opts?: IChartsOpts;
  width?: TChartsSize;
  height?: TChartsSize;
  shouldSetOption?: (prevProps: IChartProps, props: IChartProps) => boolean;
}
