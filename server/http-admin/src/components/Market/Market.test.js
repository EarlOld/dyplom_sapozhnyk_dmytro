import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import Market from "./Market";

describe.skip("Market", () => {
  it("Should match snapshot on Desktop", () => {
    const { container } = renderWithMockRedux(<Market />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should match snapshot on Mobile", () => {
    const { container } = renderWithMockRedux(<Market />, {
      initialState: { isMobile: true }
    });
    expect(container.firstChild).toMatchSnapshot();
  });
});
