import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Login from "./Login";

export default {
  title: "Login Version two",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <Login />;

Default.story = {
  name: "default"
};
