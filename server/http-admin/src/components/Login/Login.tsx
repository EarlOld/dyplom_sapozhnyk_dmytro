import React, { useEffect, useMemo, useState } from "react";
import {
  Button,
  FormGroup,
  InputGroup,
  Card,
  H2,
  Tabs,
  Tab,
  Intent
} from "@blueprintjs/core";
import { FormComponentProps } from "antd/es/form";
import { connect } from "react-redux";
import {
  setAuthToken,
  setLogin,
  setUser,
  setWebsocketAuthParams
} from "store/actions";
import { doTFA, signIn } from "services/http/http";
import _isEmpty from "lodash/isEmpty";
import _get from "lodash/get";
import _flowRight from "lodash/flowRight";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import { AxiosResponse } from "axios";
import { TopNavBar } from "../Dashboard/TopNavBar";
import { isValidEmail, isValidPassword, isValidTwoFactorToken } from "./utils";
import ControlsGroup from "../Dashboard/ControlsGroup/ControlsGroup";
import { WEB_APP_URL, WEBAPP_ROUTES } from "../../config";
import { pushNotification } from "store/actions";
import NotificationViewer from "../Dashboard/NotificationViewer";
import clsx from "clsx";
import { useHistory } from "react-router-dom";
import "./styles/index.scss";
import { Footer } from "../Dashboard/Footer";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TProps = TStateProps &
  TDispatchProps &
  FormComponentProps &
  WrappedComponentProps;

interface IValidationState {
  email: boolean;
  password: boolean;
  twoFactorAuthentication: boolean;
}

const Login = ({
  intl,
  isMobile,
  isLogin,
  setAuthToken,
  authToken,
  setWebsocketAuthParams,
  setLogin,
  setUser,
  pushNotification
}: TProps) => {
  const history = useHistory();
  const [step, setStep] = useState<1 | 2>(1);
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [tfa, setTfa] = useState<string>("");
  const [launchType, setLaunchType] = useState<string>("dashboard");
  const [loading, setLoading] = useState<boolean>(false);
  const [blurred, setBlurred] = useState<IValidationState>({
    email: false,
    twoFactorAuthentication: false,
    password: false
  });
  const hideLabel = useMemo(() => {
    return {
      twoFactorAuthentication:
        !blurred.twoFactorAuthentication ||
        (blurred.twoFactorAuthentication && isValidTwoFactorToken(tfa)),
      email: !blurred.email || (blurred.email && isValidEmail(email)),
      password:
        !blurred.password || (blurred.password && isValidPassword(password))
    };
  }, [blurred, email, password, tfa]);

  const ready: boolean = useMemo(
    () => isValidEmail(email) && isValidPassword(password),
    [email, password]
  );

  const tfaReady: boolean = useMemo(() => isValidTwoFactorToken(tfa), [tfa]);

  const changeEmailHandler = (e: React.ChangeEvent<HTMLInputElement>) =>
    setEmail(e.target.value);
  const changePasswordHandler = (e: React.ChangeEvent<HTMLInputElement>) =>
    setPassword(e.target.value);
  const changeTFAHandler = (e: React.ChangeEvent<HTMLInputElement>) =>
    setTfa(e.target.value);

  const launchTypeChangeHandler = (val: string): void => {
    if (launchType !== val) {
      setLaunchType(val);
    }
  };

  const failHandler = (e: AxiosResponse) => {
    const msg: string = _get(
      e,
      "data.message",
      _get(
        e,
        "data.errors[0].message",
        intl.formatMessage({
          defaultMessage: "Invalid credentials",
          id: "fail-2fa"
        })
      )
    );

    pushNotification({
      type: "DANGER",
      title: intl.formatMessage({ defaultMessage: "Authentication Failure" }),
      message: msg
    });
    setLoading(false);
  };

  const loginHandler = (formEvent?: React.FormEvent<HTMLFormElement>) => {
    const formWasSubmitted = !!formEvent;
    const automaticLoginAttemptOnMount = !formWasSubmitted;
    const isCurrentlyLoggedOut = !isLogin;
    const hasPasswordInState = !_isEmpty(password);
    const hasUserNameInState = !_isEmpty(email);
    const hasCredentialsInState = hasPasswordInState && hasUserNameInState;

    const _loginHandler = (loginData: ILoginRequest) => {
      const signInDataV2 = {
        email: loginData["user[email]"],
        password: loginData["user[password]"]
      };

      signIn(signInDataV2)
        .then(res => {
          setAuthToken(res);
          if (isCurrentlyLoggedOut) setStep(2);
          setLoading(false);
        })
        .catch(failHandler);
    };

    if (
      automaticLoginAttemptOnMount &&
      isCurrentlyLoggedOut &&
      hasCredentialsInState
    ) {
      _loginHandler({
        "user[email]": email,
        "user[password]": password
      });
    } else if (formWasSubmitted) {
      formEvent!.preventDefault();
      setLoading(false);
      _loginHandler({
        "user[email]": email,
        "user[password]": password
      });
    }
  };

  const clearForm = () => {
    setEmail("");
    setPassword("");
    setTfa("");
    setLoading(false);
  };

  const twoFactorAuthenticationHandler = (
    e: React.FormEvent<HTMLFormElement>
  ) => {
    e.preventDefault();

    setLoading(true);
    const valuesV2: I2FARequestV2 = { tfa_code: tfa };
    doTFA(valuesV2, authToken.token)
      .then(res => {
        const { core_id, api_key, priv_key } = res;
        setWebsocketAuthParams({ core_id, api_key, priv_key });
        setUser(res);
        setLogin(true);
        clearForm();
        setLoading(false);
        history.push("/" + launchType);
      })
      .catch(failHandler);
  };

  useEffect(loginHandler, []);

  return (
    <div className={"authentication-page-wrapper"}>
      <TopNavBar logoutHandler={null} />
      <Card className={"bp3-dark cf-login"}>
        {step === 1 ? (
          <>
            <H2>Log In</H2>
            <form onSubmit={loginHandler}>
              <FormGroup
                helperText={intl.formatMessage({
                  defaultMessage: "Please enter your Email"
                })}
                label={intl.formatMessage({ defaultMessage: "Email" })}
                labelFor="email-input"
                className={clsx("email-form-group", hideLabel.email && "hide")}
              >
                <InputGroup
                  id="email-input"
                  type={"text"}
                  onBlur={() => setBlurred({ ...blurred, email: true })}
                  large={!isMobile}
                  value={email}
                  onChange={changeEmailHandler}
                />
              </FormGroup>
              <FormGroup
                helperText={intl.formatMessage({
                  defaultMessage: "Please enter your Password"
                })}
                label={intl.formatMessage({ defaultMessage: "Password" })}
                labelFor="password-input"
                className={clsx(
                  "password-form-group",
                  hideLabel.password && "hide"
                )}
              >
                <InputGroup
                  id="password-input"
                  type={"password"}
                  onBlur={() => setBlurred({ ...blurred, password: true })}
                  large={!isMobile}
                  value={password}
                  onChange={changePasswordHandler}
                />
              </FormGroup>

              <label className={"bp3-label control-label"}>
                {intl.formatMessage({
                  defaultMessage: "Launch"
                })}
              </label>
              <ControlsGroup large={!isMobile}>
                <Tabs
                  animate={false}
                  onChange={launchTypeChangeHandler}
                  selectedTabId={launchType}
                >
                  <Tab
                    id={"dashboard"}
                    children={
                      <div className={"titles"}>
                        <span className={"launch-title"}>
                          {intl.formatMessage({ defaultMessage: "Dashboard" })}
                        </span>
                        <span className={"launch-subtitle"}>
                          {intl.formatMessage({
                            defaultMessage: "Admin Area"
                          })}
                        </span>
                      </div>
                    }
                  />
                  <Tab
                    id={"markets"}
                    children={
                      <div className={"titles"}>
                        <span className={"launch-title"}>
                          {intl.formatMessage({ defaultMessage: "Exchange" })}
                        </span>
                        <span className={"launch-subtitle"}>
                          {intl.formatMessage({
                            defaultMessage: "iot Platform"
                          })}
                        </span>
                      </div>
                    }
                  />
                </Tabs>
              </ControlsGroup>

              <Button
                type={"submit"}
                disabled={!ready}
                large
                loading={loading}
                fill
                intent={Intent.PRIMARY}
              >
                <FormattedMessage defaultMessage={"Log In"} />
              </Button>

              <div className={"login-page-links"}>
                <span>
                  <a href={WEB_APP_URL + WEBAPP_ROUTES.FORGOTTEN_PASSWORD}>
                    <FormattedMessage defaultMessage={"Forgot password?"} />
                  </a>
                </span>
                <span className={"missing-account"}>
                  <FormattedMessage defaultMessage={"Don't have an account?"} />{" "}
                  <a href={WEB_APP_URL + WEBAPP_ROUTES.SIGN_UP}>
                    <FormattedMessage defaultMessage={"Register"} />
                  </a>
                </span>
              </div>
            </form>
          </>
        ) : (
          <>
            <H2>Log In</H2>
            <form onSubmit={twoFactorAuthenticationHandler}>
              <FormGroup
                helperText={intl.formatMessage({
                  defaultMessage:
                    "Please enter your Two Factor Authentication Token"
                })}
                label={intl.formatMessage({
                  defaultMessage: "Two Factor Authentication Token"
                })}
                labelFor="two-factor-input"
                className={clsx(
                  "two-factor-form-group",
                  hideLabel.twoFactorAuthentication && "hide"
                )}
              >
                <InputGroup
                  id="two-factor-input"
                  type={"text"}
                  onBlur={() =>
                    setBlurred({ ...blurred, twoFactorAuthentication: true })
                  }
                  large
                  value={tfa}
                  onChange={changeTFAHandler}
                />
              </FormGroup>

              <Button
                type={"submit"}
                className={"two-factor-submit"}
                disabled={!tfaReady}
                large
                loading={loading}
                fill
                intent={Intent.PRIMARY}
              >
                <FormattedMessage defaultMessage={"Enter"} />
              </Button>

              <div className={"login-page-links"}>
                <Button
                  className={"two-factor-back"}
                  large
                  loading={loading}
                  onClick={() => setStep(1)}
                  icon="arrow-left"
                />
              </div>
            </form>
          </>
        )}
      </Card>
      <NotificationViewer />
      <section className={"footer-wrapper"}>
        <Footer />
      </section>
    </div>
  );
};

const mapStateToProps = (state: IAppState) => {
  return {
    isMobile: state.isMobile,
    authToken: state.authToken,
    isLogin: state.isLogin
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    pushNotification(data: INotification) {
      dispatch(pushNotification(data));
    },
    setLogin(data: boolean) {
      dispatch(setLogin(data));
    },
    setWebsocketAuthParams(data: IUserAuthentificateParams) {
      dispatch(setWebsocketAuthParams(data));
    },
    setAuthToken(data: IAuthToken) {
      dispatch(setAuthToken(data));
    },
    setUser(data: any) {
      dispatch(setUser(data));
    }
  };
};
export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(Login);
