import React from "react";
import { action } from "@storybook/addon-actions";
import { wrapWithMockRedux } from "utils/testUtils";
import OrderBook from "./OrderBook";

export default {
  title: "OrderBook",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <OrderBook openTVWidget={action("Open TV Widget!")} />
);

Default.story = {
  name: "default"
};
