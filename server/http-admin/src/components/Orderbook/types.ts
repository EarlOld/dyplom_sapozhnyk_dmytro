export interface IOrderBookListOwnProps {
  baseTicket: {
    scale: number;
  };
  quoteTicket: {
    scale: number;
  };
  decimal: number;
  isMobile: boolean;
  symbolTitle?: string;
  setAmountFromOrderBook: (data: number) => void;
  setTopTabs: (data: string) => void;
}

export interface IOrderBookListItem {
  price: number;
  size: number;
  total: number;
}

export interface IOrderBookListState {
  list: IOrderBookListItem[];
}
