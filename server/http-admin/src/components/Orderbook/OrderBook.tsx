import React, { Component } from "react";
import AskList from "./AskList";
import BidList from "./BidList";
import "./style/index.scss";
import { connect } from "react-redux";
import { decimals } from "utils/reference";
import {
  setOrderBookList,
  setAmountFromOrderBook,
  setTopTabs
} from "store/actions";
import { Button, Icon } from "antd";
import clsx from "clsx";
import { ReactComponent as TvSvg } from "assets/icons/tv.svg";
import { FormattedMessage, injectIntl } from "react-intl";
import _flowRight from "lodash/flowRight";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
interface IOwnProps {
  openTVWidget?: () => void;
}

type TProps = TStateProps & TDispatchProps & IOwnProps;

export class OrderBook extends Component<TProps, {}> {
  private lastPrice = "-";
  private lastChange = 1;
  private quoteName = "";
  private baseName = "";
  private decimal = 4;

  getTickerInfo = (data: ILastTradeData) => {
    const { currentSymbol } = this.props;
    const base = currentSymbol.base;
    const quote = currentSymbol.quote;
    this.baseName = base.spot_name ? base.spot_name : base.name;
    this.quoteName = quote.spot_name ? quote.spot_name : quote.name;
    this.decimal =
      currentSymbol.base.spot_name && decimals[currentSymbol.base.spot_name]
        ? decimals[currentSymbol.base.spot_name].price
        : this.decimal;

    this.lastPrice =
      data && data.last
        ? (data.last / quote.scale).toFixed(this.decimal)
        : this.lastPrice;
    this.lastPrice =
      data && data.price
        ? (data.price / quote.scale).toFixed(this.decimal)
        : this.lastPrice;
    if (!data.last && !data.price) {
      this.lastPrice = "-";
    }
  };

  render() {
    const {
      lastTrade,
      currentSymbol,
      openTVWidget,
      setTopTabs,
      isMobile,
      setAmountFromOrderBook
    } = this.props;
    lastTrade[currentSymbol.base.id] &&
      currentSymbol.symbolTitle &&
      this.getTickerInfo(lastTrade[currentSymbol.base.id] as ILastTradeData);

    return (
      <div className="order-book">
        <div className="header">
          <FormattedMessage defaultMessage="Order Book" />
          {!!openTVWidget && (
            <Button
              shape="circle"
              type="ghost"
              size="large"
              onClick={openTVWidget}
              data-testid="tv-widget"
            >
              <Icon component={TvSvg as React.FC} />
            </Button>
          )}
        </div>
        <ul className="flex list-head">
          <li>
            <FormattedMessage defaultMessage="Price" />{" "}
            <span>{this.quoteName}</span>
          </li>
          <li>
            <FormattedMessage defaultMessage="Amount" />{" "}
            <span>{this.baseName}</span>
          </li>
          <li>
            <FormattedMessage defaultMessage="Total" />{" "}
            <span>{this.baseName}</span>
          </li>
        </ul>
        <div className="order-book-wrapper" id="order-book-list">
          <div className="order-book-position">
            <AskList
              setAmountFromOrderBook={setAmountFromOrderBook}
              setTopTabs={setTopTabs}
              isMobile={isMobile}
              baseTicket={currentSymbol.base}
              symbolTitle={currentSymbol?.symbolTitle}
              quoteTicket={currentSymbol.quote}
              decimal={this.decimal}
            />
            <div
              className={clsx(
                "flex",
                "flex-content-center",
                "flex-align-center",
                this.lastChange === 1 && "last-price up",
                this.lastChange !== 1 && "last-price down"
              )}
            >
              <i className="direction" />
              <span className="price">{this.lastPrice}</span>
              <label>
                <FormattedMessage defaultMessage="Last Price" />
              </label>
            </div>
            <BidList
              setAmountFromOrderBook={setAmountFromOrderBook}
              isMobile={isMobile}
              setTopTabs={setTopTabs}
              symbolTitle={currentSymbol?.symbolTitle}
              baseTicket={currentSymbol.base}
              quoteTicket={currentSymbol.quote}
              decimal={this.decimal}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    currentSymbol: state.currentSymbol,
    orderBookList: state.orderBookList,
    lastTrade: state.lastTrade,
    tickersList: state.tickersList,
    isMobile: state.isMobile
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setOrderBookList(data: IOrderBookList) {
      dispatch(setOrderBookList(data));
    },
    setAmountFromOrderBook(data: number) {
      dispatch(setAmountFromOrderBook(data));
    },
    setTopTabs(data: string) {
      dispatch(setTopTabs(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(OrderBook);
