import React, { useCallback, useEffect, useRef, useState } from "react";
import Calc from "utils/math.calc";
import { useSelector } from "react-redux";
import "./style/index.scss";
import {
  IOrderBookListItem,
  IOrderBookListOwnProps,
  IOrderBookListState
} from "./types";
import _throttle from "lodash/throttle";

type TProps = IOrderBookListOwnProps;

const BidList = React.memo((props: TProps) => {
  const [list, setList] = useState<IOrderBookListState | any>([]);
  const bidData = useSelector((state: IPublicState) => state.bidData);
  const bidList = useRef<HTMLUListElement>(null);

  useEffect(() => {
    if (props.symbolTitle && bidList.current) {
      bidList.current.scrollTop = 0;
    }
  }, [props.symbolTitle]);

  const updateList = useCallback(
    _throttle((newList: any) => {
      setList(newList);
    }, 400),
    [setList]
  );

  useEffect(() => {
    const data: TDepthData = JSON.parse(JSON.stringify(bidData));
    if (data) {
      const baseScale = props.baseTicket.scale;
      const quoteScale = props.quoteTicket.scale;
      data.sort((a, b) => b[0] - a[0]);
      const list: IOrderBookListItem[] = [];
      let total = 0;
      data.forEach((item, index) => {
        if (index < 15)
          list.push({
            price: item[0] / quoteScale,
            size: item[1] / baseScale,
            total: total += item[1] / baseScale
          });
      });

      updateList(list);
    }
  }, [bidData, props.baseTicket.scale, props.quoteTicket.scale, updateList]);

  const handleItemClick = useCallback(
    (price: any) => {
      if (props.isMobile) {
        props.setTopTabs("TRADE");
        setTimeout(() => {
          props.setAmountFromOrderBook(price);
        }, 0);
      } else {
        props.setAmountFromOrderBook(price);
      }
    },
    [props]
  );

  return (
    <ul ref={bidList} className="order-book-list bid-list">
      {list.map((item: IOrderBookListItem, index: number) => (
        <li
          className="list-item flex"
          key={index}
          onClick={() => handleItemClick(item.price)}
        >
          <span className="price">{item.price.toFixed(props.decimal)}</span>
          <span className="amount">{Calc.toThousands(item.size)}</span>
          <span className="total">{item.total.toFixed(props.decimal)}</span>
          <label
            className="depth"
            style={{
              width: (item.total * 100) / list[list.length - 1].total + "%"
            }}
          />
        </li>
      ))}
    </ul>
  );
});

export default BidList;
