import React, { useState, useEffect, useRef, useCallback } from "react";
import Calc from "utils/math.calc";
import { useSelector } from "react-redux";
import "./style/index.scss";
import {
  IOrderBookListOwnProps,
  IOrderBookListState,
  IOrderBookListItem
} from "./types";
import _throttle from "lodash/throttle";

type TProps = IOrderBookListOwnProps;

const AskList = (props: TProps) => {
  const [list, setList] = useState<IOrderBookListState | any>([]);
  const askList = useRef<HTMLUListElement>(null);
  const askData = useSelector((state: IPublicState) => state.askData);

  const scrollHeight = askList.current?.scrollHeight;

  useEffect(() => {
    setTimeout(() => {
      if (askList && askList.current && askList.current.scrollHeight) {
        askList.current.scrollTop = askList.current.scrollHeight;
      }
    }, 100);
  }, [scrollHeight, props.symbolTitle]);

  const updateList = useCallback(
    _throttle((newList: any) => {
      setList(newList);
    }, 400),
    [setList]
  );

  useEffect(() => {
    const data: TDepthData = JSON.parse(JSON.stringify(askData));
    if (data) {
      const baseScale = props.baseTicket.scale;
      const quoteScale = props.quoteTicket.scale;

      data.sort((a, b) => a[0] - b[0]);
      const list: IOrderBookListItem[] = [];
      let total = 0;
      data.forEach((item: any, index: number) => {
        if (index < 15)
          list.push({
            price: item[0] / quoteScale,
            size: item[1] / baseScale,
            total: total += item[1] / baseScale
          });
      });
      list.sort((a, b) => b.price - a.price);
      updateList(list);
    }
  }, [
    askData,
    props.baseTicket.scale,
    props.quoteTicket.scale,
    updateList,
    props.symbolTitle
  ]);

  const handleItemClick = useCallback(
    (price: any) => {
      if (props.isMobile) {
        props.setTopTabs("TRADE");
        setTimeout(() => {
          props.setAmountFromOrderBook(price);
        }, 0);
      } else {
        props.setAmountFromOrderBook(price);
      }
    },
    [props]
  );

  return (
    <ul ref={askList} className="order-book-list ask-list">
      {list.map((item: IOrderBookListItem, index: number) => (
        <li
          className="list-item flex"
          key={index}
          onClick={() => handleItemClick(-item.price)}
        >
          <span className="price">{item.price.toFixed(props.decimal)}</span>
          <span className="amount">{Calc.toThousands(item.size)}</span>
          <span className="total">{item.total.toFixed(props.decimal)}</span>
          <label
            className="depth"
            style={{ width: (item.total * 100) / list[0].total + "%" }}
          />
        </li>
      ))}
    </ul>
  );
};

export default AskList;
