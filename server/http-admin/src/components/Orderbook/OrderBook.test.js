import React from "react";
import { fireEvent } from "@testing-library/react";
import { renderWithMockRedux } from "utils/testUtils";
import OrderBook from "./OrderBook";

describe("OrderBook", () => {
  const handleOpenTVWidget = jest.fn();

  const TestOrderBook = () => <OrderBook openTVWidget={handleOpenTVWidget} />;

  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<TestOrderBook />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should fire callback when opening TV Widget", () => {
    const { getByTestId } = renderWithMockRedux(<TestOrderBook />);
    const tvWigetButton = getByTestId("tv-widget");
    expect(tvWigetButton).toBeInTheDocument();
    fireEvent.click(tvWigetButton);
    expect(handleOpenTVWidget).toHaveBeenCalled();
  });
});
