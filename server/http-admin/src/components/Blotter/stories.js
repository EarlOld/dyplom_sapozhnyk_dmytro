import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Blotter from "./Blotter";

export default {
  title: "Blotter",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <Blotter />;

Default.story = {
  name: "default"
};
