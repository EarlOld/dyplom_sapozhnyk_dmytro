import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import Blotter from "./Blotter";

describe("Blotter tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Blotter />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
