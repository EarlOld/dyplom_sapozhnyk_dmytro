export interface IBalancesProps {
  allBalances: IBalance[];
  assetsList: IAsset[];
  positionList: IPositionListItem[];
  marketList: IMarket[];
}

export interface ITableRow extends IBalance {
  total: number;
  dollarWorth: number;
  id_name: string;
}
