import React from "react";
import Balances from "./Balances";
import { renderWithMockRedux } from "utils/testUtils";

describe("Balances table", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Balances />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should have FLEX/USDT in list of assets", () => {
    const { getByText } = renderWithMockRedux(<Balances />);
    const asset = getByText("FLEX/USDT");
    expect(asset).toBeInTheDocument();
  });
});
