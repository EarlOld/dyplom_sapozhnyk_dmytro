import React, { memo } from "react";
import { connect } from "react-redux";
import _get from "lodash/get";
import Table from "components/Table";
import { IBalancesProps, ITableRow } from "./types";
import history from "utils/history";
import TableWrapper from "./TableWrapper";
import { isUsdAsset } from "utils/isUsdAsset";
import { isSpotAsset } from "utils/isSpotAsset";
import { sorterFunction } from "utils/helpers";
import { injectIntl, WrappedComponentProps } from "react-intl";
import _flowRight from "lodash/flowRight";
import { setSelectedMarket } from "store/actions";
import { PUBLIC_URL } from "config";
const SCALED = 10000;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

export const Balances = memo(
  ({
    allBalances = [],
    assetsList = [],
    positionList = [],
    marketList = [],
    intl,
    setSelectedMarket
  }: IBalancesProps & WrappedComponentProps & TDispatchProps) => {
    const onRowClickHandler = (tableRowData: ITableRow) => {
      const relevantMarket = marketList.find(
        (market: IMarket) => market.base === tableRowData.id
      );
      relevantMarket && snapToSpotMarket(relevantMarket);
    };

    const tickers = positionList.reduce(
      (mem: Partial<IPositionListItem>, position: IPositionListItem) => {
        return { ...mem, [position.base]: position };
      },
      {} as Partial<IPositionListItem>
    );

    const dataSource: ITableRow[] = allBalances
      .filter((balance: IBalance): boolean => {
        const matchingAsset: IAsset | undefined = assetsList.find(
          asset => asset.id === balance.id
        );
        const isSpot = matchingAsset ? isSpotAsset(matchingAsset) : false;
        const nonZeroBalance = !!balance.available || !!balance.reserved;
        return nonZeroBalance && isSpot;
      })
      .map(
        (balance: IBalance): ITableRow => {
          const total = balance.available + balance.reserved;
          const isUsd = isUsdAsset(balance.id);
          const matchingAsset: IAsset | undefined = assetsList.find(
            asset => asset.id === balance.id
          );
          const idName = _get(
            tickers,
            [balance.id, "title"],
            _get(
              matchingAsset,
              "name",
              intl.formatMessage({ defaultMessage: "Unknown Asset" })
            )
          );
          const lastPrice = isUsd
            ? 1
            : _get(tickers, [balance.id, "priceAgo"], 0);
          const dollarWorth = lastPrice * (total / SCALED);
          return {
            ...balance,
            id_name: idName,
            total: total,
            dollarWorth: dollarWorth
          };
        }
      );

    const snapToSpotMarket = (market: IMarket) => {
      if (!isUsdAsset(market.base) || true) {
        history.push(
          PUBLIC_URL +
            "/markets/" +
            market.spot_name.replace("/", "_").toLowerCase()
        );
        setSelectedMarket(market);
      }
    };

    const columns = [
      {
        title: intl.formatMessage({ defaultMessage: "Asset" }),
        dataIndex: "id_name",
        sorter: (a: any, b: any) => sorterFunction(a, b, "id_name")
      },
      {
        title: intl.formatMessage({ defaultMessage: "Available" }),
        dataIndex: "available",
        render: (available: number) =>
          available ? `${available / SCALED}` : 0,
        sorter: (a: any, b: any) => sorterFunction(a, b, "available")
      },
      {
        title: intl.formatMessage({ defaultMessage: "Working Orders" }),
        dataIndex: "reserved",
        render: (reserved: number) => (reserved ? `${reserved / SCALED}` : 0),
        sorter: (a: any, b: any) => sorterFunction(a, b, "reserved")
      },
      {
        title: intl.formatMessage({ defaultMessage: "Total Balance" }),
        dataIndex: "total",
        render: (total: number) => (total ? `${total / SCALED}` : 0),
        sorter: (a: any, b: any) => sorterFunction(a, b, "total")
      },
      {
        title: intl.formatMessage({ defaultMessage: "Value (approx $T)" }),
        dataIndex: "dollarWorth",
        render: (dollarWorth: number) =>
          dollarWorth ? `$T${Math.round(dollarWorth).toLocaleString()}` : `$0`,
        sorter: (a: any, b: any) => sorterFunction(a, b, "dollarWorth"),
        defaultSortOrder: "descend" as "descend"
      }
    ];

    return (
      <TableWrapper>
        <Table
          columns={columns}
          dataSource={dataSource}
          pagination={false}
          rowKey="id"
          sortDirections={["ascend", "descend"]}
          onRow={(tableRow: ITableRow) => ({
            onClick: () => onRowClickHandler(tableRow)
          })}
        />
      </TableWrapper>
    );
  }
);

const mapStateToProps = (state: IBalancesProps) => ({
  allBalances: state.allBalances,
  assetsList: state.assetsList,
  positionList: state.positionList,
  marketList: state.marketList
});

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setSelectedMarket(data: IPositionListItem | IMarket) {
      dispatch(setSelectedMarket(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(Balances);
