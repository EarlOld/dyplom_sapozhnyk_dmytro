import React, { memo, ReactNode } from "react";

export const TableWrapper = memo(({ children }: { children: ReactNode }) => (
  <div className="table" data-testid="balances-table">
    {children}
  </div>
));

export default TableWrapper;
