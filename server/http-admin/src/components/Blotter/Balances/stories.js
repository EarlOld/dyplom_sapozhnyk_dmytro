import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Balances from "./Balances";

export default {
  title: "Balances",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <Balances />;

Default.story = {
  name: "default"
};
