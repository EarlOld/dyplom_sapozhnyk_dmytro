export interface IListItem extends IPosition {
  contract: string;
  takeProfit: number;
  key: string;
  base: number;
  quote: number;
  entryPrice: number;
  side: TAskBidSide;
  available: number;
  currentPrice: number;
  change: number;
  leverage: number;
  pnl: number;
  decimalPrice: number;
  decimalBalance: number;
  liquidationPrice?: number;
}

export interface IContractSection {
  id: number;
  balance: {
    apiTotal: number;
    loanAmount: number;
  };
  initialMargin: number;
  maintenanceMargin: number;
}

export interface IContract {
  markets: Map<
    number,
    {
      base: IContractSection;
      counter: IContractSection;
      liquidationPrice?: number;
      lastPrice?: number;
    }
  >;
  counter: IContractSection | {};
}

export interface IPositionsProps {
  isMobile?: boolean;
  positionList?: IPositionListItem[];
  assetsList?: IAsset[];
  allBalances?: IBalance[];
  borrowOffers?: IBorrowOffer[];
  borrowLoans?: IBorrowLoan[];
  positions?: IPositions;
  borrowCollaterals?: IBorrowCollateral[];
  lastTicker?: ITickerChange;
  marketList?: IMarket[];
  currentSymbol?: ICurrentSymbol;
  historyTrade?: ITrade[];
}
