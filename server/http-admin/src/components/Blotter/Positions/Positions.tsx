import React, { useState, useCallback, useEffect, useMemo } from "react";
import Table from "components/Table";
import history from "utils/history";
import Calc from "utils/math.calc.js";
import _throttle from "lodash/throttle";
import isEqual from "lodash/isEqual";
import _isNumber from "lodash/isNumber";
import { useDispatch, useSelector } from "react-redux";
import { getDataForPositions } from "./selectors";
import { setPanelConfirm, setTopTabs, setSelectedMarket } from "store/actions";
import { FormattedMessage, useIntl } from "react-intl";
import {
  IListItem,
  IContractSection,
  IContract,
  IPositionsProps
} from "./types";
import _has from "lodash/has";
import { PUBLIC_URL } from "config";
import { decimals } from "utils/reference";

const Positions = React.memo(
  (props: IPositionsProps) => {
    const [list, setList] = useState<any>([]);
    const {
      isMobile,
      positionList,
      assetsList,
      allBalances,
      borrowOffers,
      borrowLoans,
      positions,
      borrowCollaterals,
      lastTicker,
      marketList,
      currentSymbol,
      historyTrade
    } = useSelector(getDataForPositions);

    const updateList = useCallback(
      _throttle((newList: any) => {
        setList(newList);
      }, 1000),
      [setList]
    );
    const _updateLiquidationPrices = useCallback((contract: IContract) => {
      const formula1 = (asset: IContractSection) => {
        return (
          asset.balance.loanAmount * (1 + asset.maintenanceMargin) -
          asset.balance.apiTotal
        );
      };

      const formula2 = (asset: IContractSection) => {
        return (
          asset.balance.apiTotal -
          asset.balance.loanAmount * (1 + asset.initialMargin)
        );
      };

      const formula3 = (asset: IContractSection) => {
        const marketPrice: any = contract.markets.has(asset.id)
          ? contract.markets.get(asset.id)!.lastPrice //TODO: typescript: lastPrice is not defined?
          : 1;
        return formula2(asset) * marketPrice;
      };

      const allAssets: IContractSection[] = []; //[contract.counter];
      contract.markets.forEach(market => {
        allAssets.push(market.base);
      });

      const sumFormula3OtherAssets = (
        asset1: IContractSection,
        asset2?: IContractSection //TODO: typescript: argument asset2 not passed!
      ) =>
        allAssets.reduce((prev, asset) => {
          if (asset !== asset1 && asset !== asset2)
            return prev + formula3(asset);
          return prev;
        }, 0);

      contract.markets.forEach(market => {
        if (market.base.balance.loanAmount > 0) {
          market.liquidationPrice =
            sumFormula3OtherAssets(market.base) / formula1(market.base);
        } else if (market.base.balance.apiTotal > 0) {
          const prices = [];
          const priceCounter =
            (formula1(market.counter) -
              sumFormula3OtherAssets(market.counter, market.base)) /
            formula2(market.base);
          prices.push(priceCounter);
          allAssets.forEach(asset => {
            if (
              asset.balance.loanAmount > 0 &&
              asset !== market.counter &&
              asset !== market.base
            ) {
              const price =
                (formula1(asset) - sumFormula3OtherAssets(market.base)) /
                formula3(asset);
              prices.push(price);
            }
          });
          market.liquidationPrice = prices.reduce(
            (prev, price) => Math.max(prev, price),
            0
          );
        } else {
          market.liquidationPrice = 0;
        }
      });
      return contract;
    }, []);

    const genListData = useCallback(
      ({
        positionList,
        allBalances,
        borrowOffers,
        borrowLoans,
        positions,
        historyTrade,
        borrowCollaterals,
        assetsList
      }: {
        positionList: IPositionListItem[];
        assetsList: IAsset[];
        allBalances: IBalance[];
        borrowOffers: IBorrowOffer[];
        borrowLoans: IBorrowLoan[];
        positions: IPositions;
        historyTrade: ITrade[];
        borrowCollaterals: IBorrowCollateral[];
      }) => {
        if (
          positionList.length &&
          allBalances.length &&
          historyTrade.length &&
          borrowOffers &&
          borrowLoans &&
          positions &&
          borrowCollaterals &&
          assetsList
        ) {
          let item: number;
          const _positionList: {
            [id: number]: IListItem;
          } = {};

          let contract: IContract = {
            markets: new Map(),
            counter: {}
          };

          Object.keys(positions).forEach(itemId => {
            item = +itemId;
            const matchingTicker = positionList.find(one => one.base === item);
            const hasDollarCounter =
              matchingTicker &&
              matchingTicker.title.toUpperCase().includes("USDT");
            if (hasDollarCounter) {
              const id = positions[item].asset;
              const loanCounter = borrowLoans.filter(e => e.asset_id === id);
              const offerCounter = borrowOffers.filter(e => e.asset_id === id);
              const balance = allBalances.find(e => e.id === id);
              if (!balance) return;
              let loanAmountCounter = 0;
              if (loanCounter && loanCounter.length > 0) {
                loanCounter.forEach(e => {
                  loanAmountCounter += e.principal;
                });
              }
              contract.counter = {
                id: id,
                balance: {
                  apiTotal: balance ? balance.available : 0,
                  loanAmount: loanAmountCounter / 10000
                },
                initialMargin:
                  offerCounter && offerCounter.length > 0
                    ? offerCounter[0].initial_margin
                    : 0,
                maintenanceMargin:
                  offerCounter && offerCounter.length > 0
                    ? offerCounter[0].maintenance_margin
                    : 0
              }; //TODO: typescript: Why overwrite data?
            }
          });

          Object.keys(positions).forEach(itemId => {
            item = +itemId;
            if (positions[item].position === 0) return;
            const matchingTicker = positionList.find(one => one.base === item);
            if (!matchingTicker) {
              const loan = borrowLoans.filter(
                e => e.asset_id === positions[item].asset
              );
              const offer = borrowOffers.filter(
                e => e.asset_id === positions[item].asset
              );
              let loanAmount = 0;
              if (loan && loan.length > 0) {
                loan.forEach(e => {
                  loanAmount += e.principal;
                });
              }

              const foundBalance = allBalances.filter(
                e => e.id.toString() === itemId
              );

              const foundBalanceTotal =
                (foundBalance.length &&
                  foundBalance[0] &&
                  foundBalance[0].available) ||
                0;

              contract.counter = {
                id: positions[item].asset,
                balance: {
                  apiTotal: foundBalanceTotal,
                  loanAmount: loanAmount / 10000
                },
                initialMargin:
                  offer && offer.length > 0 ? offer[0].initial_margin : 0,
                maintenanceMargin:
                  offer && offer.length > 0 ? offer[0].maintenance_margin : 0
              };
              return;
            }

            const loan = borrowLoans.filter(
              e => e.asset_id === positions[item].asset
            );
            const offer = borrowOffers.filter(
              e => e.asset_id === positions[item].asset
            );
            const collateral = borrowCollaterals.filter(
              e => e.asset_id === positions[item].asset
            );

            let loanAmount = 0;
            if (loan && loan.length > 0) {
              loan.forEach(e => {
                loanAmount += e.principal;
              });
            }
            const collateralTotal =
              collateral && collateral.length > 0 ? collateral[0].total : 0;
            const list = historyTrade.filter(
              list => list.base === positions[item].asset
            );
            const baseAsset =
              decimals[matchingTicker.name.split(" ")[0].split("/")[0]];
            const decimalPrice = baseAsset ? baseAsset.price : 4;
            const decimalBalance = baseAsset ? baseAsset.balance : 4;
            let sumTotal = 0;
            let sumQuantity = 0;
            list.sort((a, b) => a.time - b.time);
            list.forEach(e => {
              if (sumQuantity + e.quantity === 0) {
                sumTotal = 0;
                sumQuantity = 0;
              } else if (sumQuantity * (sumQuantity + e.quantity) < 0) {
                sumQuantity += e.quantity;
                sumTotal = -((sumQuantity * e.price) / 10000);
              } else {
                sumTotal += e.total || 0;
                sumQuantity += e.quantity;
              }
            });

            const entryPrice =
              sumQuantity === 0 ? 0 : Math.abs(sumTotal / sumQuantity);
            const currentPrice = +matchingTicker.price;
            if (list.length) {
              _positionList[item] = Object.assign(positions[item], {
                key: itemId,
                base: list[0].base,
                quote: list[0].counter,
                entryPrice,
                side: (sumQuantity > 0 ? "bid" : "ask") as TAskBidSide,
                contract: matchingTicker.name,
                available: allBalances.filter(
                  e => parseInt("" + e.id) === parseInt("" + item)
                )[0].available,
                currentPrice,
                change:
                  entryPrice === 0
                    ? 0
                    : (+currentPrice / (entryPrice === 0 ? 1 : entryPrice) -
                        1) *
                      100,
                // let liquidationPrice = _positionList[item].entryPrice - _positionList[item].available / sumQuantity;
                // _positionList[item].liquidationPrice = liquidationPrice < 0 ? 0 : liquidationPrice;
                leverage:
                  collateralTotal === 0
                    ? 0
                    : +(loanAmount / collateralTotal).toFixed(decimalPrice),
                takeProfit: 0,
                pnl:
                  ((+currentPrice - entryPrice) * positions[item].position) /
                  10000,
                decimalPrice,
                decimalBalance
              });
            }

            // contract.markets = new Map();
            contract.markets.set(item, {
              base: {
                id: positions[item].asset,
                balance: {
                  apiTotal: allBalances.filter(
                    e => parseInt("" + e.id) === parseInt("" + item)
                  )[0].available,
                  loanAmount: loanAmount / 10000
                },
                initialMargin:
                  offer && offer.length > 0 ? offer[0].initial_margin : 0,
                maintenanceMargin:
                  offer && offer.length > 0 ? offer[0].maintenance_margin : 0
              },
              counter: contract.counter as IContractSection
            });
          });

          contract = _updateLiquidationPrices(contract);
          let _positions: IListItem[] = [];
          Object.keys(_positionList).forEach(itemId => {
            item = +itemId;

            const { liquidationPrice } = contract.markets.get(item)!;

            _positionList[item].liquidationPrice =
              liquidationPrice && liquidationPrice > 0 ? liquidationPrice : 0;
            _positions.push(_positionList[item]);
          });

          _positions = _positions.filter(pos => {
            const matchingAsset = assetsList.find(
              asset => asset.id === pos.asset
            );
            // Is a futures asset if matchingAsset has a spot id
            return _has(matchingAsset, "spot_id");
          });

          updateList(_positions);
        } else {
          updateList([]);
        }
      },
      [updateList, _updateLiquidationPrices]
    );

    const dispatch = useDispatch();

    const setPanelConfirmDispatched = (data: IPanelConfirm) => {
      dispatch(setPanelConfirm(data));
    };

    const setTopTabsDispatched = (data: TTopTabType) => {
      dispatch(setTopTabs(data));
    };

    const setSelectedMarketDispatched = (data: IPositionListItem | IMarket) => {
      dispatch(setSelectedMarket(data));
    };

    const intl = useIntl();

    const isScrollX = window.innerWidth < 1200;

    useEffect(() => {
      genListData({
        positionList,
        allBalances,
        borrowOffers,
        borrowLoans,
        positions,
        historyTrade,
        borrowCollaterals,
        assetsList
      });
    }, [
      genListData,
      positionList,
      allBalances,
      borrowOffers,
      borrowLoans,
      positions,
      historyTrade,
      borrowCollaterals,
      assetsList
    ]);

    useEffect(() => {
      if (list && list.length) {
        const array = list.map((listItem: IListItem) => {
          const item = { ...listItem };
          const assetId = item.asset;
          if (lastTicker.base === assetId) {
            if (lastTicker.ask && item.side === "bid") {
              item.currentPrice = lastTicker.ask / 10000;
            } else if (lastTicker.bid && item.side === "ask") {
              item.currentPrice = lastTicker.bid / 10000;
            }
            item.pnl =
              ((item.currentPrice - item.entryPrice) * item.position) / 10000;
            item.change =
              item.entryPrice === 0
                ? 0
                : (item.currentPrice /
                    (item.entryPrice === 0 ? 1 : item.entryPrice) -
                    1) *
                  100;
          }
          return item;
        });
        updateList(array);
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [lastTicker]);
    const cancel = useCallback(
      (record: IListItem) => {
        const markets = marketList.filter(item => {
          return item.base === record.base;
        });
        const market = markets[0];
        if (record && record.base) {
          if (!isMobile && currentSymbol.base.id !== market.base) {
            history.push(
              PUBLIC_URL +
                "/markets/" +
                market.name.replace("/", "_").toLowerCase()
            );
          } else {
            setTopTabsDispatched("TRADE");
          }
          setSelectedMarketDispatched(market);
          setPanelConfirmDispatched({
            panelType: "panel_trade_confirm",
            tradeData: {
              ...record,
              ...market,
              confirmAmount: Math.abs(record.position)
            }
          });
        }
      },
      // eslint-disable-next-line
      []
    );
    const columns = useMemo(
      () => [
        {
          title: intl.formatMessage({ defaultMessage: "Contract" }),
          dataIndex: "contract",
          key: "contract",
          align: "left" as TTableColumnAlign,
          width: 180,
          fixed: (isScrollX ? "left" : false) as TTableColumnFixed
        },
        {
          title: intl.formatMessage({ defaultMessage: "Side" }),
          dataIndex: "side",
          key: "side",
          align: "center" as TTableColumnAlign,
          width: 70,
          fixed: (isScrollX ? "left" : false) as TTableColumnFixed,
          render: (side: TAskBidSide) =>
            side.toLowerCase() === "bid" ? (
              <span className="fontGreen">
                <FormattedMessage defaultMessage="BUY" />
              </span>
            ) : (
              <span className="fontRed">
                <FormattedMessage defaultMessage="SELL" />
              </span>
            )
        },
        {
          title: (
            <div className="head-title">
              <FormattedMessage defaultMessage="Amount" />
              <br />
              <label className="sub">
                <FormattedMessage defaultMessage="Traded USDT" />
              </label>
            </div>
          ),
          dataIndex: "position",
          key: "position",
          align: "right" as TTableColumnAlign,
          width: 120,
          render: (position: number, record: IListItem) => (
            <span>
              {Calc.toFixeds(Math.abs(position / 10000), record.decimalBalance)}
              <br />
              <label className="sub">
                {Math.abs((position * record.entryPrice) / 10000).toFixed(
                  record.decimalPrice
                )}{" "}
                USDT
              </label>
            </span>
          )
        },
        {
          title: intl.formatMessage({ defaultMessage: "Entry Price" }),
          dataIndex: "entryPrice",
          key: "entryPrice",
          align: "right" as TTableColumnAlign,
          width: 120,
          render: (entryPrice: number, record: IListItem) =>
            entryPrice.toFixed(record.decimalPrice)
        },
        {
          title: intl.formatMessage({ defaultMessage: "Current Price" }),
          dataIndex: "currentPrice",
          key: "currentPrice",
          align: "right" as TTableColumnAlign,
          width: 120,
          render: (currentPrice: number, record: IListItem) => (
            <span>
              {Calc.toFixeds(currentPrice, record.decimalPrice)}
              <br />
              <label className="sub">
                {record.change > 0 ? "+" : ""}
                {record.change.toFixed(2)}%
              </label>
            </span>
          )
        },
        {
          title: (
            <div className="head-title">
              <FormattedMessage defaultMessage="PnL" />
              <br />
              <label className="sub">
                <FormattedMessage defaultMessage="Unrealised USDT" />
              </label>
            </div>
          ),
          dataIndex: "pnl",
          key: "pnl",
          align: "right" as TTableColumnAlign,
          width: 120,
          fixed: (isScrollX ? "right" : false) as TTableColumnFixed,
          render: (pnl: number, record: IListItem) => (
            <span className={Number(pnl) > 0 ? "fontGreen" : "fontRed"}>
              {(Number(pnl) > 0 ? "+" : "") +
                Calc.toThousands(Calc.toFixeds(pnl, record.decimalPrice))}
            </span>
          )
        },
        {
          title: intl.formatMessage({ defaultMessage: "Actions" }),
          key: "actions",
          align: "right" as TTableColumnAlign,
          width: 120,
          fixed: (isScrollX ? "right" : false) as TTableColumnFixed,
          render: (record: IListItem) => (
            <span className="flex flex-content-end">
              <button
                className="btn btn-concal"
                onClick={() => cancel(record)}
                style={{ border: "none", cursor: "pointer" }}
              >
                <i className="icon-close" />
              </button>
            </span>
          )
        }
      ],
      [intl, isScrollX, cancel]
    );
    const columnsMobile = useMemo(
      () => [
        {
          title: intl.formatMessage({ defaultMessage: "Contract" }),
          key: "contract",
          align: "left" as TTableColumnAlign,
          render: (record: IListItem) => (
            <span>
              {record.contract}
              <br />
              {record.side.toLowerCase() === "bid" ? (
                <span className="fontGreen">
                  <FormattedMessage defaultMessage="BUY" />
                </span>
              ) : (
                <span className="fontRed">
                  <FormattedMessage defaultMessage="SELL" />
                </span>
              )}
            </span>
          )
        },
        {
          title: (
            <div className="head-title">
              <FormattedMessage defaultMessage="Amount" />
              <br />
              <label className="sub">
                <FormattedMessage defaultMessage="Traded USDT" />
              </label>
            </div>
          ),
          dataIndex: "position",
          key: "position",
          align: "right" as TTableColumnAlign,
          render: (position: number) => (
            <span>
              {Calc.toFixeds(position / 10000, 2)}
              <br />
              <label className="sub">0.0001 XBT</label>
            </span>
          )
        },
        {
          title: (
            <div className="head-title">
              <FormattedMessage defaultMessage="PnL" />
              <br />
              <label className="sub">
                <FormattedMessage defaultMessage="Unrealised USDT" />
              </label>
            </div>
          ),
          dataIndex: "pnl",
          width: 120,
          key: "pnl",
          align: "right" as TTableColumnAlign,
          render: (pnl: number) => (
            <span className={Number(pnl) > 0 ? "fontGreen" : "fontRed"}>
              {(Number(pnl) > 0 ? "+" : "") +
                Calc.toThousands(Calc.toFixeds(pnl, 2))}
            </span>
          )
        },
        {
          title: intl.formatMessage({ defaultMessage: "Actions" }),
          key: "actions",
          align: "right" as TTableColumnAlign,
          render: (record: IListItem) => (
            <span className="flex flex-content-end">
              <button
                className="btn btn-concal"
                onClick={() => cancel(record)}
                style={{ border: "none", cursor: "pointer" }}
              >
                <i className="icon-close" />
              </button>
            </span>
          )
        }
      ],
      [intl, cancel]
    );
    return (
      <div className="table">
        <Table
          columns={isMobile ? columnsMobile : columns}
          dataSource={list}
          pagination={false}
          loading={!_isNumber(list.length)}
          scroll={
            isMobile
              ? { x: false, y: false }
              : { x: isScrollX ? 840 : false, y: 180 }
          }
        />
      </div>
    );
  },
  (prevProps, nextProps) => {
    if (
      !isEqual(prevProps.isMobile, nextProps.isMobile) ||
      !isEqual(prevProps.positionList, nextProps.positionList) ||
      !isEqual(prevProps.allBalances, nextProps.allBalances) ||
      !isEqual(prevProps.borrowOffers, nextProps.borrowOffers) ||
      !isEqual(prevProps.borrowLoans, nextProps.borrowLoans) ||
      !isEqual(prevProps.positions, nextProps.positions) ||
      !isEqual(prevProps.lastTicker, nextProps.lastTicker) ||
      !isEqual(prevProps.currentSymbol, nextProps.currentSymbol)
    ) {
      return false;
    }
    return true;
  }
);

export default Positions;
