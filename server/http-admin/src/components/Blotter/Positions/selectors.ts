import { createSelector } from "reselect";

const isMobile = (state: IAppState) => state.isMobile;
const positionList = (state: IAppState) => state.positionList;
const assetsList = (state: IAppState) => state.assetsList;
const allBalances = (state: IAppState) => state.allBalances;
const borrowOffers = (state: IAppState) => state.borrowOffers;
const borrowLoans = (state: IAppState) => state.borrowLoans;
const positions = (state: IAppState) => state.positions;
const borrowCollaterals = (state: IAppState) => state.borrowCollaterals;
const lastTicker = (state: IAppState) => state.lastTicker;
const marketList = (state: IAppState) => state.marketList;
const currentSymbol = (state: IAppState) => state.currentSymbol;
const historyTrade = (state: IAppState) => state.historyTrade;
export const getDataForPositions = createSelector(
  [
    isMobile,
    positionList,
    assetsList,
    allBalances,
    borrowOffers,
    borrowLoans,
    positions,
    borrowCollaterals,
    lastTicker,
    marketList,
    currentSymbol,
    historyTrade
  ],
  (
    isMobile: boolean,
    positionList: IPositionListItem[],
    assetsList: IAsset[],
    allBalances: IBalance[],
    borrowOffers: IBorrowOffer[],
    borrowLoans: IBorrowLoan[],
    positions: IPositions,
    borrowCollaterals: IBorrowCollateral[],
    lastTicker: ITickerChange,
    marketList: IMarket[],
    currentSymbol: ICurrentSymbol,
    historyTrade: ITrade[]
  ) => {
    return {
      isMobile,
      positionList,
      assetsList,
      allBalances,
      borrowOffers,
      borrowLoans,
      positions,
      borrowCollaterals,
      lastTicker,
      marketList,
      currentSymbol,
      historyTrade
    };
  }
);
