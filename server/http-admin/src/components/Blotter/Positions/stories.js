import React from "react";
import { wrapWithMockRedux, wrapWithEmptyRedux } from "utils/testUtils";
import Positions from "./Positions";

export default {
  title: "Positions"
};

export const Default = () => <Positions />;
export const Empty = () => <Positions positionsList={[]} positions={{}} />;

Default.story = {
  name: "default",
  decorators: [wrapWithMockRedux()]
};
Empty.story = {
  name: "empty",
  decorators: [wrapWithEmptyRedux()],
  parameters: { positionsList: [] }
};
