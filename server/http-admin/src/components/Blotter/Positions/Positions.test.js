import React from "react";
import Positions from "./Positions";
import { renderWithMockRedux, renderWithEmptyRedux } from "utils/testUtils";

describe("Positions table", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Positions />);
    expect(container.firstChild).toMatchSnapshot();
  });
  it("render Contract columns", () => {
    const { getAllByText } = renderWithMockRedux(<Positions />);
    expect(getAllByText("Contract")[0]).toBeInTheDocument();
  });
  it("render Actions columns", () => {
    const { getAllByText } = renderWithMockRedux(<Positions />);
    expect(getAllByText("Actions")[0]).toBeInTheDocument();
  });
  it("render Empty Table", () => {
    const { getAllByText } = renderWithEmptyRedux(<Positions />);
    expect(getAllByText("No Data")[0]).toBeInTheDocument();
  });
  it("render Contract columns in Empty Table", () => {
    const { getAllByText } = renderWithEmptyRedux(<Positions />);
    expect(getAllByText("Contract")[0]).toBeInTheDocument();
  });
  it("render Actions columns in Empty Table", () => {
    const { getAllByText } = renderWithEmptyRedux(<Positions />);
    expect(getAllByText("Actions")[0]).toBeInTheDocument();
  });
});
