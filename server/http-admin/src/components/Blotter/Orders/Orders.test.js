import React from "react";
import Orders from "./Orders";
import { renderWithMockRedux } from "utils/testUtils";

describe("Orders table", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Orders />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
