import React, { useCallback, useEffect, useMemo, useState } from "react";
import Table from "components/Table";
import { message } from "antd";
import { Toast } from "antd-mobile";
import Calc from "utils/math.calc.js";
import { connect } from "react-redux";
import moment from "moment";
import { decimals, contractCode } from "utils/reference";
import { clearLoans, repayLoans } from "services/http/http";
import coinflexWebsocket from "services/websocket";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import _flowRight from "lodash/flowRight";
import _isEqual from "lodash/isEqual";
import getAuthParams from "components/Home/utils/auth/authParams";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TProps = TStateProps & WrappedComponentProps;

interface IListItem {
  id: number;
  key: number;
  asset_id: number;
  contract: string;
  side: TBuySellSide;
  amount: number;
  enterPrice: number;
  time: number;
  decimalPrice: number;
  decimalBalance: number;
}

interface IRepayInfo {
  asset_id?: number;
  amount?: number;
  repay?: boolean;
}

const repayInfo: IRepayInfo = {};

const Orders = React.memo(
  (props: TProps) => {
    const [list, setList] = useState<IListItem[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    const {
      marketList,
      intl,
      orderList,
      isMobile,
      borrowLoans,
      allBalances,
      websocketAuthParams,
      borrowOffers
    } = props;

    const parseOrderList = useCallback(
      (data: IOrder[] | null) => {
        if (!data) return setLoading(false);
        if (!marketList || marketList.length === 0) return setLoading(false);
        const list: IListItem[] = [];
        data.forEach((item, index) => {
          const market = marketList.filter(
            e => e.base === item.base && e.counter === item.counter
          )[0];
          const baseAsset = decimals[market.spot_name.split("/")[0]];
          list.push({
            id: item.id,
            key: index,
            asset_id: item.quantity > 0 ? market.counter : market.base,
            contract: market.tenor
              ? market.spot_name +
                " " +
                contractCode[market.tenor.substring(0, 1)] +
                " " +
                market.tenor.substring(1)
              : market.spot_name,
            side: item.quantity > 0 ? "BUY" : "SELL",
            amount: Math.abs(item.quantity / 10000),
            enterPrice: item.price / 10000,
            time: parseInt("" + item.time / 1000),
            decimalPrice: baseAsset ? baseAsset.price : 4,
            decimalBalance: baseAsset ? baseAsset.balance : 4
          });
        });

        setLoading(false);
        setList([...list]);
      },
      [marketList]
    );

    const cancel = useCallback(
      (record: IListItem) => {
        const data = {
          tag: 6,
          method: "CancelOrder",
          id: record.id
        };
        coinflexWebsocket.send(data, (event: ICancelOrderResponse) => {
          if (event.error_code === 0 && orderList) {
            const index = orderList.indexOf(
              orderList.filter(e => e.id === record.id)[0]
            );
            orderList.splice(index, 1);
            parseOrderList(orderList);
            const messageText = intl.formatMessage(
              { defaultMessage: "Order id: {id} has been cancelled!" },
              { id: event.id }
            );
            if (isMobile) {
              Toast.success(messageText);
            } else {
              message.success(messageText);
            }
            repayInfo.asset_id = record.asset_id;
            repayInfo.amount =
              record.side.toUpperCase() === "BUY"
                ? record.amount * record.enterPrice * 10000
                : record.amount * 10000;
            repayInfo.repay = true;
          } else {
            if (isMobile) {
              event.error_msg && Toast.success(event.error_msg);
            } else {
              event.error_msg && message.success(event.error_msg);
            }
          }
        });
      },
      [intl, isMobile, orderList, parseOrderList]
    );

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const repayment = useCallback(() => {
      const authParams = getAuthParams(websocketAuthParams);

      message.success("Repayment : orders");
      if (!repayInfo.repay) return;
      if (repayInfo.asset_id && repayInfo.asset_id > 0) {
        let balance = allBalances.filter(e => e.id === repayInfo.asset_id)[0]
          .available;
        for (const index in borrowLoans) {
          const item = borrowLoans[index];
          if (item.asset_id === repayInfo.asset_id) {
            const id = item.id;

            let loanAmount = item.principal;
            const offer = borrowOffers.filter(
              e => e.asset_id === repayInfo.asset_id
            );
            const min = offer && offer[0].min_amount;
            if (id) {
              if (balance >= loanAmount) {
                balance -= loanAmount;
                repayInfo.repay = false;
                clearLoans(id, authParams);
              } else if (
                loanAmount > min &&
                balance > 0 &&
                balance < loanAmount
              ) {
                const speculateLoan = loanAmount - balance;
                if (speculateLoan >= min) {
                  loanAmount = balance;
                } else {
                  loanAmount -= min;
                }
                balance -= loanAmount;
                repayInfo.repay = false;
                repayLoans(id, { amount: loanAmount }, authParams);
              }
            }
          }
        }
      }
    }, [allBalances, borrowLoans, borrowOffers, websocketAuthParams]);

    useEffect(() => {
      parseOrderList(orderList);
    }, [orderList, parseOrderList]);

    const columns = useMemo(() => {
      return [
        {
          title: intl.formatMessage({ defaultMessage: "Contract" }),
          dataIndex: "contract",
          key: "contract",
          align: "left" as TTableColumnAlign,
          width: 150
          // fixed: isScrollX ? 'left' : false
        },
        {
          title: intl.formatMessage({ defaultMessage: "Side" }),
          dataIndex: "side",
          key: "side",
          align: "center" as TTableColumnAlign,
          width: 70,
          // fixed: isScrollX ? 'left' : false,
          render: (side: TBuySellSide) =>
            side === "BUY" ? (
              <span className="fontGreen">
                <FormattedMessage defaultMessage="BUY" />
              </span>
            ) : (
              <span className="fontGreen">
                <FormattedMessage defaultMessage="SELL" />
              </span>
            )
        },
        {
          title: (
            <div className="head-title">
              <FormattedMessage defaultMessage="Amount" />
              <br />
              <label className="sub">
                <FormattedMessage defaultMessage="Traded USDT" />
              </label>
            </div>
          ),
          dataIndex: "amount",
          key: "amount",
          align: "right" as TTableColumnAlign,
          width: 120,
          render: (amount: number, record: IListItem) => (
            <span>
              {Calc.toFixeds(amount, record.decimalBalance)}
              <br />
              <label className="sub">
                {(amount * record.enterPrice).toFixed(record.decimalPrice)} USDT
              </label>
            </span>
          )
        },
        {
          title: intl.formatMessage({ defaultMessage: "Trigger/Enter Price" }),
          dataIndex: "enterPrice",
          key: "enterPrice",
          align: "right" as TTableColumnAlign,
          width: 120,
          render: (price: number, record: IListItem) =>
            price.toFixed(record.decimalPrice)
        },
        // {
        //     title: 'Current Price',
        //     dataIndex: 'currentPrice',
        //     key: 'currentPrice',
        //     align: 'right',
        //     width: 120,
        //     render: currentPrice => (
        //         <span>
        //             {Calc.toFixeds(currentPrice, 2)}<br />
        //             <label className="sub">-0.33%</label>
        //         </span>
        //     )
        // },
        // {
        //     title: 'Take Profit',
        //     dataIndex: 'takeProfit',
        //     key: 'takeProfit',
        //     align: 'right',
        //     width: 120,
        //     render: takeProfit => (
        //         takeProfit ? Calc.toFixeds(takeProfit, 2) : <label onClick={this.setProfit}>SET</label>
        //     )
        // },
        {
          title: intl.formatMessage({ defaultMessage: "Ordered" }),
          dataIndex: "time",
          key: "time",
          align: "right" as TTableColumnAlign,
          width: 120,
          render: (time: number) => (
            <span>
              {moment(time).format("DD MMM YYYY")}
              <br />
              <label className="sub">{moment(time).format("HH:mm")}</label>
            </span>
          )
        },
        // {
        //     title: <div className="head-title">PnL<br /><label className="sub">Unrealised USDT</label></div>,
        //     dataIndex: 'pnl',
        //     key: 'pnl',
        //     align: 'right',
        //     width: 120,
        //     fixed: isScrollX ? 'right' : false,
        //     render: pnl => (
        //         <span className={Number(pnl) > 0 ? 'fontGreen' : 'fontRed'}>{(Number(pnl) > 0 ? '+' : '') + Calc.toThousands(Calc.toFixeds(pnl, 2))}</span>
        //     )
        // },
        {
          title: intl.formatMessage({ defaultMessage: "Actions" }),
          key: "actions",
          align: "right" as TTableColumnAlign,
          width: 100,
          // fixed: isScrollX ? 'right' : false,
          render: (record: IListItem) => (
            <span className="flex flex-content-end">
              <button
                className="btn btn-concal"
                onClick={() => cancel(record)}
                style={{ border: "none", cursor: "pointer" }}
              >
                <i className="icon-close" />
              </button>
              {/* <a className="btn btn-edit"><i className="icon-edit"></i></a> */}
              {/* <a className="btn-more"><i className="icon-more"></i></a> */}
            </span>
          )
        }
      ];
    }, [cancel, intl]);

    const columnsMobile = useMemo(() => {
      return [
        {
          title: intl.formatMessage({ defaultMessage: "Contract" }),
          key: "contract",
          align: "left" as TTableColumnAlign,
          render: (record: IListItem) => (
            <span>
              {record.contract}
              <br />
              {record.side === "BUY" ? (
                <span className="fontGreen">
                  <FormattedMessage defaultMessage="BUY" />
                </span>
              ) : (
                <span className="fontRed">
                  <FormattedMessage defaultMessage="SELL" />
                </span>
              )}
            </span>
          )
        },
        {
          title: (
            <div className="head-title">
              <FormattedMessage defaultMessage="Amount" />
              <br />
              <label className="sub">
                <FormattedMessage defaultMessage="Traded USDT" />
              </label>
            </div>
          ),
          dataIndex: "amount",
          key: "amount",
          align: "right" as TTableColumnAlign,
          render: (amount: number, record: IListItem) => (
            <span>
              {Calc.toFixeds(amount, record.decimalBalance)}
              <br />
              <label className="sub">
                {(amount * record.enterPrice).toFixed(record.decimalPrice)} USDT
              </label>
            </span>
          )
        },
        {
          title: intl.formatMessage({ defaultMessage: "Price" }),
          dataIndex: "enterPrice",
          key: "enterPrice",
          align: "right" as TTableColumnAlign,
          render: (price: number, record: IListItem) =>
            price.toFixed(record.decimalPrice)
        },
        {
          title: intl.formatMessage({ defaultMessage: "Actions" }),
          key: "actions",
          align: "right" as TTableColumnAlign,
          render: (record: IListItem) => (
            <span className="flex flex-content-end">
              <button
                className="btn btn-concal"
                onClick={() => cancel(record)} //TODO: typescript: "cancel(record.id)" is a mistake?
                style={{ border: "none", cursor: "pointer" }}
              >
                <i className="icon-concal">&times;</i>
              </button>
            </span>
          )
        }
      ];
    }, [cancel, intl]);

    return (
      <div className="table">
        <Table
          columns={isMobile ? columnsMobile : columns}
          dataSource={list}
          pagination={false}
          // scroll={!this.props.isMobile ? {x: (isScrollX && !this.loading) ? 730 : false, y: 200} : {x: false, y: false}}
          scroll={{ y: 200 }}
          loading={loading}
        />
      </div>
    );
  },
  (prevProps, nextProps) => {
    if (!_isEqual(nextProps.orderList, prevProps.orderList)) {
      return false;
    }
    return true;
  }
);

const mapStateToProps = (state: IAppState) => {
  return {
    isMobile: state.isMobile,
    orderList: state.orderList,
    marketList: state.marketList,
    tickersList: state.tickersList,
    allBalances: state.allBalances,
    borrowLoans: state.borrowLoans,
    borrowOffers: state.borrowOffers,
    websocketAuthParams: state.websocketAuthParams
  };
};

export default _flowRight(connect(mapStateToProps), injectIntl)(Orders);
