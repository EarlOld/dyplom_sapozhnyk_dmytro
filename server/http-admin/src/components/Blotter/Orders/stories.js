import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Orders from "./Orders";

export default {
  title: "Orders",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <Orders />;

Default.story = {
  name: "default"
};
