import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import History from "./History";

export default {
  title: "History",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <History />;

Default.story = {
  name: "default"
};
