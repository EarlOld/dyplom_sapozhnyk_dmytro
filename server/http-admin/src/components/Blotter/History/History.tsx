import React, { useCallback, useEffect, useMemo, useState } from "react";
import Table from "components/Table";
import Calc from "utils/math.calc.js";
import { connect } from "react-redux";
import moment from "moment";
import _isEqual from "lodash/isEqual";

import { contractCode, decimals } from "utils/reference";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import _flowRight from "lodash/flowRight";
import _isNumber from "lodash/isNumber";

type TStateProps = ReturnType<typeof mapStateToProps>;

type TProps = TStateProps & WrappedComponentProps;

export interface IListItem {
  key: number;
  description: string;
  type: TBuySellSide;
  time: number;
  amount: string;
  price: string;
}

const History = React.memo(
  (props: TProps) => {
    const [list, setList] = useState<IListItem[]>([]);
    const { intl, marketList, historyTrade } = props;

    const calculateList = useCallback(
      (marketList: IMarket[], historyTrade: ITrade[]) => {
        if (
          marketList &&
          marketList.length &&
          historyTrade &&
          historyTrade.length
        ) {
          const listItems: IListItem[] =
            historyTrade
              .filter((item: ITrade) => {
                return marketList.find(
                  (e: { base: number; counter: number }) =>
                    e.base === item.base && e.counter === item.counter
                );
              })
              .map(
                (item: ITrade): IListItem => {
                  const market = marketList.filter(
                    (e: { base: number; counter: number }) =>
                      e.base === item.base && e.counter === item.counter
                  )[0];
                  const decimalPrice = decimals[market.spot_name.split("/")[0]]
                    ? decimals[market.spot_name.split("/")[0]].price
                    : 4;
                  const decimalBalance = decimals[
                    market.spot_name.split("/")[0]
                  ]
                    ? decimals[market.spot_name.split("/")[0]].balance
                    : 4;
                  const description = market.tenor
                    ? market.spot_name +
                      " " +
                      contractCode[market.tenor.substring(0, 1)] +
                      " " +
                      market.tenor.substring(1)
                    : market.spot_name;

                  return {
                    key: item.time,
                    description: description,
                    type: item.quantity > 0 ? "BUY" : ("SELL" as TBuySellSide),
                    time: parseInt("" + item.time / 1000),
                    amount: Math.abs(item.quantity / 10000).toFixed(
                      decimalBalance
                    ),
                    price: (item.price / 10000).toFixed(decimalPrice)
                  };
                }
              ) || [];

          setList(listItems);
        }
      },
      [setList]
    );

    useEffect(() => {
      calculateList(marketList, historyTrade);
    }, [marketList, historyTrade, calculateList]);

    const columns = useMemo(() => {
      return [
        {
          title: intl.formatMessage({ defaultMessage: "Contract" }),
          dataHistory: "description",
          key: "description",
          render: ({ description }: IListItem) => description
        },
        {
          title: intl.formatMessage({ defaultMessage: "Side" }),
          dataHistory: "type",
          key: "type",
          align: "center" as TTableColumnAlign,
          render: ({ type }: IListItem) =>
            type === "BUY" ? (
              <span className="fontGreen">
                <FormattedMessage defaultMessage="BUY" />
              </span>
            ) : (
              <span className="fontRed">
                <FormattedMessage defaultMessage="SELL" />
              </span>
            )
        },
        {
          title: intl.formatMessage({ defaultMessage: "Time" }),
          dataHistory: "time",
          key: "time",
          align: "right" as TTableColumnAlign,
          render: ({ time }: IListItem) => (
            <span>
              {moment(time).format("DD MMM YYYY")}
              <br />
              <label className="sub">{moment(time).format("HH:mm")}</label>
            </span>
          )
        },
        {
          title: (
            <div className="head-title">
              <FormattedMessage defaultMessage="Amount" />
            </div>
          ),
          dataHistory: "amount",
          key: "amount",
          align: "right" as TTableColumnAlign,
          render: ({ amount }: IListItem) => amount
        },
        {
          title: intl.formatMessage({ defaultMessage: "Price" }),
          dataHistory: "price",
          key: "price",
          align: "right" as TTableColumnAlign,
          render: ({ price }: IListItem) => {
            return Calc.toThousands(price);
          }
        }
      ];
    }, [intl]);

    return (
      <div className="table">
        <Table
          columns={columns}
          dataSource={list.length ? list : []}
          pagination={false}
          loading={!_isNumber(list.length)}
          scroll={{ y: 200 }}
        />
      </div>
    );
  },
  (prevProps, nextProps) => {
    return !(
      !_isEqual(prevProps.marketList, nextProps.marketList) ||
      !_isEqual(prevProps.historyTrade, nextProps.historyTrade)
    );
  }
);

const mapStateToProps = (state: IPublicState) => {
  return {
    marketList: state.marketList,
    isMobile: state.isMobile,
    historyTrade: state.historyTrade,
    websocketAuthParams: state.websocketAuthParams
  };
};

export default _flowRight(connect(mapStateToProps), injectIntl)(History);
