import React from "react";
import History from "./History";
import { renderWithMockRedux } from "utils/testUtils";

describe("History table", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<History />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
