import React, { Component } from "react";
import { Radio } from "antd";
import { RadioChangeEvent } from "antd/es/radio";
import Positions from "components/Blotter/Positions";
import Balances from "components/Blotter/Balances";
import Orders from "components/Blotter/Orders";
import History from "components/Blotter/History";
import "./style/index.scss";
import { connect } from "react-redux";
import { isSpotAsset } from "utils/isSpotAsset";
import { FormattedMessage, injectIntl } from "react-intl";
import _flowRight from "lodash/flowRight";

type TStateProps = ReturnType<typeof mapStateToProps>;

type TProps = TStateProps;

interface IState {
  blotterTab: "positions" | "orders" | "history" | "balances";
}

class Blotter extends Component<TProps, IState> {
  constructor(props: TProps) {
    super(props);
    this.state = {
      blotterTab: "positions"
    };
  }

  setBlotterTab = (event: IMarket | IPositionListItem) => {
    const { assetsList } = this.props;
    if (event) {
      const { base } = event;
      const matchingAsset = assetsList.find(asset => {
        return asset.id === base;
      });
      const isSpot = matchingAsset ? isSpotAsset(matchingAsset) : false;
      const balancesOrPositionsAreActive = ["positions", "balances"].includes(
        this.state.blotterTab
      );
      if (balancesOrPositionsAreActive) {
        this.setState({ blotterTab: isSpot ? "balances" : "positions" });
      }
    }
  };

  onChangeBlotter = (e: RadioChangeEvent) => {
    this.setState({ blotterTab: e.target.value });
  };

  blotterComponent = () => {
    const { blotterTab } = this.state;
    switch (blotterTab) {
      case "positions":
        return <Positions />;
      case "orders":
        return <Orders />;
      case "balances":
        return <Balances />;
      case "history":
        return <History />;
      default:
        return <History />;
    }
  };

  componentDidUpdate(prevProps: TProps) {
    if (this.props.selectedMarket !== prevProps.selectedMarket) {
      this.setBlotterTab(this.props.selectedMarket);
    }
  }

  render() {
    const { blotterTab } = this.state;
    return (
      <div className="blotter">
        <Radio.Group
          onChange={this.onChangeBlotter}
          defaultValue="positions"
          buttonStyle="solid"
          className="blotter-radio"
        >
          <Radio.Button value="positions" checked={"positions" === blotterTab}>
            <FormattedMessage defaultMessage="My Futures Positions" />
          </Radio.Button>
          <Radio.Button value="balances" checked={"balances" === blotterTab}>
            <FormattedMessage defaultMessage="My Spot Balances" />
          </Radio.Button>
          <Radio.Button value="orders" checked={"orders" === blotterTab}>
            <FormattedMessage defaultMessage="My Orders" />
          </Radio.Button>
          <Radio.Button value="history" checked={"history" === blotterTab}>
            <FormattedMessage defaultMessage="My History" />
          </Radio.Button>
        </Radio.Group>
        {this.blotterComponent()}
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    currentSymbol: state.currentSymbol,
    assetsList: state.assetsList,
    selectedMarket: state.selectedMarket
  };
};

export default _flowRight(connect(mapStateToProps), injectIntl)(Blotter);
