import React from "react";
import { action } from "@storybook/addon-actions";
import Button from "components/Button";
import Modal from "./Modal";

export default {
  title: "Modal"
};

export const Default = () => (
  <Modal
    visible={true}
    title="Basic Modal"
    footer={[
      <Button key={"fail"} link tertiary onClick={action("Cancelled!")}>
        Cancel
      </Button>,
      <Button key={"win"} tertiary onClick={action("Win!")}>
        Done
      </Button>
    ]}
  >
    <p>Content</p>
  </Modal>
);

Default.story = {
  name: "default"
};
