import styled from "styled-components";
import { Modal as AntDModal } from "antd";
export const StyledModal = styled(AntDModal)`
  &.ant-modal {
    padding-bottom: 0;
    color: white;

    .ant-modal-content {
      background: none;
      border: 2px solid #252526;
      border-top-left-radius: 4px;
      border-top-right-radius: 4px;
    }

    .ant-modal-header {
      padding: 16px 16px 13px 16px;
      background: #252526;
      border-bottom: none;
      border-radius: 0;

      .ant-modal-title {
        color: #fff;
        font-size: 14px;
        line-height: normal;
        font-weight: 400;
      }
    }

    .ant-modal-close {
      color: #ccc;

      .ant-modal-close-x {
        width: 48px;
        height: 44px;
        line-height: 50px;
        font-size: 22px;
      }

      &:hover {
        color: #ccc;
      }
    }

    .ant-modal-body,
    .ant-modal-footer {
      background: black;
      padding: 10px;
      border-top: none;
    }

    .ant-modal-footer {
      overflow: hidden;
      display: flex;
    }
  }
`;
export default StyledModal;
