import React from "react";
import { fireEvent } from "@testing-library/react";
import { render } from "utils/testUtils";
import Button from "components/Button/Button";
import { Modal } from "./Modal";

describe("Modal", () => {
  const handleClose = jest.fn();
  const handleWin = jest.fn();

  const TestModal = () => (
    <Modal
      visible={true}
      title="Basic Modal"
      footer={[
        <Button key={"fail"} link tertiary onClick={handleClose}>
          Cancel
        </Button>,
        <Button key={"win"} tertiary onClick={handleWin}>
          Done
        </Button>
      ]}
    >
      <p>Content</p>
    </Modal>
  );

  it("Should match snapshot", () => {
    const { getByRole } = render(<TestModal />);
    const modal = getByRole("dialog");
    expect(modal).toMatchSnapshot();
  });

  it("Should fire the success callback when done is clicked", () => {
    const { getByText } = render(<TestModal />);
    const doneButton = getByText("Done");
    expect(doneButton).toBeInTheDocument();
    fireEvent.click(doneButton);
    expect(handleWin).toHaveBeenCalled();
  });
});
