export const isNotDivisibleByTwo = num => num % 2 !== 0;
