import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import Basic from "./Basic";

describe("Basic tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Basic />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
