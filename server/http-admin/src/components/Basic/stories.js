import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Basic from "./Basic";

export default {
  title: "Basic",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <Basic />;

Default.story = {
  name: "default"
};
