import React, { memo } from "react";
import data from "./data";
import _flowRight from "lodash/flowRight";
import { connect } from "react-redux";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import { setSelectedMarket } from "store/actions";
import { INumberArray } from "./types";
import { isNotDivisibleByTwo } from "./utils";
import "./styles/index.scss";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TProps = TStateProps & TDispatchProps & WrappedComponentProps;

export const Basic = memo(({ intl, selectedMarket }: TProps) => {
  const { stringArray }: INumberArray = data;
  console.log("selectedMarket", selectedMarket);
  const command = intl.formatMessage({
    defaultMessage: "yarn component"
  });
  return (
    <div className={"cf-basic"}>
      {stringArray.filter(isNotDivisibleByTwo).map(val => (
        <div key={val}>
          <FormattedMessage defaultMessage="Test Title" />
          <FormattedMessage defaultMessage="Duplicate me using" />:
          &nbsp;&nbsp;&nbsp;
          <code>{command}</code>
        </div>
      ))}
    </div>
  );
});

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setSelectedMarket(data: IPositionListItem | IMarket) {
      dispatch(setSelectedMarket(data));
    }
  };
};

const mapStateToProps = (state: IPublicState) => {
  return {
    selectedMarket: state.selectedMarket
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(Basic);
