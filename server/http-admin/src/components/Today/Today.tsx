import React, { Component } from "react";
import { Tabs, Progress } from "antd-mobile";
import Calc from "utils/math.calc";
import "./style/index.scss";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";

type TProps = WrappedComponentProps;

interface IState {
  tradeType: string;
  biggestBuy: number;
  biggestSell: number;
  buys: number;
  sells: number;
}

export class Today extends Component<TProps, IState> {
  constructor(props: TProps) {
    super(props);
    this.state = {
      tradeType: "1",
      biggestBuy: 980345,
      biggestSell: 3128884,
      buys: 192,
      sells: 834
    };
  }
  render() {
    const { intl } = this.props;
    const { tradeType, biggestBuy, biggestSell, buys, sells } = this.state;
    const tabs = [
      { title: intl.formatMessage({ defaultMessage: "Market" }), key: "1" },
      { title: intl.formatMessage({ defaultMessage: "Limit" }), key: "2" }
    ];

    //TODO: typescript: removed: onChange={this.onChangeTradeType}
    // (this.onChangeTradeType is not defined)

    return (
      <div className="today">
        <div className="header">
          <FormattedMessage defaultMessage="Today" />{" "}
          <span>
            <FormattedMessage defaultMessage="Coinflex Community" />
          </span>
        </div>
        <Tabs
          tabs={tabs}
          initialPage={tradeType}
          tabBarBackgroundColor="none"
          tabBarUnderlineStyle={{
            zIndex: 2,
            borderColor: "#4C8CB8",
            width: 18,
            marginLeft: "calc(25% - 9px)",
            boxShadow: "0 0 8px 5px rgba(255, 255, 255, 0.2)"
          }}
          tabBarInactiveTextColor="#474748"
          tabBarActiveTextColor="#4C8CB8"
        />
        <div className="flex flex-content-between statistics">
          <div className="buy">
            <p className="title">
              <span>
                <FormattedMessage defaultMessage="Biggest Buy" />{" "}
              </span>
              <label>USDT</label>
            </p>
            <p className="amount">{Calc.toThousands(biggestBuy)}</p>
          </div>
          <div className="sell">
            <p className="title">
              <span>
                <FormattedMessage defaultMessage="Biggest Sell" />{" "}
              </span>
              <label>USDT</label>
            </p>
            <p className="amount">{Calc.toThousands(biggestSell)}</p>
          </div>
        </div>
        <Progress
          percent={(biggestBuy * 100) / (biggestBuy + biggestSell)}
          position="normal"
          style={{
            height: 4,
            margin: "20px 15px 0 15px",
            background: "#FF2366"
          }}
          barStyle={{ borderColor: "#00FCC6" }}
        />
        <div className="flex flex-content-between statistics">
          <div className="buy">
            <p className="title">
              <span>
                <FormattedMessage defaultMessage="No of Buys" />
              </span>
            </p>
            <p className="amount">{Calc.toThousands(buys)}</p>
          </div>
          <div className="sell">
            <p className="title">
              <span>
                <FormattedMessage defaultMessage="No of Sells" />
              </span>
            </p>
            <p className="amount">{Calc.toThousands(sells)}</p>
          </div>
        </div>
        <Progress
          percent={(buys * 100) / (buys + sells)}
          position="normal"
          style={{
            height: 4,
            margin: "20px 15px 0 15px",
            background: "#FF2366"
          }}
          barStyle={{ borderColor: "#00FCC6" }}
        />
      </div>
    );
  }
}

export default injectIntl(Today);
