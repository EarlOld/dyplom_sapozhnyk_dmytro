import React from "react";
import { render } from "utils/testUtils";
import Today from "./Today";

describe("Today", () => {
  it("Should match snapshot when", () => {
    const { container } = render(<Today />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("renders today", () => {
    const { getByText } = render(<Today />);
    expect(getByText("Biggest Buy")).toBeInTheDocument();
  });
});
