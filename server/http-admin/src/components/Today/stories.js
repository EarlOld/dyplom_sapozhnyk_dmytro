import React from "react";
import Today from "./Today";

export default {
  title: "Today"
};

export const Default = () => <Today />;

Default.story = {
  name: "default"
};
