export const getAuthParams = websocketAuthParams => ({
  auth: {
    username: [websocketAuthParams.core_id, websocketAuthParams.api_key].join(
      "/"
    ),
    password: websocketAuthParams.priv_key
  }
});
export default getAuthParams;
