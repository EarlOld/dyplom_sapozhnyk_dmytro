import { ecp_sign, mpn_new, mpn_pack, mpn_unpack } from "./auth/ecp";
import {
  hash_string_to_words,
  string_to_words,
  words_to_string
} from "./auth/coinflex_worker";
import coinflexWebsocket from "services/websocket";
export const authenticateUser = (e, serverNonce) => {
  const uid = e?.core_id;
  const cookies = e?.api_key;
  const packed_user_id = String.fromCharCode(
    0,
    0,
    0,
    0,
    (uid >> 24) & 0xff,
    (uid >> 16) & 0xff,
    (uid >> 8) & 0xff,
    uid & 0xff
  );
  let client_nonce = "";
  for (let i = 0; i < 16; ++i) {
    client_nonce += String.fromCharCode(Math.random() * 256);
  }

  const data = {
    content: packed_user_id + atob(serverNonce) + client_nonce,
    privkey: atob(e?.priv_key || "")
  };
  const d = mpn_pack(string_to_words(data.privkey));
  const z = mpn_pack(hash_string_to_words(data.content));
  const r = mpn_new(8),
    s = mpn_new(8);
  ecp_sign(r, s, secp224k1_p, secp224k1_a, secp224k1_G, secp224k1_n, d, z, 8);
  const data2 = [
    words_to_string(mpn_unpack(r)),
    words_to_string(mpn_unpack(s))
  ];
  const dataSend = {
    method: "Authenticate",
    user_id: parseInt("" + uid),
    cookie: cookies,
    nonce: btoa(client_nonce),
    signature: [btoa(data2[0]), btoa(data2[1])],
    tag: 1
  };
  trackUser(uid);
  coinflexWebsocket.send(dataSend);
};

export const trackUser = userId => {
  const { Rollbar, FS, embedded_svc } = window;

  if (Rollbar) {
    Rollbar.configure({
      payload: {
        person: {
          id: parseInt(userId)
        }
      }
    });
  }

  if (FS) {
    FS.identify(parseInt(userId), {});
  }

  if (embedded_svc) {
    const SALESFORCE_SETTINGS = {
      displayHelpButton: true,
      defaultMinimizedText: "&#xf075;",
      enabledFeatures: ["LiveAgent"],
      entryFeature: "LiveAgent"
    };

    embedded_svc.settings = {
      ...embedded_svc.settings,
      ...SALESFORCE_SETTINGS
    };

    embedded_svc.init(
      "https://coinflex--newuat.my.salesforce.com",
      "https://newuat-coinflex.cs87.force.com/HelpCenter",
      "https://service.force.com/",
      "00D8E0000001LwD",
      "CoinFLEX_Website_Chat",
      {
        baseLiveAgentContentURL:
          "https://c.la1-c1cs-lo2.salesforceliveagent.com/content",
        deploymentId: "5724J000000LFZW",
        buttonId: "5734J000000LGme",
        baseLiveAgentURL: "https://d.la1-c1cs-lo2.salesforceliveagent.com/chat",
        eswLiveAgentDevName:
          "EmbeddedServiceLiveAgent_Parent04I8E0000008OY3UAM_17058b8d62c",
        isOfflineSupportEnabled: true
      }
    );
  }
};

export const MONTH_BY_NUM = [
  "JAN",
  "FEB",
  "MAR",
  "APR",
  "MAY",
  "JUN",
  "JUL",
  "AUG",
  "SEP",
  "OCT",
  "NOV",
  "DEC"
];

/*
 * Parameters of the secp224k1 elliptic curve, packed into multi-precision
 * integers suitable for use with the preceding functions.
 */
export const secp224k1_p = [
  0x7fffe56d,
  0x7ffffffd,
  0x7fffffff,
  0x7fffffff,
  0x7fffffff,
  0x7fffffff,
  0x7fffffff,
  0x7f
];
export const secp224k1_a = [0, 0, 0, 0, 0, 0, 0, 0];
export const secp224k1_G = [
  [
    0x36b7a45c,
    0x1efcca1d,
    0x11c1d6a4,
    0x4d233f4f,
    0x0fc28a16,
    0x3e133be6,
    0x5156ccd3,
    0x50
  ],
  [
    0x556d61a5,
    0x459497b6,
    0x02c2f567,
    0x3f18cfbe,
    0x2cafbd6f,
    0x77468850,
    0x0227fb5f,
    0x3f
  ],
  [1, 0, 0, 0, 0, 0, 0, 0]
];
export const secp224k1_n = [
  0x769fb1f7,
  0x15e152e2,
  0x4bb18613,
  0x000ee746,
  0x00000000,
  0x00000000,
  0x00000000,
  0x80
];

export const isMobile = () => {
  const userAgentInfo = navigator.userAgent;
  const Agents = [
    "Android",
    "iPhone",
    "SymbianOS",
    "Windows Phone",
    "iPad",
    "iPod"
  ];
  let isMobileAgent = false;
  for (let v = 0; v < Agents.length; v++) {
    if (userAgentInfo.indexOf(Agents[v]) > 0) {
      isMobileAgent = true;
      break;
    }
  }
  return isMobileAgent || isMobileSize();
};

export const isMobileSize = () => {
  const screenWidth = document.documentElement.clientWidth;
  const mobileSize = 640;
  return screenWidth < mobileSize;
};
