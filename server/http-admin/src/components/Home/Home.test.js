import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import Home from "./Home";

describe.skip("Home", () => {
  it("Should match snapshot on Desktop", () => {
    const { container } = renderWithMockRedux(<Home />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should match snapshot on Mobile", () => {
    const { container } = renderWithMockRedux(<Home />, {
      initialState: { isMobile: true }
    });
    expect(container.firstChild).toMatchSnapshot();
  });
});
