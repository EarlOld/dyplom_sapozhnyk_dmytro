import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Home from "./Home";

export default {
  title: "Home",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <Home />;

Default.story = {
  name: "default"
};
