import React, { Component } from "react";
import Header from "components/Header";
import TicketForm from "components/TicketForm";
import Blotter from "components/Blotter";
import OrderBook from "components/Orderbook";
import RecentTrade from "components/RecentTrades";
import Market from "components/Market";
import iotView from "components/iotView";
import TVWidget from "components/TVWidget";
import BottomBtns from "components/BottomBtns";
import Login from "components/TicketForm/Login";
import coinflexWebsocket from "services/websocket";
import coinflexEventstream from "services/eventstream";
import { getPreferedLocale } from "utils/i18n";
import { authenticateUser, isMobile } from "./utils";
import { ReactCookieProps, withCookies } from "react-cookie";
import { connect } from "react-redux";
import {
  setMobilePage,
  getMarketList,
  getAssetsList,
  setCurrentSymbol,
  setOrderBookList,
  setLastTrade,
  getChangeList,
  getTickersList,
  setLogin,
  setPanelConfirm,
  setTopTabs,
  getI18n,
  setOrderBookData,
  setAskData,
  setBidData,
  setWebsocketAuthParams,
  getHistoryTrade,
  getBorrowOffers,
  getBorrowLoans,
  getPositions,
  getBalances,
  getOrders,
  getBorrowCollaterals,
  getBorrowConvertedTotals,
  setUser,
  setAuthToken
} from "store/actions";

import "./utils/auth/sha";
import { getAuthParams } from "./utils/auth/authParams";
import { Tabs } from "antd-mobile";
import {
  getAuthTokenWithPatch,
  getDepthData,
  getUser,
  signOut
} from "services/http/http";
import "./style/index.scss";
import TradeConfirmPanel from "components/TicketForm/TradeConfirmPanel";
import TradeConfirmResult from "components/TicketForm/TradeConfirmResult";
import _get from "lodash/get";
import _isNumber from "lodash/isNumber";
import { injectIntl, WrappedComponentProps } from "react-intl";
import _flowRight from "lodash/flowRight";
import { getCurrentMonth } from "utils/helpers";
import { WEB_APP_URL, WEBAPP_ROUTES } from "config";
import { deleteCookie } from "../../utils/cookie";
import { removeItemFromLocalStorageState } from "../../utils/localStorage";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TProps = TStateProps &
  TDispatchProps &
  ReactCookieProps &
  WrappedComponentProps;

interface IState {
  isMobile: boolean;
  allAssets: [];
  allMarkets: [];
  baseTicket: IAsset | {};
  quoteTicket?: IAsset | {};
  tabPage: string;
  direction: TDirection;
  isTVWidgetVisible: boolean;
  tabs: ITab[];
}

export class Home extends Component<TProps, IState> {
  private isInited = false;
  private baseTicket?: IAsset;
  private quoteTicket?: IAsset;
  private isLogin = false;
  private sentSockets: {
    base?: number;
    counter?: number;
    quote?: number;
  } = {};
  private userAuthentificateParams?: IUserAuthentificateParams;

  setLoginState = async (e?: boolean) => {
    const {
      setUser,
      setWebsocketAuthParams,
      authToken,
      setAuthToken,
      setPanelConfirm
    } = this.props;

    try {
      if (e) {
        this.isLogin = true;
        this.loginCallback();
        return;
      }
      if (authToken) {
        const userDetails = await getUser(authToken);
        this.isLogin = true;
        const { core_id, priv_key, api_key } = userDetails;
        setUser(userDetails);
        setWebsocketAuthParams({ core_id, priv_key, api_key });
        this.loginCallback();
        setPanelConfirm({ panelType: "panel_trade" });
      }
    } catch (e) {
      if (e.err === 5) {
        // TOKEN_EXPIRED
        const newAuthToken: any = await getAuthTokenWithPatch(authToken);
        setAuthToken(newAuthToken);
      }
      this.logoutHandler();
    }
  };

  loginCallback = () => {
    const {
      getBorrowOffers,
      getBorrowLoans,
      getPositions,
      getBalances,
      getOrders,
      setLogin,
      getBorrowCollaterals,
      getBorrowConvertedTotals,
      setPanelConfirm,
      websocketAuthParams,
      getHistoryTrade
    } = this.props;

    const authParams: IAuthParams = getAuthParams(websocketAuthParams);
    coinflexEventstream.init(authParams);
    setLogin(true);
    getBorrowOffers();
    getBorrowLoans();
    getPositions();
    getBalances();
    getOrders();
    getBorrowConvertedTotals();
    getBorrowCollaterals();
    getHistoryTrade();
    setPanelConfirm({ panelType: "panel_trade" });
  };

  initMarket = () => {
    this.sendRequest(false);
    const { assetsList, marketList } = this.props;
    this.isInited = true;
    const url = window.location.pathname;
    const urlArr = url.split("/");
    const symbol = urlArr[urlArr.length - 1];
    let market = "";
    if (symbol && symbol.indexOf("_") > -1) {
      market = symbol.toUpperCase();
    } else {
      market = this.getDefaultMarket(marketList);
    }
    const baseName = market.split("_")[0].toUpperCase();
    const quoteName = market.split("_")[1].toUpperCase();
    this.baseTicket = assetsList.find(i => i.name.includes(baseName));
    this.quoteTicket = assetsList.find(i => i.name.includes(quoteName));
    const baseTicketID = _get(this, "baseTicket.id");
    const quoteTicketID = _get(this, "quoteTicket.id");
    const validBaseQuotePair = [baseTicketID, quoteTicketID].every(_isNumber);
    if (this.baseTicket && this.quoteTicket && validBaseQuotePair) {
      this.getDepthData(this.baseTicket.id, this.quoteTicket.id);
      const spotBaseName = this.baseTicket.spot_name
        ? this.baseTicket.spot_name
        : this.baseTicket.name;
      const spotQuoteName = this.quoteTicket.spot_name
        ? this.quoteTicket.spot_name
        : this.quoteTicket.name;
      const curMarket = marketList.filter(
        item =>
          this.baseTicket &&
          this.quoteTicket &&
          item.base === this.baseTicket.id &&
          item.counter === this.quoteTicket.id
      )[0];
      const month = this.baseTicket.spot_name
        ? this.baseTicket.name.split(this.baseTicket.spot_name)[1]
        : "";
      const year =
        curMarket && curMarket.tenor ? curMarket.tenor.slice(1, 3) : "";
      const min = curMarket && curMarket.tick ? curMarket.tick / 10000 : 0;
      const symbolTitle =
        spotBaseName +
        "/" +
        spotQuoteName +
        " " +
        month.substring(0, 1) +
        month.substring(1).toLowerCase() +
        " " +
        year;
      if (!this.baseTicket.spot_name) this.baseTicket.spot_name = spotBaseName;
      if (!this.quoteTicket.spot_name)
        this.quoteTicket.spot_name = spotQuoteName;
      this.props.setCurrentSymbol({
        base: this.baseTicket,
        quote: this.quoteTicket,
        symbolTitle,
        min
      });
    }
  };

  logoutHandler = async () => {
    const {
      setPanelConfirm,
      setLogin,
      setUser,
      setWebsocketAuthParams,
      setAuthToken,
      authToken
    } = this.props;

    try {
      await signOut(authToken);
      this.isLogin = false;
      deleteCookie("access_token");
      removeItemFromLocalStorageState("authToken");
      setUser(null);
      setWebsocketAuthParams(null);
      setAuthToken(null);
      setLogin(false);
      setPanelConfirm({ panelType: "login" });
      window.location.href = WEB_APP_URL + WEBAPP_ROUTES.SIGN_OUT;
    } catch (e) {
      console.log("Could not log out", e);
    }
  };

  getDefaultMarket = (markets: IMarket[]) => {
    const defaultMarketName = "XBT_USDT";
    const XbtList = markets.filter(item => item.name.indexOf("XBT") !== -1);
    const currentMonth = getCurrentMonth();
    const latestMarket = XbtList.find(
      item => item.name.indexOf(currentMonth) !== -1
    );
    if (!latestMarket) return defaultMarketName;
    return latestMarket.name.split("/").join("_");
  };

  sendRequest = (watch: boolean) => {
    if (!this.baseTicket || !this.quoteTicket) return;
    const baseId = this.baseTicket.id;
    const quoteId = this.quoteTicket.id;
    if (!baseId || !quoteId) return;
    if (!watch) {
      const data = {
        method: "WatchOrders",
        base: this.sentSockets.base,
        counter: this.sentSockets.quote,
        watch: watch,
        tag: 2
      };
      const data2 = {
        method: "WatchTicker",
        base: this.sentSockets.base,
        counter: this.sentSockets.quote,
        watch: watch,
        tag: 3
      };
      coinflexWebsocket.send(data, (event: IOrderBookList) => {
        if (event.error_code === 0) {
          this.props.setOrderBookList({
            ...event,
            base: this.sentSockets.base
          });
        }
      });
      coinflexWebsocket.send(data2, (event: ILastTradeData) => {
        if (event.error_code === 0) {
          this.props.setLastTrade({ ...event, base: this.sentSockets.base! });
        }
      });
    } else {
      if (
        this.sentSockets.base === baseId &&
        this.sentSockets.quote === quoteId
      ) {
        return;
      } else {
        this.sentSockets.base = baseId;
        this.sentSockets.quote = quoteId;
        const data = {
          method: "WatchOrders",
          base: this.sentSockets.base,
          counter: this.sentSockets.quote,
          watch: watch,
          tag: 2
        };
        const data2 = {
          method: "WatchTicker",
          base: this.sentSockets.base,
          counter: this.sentSockets.quote,
          watch: watch,
          tag: 3
        };
        coinflexWebsocket.send(data, (event: IOrderBookList) => {
          if (event.error_code === 0) {
            this.props.setOrderBookData(event);
            this.props.setOrderBookList({
              ...event,
              base: this.sentSockets.base
            });
          }
        });
        coinflexWebsocket.send(data2, (event: ILastTradeData) => {
          if (event.error_code === 0) {
            this.props.setLastTrade({ ...event, base: this.sentSockets.base! });
          }
        });
      }
    }
  };

  onChangeTabs = (e: { key?: string }) => {
    this.props.setTopTabs(e.key as TTopTabType);
  };

  jumpToTrade = (e: TDirection) => {
    this.props.setTopTabs("TRADE");
    this.setState({
      direction: e
    });
  };

  openTVWidget = () => {
    this.setState({
      isTVWidgetVisible: true
    });
  };

  closeTVWidget = () => {
    this.setState({
      isTVWidgetVisible: false
    });
  };

  getDepthData = (baseId: number, quoteId: number) => {
    getDepthData(baseId + ":" + quoteId).then(res => {
      const asks = res.asks || [];
      const bids = res.bids || [];
      this.props.setAskData(asks);
      this.props.setBidData(bids);
    });
  };

  authenticateUser = (e: IUserAuthentificateParams) => {
    const { serverNonce } = this.props;

    this.userAuthentificateParams = e;

    if (serverNonce) {
      authenticateUser(e, serverNonce);
      this.userAuthentificateParams = undefined;
    }
  };

  getMobileComponent = () => {
    let {
      panelConfirm: { panelType },
      topTabsType
    } = this.props;
    const { direction } = this.state;
    switch (topTabsType) {
      case "CHART":
        return <iotView />;
      case "TRADE":
        switch (panelType) {
          case "panel_trade":
            return <TicketForm direction={direction} />;
          case "panel_trade_confirm":
            return <TradeConfirmPanel />;
          case "panel_trade_confirm_result":
            return <TradeConfirmResult />;
          case "login":
            return <Login />;
          default:
            return "";
        }
      case "POSITIONS":
        return <Blotter />;
      case "ORDERS":
        return <OrderBook />;
      case "RECENT":
        return <RecentTrade />;
      default:
        return <iotView />;
    }
  };

  setLocale = () => {
    const preferedLocale = getPreferedLocale(this.props.i18n.locale);
    this.props.getI18n(preferedLocale);
  };

  resize = () => {
    this.setState({ isMobile: isMobile() });
    this.props.setMobilePage(isMobile());
  };

  componentWillMount() {
    const { intl, setMobilePage } = this.props;
    this.setState({
      isMobile: isMobile(),
      allAssets: [],
      allMarkets: [],
      baseTicket: {},
      quoteTicket: {},
      tabPage: "1",
      direction: "1",
      isTVWidgetVisible: false,
      tabs: [
        {
          title: intl.formatMessage({ defaultMessage: "POSITIONS" }),
          key: "POSITIONS"
        },
        {
          title: intl.formatMessage({ defaultMessage: "TRADE" }),
          key: "TRADE"
        },
        {
          title: intl.formatMessage({ defaultMessage: "CHART" }),
          key: "CHART"
        },
        {
          title: intl.formatMessage({ defaultMessage: "BOOK" }),
          key: "ORDERS"
        },
        {
          title: intl.formatMessage({ defaultMessage: "RECENT" }),
          key: "RECENT"
        }
      ]
    });
    setMobilePage(isMobile());
    window.addEventListener("resize", this.resize);
  }

  componentDidMount() {
    const {
      getMarketList,
      getAssetsList,
      getChangeList,
      getTickersList
    } = this.props;
    getMarketList();
    getAssetsList();
    getChangeList();
    getTickersList();
    this.setLoginState();
    this.setLocale();
  }

  componentDidUpdate(prevProps: TProps) {
    const { isLogin, selectedMarket, websocketAuthParams } = this.props;
    if (isLogin && !prevProps.isLogin && !this.isLogin) {
      this.setLoginState(isLogin);
    }
    if (
      !prevProps.serverNonce &&
      this.props.serverNonce &&
      this.userAuthentificateParams
    ) {
      this.authenticateUser(this.userAuthentificateParams);
    }
    if (selectedMarket !== prevProps.selectedMarket) {
      this.initMarket();
    }
    if (websocketAuthParams !== prevProps.websocketAuthParams) {
      this.authenticateUser(websocketAuthParams);
    }
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resize);
  }

  render() {
    const { marketList, assetsList, topTabsType } = this.props;
    const { isMobile, tabs, direction, isTVWidgetVisible } = this.state;
    if (assetsList.length > 0 && marketList.length > 0 && !this.isInited) {
      this.initMarket();
    }
    this.sendRequest(true);
    return (
      <div className="home">
        {!isMobile ? (
          <div className="page">
            <div className="header-wrapper">
              <Header
                openTVWidget={this.openTVWidget}
                logoutHandler={this.logoutHandler}
              />
            </div>
            <div className="scroll-wrapper container">
              <div className="grid">
                <div className="item item-markets">
                  <div className="markets-wrapper">{<Market />}</div>
                </div>
                <div className="item item-trade-ticket">
                  <div className="module-bg overflowHidden">
                    <TicketForm direction={direction} />
                  </div>
                </div>
                <div className="item item-chart">
                  <div className="module-bg overflowHidden">
                    {<iotView />}
                  </div>
                </div>
                <div className="item item-order-book">
                  <div className="module-bg overflowHidden">
                    {<OrderBook />}
                  </div>
                </div>
                <div className="item item-recent">
                  <div className="module-bg overflowHidden">
                    {<RecentTrade />}
                  </div>
                </div>
                <div className="item item-blotter">
                  <div className="module-bg overflowHidden">{<Blotter />}</div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="mobile-page">
            {topTabsType !== "TRADE" && (
              <BottomBtns jumpToTrade={this.jumpToTrade.bind(this)} />
            )}
            <div className="market-wrapper">
              <Market />
            </div>
            <div
              className={
                topTabsType === "TRADE"
                  ? "tab-content"
                  : "tab-content tab-content-hasbottom"
              }
            >
              <div className="item">
                <div className="module-bg overflowHidden">
                  {this.getMobileComponent()}
                </div>
              </div>
            </div>
            <div className="top-header-bar">
              <Tabs
                tabs={tabs}
                page={topTabsType}
                tabBarBackgroundColor="none"
                tabBarUnderlineStyle={{
                  zIndex: 2,
                  borderColor: "#4C8CB8",
                  width: 18,
                  marginLeft: "calc(10% - 9px)",
                  boxShadow: "0 0 8px 5px rgba(255, 255, 255, 0.2)"
                }}
                tabBarInactiveTextColor="#474748"
                tabBarActiveTextColor="#ffffff"
                onChange={this.onChangeTabs}
              />
            </div>
          </div>
        )}
        <TVWidget visible={isTVWidgetVisible} onClose={this.closeTVWidget} />
      </div>
    );
  }
}

const mapStateToProps = (state: IAppState) => {
  return {
    currentSymbol: state.currentSymbol,
    marketList: state.marketList,
    assetsList: state.assetsList,
    isLogin: state.isLogin,
    topTabsType: state.topTabsType,
    panelConfirm: state.panelConfirm,
    serverNonce: state.serverNonce,
    i18n: state.i18n,
    selectedMarket: state.selectedMarket,
    websocketAuthParams: state.websocketAuthParams,
    authToken: state.authToken
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setMobilePage(data: boolean) {
      dispatch(setMobilePage(data));
    },
    setCurrentSymbol(data: ICurrentSymbol) {
      dispatch(setCurrentSymbol(data));
    },
    getMarketList() {
      dispatch(getMarketList());
    },
    getAssetsList() {
      dispatch(getAssetsList());
    },
    setOrderBookList(data: IOrderBookList) {
      dispatch(setOrderBookList(data));
    },
    setLastTrade(data: ILastTradeData) {
      dispatch(setLastTrade(data));
    },
    getChangeList() {
      dispatch(getChangeList());
    },
    getTickersList() {
      dispatch(getTickersList());
    },
    getBorrowOffers() {
      dispatch(getBorrowOffers());
    },
    getBorrowLoans() {
      dispatch(getBorrowLoans());
    },
    getPositions() {
      dispatch(getPositions());
    },
    getBalances() {
      dispatch(getBalances());
    },
    getOrders() {
      dispatch(getOrders());
    },
    setLogin(data: boolean) {
      dispatch(setLogin(data));
    },
    getBorrowCollaterals() {
      dispatch(getBorrowCollaterals());
    },
    getBorrowConvertedTotals() {
      dispatch(getBorrowConvertedTotals());
    },
    getHistoryTrade() {
      dispatch(getHistoryTrade());
    },
    setPanelConfirm(data: IPanelConfirm) {
      dispatch(setPanelConfirm(data));
    },
    setTopTabs(data: TTopTabType) {
      dispatch(setTopTabs(data));
    },
    getI18n(data: string) {
      dispatch(getI18n(data));
    },
    setOrderBookData(data: ILastTradeData | IOrdersMatched | IOrderBookList) {
      dispatch(setOrderBookData(data));
    },
    setAskData(data: TDepthData) {
      dispatch(setAskData(data));
    },
    setBidData(data: TDepthData) {
      dispatch(setBidData(data));
    },
    setUser(user: IUser | null) {
      dispatch(setUser(user));
    },
    setAuthToken(token: IAuthToken | null) {
      dispatch(setAuthToken(token));
    },
    setWebsocketAuthParams(data: IUserAuthentificateParams | null) {
      dispatch(setWebsocketAuthParams(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(withCookies(Home));
