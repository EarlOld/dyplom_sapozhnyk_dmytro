import React from "react";
import { action } from "@storybook/addon-actions";
import { localesList } from "utils/i18n";
import LanguageDropdown from "./LanguageDropdown";

export default {
  title: "LanguageDropdown"
};

export const Default = () => (
  <div className="header flex flex-center">
    <div className="flex right-part">
      <LanguageDropdown
        value={localesList[0]}
        onChange={action("onChange")}
        options={localesList}
      />
    </div>
  </div>
);

Default.story = {
  name: "default"
};
