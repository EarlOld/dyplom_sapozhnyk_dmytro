import React from "react";
import { fireEvent, waitForElement } from "@testing-library/react";
import { render } from "utils/testUtils";
import { localesList } from "utils/i18n";
import LanguageDropdown from "./LanguageDropdown";

describe("LanguageDropdown", () => {
  const handleChange = jest.fn();

  const TestLanguageDropdown = () => (
    <LanguageDropdown
      value={localesList[0]}
      onChange={handleChange}
      options={localesList}
    />
  );

  it("Should match snapshot", () => {
    const { container } = render(<TestLanguageDropdown />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should display selected language", async () => {
    const { getByLabelText } = render(<TestLanguageDropdown />);
    const selectedLanguage = getByLabelText(localesList[0].code);
    expect(selectedLanguage).toBeInTheDocument();
  });

  it("Should fire callback when language is changed", async () => {
    const { getAllByText, container } = render(<TestLanguageDropdown />);
    fireEvent.click(container.firstChild);
    const languageItem = await waitForElement(
      () => getAllByText(localesList[0].nativeTitle)[1]
    );
    expect(languageItem).toBeInTheDocument();
    fireEvent.click(languageItem);
    expect(handleChange).toHaveBeenCalledWith(localesList[0]);
  });
});
