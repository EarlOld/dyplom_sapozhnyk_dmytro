import React, { Component } from "react";
import { Dropdown, Menu } from "antd";
import { ClickParam } from "antd/es/menu";
import clsx from "clsx";
import "components/Header/style/index.scss";

interface TProps {
  value: ILocale;
  onChange: (arg: ILocale) => void;
  options: ILocale[];
  isTopNavBar?: boolean;
}

class LanguageDropdown extends Component<TProps> {
  handleClick = (item: ClickParam) => {
    const { options, onChange } = this.props;
    const newValue = options.find(one => one.code === item.key);
    if (newValue) onChange(newValue);
  };

  render() {
    const { value, options, isTopNavBar } = this.props;
    const dropDownClasses = isTopNavBar
      ? "icon-button language-dropdown top-nav-bar"
      : "icon-button language-dropdown";
    return (
      <Dropdown
        trigger={["click"]}
        overlay={
          <Menu onClick={this.handleClick} className="language-menu">
            {options.map(one => (
              <Menu.Item
                key={one.code}
                className={clsx(
                  "language-item",
                  one.code === value.code && "language-selected"
                )}
              >
                <span role="img" aria-label={one.code}>
                  {one.nativeTitle}
                </span>
              </Menu.Item>
            ))}
          </Menu>
        }
      >
        {/* <div className="icon-button language-dropdown"> */}
        <div className={dropDownClasses}>
          <span role="img" aria-label={value.code}>
            {value.nativeTitle}
          </span>
        </div>
      </Dropdown>
    );
  }
}

export default LanguageDropdown;
