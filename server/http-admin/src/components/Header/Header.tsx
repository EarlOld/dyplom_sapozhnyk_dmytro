import React, { Component } from "react";
import { connect } from "react-redux";
import { Button, Icon } from "antd";
import { localesList, localesMap } from "utils/i18n";
import DashboardDropDown from "components/Header/DashboardDropdown";
import LanguageDropdown from "components/Header/LanguageDropdown";
import logoWhitePng from "assets/images/icons/logo_white.png";
import { ReactComponent as HeadphonesSvg } from "assets/icons/headphones.svg";
import walletPng from "assets/images/icons/wallet.png";
import clsx from "clsx";
import { ReactComponent as AppsSvg } from "assets/icons/apps.svg";
import { ReactComponent as TvSvg } from "assets/icons/tv.svg";
import "components/Header/style/index.scss";
import { getI18n } from "store/actions";
import { FormattedMessage, injectIntl } from "react-intl";
import _flowRight from "lodash/flowRight";
import { PUBLIC_URL } from "config";
import { withRouter, RouteComponentProps, Link } from "react-router-dom";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

interface IOwnProps {
  openTVWidget: () => void;
  logoutHandler: () => void;
}

interface RouteInfo {
  chatId?: string;
}

type TProps = TStateProps &
  TDispatchProps &
  IOwnProps &
  RouteComponentProps<RouteInfo>;

interface IState {
  profit: string;
  showDetail: boolean;
  showDashboard: boolean;
}

export class Header extends Component<TProps, IState> {
  constructor(props: TProps) {
    super(props);
    this.state = {
      profit: "",
      showDetail: false,
      showDashboard: false
    };
  }

  handleDashboard = () => {
    this.setState({
      showDashboard: !this.state.showDashboard,
      showDetail: false
    });
  };

  closeDashboard = () => {
    this.setState({
      showDashboard: false
    });
  };

  handleChangeLanguage = (newLocale: ILocale) => {
    this.props.getI18n(newLocale.code);
  };

  render() {
    const { isLogin, openTVWidget, i18n } = this.props;
    const selectedLocale = localesMap[i18n.locale] || localesList[0];
    return (
      <div className="header flex flex-content-between">
        <div className="logo">
          <img src={logoWhitePng} alt={"white logo"} />
        </div>
        {isLogin ? (
          <>
            <div className="flex right-part">
              <LanguageDropdown
                value={selectedLocale}
                onChange={this.handleChangeLanguage}
                options={localesList}
              />
              <div
                className="icon-button"
                onClick={() => this.handleDashboard()}
              >
                <Icon component={AppsSvg} />
              </div>
              <div
                className="icon-button"
                onClick={openTVWidget}
                data-testid="tv-widget"
              >
                <Icon component={TvSvg} />
              </div>
              <div
                className="icon-button"
                onClick={() =>
                  (window as any).open(
                    `${PUBLIC_URL}/pit`,
                    "_blank",
                    "resizable=yes, scrollbars=no, titlebar=yes, width=600, height=600, top=10, left=10"
                  )
                }
              >
                <Icon component={HeadphonesSvg} />
              </div>
              <Link to="/dashboard">
                <div
                  className={clsx(
                    "flex",
                    "balance-info",
                    this.state.showDetail && "active"
                  )}
                >
                  <img src={walletPng} alt={"wallet"} />
                </div>
              </Link>
              <div className="logout">
                <Button
                  type="link"
                  className="flex flex-align-center"
                  onClick={this.props.logoutHandler}
                >
                  <span>
                    <FormattedMessage defaultMessage="Logout" />
                  </span>
                  <i className="icon-logout" />
                </Button>
              </div>
            </div>
            <DashboardDropDown
              show={this.state.showDashboard}
              onClose={this.closeDashboard}
            />
          </>
        ) : (
          <>
            <div className="flex right-part">
              <LanguageDropdown
                value={selectedLocale}
                onChange={this.handleChangeLanguage}
                options={localesList}
              />
              <div className="icon-button" onClick={openTVWidget}>
                <Icon component={TvSvg} />
              </div>
            </div>
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    isLogin: state.isLogin,
    i18n: state.i18n
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    getI18n(data: string) {
      dispatch(getI18n(data));
    }
  };
};

export default _flowRight(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(Header);
