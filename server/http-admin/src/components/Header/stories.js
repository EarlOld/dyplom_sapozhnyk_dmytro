import React from "react";
import { action } from "@storybook/addon-actions";
import { wrapWithMockRedux } from "utils/testUtils";
import Header from "./Header";

export default {
  title: "Header",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <Header
    openTVWidget={action("Open TV Widget!")}
    logoutHandler={action("Logout!")}
  />
);

Default.story = {
  name: "default"
};
