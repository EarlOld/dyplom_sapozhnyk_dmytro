import React from "react";
import { fireEvent } from "@testing-library/react";
import { render } from "utils/testUtils";
import DashboardDropdown from "./DashboardDropdown";

describe("Dashboard Dropdown", () => {
  it("Should match snapshot", () => {
    const { container } = render(<DashboardDropdown />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should fire close handler when link is clicked", () => {
    const handleClose = jest.fn();
    const { getByText } = render(<DashboardDropdown onClose={handleClose} />);
    const link = getByText("FLEX Coin");
    expect(link).toBeInTheDocument();
    fireEvent.click(link);
    expect(handleClose).toHaveBeenCalled();
  });

  it("Should fire close handler when backdrop is clicked", () => {
    const handleClose = jest.fn();
    const { getByTestId } = render(<DashboardDropdown onClose={handleClose} />);
    const backdrop = getByTestId("backdrop");
    expect(backdrop).toBeInTheDocument();
    fireEvent.click(backdrop);
    expect(handleClose).toHaveBeenCalled();
  });

  it("Should fire close handler when close icon is clicked", () => {
    const handleClose = jest.fn();
    const { getByTestId } = render(<DashboardDropdown onClose={handleClose} />);
    const close = getByTestId("close");
    expect(close).toBeInTheDocument();
    fireEvent.click(close);
    expect(handleClose).toHaveBeenCalled();
  });

  // it("Should fire the success callback when Button is clicked", () => {
  //   const { getByText } = render(<TestButton />);
  //   const doneButton = getByText("My Button");
  //   expect(doneButton).toBeInTheDocument();
  //   fireEvent.click(doneButton);
  //   expect(handleWin).toHaveBeenCalled();
  // });
});
