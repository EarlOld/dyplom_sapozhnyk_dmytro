import React from "react";
import { action } from "@storybook/addon-actions";
import DashboardDropdown from "./DashboardDropdown";

export default {
  title: "DashboardDropdown"
};

export const Default = () => (
  <DashboardDropdown show={true} onClose={action("onClose")} />
);

Default.story = {
  name: "default"
};
