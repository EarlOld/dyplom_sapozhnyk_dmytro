import React, { Component } from "react";
import { Icon } from "antd";
import "components/Header/style/index.scss";
import { getCurrentUrl } from "utils/helpers";
import { ReactComponent as BracketOrderBattleSvg } from "assets/icons/bracket-order-battle.svg";
import { ReactComponent as WalletSvg } from "assets/icons/wallet.svg";
import { ReactComponent as WithdrawSvg } from "assets/icons/withdraw.svg";
import { ReactComponent as AccountingSvg } from "assets/icons/accounting.svg";
import { ReactComponent as AffiliateSvg } from "assets/icons/affiliate.svg";
import { ReactComponent as ApiSvg } from "assets/icons/api.svg";
import { injectIntl, WrappedComponentProps } from "react-intl";

interface IOwnProps {
  show: boolean;
  onClose: () => void;
}

class DashboardDropdown extends Component<IOwnProps & WrappedComponentProps> {
  get externalLinks() {
    const { intl } = this.props;
    return [
      {
        name: intl.formatMessage({ defaultMessage: "Bracket Order Battle" }),
        SvgCmp: BracketOrderBattleSvg,
        path: "users_bracket_competitions"
      },
      {
        name: intl.formatMessage({ defaultMessage: "FLEX Staking Lower Fees" }),
        SvgCmp: WalletSvg,
        path: "flex_coins/stakings"
      },
      {
        name: intl.formatMessage({ defaultMessage: "FLEX Coin" }),
        SvgCmp: WalletSvg,
        path: "flex_coins/redemption"
      },
      {
        name: intl.formatMessage({ defaultMessage: "Deposit" }),
        SvgCmp: WalletSvg,
        path: "deposit"
      },
      {
        name: intl.formatMessage({ defaultMessage: "Withdraw" }),
        SvgCmp: WithdrawSvg,
        path: "withdraw"
      },
      {
        name: intl.formatMessage({ defaultMessage: "Balances" }),
        SvgCmp: AccountingSvg,
        path: "balances"
      },
      {
        name: intl.formatMessage({ defaultMessage: "Accounts History" }),
        SvgCmp: WalletSvg,
        path: "history"
      },
      {
        name: intl.formatMessage({ defaultMessage: "Trade History" }),
        SvgCmp: WalletSvg,
        path: "trade_history"
      },
      {
        name: intl.formatMessage({ defaultMessage: "Affiliate Program" }),
        SvgCmp: AffiliateSvg,
        path: "affiliates"
      },
      {
        name: intl.formatMessage({ defaultMessage: "Conversions" }),
        SvgCmp: WalletSvg,
        path: "conversions"
      },
      {
        name: intl.formatMessage({ defaultMessage: "API Details" }),
        SvgCmp: ApiSvg,
        path: "api_access"
      },
      {
        name: intl.formatMessage({ defaultMessage: "Enable Stablecoin" }),
        SvgCmp: WalletSvg,
        path: "my_profile"
      }
    ];
  }
  render() {
    const { show, onClose } = this.props;
    return (
      <div className="dashboard-dropdown" style={!show ? { height: 0 } : {}}>
        <div
          data-testid="backdrop"
          className="dashboard-backdrop"
          onClick={onClose}
        />
        <div className="dashboard-container">
          <div className="links-container">
            {this.externalLinks.map(({ name, SvgCmp, path }) => {
              const url = getCurrentUrl(path);
              return (
                <a
                  key={name}
                  onClick={onClose}
                  className="link"
                  href={url}
                  target="_blank"
                  rel="noreferrer noopener"
                >
                  <Icon className="link-icon" component={SvgCmp} />
                  <div className="link-name">{name}</div>
                </a>
              );
            })}
          </div>
        </div>
        <Icon
          data-testid="close"
          onClick={onClose}
          className="close-button"
          type="close"
        />
      </div>
    );
  }
}

export default injectIntl(DashboardDropdown);
