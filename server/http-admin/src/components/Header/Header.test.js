import React from "react";
import { fireEvent } from "@testing-library/react";
import { renderWithMockRedux } from "utils/testUtils";
import Header from "./Header";

describe("Header", () => {
  const handleOpenTVWidget = jest.fn();
  const handleLogoutHandler = jest.fn();

  const TestHeader = () => (
    <Header
      openTVWidget={handleOpenTVWidget}
      logoutHandler={handleLogoutHandler}
    />
  );

  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<TestHeader />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should fire callback when opening TV Widget", () => {
    const { getByTestId } = renderWithMockRedux(<TestHeader />);
    const tvWigetButton = getByTestId("tv-widget");
    expect(tvWigetButton).toBeInTheDocument();
    fireEvent.click(tvWigetButton);
    expect(handleOpenTVWidget).toHaveBeenCalled();
  });

  it("Should fire callback when logging out", () => {
    const { getByText } = renderWithMockRedux(<TestHeader />);
    const logoutButton = getByText("Logout");
    expect(logoutButton).toBeInTheDocument();
    fireEvent.click(logoutButton);
    expect(handleLogoutHandler).toHaveBeenCalled();
  });
});
