import React, { memo, ReactNode } from "react";
import { Card, Icon, Tab, Tabs } from "@blueprintjs/core";

import "./styles.scss";
import { useHistory } from "react-router-dom";

interface IOwnProps {
  title?: string;
  tabs?: { id: string; title: string }[];
  tabsId?: string;
  onTabsChange?: (id: string) => void;
  children?: ReactNode;
  footer?: ReactNode;
  link?: string;
}

export default memo((props: IOwnProps) => {
  const history = useHistory();
  return (
    <Card className="balances" tabIndex={1}>
      <div className="bp3-card-header">
        <div>
          {props.title && <span className="bp3-card-title">{props.title}</span>}
          {props.tabs && (
            <Tabs
              onChange={props.onTabsChange}
              animate={false}
              id={props.tabsId}
            >
              {props.tabs.map(tab => (
                <Tab id={tab.id} key={tab.id} title={tab.title} />
              ))}
            </Tabs>
          )}
        </div>
        {props.link && (
          <Icon
            icon="circle-arrow-right"
            iconSize={23}
            className="right-icon"
            onClick={() => props.link && history.push(props.link)}
          />
        )}
      </div>
      <div className="bp3-card-content">{props.children}</div>
      {props.footer && <div className="bp3-card-footer">{props.footer}</div>}
    </Card>
  );
});
