import React from "react";
import Pane from "./Pane";

export default {
  title: "Dashboard Pane"
};

const tabs = [
  {
    id: "spot",
    title: "spot"
  },
  {
    id: "futures",
    title: "Futures"
  },
  {
    id: "leverage",
    title: "Leverage"
  }
];

export const Default = () => (
  <div className="bp3-dark">
    <Pane title="Balances" tabs={tabs}>
      Content
    </Pane>
  </div>
);

Default.story = {
  name: "default"
};
