import React from "react";
import { Button, InputGroup, Tabs, Tab } from "@blueprintjs/core";

import ControlsGroup from "./ControlsGroup";

export default {
  title: "ControlsGroup"
};

export const Default = () => (
  <div className="bp3-dark">
    <ControlsGroup large={true}>
      <InputGroup placeholder="Input" />
      <Button>Button 1</Button>
      <Button>Button 2</Button>
      <Tabs animate={false}>
        <Tab id="tab1" title="Tab 1" />
        <Tab id="tab2" title="Tab 2" />
        <Tab id="tab3" title="Tab 3" />
      </Tabs>
    </ControlsGroup>
  </div>
);

export const ButtonsOnly = () => (
  <div className="bp3-dark">
    <ControlsGroup large={true}>
      <Button>Level 1</Button>
      <Button>Level 2</Button>
      <Button>Level 3</Button>
      <Button>Level 4</Button>
      <Button>Level 5</Button>
      <Button>Level 6</Button>
    </ControlsGroup>
  </div>
);

export const TabsOnly = () => (
  <div className="bp3-dark">
    <ControlsGroup large={true}>
      <Tabs animate={false}>
        <Tab id="tab1" title="Tab 1" />
        <Tab id="tab2" title="Tab 2" />
        <Tab id="tab3" title="Tab 3" />
        <Tab id="tab4" title="Tab 4" />
        <Tab id="tab5" title="Tab 5" />
      </Tabs>
    </ControlsGroup>
  </div>
);

Default.story = {
  name: "default"
};

ButtonsOnly.story = {
  name: "Buttons Only"
};

TabsOnly.story = {
  name: "Tabs Only"
};
