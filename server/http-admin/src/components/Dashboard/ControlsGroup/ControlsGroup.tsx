import React, { memo, ReactNode } from "react";

import "./styles.scss";

export default memo(
  ({ children, large }: { children: ReactNode; large?: boolean }) => (
    <div className={`controls-group ${large ? "large" : ""}`}>{children}</div>
  )
);
