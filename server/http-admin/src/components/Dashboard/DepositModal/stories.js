import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import DepositModal from "components/Dashboard/DepositModal/DepositModal";

export default {
  title: "DepositModal",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <DepositModal isOpen={true} assetId={63488} />;

Default.story = {
  name: "Open DepositModal"
};
