import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import DepositModal from "components/Dashboard/DepositModal/DepositModal";

describe("DepositModal tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<DepositModal isOpen />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
