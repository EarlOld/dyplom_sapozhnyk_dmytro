import React, { useMemo } from "react";
import { dialogOptions } from "./data";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import { IDialogOptions } from "./types";
import {
  Dialog,
  Classes,
  InputGroup,
  Button,
  Elevation,
  Card,
  Intent,
  H5,
  H2
} from "@blueprintjs/core";
import QRCode from "qrcode.react";
import {
  StyledContainer,
  StyledQRCodeContainer,
  StyledModalElement,
  StyledModalElementInline,
  StyledModalElementInlineTwo,
  StyledModalElementCard
} from "./styles";
import Clipboard from "react-clipboard.js";
import _flowRight from "lodash/flowRight";
import _get from "lodash/get";
import { connect } from "react-redux";
import { pushNotification } from "store/actions";
import { isValidAddress } from "../utils";

type TOwnProps = {
  assetId: number;
  isOpen: boolean;
  onClose: () => void;
};
type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TProps = WrappedComponentProps & TOwnProps & TStateProps & TDispatchProps;
const options: IDialogOptions = dialogOptions;
export const DepositModal = ({
  intl,
  userAssets,
  assetId,
  isOpen,
  onClose = () => {},
  pushNotification
}: TProps) => {
  const userAsset: IUserAsset | undefined = useMemo(
    () =>
      (userAssets &&
        userAssets.find(
          (userAsset: IUserAsset) => userAsset.asset.corecode === assetId
        )) ||
      undefined,
    [userAssets, assetId]
  );

  const address: string | null = _get(userAsset, "deposit_address", "");

  const validAddress: boolean = useMemo(() => {
    return !!address ? isValidAddress(address, assetId) : false;
  }, [assetId, address]);

  if (userAsset) {
    return (
      <>
        <Dialog
          className={Classes.DARK}
          onClose={onClose}
          title={`${userAsset.asset && userAsset.asset.name} Deposit`}
          {...options}
          isOpen={isOpen}
        >
          <div className={Classes.DIALOG_BODY}>
            <StyledContainer>
              {address ? (
                <>
                  <StyledQRCodeContainer>
                    {userAsset && validAddress && (
                      <QRCode size={200} value={address} />
                    )}
                  </StyledQRCodeContainer>
                  <StyledModalElement>
                    <H5 className="bp3-heading">
                      {userAsset.asset && userAsset.asset.name}
                      <FormattedMessage defaultMessage={" Deposit Address"} />
                    </H5>
                    {userAsset && validAddress && (
                      <Clipboard
                        data-clipboard-text={address}
                        onSuccess={() =>
                          pushNotification({
                            type: "SUCCESS",
                            message: intl.formatMessage({
                              defaultMessage: "Copied to clipboard",
                              id: "clipboard-copy"
                            })
                          })
                        }
                        component="span"
                      >
                        <Card interactive={true} elevation={Elevation.ONE}>
                          <InputGroup
                            large={true}
                            placeholder={address}
                            readOnly
                          />
                          <Button intent={Intent.PRIMARY}>
                            <FormattedMessage defaultMessage={"Copy"} />
                          </Button>
                        </Card>
                      </Clipboard>
                    )}
                  </StyledModalElement>
                </>
              ) : (
                <H2 className="bp3-heading">
                  <FormattedMessage defaultMessage="Please wait while we generate a deposit address. You will be notified when it is ready" />
                </H2>
              )}
              <StyledModalElementInline>
                <H5 className="bp3-heading">
                  <FormattedMessage defaultMessage={"Min Deposit"} />
                </H5>
                <H5 className="bp3-heading">
                  <FormattedMessage defaultMessage={"Deposit Fee"} />
                </H5>
              </StyledModalElementInline>
              <StyledModalElementInlineTwo>
                <label className="bp3-text-muted">
                  {userAsset.asset?.min_deposit + " " + userAsset.asset?.name}
                </label>
                <label className="bp3-text-muted">
                  {userAsset.asset?.deposit_fee}
                </label>
              </StyledModalElementInlineTwo>
              <StyledModalElementInline>
                <H5 className="bp3-heading">
                  <FormattedMessage defaultMessage={"Important Information"} />
                </H5>
              </StyledModalElementInline>
              <StyledModalElementCard>
                <Card interactive={false} elevation={Elevation.ONE}>
                  <p>
                    <label className="bp3-text-muted">
                      <FormattedMessage
                        defaultMessage={
                          "Please make sure to always review the below information before making a deposit as it may change in future."
                        }
                      />
                    </label>
                    <br />
                    <br />
                    <label className="bp3-text-muted">
                      <FormattedMessage defaultMessage={"Network: "} />
                    </label>
                    <label className="bp3-text-muted light">
                      <FormattedMessage
                        defaultMessage={
                          "Send only {coin} to your {coin} deposit address. If you send any other asset to this address it will not be credited or recovered."
                        }
                        values={{
                          coin: userAsset.asset && userAsset.asset.name
                        }}
                      />
                    </label>
                    <br />
                    <br />
                    <label className="bp3-text-muted">
                      <FormattedMessage defaultMessage={"Source: "} />
                    </label>
                    <label className="bp3-text-muted light">
                      <FormattedMessage
                        defaultMessage={
                          "Please only send {coin} from your own wallet. You can not accept payments from third parties directly to your CoinFLEX deposit address. Click here to find out more. CoinFLEX performs transaction monitoring and has a zero tolerance policy for activity associated with money laundering, terrorism, fraud, crime or darknet markets. Third party deposits and association with illicit activities will result in your accounts closure."
                        }
                        values={{
                          coin: userAsset.asset && userAsset.asset.name
                        }}
                      />
                    </label>
                    <br />
                    <br />
                    <label className="bp3-text-muted">
                      <FormattedMessage defaultMessage={"Clearing time: "} />
                    </label>
                    <label className="bp3-text-muted light">
                      <FormattedMessage
                        defaultMessage={
                          "Deposits are processed automatically 24/7. Be sure to send the deposit with an adequate network fee and account for the time required for the network to confirm the transaction. You can track the progress of your transaction's confirmations on the blockchain. CoinFLEX reserves the right to delay the processing of transfers without stating a reason. Deposits below the stated minimum or in a non-matching asset are not recoverable."
                        }
                        values={{
                          coin: userAsset.asset && userAsset.asset.name
                        }}
                      />
                    </label>
                  </p>
                </Card>
              </StyledModalElementCard>
            </StyledContainer>
          </div>
        </Dialog>
      </>
    );
  } else {
    return null;
  }
};
const mapStateToProps = (state: IAppState) => {
  return {
    userAssets: state.userAssets
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    pushNotification(data: INotification) {
      dispatch(pushNotification(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(DepositModal);
