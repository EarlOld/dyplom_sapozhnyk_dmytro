import styled from "styled-components";
export const StyledContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  flex-direction: column;
`;
export const StyledQRCodeContainer = styled.div`
  flex: 1;
  text-align: center;
`;

export const StyledModalElement = styled.div`
  margin-top: 16px;
  flex: 1;
  .bp3-card {
    display: flex;
    padding-left: 0;

    > div {
      flex: 1;

      input.bp3-input {
        padding-left: 8px;
        box-shadow: none;
        border-color: transparent;
        cursor: pointer;
        font-size: 12px !important;
      }
    }
    button {
      margin-top: 1px;
      margin-left: 10px;
      height: 44px;
    }
  }
`;

export const StyledModalElementInline = styled.div`
  display: flex;
  flex: 1;
  margin-top: 40px;
  h5 {
    flex: 1;
  }
`;

export const StyledModalElementInlineTwo = styled.div`
  width: 100%;
  display: flex;
  label {
    flex: 1;
  }
`;

export const StyledModalElementCard = styled.div`
  width: 100%;
  display: flex;
  .bp3-card {
    overflow-y: scroll;
    height: 150px;
    .light {
      font-weight: 300;
    }
  }
`;
