import React, { memo, useEffect, useCallback, useState } from "react";
import "./style/index.scss";
import Navigation from "./Navigation/MainMenu/MainMenu";
import iotInterfaceLinks from "./Navigation/iotInterfaceLinks/iotInterfaceLinks";
import ChangeWarning from "./ChangeWarning/ChangeWarning";
import Balances from "./Balances/Balances";
import OneClickLeverage from "./OneClickLeverage";
import CompetitionWidget from "./CompetitionWidget";
import TopNavBar from "./TopNavBar";
import NotificationManager from "./NotificationManager/NotificationManager";
import {
  getAssetsList,
  getMarketList,
  setWebsocketAuthParams,
  getBalances,
  getCompetitions,
  getBorrowConversions,
  getPositions,
  getBorrowCollaterals,
  getBorrowOffers,
  getTickersList,
  getDepositAddress,
  getUserAssets,
  getBorrowConvertedTotals,
  setUser,
  getChangeList,
  setLogin,
  setPanelConfirm,
  setAuthToken,
  pushNotification
} from "store/actions";
import { connect } from "react-redux";
import { Route, Switch, Redirect } from "react-router-dom";
import { getAuthParams } from "../Home/utils/auth/authParams";
import { UserInfo } from "./UserInfo";
import Footer from "./Footer";
import { authenticateUser } from "../Home/utils";
import coinflexEventstream from "services/eventstream";
import NotificationViewer from "./NotificationViewer";
import DepositModal from "./DepositModal";
import { TransferModal } from "./TransferModal";
import { QuickTransferModal } from "./QuickTransferModal";
import WithdrawalModal from "./WithdrawalModal";
import DepositWithdrawalsHistory from "./DepositWithdrawalsHistory/";
import _flowRight from "lodash/flowRight";
import _filter from "lodash/filter";
import _get from "lodash/get";
import { injectIntl, WrappedComponentProps } from "react-intl";
import { PUBLIC_URL, WEB_APP_URL, WEBAPP_ROUTES } from "../../config";
import {
  getAuthTokenWithPatch,
  getUser,
  signOut
} from "../../services/http/http";
import { deleteCookie, setCookieOnCoinflexDomain } from "utils/cookie";
import { removeItemFromLocalStorageState } from "../../utils/localStorage";
type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TProps = TStateProps & TDispatchProps & WrappedComponentProps;

export interface ITransferModalData {
  fromAssetId: number;
  toAssetId: number;
}
export interface IOpenTransferModal {
  openTransferModal: (data: ITransferModalData) => void;
}
export interface IAssetMap {
  [x: string]: number;
}

const Dashboard = memo((props: TProps) => {
  const [hasInitiated, setHasInitiated] = useState<boolean>(false);

  const {
    userAssets,
    intl,
    pushNotification,
    setWebsocketAuthParams,
    setUser,
    getUserAssets,
    authToken,
    serverNonce,
    getBalances,
    getBorrowCollaterals,
    getBorrowOffers,
    getBorrowConvertedTotals,
    getPositions,
    getCompetitions,
    websocketAuthParams,
    getMarketList,
    getAssetsList,
    getTickersList,
    getBorrowConversions,
    getChangeList,
    getDepositAddress,
    setPanelConfirm,
    setLogin,
    setAuthToken,
    getAddressState
  } = props;

  const setLoginState = useCallback(async () => {
    try {
      const userDetails = await getUser(authToken);
      const { core_id, priv_key, api_key } = userDetails;
      setWebsocketAuthParams({ core_id, priv_key, api_key });
      setUser(userDetails);
      setCookieOnCoinflexDomain("landingpage_auth", new Date().toJSON(), 1);
    } catch (e) {
      if (e.err === 5) {
        // TOKEN_EXPIRED
        const newAuthToken: any = await getAuthTokenWithPatch(authToken);
        setAuthToken(newAuthToken);
        setHasInitiated(false);
      }
    }
  }, [setWebsocketAuthParams, setUser, authToken, setAuthToken]);

  const logoutHandler = useCallback(async () => {
    try {
      signOut(authToken);
      deleteCookie("access_token");
      deleteCookie("landingpage_auth");
      setLogin(false);
      removeItemFromLocalStorageState("authToken");
      setUser(null);
      setAuthToken(null);
      setWebsocketAuthParams(null);
      setPanelConfirm({ panelType: "login" });
      window.location.href = WEB_APP_URL + WEBAPP_ROUTES.SIGN_OUT;
    } catch (e) {
      console.log("Could not sign out", e);
    }
  }, [
    authToken,
    setAuthToken,
    setLogin,
    setPanelConfirm,
    setUser,
    setWebsocketAuthParams
  ]);

  useEffect(() => {
    if (hasInitiated) return;
    getMarketList();
    getAssetsList();
    getTickersList();
    getBorrowConversions();
    getChangeList();
    setLoginState();
    setHasInitiated(true);
  }, [
    hasInitiated,
    setLoginState,
    getMarketList,
    getAssetsList,
    getTickersList,
    getBorrowConversions,
    getChangeList
  ]);

  useEffect(() => {
    if (authToken && authToken.token) {
      getUserAssets();
    }
  }, [authToken, getUserAssets]);

  const loginCallback = useCallback(() => {
    const authParams: IAuthParams = getAuthParams(websocketAuthParams);
    coinflexEventstream.init(authParams);
    getPositions();
    getBalances();
    getBorrowCollaterals();
    getBorrowOffers();
    getBorrowConvertedTotals();
    getCompetitions();
  }, [
    websocketAuthParams,
    getBalances,
    getBorrowCollaterals,
    getBorrowOffers,
    getBorrowConvertedTotals,
    getPositions,
    getCompetitions
  ]);

  useEffect(() => {
    if (websocketAuthParams) {
      loginCallback();
      if (serverNonce) {
        authenticateUser(websocketAuthParams, serverNonce);
      }
    }
  }, [loginCallback, websocketAuthParams, serverNonce]);

  const [depositModalAssetId, setDepositModalAssetId] = useState<
    number | undefined
  >();

  const userAssetsMap: any = useCallback(() => {
    return (userAssets || [])
      .map((userAsset: IUserAsset) => userAsset.asset.name)
      .reduce((mem: any, v: string) => ({ ...mem, [v]: null }), {});
  }, [userAssets]);

  const [pendingDeposits, setPendingDeposits] = useState<IAssetMap>(
    userAssetsMap
  );

  const openDepositModal = (assetId: number) => {
    const userAsset: IUserAsset | undefined =
      (userAssets &&
        userAssets.find(
          (userAsset: IUserAsset) => userAsset.asset.corecode === assetId
        )) ||
      undefined;

    const hasDepositAddress = userAsset && userAsset.deposit_address;

    const hasNotYetRequestedDepositAddress =
      userAsset &&
      !userAsset.deposit_address_requested &&
      !userAsset.deposit_address;

    if (hasDepositAddress) {
      setDepositModalAssetId(assetId);
    } else if (hasNotYetRequestedDepositAddress) {
      userAsset &&
        getDepositAddress({ name: userAsset.asset.name, ...userAsset });

      const assetName: string = _get(userAsset, "asset.name", "");
      if (!_get(pendingDeposits, [assetName])) {
        getUserAssets();
        setPendingDeposits({
          [assetName]: setInterval(getUserAssets, 1500)
        });
      }
    }
  };

  useEffect(() => {
    if (getAddressState.pending) {
      pushNotification({
        type: "SUCCESS",
        title: intl.formatMessage({
          defaultMessage: "Requesting a Deposit Address"
        })
      });
    } else if (getAddressState.error) {
      pushNotification({
        type: "DANGER",
        title: intl.formatMessage({
          defaultMessage: "Deposit Address Failed"
        }),
        message:
          getAddressState.errorCode === "errors.account.stablecoin.not_enabled"
            ? intl.formatMessage({
                defaultMessage: "Please complete your Stablecoin Application."
              })
            : intl.formatMessage({
                defaultMessage:
                  "We could not request an address for this asset. "
              })
      });
      clearInterval(pendingDeposits[getAddressState.name]);
    }
  }, [getAddressState, intl, pushNotification, pendingDeposits]);

  useEffect(() => {
    if (pendingDeposits) {
      const pendingNames = Object.keys(_filter(pendingDeposits));

      const matchingUserAsset: IUserAsset | undefined =
        (userAssets &&
          userAssets.find((userAsset: IUserAsset) =>
            pendingNames.includes(userAsset.asset.name)
          )) ||
        undefined;

      if (matchingUserAsset && matchingUserAsset.deposit_address) {
        pushNotification({
          type: "SUCCESS",
          title: intl.formatMessage({
            defaultMessage: "Deposit Address",
            id: "deposit-ready-title"
          }),
          message: intl.formatMessage({
            defaultMessage: "Your deposit address is now available",
            id: "deposit-ready-message"
          })
        });

        clearInterval(pendingDeposits[matchingUserAsset.asset.name]);
        const copyOfPendingDeposits = { ...pendingDeposits };
        delete copyOfPendingDeposits[matchingUserAsset.asset.name];
        setPendingDeposits(copyOfPendingDeposits);
      }
    }
  }, [userAssets, pendingDeposits, intl, pushNotification]);

  const closeDepositModal = () => setDepositModalAssetId(undefined);

  const [openWithdrawalModalId, setWithdrawalModalId] = useState<
    number | undefined
  >();

  const openWithdrawalModal = (assetId: number) =>
    setWithdrawalModalId(assetId);
  const closeWithdrawalModal = () => setWithdrawalModalId(undefined);

  const [
    transferFromToData,
    setTransferFromToData
  ] = useState<ITransferModalData | null>(null);

  const openTransferModal = (transferData: ITransferModalData) =>
    setTransferFromToData(transferData);
  const closeTransferModal = () => setTransferFromToData(null);

  const [
    quickTransferFromToData,
    setQuickTransferFromToData
  ] = useState<ITransferModalData | null>(null);

  const openQuickTransferModal = (transferData: ITransferModalData) =>
    setQuickTransferFromToData(transferData);
  const closeQuickTransferModal = () => setQuickTransferFromToData(null);

  return (
    <div className={"dashboard-page-wrapper"}>
      <TopNavBar logoutHandler={logoutHandler} />
      <div className="dashboard-page bp3-dark">
        <div className="content-container">
          <section className="topbar">
            <UserInfo />
          </section>
          <section className="content-wrapper">
            <div className="sidebar-wrapper">
              <Navigation />
            </div>
            <div className="main-wrapper">
              <Switch>
                <Route path={"/dashboard"} exact>
                  <Redirect to={PUBLIC_URL + "/dashboard/home"} />
                </Route>
                <Route
                  path={PUBLIC_URL + "/dashboard/home"}
                  exact
                  component={() => (
                    <div className="inner-dashboard-home-container">
                      <div className="inner-dashboard-home-content">
                        <iotInterfaceLinks />
                        <OneClickLeverage
                          openDepositModal={openDepositModal}
                          openQuickTransferModal={openQuickTransferModal}
                        />
                        <Balances
                          openTransferModal={openTransferModal}
                          openWithdrawalModal={openWithdrawalModal}
                          openDepositModal={openDepositModal}
                        />
                      </div>
                      <div className="right-sidebar-wrapper">
                        <CompetitionWidget />
                      </div>
                    </div>
                  )}
                />
                <Route
                  path={PUBLIC_URL + "/dashboard/balances"}
                  exact
                  component={() => (
                    <div className="inner-dashboard-home-container">
                      <Balances
                        openTransferModal={openTransferModal}
                        openWithdrawalModal={openWithdrawalModal}
                        openDepositModal={openDepositModal}
                      />
                    </div>
                  )}
                />
                <Route
                  path={PUBLIC_URL + "/dashboard/history"}
                  exact
                  component={() => (
                    <div className="inner-dashboard-home-container">
                      <DepositWithdrawalsHistory />
                    </div>
                  )}
                />
              </Switch>
            </div>
          </section>
        </div>
        <section className="footer-wrapper">
          <Footer />
        </section>
      </div>
      <DepositModal
        isOpen={!!depositModalAssetId}
        assetId={depositModalAssetId}
        onClose={closeDepositModal}
      />
      <WithdrawalModal
        isOpen={!!openWithdrawalModalId}
        assetId={openWithdrawalModalId}
        onClose={closeWithdrawalModal}
      />
      {transferFromToData && (
        <TransferModal
          isOpen={!!transferFromToData}
          {...transferFromToData}
          onClose={closeTransferModal}
        />
      )}
      {quickTransferFromToData && (
        <QuickTransferModal
          isOpen={!!quickTransferFromToData}
          {...quickTransferFromToData}
          onClose={closeQuickTransferModal}
        />
      )}
      <ChangeWarning />
      <NotificationManager />
      <NotificationViewer />
    </div>
  );
});

const mapStateToProps = (state: IAppState) => ({
  isLogin: state.isLogin,
  serverNonce: state.serverNonce,
  websocketAuthParams: state.websocketAuthParams,
  userAssets: state.userAssets,
  depositAddress: state.depositAddress,
  withdrawalAddress: state.withdrawalAddress,
  authToken: state.authToken,
  getAddressState: state.getAddressState
});

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    getMarketList() {
      dispatch(getMarketList());
    },
    setUser(user: IUser | null) {
      dispatch(setUser(user));
    },
    getAssetsList() {
      dispatch(getAssetsList());
    },
    setLogin(data: boolean) {
      dispatch(setLogin(data));
    },
    setPanelConfirm(data: IPanelConfirm) {
      dispatch(setPanelConfirm(data));
    },
    getTickersList() {
      dispatch(getTickersList());
    },
    getChangeList() {
      dispatch(getChangeList());
    },
    getBalances() {
      dispatch(getBalances());
    },
    getPositions() {
      dispatch(getPositions());
    },
    getBorrowCollaterals() {
      dispatch(getBorrowCollaterals());
    },
    getBorrowOffers() {
      dispatch(getBorrowOffers());
    },
    getBorrowConversions() {
      dispatch(getBorrowConversions());
    },
    getBorrowConvertedTotals() {
      dispatch(getBorrowConvertedTotals());
    },
    getCompetitions() {
      dispatch(getCompetitions());
    },
    getDepositAddress(asset: { name: string }) {
      dispatch(getDepositAddress(asset));
    },
    getUserAssets() {
      dispatch(getUserAssets());
    },
    setAuthToken(token: IAuthToken | null) {
      dispatch(setAuthToken(token));
    },
    pushNotification(data: INotification) {
      dispatch(pushNotification(data));
    },
    setWebsocketAuthParams(data: IUserAuthentificateParams | null) {
      dispatch(setWebsocketAuthParams(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(Dashboard);
