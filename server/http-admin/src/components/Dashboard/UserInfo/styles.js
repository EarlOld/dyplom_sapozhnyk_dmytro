import styled from "styled-components";
export const StyledContainer = styled.div`
  display: flex;
  flex-direction: row;
  color: #879ba6;
  padding: 16px 0;
`;

export const StyledInitials = styled.div`
  border: 2px solid #879ba6;
  border-radius: 50%;
  padding: 5px;
  height: 48px;
  width: 48px;
  font-weight: 500;
  color: #879ba6;
  text-align: center;
  font-size: 24px;
  line-height: 36px;
  align-items: flex-start;
`;

export const StyledCredentials = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 10px;
  justify-content: flex-end;
  flex: 1;
`;

export const StyledEmail = styled.div`
  font-size: 20px;
  font-weight: 400;
  justify-self: flex-start;
  margin-bottom: 4px;
`;

export const StyledAccountID = styled.div`
  color: #cccccc;
  font-size: 12px;
  display: block;
  justify-self: flex-start;
`;

export const StyledLevel = styled.div`
  height: 32px;
  line-height: 14px;
  font-size: 14px;
  padding: 8px;
  border-radius: 4px;
  font-weight: 500;
  text-align: center;
  align-self: flex-end;
  justify-self: flex-end;
  span {
    background-color: rgba(135, 72, 227, 0.3);
    border-radius: 50%;
    padding: 8px 12px;
    margin-left: 8px;
    color: #ffffff;
  }
`;
