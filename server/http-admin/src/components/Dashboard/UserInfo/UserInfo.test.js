import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import UserInfo from "./UserInfo";

describe("UserInfo tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<UserInfo />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
