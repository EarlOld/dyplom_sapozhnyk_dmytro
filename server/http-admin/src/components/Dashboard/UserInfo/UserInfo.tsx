import React, { memo } from "react";
import _flowRight from "lodash/flowRight";
import _first from "lodash/first";
import { connect } from "react-redux";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import { setSelectedMarket } from "store/actions";
import {
  StyledContainer,
  StyledInitials,
  StyledCredentials,
  StyledEmail,
  StyledAccountID,
  StyledLevel
} from "./styles";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TProps = TStateProps & TDispatchProps & WrappedComponentProps;

export const UserInfo = memo(({ intl, user }: TProps) => {
  const hasInitials = user && user.first_name && user.last_name;

  return (
    <StyledContainer>
      <StyledInitials>
        {hasInitials
          ? [_first(user.first_name || ""), _first(user.last_name || "")].join(
              ""
            )
          : user && user.email
          ? user.email.charAt(0)
          : "??"}
      </StyledInitials>
      <StyledCredentials>
        <StyledEmail>
          {" "}
          {(user && user.email) ||
            intl.formatMessage({
              defaultMessage: "no email address",
              id: "unknown-user"
            })}{" "}
        </StyledEmail>
        <StyledAccountID>
          <FormattedMessage defaultMessage={"Account ID"} />{" "}
          {(user && user.core_id) ||
            intl.formatMessage({
              defaultMessage: "no core id",
              id: "missing-core-id"
            })}{" "}
        </StyledAccountID>
      </StyledCredentials>
      <StyledLevel>
        <FormattedMessage defaultMessage={"Affiliate Level"} />{" "}
        <span>{(user && user.affiliate_level) || 0}</span>
      </StyledLevel>
      {user && user.vip_level && (
        <StyledLevel>
          <FormattedMessage defaultMessage={"VIP Level"} />{" "}
          <span>{user.vip_level}</span>
        </StyledLevel>
      )}
    </StyledContainer>
  );
});

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setSelectedMarket(data: IPositionListItem | IMarket) {
      dispatch(setSelectedMarket(data));
    }
  };
};

const mapStateToProps = (state: IAppState) => {
  return {
    user: state.user
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(UserInfo);
