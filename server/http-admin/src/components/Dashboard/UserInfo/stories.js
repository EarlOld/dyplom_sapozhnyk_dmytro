import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import UserInfo from "./UserInfo";

export default {
  title: "UserInfo",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <UserInfo />;

Default.story = {
  name: "default"
};
