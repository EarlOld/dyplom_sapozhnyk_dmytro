import styled from "styled-components";
import { Button } from "@blueprintjs/core";
import React from "react";

const ButtonTemplate = props => <Button {...props} />;
export const StyledMenuItem = styled(ButtonTemplate)`
  width: 460px;
  display: block !important;
  background: black;
  height: 46px;
  > span {
    font-weight: 400;
  }
  .bp3-input-group {
    width: 460px;
  }
`;
