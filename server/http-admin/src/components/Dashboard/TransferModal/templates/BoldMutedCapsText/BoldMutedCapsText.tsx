import React from "react";
import "./index.scss";
interface IBoldMutedCapsText {
  firstString: string;
  secondString: string;
}
export const BoldMutedCapsText = ({
  firstString,
  secondString
}: IBoldMutedCapsText) => {
  return (
    <span className={"bold-muted-caps"}>
      <span className={"bold"}>{firstString}</span>
      <span className={"muted"}>{secondString}</span>
    </span>
  );
};
