import React from "react";
import { DEFAULT_SELECT_SETTINGS } from "../../data";
import { Select } from "@blueprintjs/select";
import { StyledMenuItem } from "../../StyledMenuItem";
import { Button } from "@blueprintjs/core";
import { injectIntl, WrappedComponentProps } from "react-intl";
import "./index.scss";
import { isSpotAsset } from "../../../../../utils/isSpotAsset";
import { BoldMutedCapsText } from "../BoldMutedCapsText/BoldMutedCapsText";

interface IProps extends WrappedComponentProps {
  asset: IAsset;
  onChange: Function;
  items: IAsset[];
  disabled?: boolean;
}

const popoverProps = {
  popoverClassName: "asset-dropper-popover"
};

const filterItem = (query: string, item: IAsset) => {
  const normalizedTitle = (item.name || "").trim().toLowerCase();
  const normalizedQuery = query.trim().toLowerCase();

  if (!query) {
    return true;
  }

  return normalizedTitle.indexOf(normalizedQuery) >= 0;
};

export const AssetDropper = injectIntl(
  ({ intl, asset, onChange, items, disabled = false }: IProps) => {
    const RowSelect = Select.ofType<IAsset>();
    const _isSpotAsset = isSpotAsset(asset);

    return (
      <RowSelect
        className={"bp3-dark"}
        disabled={disabled}
        {...DEFAULT_SELECT_SETTINGS}
        items={items}
        itemRenderer={(contractMarket: IAsset, { handleClick }: any) => (
          <StyledMenuItem
            onClick={handleClick}
            key={contractMarket.name}
            style={{ width: "460px" }}
          >
            {contractMarket.name}
          </StyledMenuItem>
        )}
        onItemSelect={(asset: IAsset) => onChange(asset)}
        itemPredicate={filterItem}
        popoverProps={popoverProps}
      >
        <Button
          className={"asset-dropper-button"}
          rightIcon="caret-down"
          disabled={disabled}
        >
          <BoldMutedCapsText
            firstString={
              asset
                ? asset.name
                : intl.formatMessage({ defaultMessage: "None available" })
            }
            secondString={
              asset
                ? _isSpotAsset
                  ? intl.formatMessage({ defaultMessage: "SPOT" })
                  : intl.formatMessage({ defaultMessage: "FUTURES" })
                : ""
            }
          />
        </Button>
      </RowSelect>
    );
  }
);
