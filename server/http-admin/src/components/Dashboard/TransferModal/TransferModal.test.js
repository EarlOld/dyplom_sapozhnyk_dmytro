import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import TransferModal from "components/Dashboard/TransferModal/TransferModal";

describe("TransferModal tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(
      <TransferModal isOpen={true} fromAssetId={63488} toAssetId={51202} />
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
