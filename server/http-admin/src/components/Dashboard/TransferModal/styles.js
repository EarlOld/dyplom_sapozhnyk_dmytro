import styled from "styled-components";
export const StyledContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  flex-direction: column;
  .cf-helper {
    color: #61686b;
    margin-top: 4px;
    font-size: 12px;
    font-weight: 300;
  }
`;

export const StyledIconContainer = styled.div``;

export const StyledModalElement = styled.div`
  margin-top: 16px;
  flex: 1;
  col .bp3-card {
    display: flex;
    border: none;
    > div {
      flex: 1;
      &.predefined > input {
        border-color: transparent !important;
        box-shadow: none !important;
        &:hover,
        &:active {
          border-color: transparent !important;
        }
      }

      input.bp3-input {
        font-size: 14px;
      }
    }
    button {
      margin-top: 1px;
      margin-left: 10px;
      height: 44px;
    }
    .back {
      margin-right: 10px;
      margin-left: 0;
    }
  }
`;

export const StyledModalElementInline = styled.div`
  display: flex;
  flex: 1;
  margin-top: 40px;
  h5 {
    flex: 1;
  }
`;

export const StyledModalElementInlineTwo = styled.div`
  width: 100%;
  display: flex;
  label {
    flex: 1;
  }
  p {
    color: #61686b;
  }
`;

export const StyledModalElementCard = styled.div`
  width: 100%;
  display: flex;
  .bp3-card {
    overflow-y: scroll;
    height: 150px;
    .light {
      font-weight: 300;
    }
  }
`;
