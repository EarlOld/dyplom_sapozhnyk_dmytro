import React, { FormEvent, useCallback, useMemo, useState } from "react";
import { dialogOptions } from "./data";
import _flowRight from "lodash/flowRight";
import { connect } from "react-redux";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import ControlsGroup from "components/Dashboard/ControlsGroup";
import { IDialogOptions } from "./types";
import {
  Button,
  Classes,
  Dialog,
  H4,
  H5,
  Icon,
  InputGroup,
  Intent,
  Tab,
  Tabs
} from "@blueprintjs/core";
import _round from "lodash/round";
import _groupBy from "lodash/groupBy";
import { AssetDropper } from "./templates/AssetDropper/AssetDropper";
import { isSpotAsset } from "utils/isSpotAsset";
import "./styles/index.scss";
import { getFuturesBalances, expandFuturesBalance } from "../Balances/utils";
import _get from "lodash/get";
import { balanceConvert } from "store/actions";
import { SCALED } from "../../TicketForm/utils/config";
import { isUSDT } from "utils/isUsdAsset";
import { MONTHS_LIST } from "../data";
import {
  isMatchingMarketForAsset,
  sortAssetsByMarketsIndexes,
  getClosestActiveAsset
} from "../utils";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TOwnProps = {
  fromAssetId: number;
  toAssetId: number;
  isOpen: boolean;
  onClose: () => void;
  onChange: () => void;
};
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TProps = TStateProps & TDispatchProps & WrappedComponentProps & TOwnProps;
const options: IDialogOptions = dialogOptions;

export const TransferModal = ({
  intl,
  fromAssetId,
  toAssetId,
  allBalances,
  assetsList,
  marketList,
  tickersList,
  borrowConversions,
  isOpen,
  borrowConvertedTotals,
  onClose = () => {},
  convert,
  borrowLoans,
  borrowCollaterals
}: TProps) => {
  const [fromAsset, setFromAsset] = useState<IAsset>(
    assetsList.filter((asset: IAsset) => asset.id === fromAssetId)[0]
  );
  const [toAsset, setToAsset] = useState<IAsset | undefined>(
    assetsList.filter((asset: IAsset) => asset.id === toAssetId)[0]
  );

  const [amountToTransferAsAString, setAmountToTransferAsAString] = useState<
    string
  >("");
  const amountToTransfer = useMemo(
    () => parseFloat(amountToTransferAsAString),
    [amountToTransferAsAString]
  );

  const currentMonthName = MONTHS_LIST[new Date().getMonth()].toUpperCase();

  const isVisibleFuturesAsset = useCallback(
    (asset: IAsset) => {
      const matchingMarket = marketList.find(market =>
        isMatchingMarketForAsset(asset, market)
      );

      return (
        !!matchingMarket ||
        (isUSDT(asset.spot_id!) && asset.name.endsWith(currentMonthName))
      );
    },
    [marketList, currentMonthName]
  );

  const _groupedAssets = _groupBy(assetsList, asset => isSpotAsset(asset));
  const groupedAssets: { spot: IAsset[]; futures: IAsset[] } = {
    spot: sortAssetsByMarketsIndexes(marketList, _groupedAssets["true"]),
    futures: sortAssetsByMarketsIndexes(
      marketList,
      _groupedAssets["false"].filter(asset => {
        const matchingBalance = allBalances.find(
          balane => balane.id === asset.id
        );

        if (
          matchingBalance &&
          (matchingBalance.available || matchingBalance.reserved)
        ) {
          return true;
        }

        return isVisibleFuturesAsset(asset);
      })
    )
  };

  const fromAssetTotal: number = useMemo(() => {
    const isSpot: boolean = isSpotAsset(fromAsset);
    const expandedFuturesBalances: IFuturesBalance[] = getFuturesBalances(
      allBalances,
      assetsList,
      marketList,
      tickersList
    ).map((futuresBalance: IFuturesBalance) =>
      expandFuturesBalance(
        futuresBalance,
        borrowLoans,
        assetsList,
        borrowConvertedTotals,
        borrowCollaterals
      )
    );

    const balance = (isSpot ? allBalances : expandedFuturesBalances).find(
      balance => balance.id === fromAsset.id
    );

    const bal = _get(balance, isSpot ? "available" : "availableToWithdraw", 0);

    return isSpot ? bal / SCALED : bal;
  }, [
    fromAsset,
    allBalances,
    assetsList,
    tickersList,
    marketList,
    borrowCollaterals,
    borrowConvertedTotals,
    borrowLoans
  ]);

  const toAssetOptions: IAsset[] = useMemo(() => {
    const allPossibleTosIds = borrowConversions
      .filter(
        (borrowConversion: IBorrowConversion) =>
          borrowConversion.asset_from === fromAsset.id
      )
      .map(aPT => aPT.asset_to);
    const filteredToAssets = assetsList.filter((asset: IAsset) => {
      if (!allPossibleTosIds.includes(asset.id)) {
        return false;
      }

      if (isSpotAsset(asset)) {
        return true;
      }

      return isVisibleFuturesAsset(asset);
    });

    setToAsset(getClosestActiveAsset(filteredToAssets) || filteredToAssets[0]);
    return sortAssetsByMarketsIndexes(marketList, filteredToAssets);
  }, [
    borrowConversions,
    assetsList,
    marketList,
    fromAsset,
    isVisibleFuturesAsset
  ]);

  const spotToFutures: boolean = useMemo(() => isSpotAsset(fromAsset), [
    fromAsset
  ]);

  const fromAssetName: string = useMemo(() => fromAsset && fromAsset.name, [
    fromAsset
  ]);

  const noToOptions: boolean = useMemo(() => !toAssetOptions.length, [
    toAssetOptions
  ]);

  const disableTransfer: boolean = useMemo(
    () => noToOptions || !amountToTransfer || !toAsset,
    [noToOptions, amountToTransfer, toAsset]
  );

  const onFromAssetChangeHandler = (asset: IAsset) => setFromAsset(asset);
  const onToAssetChangeHandler = (asset: IAsset) => setToAsset(asset);
  const onToggleFromToHandler = () => {
    toAsset && setFromAsset(toAsset);
  };
  const onResetHandler = () => {
    setFromAsset(
      assetsList.filter((asset: IAsset) => asset.id === fromAssetId)[0]
    );
  };

  const tabChangeHandler = (percent: string) =>
    setAmountToTransferAsAString((+percent * 0.01 * fromAssetTotal).toString());

  const onInputChangeHandler = (event: FormEvent<HTMLInputElement>) =>
    setAmountToTransferAsAString((event.target as HTMLInputElement).value);

  const onSubmitHandler = () => {
    let requestData: IConvertAndDoBorrowData = {
      asset_from: fromAsset.id,
      asset_to: toAsset!.id,
      amount: _round(amountToTransfer * SCALED)
    };

    convert(requestData);
    onClose();
  };

  return (
    <>
      <Dialog
        className={Classes.DARK}
        onClose={onClose}
        title={`Transfer`}
        {...options}
        enforceFocus={false}
        isOpen={isOpen}
      >
        <div className={Classes.DIALOG_BODY}>
          <div className={"transfer-modal-container"}>
            <p className={"modal-info"}>
              <FormattedMessage
                defaultMessage={
                  "To trade futures you must first allocate collateral to a selected futures asset"
                }
              />
            </p>
            <H4 className="bp3-heading" style={{ color: "#FFF" }}>
              <FormattedMessage defaultMessage={"Transfer FROM"} />
            </H4>
            <H5 className="bp3-heading">
              {spotToFutures ? (
                <FormattedMessage defaultMessage={"From Balance"} />
              ) : (
                <FormattedMessage defaultMessage={"From Futures Asset"} />
              )}
            </H5>
            <AssetDropper
              asset={fromAsset}
              items={groupedAssets[spotToFutures ? "spot" : "futures"]}
              onChange={onFromAssetChangeHandler}
            />
            <div className={"cf-helper"}>
              <FormattedMessage
                defaultMessage={"Select a futures balance to convert from"}
              />
            </div>

            <H5 className="bp3-heading">
              <FormattedMessage
                defaultMessage={"Amount to transfer"}
                values={{ fromAsset: fromAssetName }}
              />
              <span className={"totals"}>
                {fromAssetTotal} {fromAssetName}
              </span>
            </H5>

            <ControlsGroup large={true}>
              <InputGroup
                type="text"
                className={"amount-input"}
                large
                value={amountToTransferAsAString.toString()}
                onChange={onInputChangeHandler}
                placeholder={intl.formatMessage(
                  {
                    defaultMessage: `Enter amount`
                  },
                  { fromAsset: fromAssetName }
                )}
              />
              <Tabs
                animate={false}
                onChange={tabChangeHandler}
                selectedTabId={undefined}
              >
                <Tab id={"25"} title="25%" />
                <Tab id={"50"} title="50%" />
                <Tab id={"75"} title="75%" />
                <Tab id={"100"} title="100%" />
              </Tabs>
            </ControlsGroup>
            <p className={"cf-helper"}>
              <FormattedMessage
                defaultMessage={
                  "Enter or select and amount of collateral to allocate"
                }
              />
            </p>

            <div className={"asset-switcher-container"}>
              {noToOptions ? (
                <Button
                  className={"asset-switcher"}
                  onClick={onResetHandler}
                  intent={Intent.PRIMARY}
                >
                  <Icon icon="refresh" iconSize={23} />
                </Button>
              ) : (
                <Button
                  className={"asset-switcher"}
                  onClick={onToggleFromToHandler}
                  intent={Intent.PRIMARY}
                >
                  <Icon icon="swap-vertical" iconSize={23} />
                </Button>
              )}
            </div>
            {toAsset && (
              <>
                <H4 className="bp3-heading" style={{ color: "#FFF" }}>
                  <FormattedMessage defaultMessage={"TO Asset"} />
                </H4>
                <H5 className="bp3-heading">
                  {spotToFutures ? (
                    <FormattedMessage defaultMessage={"To Futures Asset"} />
                  ) : (
                    <FormattedMessage defaultMessage={"To Balance"} />
                  )}
                </H5>
                <AssetDropper
                  asset={toAsset}
                  items={toAssetOptions}
                  onChange={onToAssetChangeHandler}
                />
              </>
            )}
          </div>

          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button
                className={"transfer-confirm"}
                large={true}
                onClick={onSubmitHandler}
                fill
                intent={Intent.PRIMARY}
                style={{ minHeight: "50px", marginLeft: 0 }}
                disabled={disableTransfer}
              >
                <FormattedMessage
                  defaultMessage={"Transfer {fromAsset}"}
                  values={{ fromAsset: fromAssetName }}
                />
              </Button>
            </div>
          </div>
        </div>
      </Dialog>
    </>
  );
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    convert(data: IConvertAndDoBorrowData) {
      dispatch(balanceConvert(data));
    }
  };
};

const mapStateToProps = (state: IAppState) => {
  return {
    assetsList: state.assetsList,
    borrowLoans: state.borrowLoans,
    borrowConversions: state.borrowConversions,
    borrowCollaterals: state.borrowCollaterals,
    marketList: state.marketList,
    allBalances: state.allBalances,
    tickersList: state.tickersList,
    borrowConvertedTotals: state.borrowConvertedTotals
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(TransferModal);
