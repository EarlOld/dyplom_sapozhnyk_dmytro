export const DEFAULT_SELECT_SETTINGS = {
  allowCreate: false,
  filterable: true,
  hasInitialContent: false,
  minimal: false,
  resetOnClose: false,
  resetOnQuery: false,
  resetOnSelect: false,
  disableItems: false,
  large: true
};

export const dialogOptions = {
  autoFocus: true,
  canEscapeKeyClose: true,
  canOutsideClickClose: true,
  enforceFocus: true,
  usePortal: true
};
