import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import TransferModal from "components/Dashboard/TransferModal/TransferModal";

export default {
  title: "TransferModal",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <TransferModal isOpen={true} fromAssetId={63488} toAssetId={51202} />
);

Default.story = {
  name: "Open TransferModal"
};
