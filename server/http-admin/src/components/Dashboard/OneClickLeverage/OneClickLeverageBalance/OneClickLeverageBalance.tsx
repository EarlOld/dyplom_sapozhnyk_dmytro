import React, { memo, useEffect, useMemo, useState } from "react";
import _flowRight from "lodash/flowRight";
import { connect } from "react-redux";
import { injectIntl, WrappedComponentProps } from "react-intl";
import { SCALED } from "../../Balances/data";
import { isUsdAsset } from "utils//isUsdAsset";
import _round from "lodash/round";
import { decimals } from "utils//reference";
import { OneClickLeverageBalanceView } from "./templates";
import { ITransferModalData } from "../../index";
import { getClosestActiveAsset } from "../../utils";
import { balanceConvert, pushNotification } from "store/actions";

interface IOwnProps {
  assetId: number;
  openDepositModal: (assetName: number) => void;
  openQuickTransferModal: (transferData: ITransferModalData) => void;
}

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TProps = TStateProps & IOwnProps & WrappedComponentProps & TDispatchProps;

export const OneClickLeverageBalance = memo(
  ({
    assetId,
    allBalances,
    assetsList,
    changeList,
    marketList,
    borrowConversions,
    balancesConversionsState,
    intl,
    balanceConvert,
    pushNotification,
    openDepositModal,
    userAssets,
    openQuickTransferModal
  }: TProps) => {
    const data = useMemo(() => {
      const balance = allBalances.find(
        balance => balance.id === assetId
      ) as IBalance;

      const userAsset = userAssets.find(
        (userAsset: IUserAsset) => userAsset.asset.corecode === assetId
      ) as IUserAsset;

      if (!balance) {
        return {
          id: userAsset && userAsset.asset && userAsset.asset.corecode,
          name: userAsset && userAsset.asset && userAsset.asset.name,
          totalScaled: 0,
          total: 0,
          dollarWorth: 0,
          pending: false
        };
      }

      const totalScaled = balance.available + balance.reserved;
      const total = totalScaled / SCALED;
      const isUsd = isUsdAsset(balance.id);
      const matchingAsset: IAsset | undefined = assetsList.find(
        asset => asset.id === balance.id
      );
      const matchingChange = changeList.find(
        change => change.base === balance.id && isUsdAsset(change.counter)
      );
      const price = isUsd ? 1 : matchingChange ? matchingChange.price : 0;
      const name =
        (matchingAsset && matchingAsset.name) ||
        intl.formatMessage({ defaultMessage: "Unknown Asset" });

      const dollarWorth = price * total;
      const conversionState = balancesConversionsState[balance.id];

      return {
        id: balance.id,
        name: name,
        totalScaled,
        total: _round(total, (decimals[name] && decimals[name].balance) || 2),
        dollarWorth: _round(dollarWorth, decimals["USDT"].balance),
        pending: conversionState && conversionState.pending
      };
    }, [
      userAssets,
      allBalances,
      assetsList,
      changeList,
      balancesConversionsState,
      assetId,
      intl
    ]);

    const contractMarkets: IAsset[] = useMemo(() => {
      const assets: IBorrowConversion[] = borrowConversions.filter(
        (borrowConversion: IBorrowConversion) =>
          borrowConversion.asset_from === assetId
      );
      return assetsList.filter((asset: IAsset) =>
        assets.find(asst => asst.asset_to === asset.id)
      );
    }, [borrowConversions, assetsList, assetId]);

    const [selectedContractMarket, setSelectedContractMarket] = useState<
      IAsset
    >();

    useEffect(() => {
      if (contractMarkets.length && !selectedContractMarket) {
        setSelectedContractMarket(
          getClosestActiveAsset(contractMarkets) || contractMarkets[0]
        );
      }
    }, [contractMarkets, selectedContractMarket, setSelectedContractMarket]);

    const onTransferHandler = () => {
      if (!data || !data.total) {
        pushNotification({
          type: "DANGER",
          title: intl.formatMessage({ defaultMessage: "Deposit Required" }),
          message: intl.formatMessage({
            defaultMessage: "Insufficient funds. Please make a deposit first"
          })
        });
        return;
      }

      let requestData: IConvertAndDoBorrowData = {
        asset_from: assetId,
        asset_to: selectedContractMarket!.id,
        amount: data!.totalScaled
      };

      balanceConvert(requestData);
    };

    const onDepositHandler = () => openDepositModal(data!.id);

    return (
      <OneClickLeverageBalanceView
        {...data}
        fromAssetId={assetId}
        toAssetId={selectedContractMarket ? selectedContractMarket.id : 0}
        contractMarkets={contractMarkets}
        allMarketsList={marketList}
        selectedContractMarket={selectedContractMarket}
        setSelectedContractMarket={setSelectedContractMarket}
        onTransferHandler={onTransferHandler}
        onDepositHandler={onDepositHandler}
        userAssets={userAssets}
        openQuickTransferModal={openQuickTransferModal}
        isTransferButtonDisabled={!data.total}
      />
    );
  }
);

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    balanceConvert(data: IConvertAndDoBorrowData) {
      dispatch(balanceConvert(data));
    },
    pushNotification(data: INotification) {
      dispatch(pushNotification(data));
    }
  };
};

const mapStateToProps = (state: IAppState) => ({
  allBalances: state.allBalances,
  borrowConversions: state.borrowConversions,
  userAssets: state.userAssets,
  assetsList: state.assetsList,
  changeList: state.changeList,
  marketList: state.marketList,
  balancesConversionsState: state.balancesConversionsState
});

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(OneClickLeverageBalance);
