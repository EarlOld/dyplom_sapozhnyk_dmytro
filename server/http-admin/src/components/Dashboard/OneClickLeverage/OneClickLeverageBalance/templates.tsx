import React, { memo } from "react";
import { injectIntl, WrappedComponentProps } from "react-intl";
import { Button } from "@blueprintjs/core";
import "../style.scss";
import { ITableRow } from "../types";
import { ContractMonthDropdown } from "../ContractMonthDropdown/ContractMonthDropdown";
import clsx from "clsx";
import { ITransferModalData } from "../../index";

interface IOwnProps extends ITableRow {
  setSelectedContractMarket: (market: IAsset) => void;
  openQuickTransferModal: (transferData: ITransferModalData) => void;
  isTransferButtonDisabled: boolean;
  onDepositHandler: () => void;
  onTransferHandler: () => void;
  userAssets: IUserAsset[];
  allMarketsList: IMarket[];
  fromAssetId: number;
  toAssetId: number;
}

export const OneClickLeverageBalanceView = injectIntl(
  memo((props: IOwnProps & WrappedComponentProps) => {
    const { intl, userAssets, fromAssetId, toAssetId } = props;

    const userAsset: IUserAsset | undefined =
      (userAssets &&
        userAssets.find(
          (userAsset: IUserAsset) => userAsset.asset.corecode === props.id
        )) ||
      undefined;

    const pendingDeposit =
      userAsset &&
      userAsset.deposit_address_requested &&
      !userAsset.deposit_address;

    return (
      <tr key={props.id}>
        <td className="primary title">{props.name}</td>
        <td className="total" colSpan={2}>
          {props.total} <span className="currency">{props.name}</span>
        </td>
        <td className="deposit">
          <Button
            className="bp3-intent-primary"
            onClick={props.onDepositHandler}
            loading={pendingDeposit}
          >
            {intl.formatMessage({ defaultMessage: "Deposit" })}
          </Button>
        </td>
        <td className="month-selector">
          {props.selectedContractMarket && (
            <ContractMonthDropdown
              contractMarkets={props.contractMarkets}
              allMarketsList={props.allMarketsList}
              selectedContractMarket={props.selectedContractMarket}
              onChange={props.setSelectedContractMarket}
            />
          )}
        </td>
        <td className="actions" colSpan={4}>
          <Button
            disabled={props.isTransferButtonDisabled}
            onClick={props.onTransferHandler}
          >
            {intl.formatMessage({ defaultMessage: "Transfer" })}
          </Button>
          {fromAssetId && toAssetId && (
            <Button
              style={{ float: "right" }}
              className={clsx(props.total && "bp3-intent-primary")}
              disabled={props.isTransferButtonDisabled || props.pending}
              onClick={() =>
                props.openQuickTransferModal({
                  fromAssetId: fromAssetId,
                  toAssetId: toAssetId
                })
              }
            >
              {intl.formatMessage({
                defaultMessage: "Transfer + Leverage"
              })}
            </Button>
          )}
        </td>
      </tr>
    );
  })
);
