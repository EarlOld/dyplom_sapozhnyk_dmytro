import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import OneClickLeverage from "components/Dashboard/OneClickLeverage/OneClickLeverage";

export default {
  title: "OneClickLeverage",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <OneClickLeverage openDepositModal={() => {}} />;

Default.story = {
  name: "default"
};
