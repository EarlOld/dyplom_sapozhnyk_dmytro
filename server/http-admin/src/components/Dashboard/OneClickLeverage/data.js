export const DEFAULT_SELECT_SETTINGS = {
  allowCreate: false,
  filterable: false,
  hasInitialContent: false,
  minimal: false,
  resetOnClose: false,
  resetOnQuery: false,
  resetOnSelect: false,
  disableItems: false,
  disabled: false
};

export const PERMANENTLY_VISIBLE_ASSETS = [63520, 63488]; // ETH and XBT
export const REMAINING_ASSETS = [63496, 65283, 65282]; // BCH and USDT and USDC
export default DEFAULT_SELECT_SETTINGS;
