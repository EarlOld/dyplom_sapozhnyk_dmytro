import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import OneClickLeverage from "components/Dashboard/OneClickLeverage/OneClickLeverage";

describe("OneClickLeverage tables", () => {
  const handleOpenDepositModal = jest.fn();

  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(
      <OneClickLeverage openDepositModal={handleOpenDepositModal} />
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
