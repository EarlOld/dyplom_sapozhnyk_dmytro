import styled from "styled-components";
import { Button } from "@blueprintjs/core";
import React from "react";

const ButtonTemplate = props => <Button {...props} />;
export const MenuItem = styled(ButtonTemplate)`
  width: 100%;
  display: block !important;
  background: black;
  > span {
    font-weight: 400;
  }
`;
