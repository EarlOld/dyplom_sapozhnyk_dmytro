import React, { memo, useMemo, useState } from "react";
import {
  getCurrentAssetMonthIndex,
  getAssetMonthIndexByName,
  sortAssetsByMarketsIndexes
} from "../../utils";
import { DEFAULT_SELECT_SETTINGS } from "../data";
import { Select } from "@blueprintjs/select";
import { MenuItem } from "./MenuItem";
import { Button } from "@blueprintjs/core";

interface IProps {
  selectedContractMarket: IAsset;
  contractMarkets: IAsset[];
  allMarketsList: IMarket[];
  onChange: Function;
}

interface TempIAsset {
  index?: number;
}

type TempIAssetType = TempIAsset & IAsset;

export const ContractMonthDropdown = memo(
  ({
    selectedContractMarket,
    contractMarkets,
    onChange,
    allMarketsList
  }: IProps) => {
    const contractMarketsParsed = useMemo(() => {
      const temp = contractMarkets.map((item: TempIAssetType) => {
        return {
          ...item,
          index: getAssetMonthIndexByName(item)
        };
      });
      const getCurrentItemIndex = (currentMonth: number): number => {
        const itemIndex = temp.findIndex((item: TempIAssetType) => {
          return currentMonth === item.index;
        });
        return itemIndex === -1
          ? getCurrentItemIndex(currentMonth + 1)
          : itemIndex;
      };
      const currentMonth = getCurrentAssetMonthIndex();
      const itemIndex = getCurrentItemIndex(currentMonth);
      const sortFn = sortAssetsByMarketsIndexes.bind(null, allMarketsList);
      if (itemIndex === 0) {
        return [temp[itemIndex], ...sortFn(temp.slice(itemIndex + 1))];
      }
      if (itemIndex === contractMarkets.length - 1) {
        return [temp[itemIndex], ...sortFn(temp.slice(0, itemIndex))];
      }

      if (itemIndex !== -1) {
        const firstArr = sortFn(temp.slice(0, itemIndex));
        const lastArr = sortFn(temp.slice(itemIndex + 1));

        return [temp[itemIndex], ...firstArr, ...lastArr];
      }

      return contractMarkets;
    }, [contractMarkets, allMarketsList]);

    const RowSelect = Select.ofType<IAsset>();
    const [title, setTitle] = useState<string>(selectedContractMarket.name);

    return (
      <RowSelect
        className={"one-click-leverage-dropdown"}
        {...DEFAULT_SELECT_SETTINGS}
        items={contractMarketsParsed}
        itemRenderer={(contractMarket: IAsset, { handleClick }: any) => (
          <MenuItem onClick={handleClick} key={contractMarket.name}>
            {contractMarket.name}
          </MenuItem>
        )}
        onItemSelect={(contractMarket: IAsset) => {
          onChange(contractMarket);
          setTitle(contractMarket.name);
        }}
        disabled={contractMarkets.length === 1}
      >
        <Button rightIcon="caret-down">{title}</Button>
      </RowSelect>
    );
  }
);
