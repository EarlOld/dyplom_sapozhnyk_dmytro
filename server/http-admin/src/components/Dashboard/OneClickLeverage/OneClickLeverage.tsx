import React, { memo, useState } from "react";
import _flowRight from "lodash/flowRight";
import { connect } from "react-redux";
import { injectIntl, WrappedComponentProps } from "react-intl";
import { PERMANENTLY_VISIBLE_ASSETS, REMAINING_ASSETS } from "./data";
import { Card, Icon } from "@blueprintjs/core";
import { isSpotAsset } from "../../../utils/isSpotAsset";
import "./style.scss";
import OneClickLeverageBalance from "./OneClickLeverageBalance/OneClickLeverageBalance";
import clsx from "clsx";
import { ITransferModalData } from "../index";

interface IOwnProps {
  openDepositModal: (assetName: number) => void;
  openQuickTransferModal: (transferData: ITransferModalData) => void;
}

type TStateProps = ReturnType<typeof mapStateToProps>;
type TProps = TStateProps & IOwnProps & WrappedComponentProps;

export const OneClickLeverage = memo(
  ({
    assetsList,
    intl,
    borrowConversions,
    openDepositModal,
    userAssets = [],
    openQuickTransferModal
  }: TProps) => {
    const message = intl.formatMessage({
      defaultMessage: `Quick Start: `
    });
    const messageTwo = intl.formatMessage({
      defaultMessage: `Use the `
    });
    const messageThree = intl.formatMessage({
      defaultMessage: `Click Transfer and Leverage`
    });
    const messageFour = intl.formatMessage({
      defaultMessage: `to transfer and take 10x Leverage in one step`
    });

    const [isOpen, setIsOpen] = useState<boolean>(false);

    const getContractMarketMonths = (assetId: number): IAsset[] => {
      const assets: IBorrowConversion[] = borrowConversions.filter(
        (borrowConversion: IBorrowConversion) =>
          borrowConversion && borrowConversion.asset_from === assetId
      );
      return assetsList.filter((asset: IAsset) =>
        assets.find(asst => asst.asset_to === asset.id)
      );
    };

    const assetIds: number[] = (!!userAssets ? userAssets : [])
      .filter((userAsset: IUserAsset): boolean => {
        const matchingAsset: IAsset | undefined = assetsList.find(
          asset => asset.id === userAsset.asset.corecode
        );
        const hasCurrentContractMonths =
          matchingAsset && getContractMarketMonths(matchingAsset.id).length;
        const depositableAssets = isOpen
          ? [...PERMANENTLY_VISIBLE_ASSETS, ...REMAINING_ASSETS]
          : [...PERMANENTLY_VISIBLE_ASSETS];

        return !!(
          matchingAsset &&
          isSpotAsset(matchingAsset) &&
          depositableAssets.includes(matchingAsset.id) &&
          hasCurrentContractMonths
        );
      })
      .map(userAsset => userAsset.asset.corecode);

    return (
      <div>
        <Card className="one-click-leverage-wrapper" tabIndex={1}>
          <div className="bp3-card-header">
            <div>
              <span className="bp3-card-title">
                <span className={"yellow"}>{message}</span>
                <span className={"white"}>
                  {[messageTwo, `"`, messageThree, `" `, messageFour].join("")}
                </span>
              </span>
            </div>
          </div>
          <div className="bp3-card-content">
            <div className="one-click-leverage">
              <table
                className={clsx({
                  "bp3-html-table one-click-leverage-table": true,
                  "one-click-leverage-table-open": isOpen
                })}
              >
                <thead>
                  <tr>
                    <th className="coin">
                      {intl.formatMessage({ defaultMessage: "Asset" })}
                    </th>
                    <th className="balance" colSpan={2}>
                      {intl.formatMessage({ defaultMessage: "Balance" })}
                    </th>
                    <th className="deposit">
                      {intl.formatMessage({ defaultMessage: "Deposit" })}
                    </th>
                    <th className="month-selector">
                      {intl.formatMessage({ defaultMessage: "Contract Month" })}
                    </th>
                    <th className="actions" colSpan={4}>
                      {intl.formatMessage({ defaultMessage: "Actions" })}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {assetIds.map(assetId => (
                    <OneClickLeverageBalance
                      key={assetId}
                      assetId={assetId}
                      openDepositModal={openDepositModal}
                      openQuickTransferModal={openQuickTransferModal}
                    />
                  ))}
                </tbody>
              </table>
              <div className="footer-button" onClick={() => setIsOpen(!isOpen)}>
                <Icon
                  icon={isOpen ? "chevron-up" : "chevron-down"}
                  iconSize={24}
                />
              </div>
            </div>
          </div>
        </Card>
      </div>
    );
  }
);

const mapStateToProps = (state: IAppState) => ({
  userAssets: state.userAssets,
  allBalances: state.allBalances,
  assetsList: state.assetsList,
  borrowConversions: state.borrowConversions
});

export default _flowRight(
  connect(mapStateToProps),
  injectIntl
)(OneClickLeverage);
