export interface ITableRow {
  id: number;
  name: string;
  total: number;
  dollarWorth: number;
  pending: boolean;
  contractMarkets: IAsset[];
  selectedContractMarket?: IAsset;
}

export interface IOneClickLeverageRow {
  allowCreate: boolean;
  filterable: boolean;
  hasInitialContent: boolean;
  items: ITableRow[];
  minimal: boolean;
  resetOnClose: boolean;
  resetOnQuery: boolean;
  resetOnSelect: boolean;
  disableItems: boolean;
  disabled: boolean;
}
