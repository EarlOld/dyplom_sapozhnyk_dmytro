import React from "react";

import IconButton from "./IconButton";

export default {
  title: "IconButton"
};

// @ts-ignore
export const Default = () => (
  <div className="bp3-dark">
    <IconButton icon="delete" iconSize={23} />
  </div>
);

Default.story = {
  name: "default"
};
