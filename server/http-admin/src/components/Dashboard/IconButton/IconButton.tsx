import React, { memo } from "react";

import { Icon, IconName, MaybeElement } from "@blueprintjs/core";

import "./styles.scss";

export default memo(
  ({
    icon,
    iconSize,
    className,
    onClick,
    disabled
  }: {
    icon: IconName | MaybeElement;
    iconSize?: number;
    className?: string;
    onClick?: () => void;
    disabled?: boolean;
  }) => (
    <Icon
      className={`icon-button ${className || ""} ${disabled ? "disabled" : ""}`}
      icon={icon}
      iconSize={iconSize}
      onClick={disabled ? () => {} : onClick}
    />
  )
);
