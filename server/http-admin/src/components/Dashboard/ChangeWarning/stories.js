import React from "react";
import ChangeWarning from "./ChangeWarning";

export default {
  title: "DashboardChangeWarning"
};

export const Default = () => <ChangeWarning />;

Default.story = {
  name: "default"
};
