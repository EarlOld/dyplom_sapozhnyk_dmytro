import React, { useState } from "react";
import { Overlay, Classes, Icon } from "@blueprintjs/core";
import { FormattedMessage } from "react-intl";
import clsx from "clsx";

import "./styles.scss";

export default () => {
  const [isClosed, setIsClosed] = useState<boolean>(false);

  const alreadyWarned = !isClosed && !localStorage.getItem("warned");
  const doneHandler = () => {
    setIsClosed(true);
    localStorage.setItem("warned", "true");
  };

  const overlayClasses = clsx(
    Classes.OVERLAY,
    Classes.DARK,
    "change-warning-overlay"
  );

  return (
    <Overlay
      isOpen={alreadyWarned}
      onClose={doneHandler}
      className={overlayClasses}
    >
      <div className="change-warning">
        <div>
          <Icon
            icon="cross"
            iconSize={80}
            className="close-icon"
            onClick={doneHandler}
          />
          <h3>
            <FormattedMessage
              defaultMessage={
                "Hello, we’re making some changes to improve your experience. "
              }
            />
          </h3>
          <h4>
            <FormattedMessage
              defaultMessage={
                "Some areas of the platform may appear different as a result."
              }
            />
            <br />
            <FormattedMessage
              defaultMessage={"Should you have any questions then please "}
            />
            <a href="mailto:contact@coinflex.com">
              <FormattedMessage defaultMessage={"contact us"} />
            </a>
            .
          </h4>
        </div>
      </div>
    </Overlay>
  );
};
