import React from "react";
import ChangeWarning from "./ChangeWarning";
import { render } from "utils/testUtils";

describe("ChangeWarning", () => {
  it("Should match snapshot", () => {
    const { container } = render(<ChangeWarning />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
