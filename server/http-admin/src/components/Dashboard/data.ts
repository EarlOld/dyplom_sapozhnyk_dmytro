export const QR_CODE_CONFIG = [
  {
    name: "USDT",
    id: 65283
  },
  {
    name: "XBT",
    id: 63488
  },
  {
    name: "BCH",
    id: 63496
  },
  {
    name: "ETH",
    id: 63520
  },
  {
    name: "USDC",
    id: 65282
  },
  {
    name: "FLEX",
    id: 65285
  }
];

export const MONTHS_LIST = [
  "JAN",
  "FEB",
  "MAR",
  "APR",
  "MAY",
  "JUN",
  "JUL",
  "AUG",
  "SEP",
  "OCT",
  "NOV",
  "DEC"
];
