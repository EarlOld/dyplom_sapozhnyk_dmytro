import React, { memo, useEffect } from "react";
import { connect } from "react-redux";
import { Toaster, Intent } from "@blueprintjs/core";

import "./style.scss";

type TStateProps = ReturnType<typeof mapStateToProps>;

type TProps = TStateProps;

let lastDisplayedIndex = 0;
let toaster: Toaster | null;

const toasterRefHandler = (ref: Toaster) => (toaster = ref);

const NotificationViewer = memo(({ notifications }: TProps) => {
  useEffect(() => {
    if (notifications.length > lastDisplayedIndex) {
      const notification = notifications[lastDisplayedIndex];

      if (notification.title || notification.message) {
        toaster!.show({
          message: (
            <span>
              {notification.title && (
                <span className="title">{notification.title}</span>
              )}
              {notification.message && notification.title && " - "}
              {notification.message}
            </span>
          ),
          intent: Intent[notification.type || "NONE"]
        });
      }
      lastDisplayedIndex++;
    }
  }, [notifications]);

  return <Toaster ref={toasterRefHandler} className="toast-dashboard" />;
});

const mapStateToProps = (state: IAppState) => ({
  notifications: state.notifications
});

export default connect(mapStateToProps)(NotificationViewer);
