import React from "react";
import MainMenu from "./MainMenu";
import { render } from "utils/testUtils";

describe("Dashboard Main Menu", () => {
  it("Should match snapshot", () => {
    const { container } = render(<MainMenu />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
