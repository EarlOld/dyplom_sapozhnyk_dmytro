import React from "react";
import MainMenu from "./MainMenu";

export default {
  title: "Dashboard Main Menu"
};

export const Default = () => (
  <div className="bp3-dark">
    <MainMenu />
  </div>
);

Default.story = {
  name: "default"
};
