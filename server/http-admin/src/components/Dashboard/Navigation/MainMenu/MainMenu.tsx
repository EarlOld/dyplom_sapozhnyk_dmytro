import React from "react";
import { Menu, MenuItem, Icon } from "@blueprintjs/core";
import { NavLink } from "react-router-dom";
import { getCurrentUrl } from "utils/helpers";
import { injectIntl, WrappedComponentProps } from "react-intl";
import "./styles.scss";

interface IMenuItem {
  title: string;
  link: string;
  isNew?: boolean;
  isHot?: boolean;
  isHome?: boolean;
  isInternal?: boolean;
  secondaryTitle?: string;
  isPurple?: boolean;
}

const MainMenu = ({ intl }: WrappedComponentProps) => {
  const menuItems: IMenuItem[] = [
    {
      title: intl.formatMessage({
        defaultMessage: "Dashboard",
        id: "menu-dashboard"
      }),
      link: "/dashboard/home",
      isHome: true,
      isInternal: true
    },
    {
      title: intl.formatMessage({
        defaultMessage: "Balances",
        id: "menu-balances"
      }),
      secondaryTitle: intl.formatMessage({
        defaultMessage: "Deposit and Withdraw",
        id: "menu-deposit-withdrawal"
      }),
      link: "/dashboard/balances",
      isInternal: true
    },
    {
      title: intl.formatMessage({
        defaultMessage: "Deposit and Withdrawal History",
        id: "menu-deposit-withdrawal-history"
      }),
      link: "/dashboard/history",
      isInternal: true
    },
    {
      title: intl.formatMessage({
        defaultMessage: "Looking for the legacy dashboard?",
        id: "menu-find-legacy"
      }),
      secondaryTitle: intl.formatMessage({
        defaultMessage: "Click here or the links below",
        id: "menu-follow-link"
      }),
      isPurple: true,
      link: getCurrentUrl("show")
    },
    {
      title: intl.formatMessage({
        defaultMessage: "Competitions",
        id: "menu-competitions"
      }),
      link: getCurrentUrl("users_bracket_competitions"),
      isNew: true
    },
    {
      title: intl.formatMessage({
        defaultMessage: "FLEX Coin",
        id: "menu-flex-coin"
      }),
      link: getCurrentUrl("flex_coins/stakings"),
      isNew: true
    },
    {
      title: intl.formatMessage({
        defaultMessage: "FLEXMaker Program",
        id: "menu-flexmaker-program"
      }),
      link: getCurrentUrl("dmm_contests"),
      isHot: true
    },
    {
      title: intl.formatMessage({
        defaultMessage: "Trade History",
        id: "menu-trade-history"
      }),
      link: getCurrentUrl("trade_history")
    },
    {
      title: intl.formatMessage({
        defaultMessage: "Affiliate Program",
        id: "menu-affiliate-program"
      }),
      link: getCurrentUrl("affiliates")
    },
    {
      title: intl.formatMessage({
        defaultMessage: "Conversions",
        id: "menu-conversions"
      }),
      link: getCurrentUrl("conversions")
    },
    {
      title: intl.formatMessage({
        defaultMessage: "API Details",
        id: "menu-api-details"
      }),
      link: getCurrentUrl("api_access")
    },
    {
      title: intl.formatMessage({
        defaultMessage: "Enable Stablecoin",
        id: "menu-enable-stablecoin"
      }),
      link: getCurrentUrl("my_profile")
    }
  ];

  return (
    <Menu className="main-menu">
      {menuItems.map((item, index) => {
        const label = item.isNew ? (
          <span className="label new">
            {intl.formatMessage({ defaultMessage: "New!" })}
          </span>
        ) : item.isHot ? (
          <span className="label hot">
            {intl.formatMessage({ defaultMessage: "Hot!" })}
          </span>
        ) : null;

        if (item.isInternal) {
          return (
            <NavLink
              key={index}
              to={item.link}
              className="a-link-menu-item"
              activeClassName={"bp3-active"}
            >
              <MenuItem
                tagName={"div"}
                key={index}
                text={
                  <span>
                    {item.title}
                    <span className="label grey">{item.secondaryTitle}</span>
                  </span>
                }
                className="menu-item"
              />
            </NavLink>
          );
        }
        if (item.isPurple) {
          return (
            <MenuItem
              key={index}
              text={
                <div className="purple-menu-item-inner">
                  <span>{item.title}</span>
                  <span>{item.secondaryTitle}</span>
                </div>
              }
              href={item.link}
              target={item.isHome ? undefined : "_blank"}
              className="purple-menu-item"
            />
          );
        }
        return (
          <MenuItem
            key={index}
            text={
              <span>
                {item.title}
                {label}
                <Icon icon={"share"} style={{ float: "right" }} />
              </span>
            }
            href={item.link}
            active={item.isHome}
            target={item.isHome ? undefined : "_blank"}
            className="menu-item"
          />
        );
      })}
    </Menu>
  );
};

export default injectIntl(MainMenu);
