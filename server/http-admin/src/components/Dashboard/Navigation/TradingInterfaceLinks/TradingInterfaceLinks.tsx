import React, { memo } from "react";
import { Card, Icon } from "@blueprintjs/core";
import { ReactComponent as MarketsSVG } from "assets/icons/logo.markets.white.svg";
import { ReactComponent as ExecuteSVG } from "assets/icons/logo.execute.white.svg";

import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import "./styles.scss";

export const iotInterfaceLinks = memo(() => (
  <div className="platforms-links">
    <Card className="platforms-links-card">
      <a
        target="_blank"
        rel="noreferrer noopener"
        href="https://coinflex.trade.tt/live/workspaces/latest"
        className={"platform-links-link"}
      >
        <div className="logo">
          <ExecuteSVG />
        </div>
        <span>
          <FormattedMessage defaultMessage={"LAUNCH PLATFORM"} />
        </span>
        <Icon icon="circle-arrow-right" iconSize={23} className="right-icon" />
      </a>
    </Card>
    <Card className="platforms-links-card">
      <Link
        to={"/markets"}
        target="_blank"
        rel="noreferrer noopener"
        className={"platform-links-link"}
      >
        <div className="logo">
          <MarketsSVG />
        </div>
        <span>
          <FormattedMessage defaultMessage={"LAUNCH PLATFORM"} />
        </span>
        <Icon icon="circle-arrow-right" iconSize={23} className="right-icon" />
      </Link>
    </Card>
  </div>
));

export default iotInterfaceLinks;
