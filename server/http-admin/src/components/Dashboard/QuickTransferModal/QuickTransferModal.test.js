import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import QuickTransferModal from "components/Dashboard/QuickTransferModal";

describe("QuickTransferModal tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(
      <QuickTransferModal isOpen={true} fromAssetId={63488} toAssetId={51202} />
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
