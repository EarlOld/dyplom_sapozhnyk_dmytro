import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import QuickTransferModal from "components/Dashboard/QuickTransferModal/QuickTransferModal";

export default {
  title: "QuickTransferModal",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <QuickTransferModal isOpen={true} fromAssetId={63488} toAssetId={51202} />
);

Default.story = {
  name: "Open QuickTransferModal"
};
