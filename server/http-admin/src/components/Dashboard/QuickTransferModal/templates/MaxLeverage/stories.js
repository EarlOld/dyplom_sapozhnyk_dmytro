import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import MaxLeverage from "./";

export default {
  title: "MaxLeverage",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <MaxLeverage
    spotToFutures={true}
    assetOneString={"XBT"}
    assetTwoString={"XBT Mar"}
  />
);

Default.story = {
  name: "Open MaxLeverage"
};
