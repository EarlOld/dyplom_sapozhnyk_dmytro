import React, { useState } from "react";
import { Tab, Tabs } from "@blueprintjs/core";
import "./index.scss";
import ControlsGroup from "../../../ControlsGroup/ControlsGroup";

type TOwnProps = {
  onChange: (num: number) => void;
  defaultSelectedTabId?: string;
};

export const MaxLeverage = ({
  onChange = () => {},
  defaultSelectedTabId
}: TOwnProps) => {
  const [selectedTabId, setSelectedTabId] = useState<string>(
    defaultSelectedTabId || "5"
  );

  const onChangeHandler = (val: string): void => {
    if (selectedTabId !== val) {
      onChange(parseInt(val));
    }
    setSelectedTabId(val);
  };

  return (
    <ControlsGroup large={true}>
      <Tabs
        animate={false}
        onChange={onChangeHandler}
        selectedTabId={selectedTabId}
        className={"max-leverage"}
      >
        <Tab id={"0"} title="0x" />
        <Tab id={"5"} title="5x" />
        <Tab id={"10"} title="10x" />
        <Tab id={"20"} title="20x" />
      </Tabs>
    </ControlsGroup>
  );
};

export default MaxLeverage;
