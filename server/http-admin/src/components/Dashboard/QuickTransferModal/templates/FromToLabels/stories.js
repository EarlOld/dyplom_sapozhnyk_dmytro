import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import FromToLabels from "./";

export default {
  title: "FromToLabels",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <FromToLabels
    spotToFutures={true}
    assetOneString={"XBT"}
    assetTwoString={"XBT Mar"}
  />
);

Default.story = {
  name: "Open FromToLabels"
};
