import React from "react";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import { Icon, Tag } from "@blueprintjs/core";
import "./index.scss";

type TOwnProps = {
  assetOneString: string;
  spotToFutures: boolean;
  assetTwoString: string;
};
type TProps = WrappedComponentProps & TOwnProps;

export const Index = ({
  assetOneString,
  spotToFutures,
  assetTwoString
}: TProps) => {
  return (
    <div className={"from-to-label-container"}>
      <div className={"label-tag"}>
        <label>
          {spotToFutures ? (
            <FormattedMessage defaultMessage={"From Spot Balance"} />
          ) : (
            <FormattedMessage defaultMessage={"From Futures Contract"} />
          )}
        </label>
        <Tag round>{assetOneString}</Tag>
      </div>
      <Icon icon="arrow-right" />
      <div className={"label-tag"}>
        <label>
          {spotToFutures ? (
            <FormattedMessage defaultMessage={"To Futures Contract"} />
          ) : (
            <FormattedMessage defaultMessage={"To Spot Balance"} />
          )}
        </label>
        <Tag round>{assetTwoString}</Tag>
      </div>
    </div>
  );
};

export default injectIntl(Index);
