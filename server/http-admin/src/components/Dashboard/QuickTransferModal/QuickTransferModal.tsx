import React, { FormEvent, useMemo, useState } from "react";
import { dialogOptions } from "./data";
import _flowRight from "lodash/flowRight";
import { connect } from "react-redux";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import ControlsGroup from "components/Dashboard/ControlsGroup";
import { IDialogOptions } from "./types";
import {
  Button,
  Classes,
  Dialog,
  H5,
  InputGroup,
  Intent,
  Tab,
  Tabs
} from "@blueprintjs/core";
import _round from "lodash/round";
import { isSpotAsset } from "../../../utils/isSpotAsset";
import "./styles/index.scss";
import { getFuturesBalances, expandFuturesBalance } from "../Balances/utils";
import _get from "lodash/get";
import { balanceConvert } from "../../../store/actions";
import { SCALED } from "../../TicketForm/utils/config";
import FromToLabels from "./templates/FromToLabels";
import { MaxLeverage } from "./templates/MaxLeverage";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TOwnProps = {
  fromAssetId: number;
  toAssetId: number;
  isOpen: boolean;
  onClose: () => void;
  onChange: () => void;
};
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TProps = TStateProps & TDispatchProps & WrappedComponentProps & TOwnProps;
const options: IDialogOptions = dialogOptions;
export const QuickTransferModal = ({
  intl,
  fromAssetId,
  toAssetId,
  allBalances,
  assetsList,
  marketList,
  tickersList,
  isOpen,
  borrowConvertedTotals,
  onClose = () => {},
  convert,
  borrowLoans,
  borrowCollaterals,
  borrowOffers
}: TProps) => {
  const [loanMultiple, setLoanMultiple] = useState<number>(10);

  const [fromAsset] = useState<IAsset>(
    assetsList.filter((asset: IAsset) => asset.id === fromAssetId)[0]
  );
  const [toAsset] = useState<IAsset>(
    assetsList.filter((asset: IAsset) => asset.id === toAssetId)[0]
  );

  const [amountToTransferAsAString, setAmountToTransferAsAString] = useState<
    string
  >("");
  const amountToTransfer = useMemo(
    () => parseFloat(amountToTransferAsAString),
    [amountToTransferAsAString]
  );

  const fromAssetTotal: number = useMemo(() => {
    const isSpot: boolean = isSpotAsset(fromAsset);
    const expandedFuturesBalances: IFuturesBalance[] = getFuturesBalances(
      allBalances,
      assetsList,
      marketList,
      tickersList
    ).map((futuresBalance: IFuturesBalance) =>
      expandFuturesBalance(
        futuresBalance,
        borrowLoans,
        assetsList,
        borrowConvertedTotals,
        borrowCollaterals
      )
    );

    const balance = (isSpot ? allBalances : expandedFuturesBalances).find(
      balance => balance.id === fromAsset.id
    );

    const bal = _get(balance, isSpot ? "available" : "availableToWithdraw", 0);

    return isSpot ? bal / SCALED : bal;
  }, [
    fromAsset,
    allBalances,
    assetsList,
    tickersList,
    marketList,
    borrowCollaterals,
    borrowConvertedTotals,
    borrowLoans
  ]);

  const fromAssetName: string = useMemo(() => fromAsset && fromAsset.name, [
    fromAsset
  ]);

  const disableTransfer: boolean = useMemo(
    () => !amountToTransfer || !toAsset,
    [amountToTransfer, toAsset]
  );

  const tabChangeHandler = (percent: string) =>
    setAmountToTransferAsAString((+percent * 0.01 * fromAssetTotal).toString());

  const onInputChangeHandler = (event: FormEvent<HTMLInputElement>) =>
    setAmountToTransferAsAString((event.target as HTMLInputElement).value);

  const borrowOffer: IBorrowOffer | undefined = useMemo(() => {
    return borrowOffers
      ? borrowOffers.find(offer => offer.asset_id === toAsset.id)
      : undefined;
  }, [borrowOffers, toAsset.id]);

  const onSubmitHandler = () => {
    let requestData: IConvertAndDoBorrowTwiceData = {
      asset_from: fromAsset.id,
      asset_to: toAsset!.id,
      amount: _round(amountToTransfer * SCALED)
    };

    if (loanMultiple) {
      requestData = {
        ...requestData,
        borrow_offer_id: borrowOffer!.id,
        borrow_amount: _round(amountToTransfer * SCALED) * loanMultiple
      };

      const counterId = _get(
        marketList.filter(market => market.base === toAssetId),
        [0, "counter"]
      );

      const counterBorrowOffer = borrowOffers.find(
        offer => offer.asset_id === counterId
      );

      const counterAmount =
        (_get(
          tickersList.filter(
            ticker => ticker.base === toAssetId && ticker.counter === counterId
          ),
          "[0].last",
          0
        ) *
          _get(requestData, "borrow_amount", 1)) /
        SCALED;

      if (counterId && counterBorrowOffer && counterAmount) {
        requestData = {
          ...requestData,
          counter_borrow_offer_id: counterBorrowOffer!.id,
          counter_borrow_amount: counterAmount
        };
      }
    }

    convert(requestData);
    onClose();
  };

  return (
    <>
      <Dialog
        className={Classes.DARK}
        onClose={onClose}
        title={`Quick Transfer and Set Leverage`}
        {...options}
        enforceFocus={false}
        isOpen={isOpen}
      >
        <div className={Classes.DIALOG_BODY}>
          <div className={"quick-transfer-modal-container"}>
            <p className={"modal-info"}>
              <FormattedMessage defaultMessage="Select the amount you wish to transfer from your available Spot balance to the futures contract you wish to trade" />
            </p>
            <FromToLabels
              spotToFutures={true}
              assetOneString={fromAsset.name}
              assetTwoString={toAsset.name}
            />
            <H5 className="bp3-heading" style={{ color: "#FFF" }}>
              <FormattedMessage defaultMessage={"Transfer Amount"} />
              <span className={"totals"}>
                {amountToTransferAsAString || 0} {fromAssetName}
              </span>
            </H5>
            <ControlsGroup large={true}>
              <InputGroup
                type="text"
                className={"amount-input"}
                large
                value={amountToTransferAsAString}
                onChange={onInputChangeHandler}
                placeholder={intl.formatMessage(
                  {
                    defaultMessage: `Enter amount`
                  },
                  { fromAsset: fromAssetName }
                )}
              />
              <Tabs
                animate={false}
                onChange={tabChangeHandler}
                selectedTabId={undefined}
              >
                <Tab id={"25"} title="25%" />
                <Tab id={"50"} title="50%" />
                <Tab id={"75"} title="75%" />
                <Tab id={"100"} title="100%" />
              </Tabs>
            </ControlsGroup>
            <p className={"cf-helper"}>
              <FormattedMessage
                defaultMessage={
                  "Enter or select and amount of collateral to allocate"
                }
              />
            </p>
            <H5 className="bp3-heading" style={{ color: "#FFF" }}>
              <FormattedMessage defaultMessage={"Set MAX Leverage"} />
            </H5>
            <MaxLeverage
              onChange={setLoanMultiple}
              defaultSelectedTabId={"10"}
            />
            <p className={"cf-helper"}>
              <FormattedMessage
                defaultMessage={
                  "Take {loanMultiple}x Leverage on both USDT{month} and {toAsset}"
                }
                values={{
                  toAsset: toAsset && toAsset.name,
                  month: toAsset.name.slice(3, 6),
                  loanMultiple: loanMultiple
                }}
              />
            </p>
          </div>
          <div className={Classes.DIALOG_FOOTER}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button
                className={"transfer-confirm"}
                large={true}
                onClick={onSubmitHandler}
                fill
                intent={Intent.PRIMARY}
                style={{ minHeight: "50px", marginLeft: 0 }}
                disabled={disableTransfer}
              >
                <FormattedMessage
                  defaultMessage={"Transfer and Set Leverage"}
                  values={{ fromAsset: fromAssetName }}
                />
              </Button>
            </div>
          </div>
        </div>
      </Dialog>
    </>
  );
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    convert(data: IConvertAndDoBorrowTwiceData) {
      dispatch(balanceConvert(data));
    }
  };
};

const mapStateToProps = (state: IAppState) => {
  return {
    assetsList: state.assetsList,
    borrowLoans: state.borrowLoans,
    borrowCollaterals: state.borrowCollaterals,
    marketList: state.marketList,
    allBalances: state.allBalances,
    tickersList: state.tickersList,
    borrowConvertedTotals: state.borrowConvertedTotals,
    borrowOffers: state.borrowOffers
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(QuickTransferModal);
