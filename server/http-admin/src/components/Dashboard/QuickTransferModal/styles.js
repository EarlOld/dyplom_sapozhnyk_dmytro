import styled from "styled-components";
export const StyledContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  flex-direction: column;
  .cf-helper {
    color: #61686b;
    margin-top: 4px;
    font-size: 12px;
    font-weight: 300;
  }
`;
