import { PureComponent } from "react";
import { connect } from "react-redux";
import _flowRight from "lodash/flowRight";
import { injectIntl, WrappedComponentProps } from "react-intl";

import { pushNotification } from "store/actions";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TProps = TStateProps & TDispatchProps & WrappedComponentProps;

class NotificationManager extends PureComponent<TProps> {
  componentDidUpdate(prevProps: TProps) {
    this.checkBalancesConvertionsState(prevProps);
    this.checkUploadWithdrowalAddressState(prevProps);
    this.checkWithdrawalState(prevProps);
  }

  checkBalancesConvertionsState(prevProps: TProps) {
    const {
      balancesConversionsState,
      pushNotification,
      intl,
      assetsList
    } = this.props;
    const prevBalancesConvertionsState = prevProps.balancesConversionsState;

    if (balancesConversionsState !== prevBalancesConvertionsState) {
      for (const [balanceId, convertState] of Object.entries(
        balancesConversionsState
      )) {
        const prevConvertState = prevBalancesConvertionsState[+balanceId];
        const isQuickTransfer =
          convertState.borrow_offer_id && convertState.borrow_amount;
        if (
          convertState.success &&
          (!prevConvertState || !prevConvertState.success)
        ) {
          const title = isQuickTransfer
            ? intl.formatMessage({
                defaultMessage: "Quick Transfer Successful"
              })
            : intl.formatMessage({
                defaultMessage: "Transfer Successful"
              });

          const asset =
            assetsList &&
            assetsList.find(asset => asset.id === convertState.asset_to);

          const assetName =
            (asset && asset.name) ||
            intl.formatMessage({ defaultMessage: "Unknown Asset" });

          const message = isQuickTransfer
            ? intl.formatMessage(
                {
                  defaultMessage: "{name} contracts now tradeable"
                },
                {
                  name: assetName
                }
              )
            : "";

          pushNotification({
            type: "SUCCESS",
            title,
            message
          });
        }
        if (
          convertState.error &&
          (!prevConvertState || !prevConvertState.error)
        ) {
          const title = isQuickTransfer
            ? intl.formatMessage({
                defaultMessage: "Quick Transfer Error"
              })
            : intl.formatMessage({
                defaultMessage: "Transfer Error"
              });

          pushNotification({
            type: "DANGER",
            title,
            message: convertState.errorMessage
          });
        }
      }
    }
  }

  checkUploadWithdrowalAddressState(prevProps: TProps) {
    const uploadState = this.props.uploadBalanceWithdrawalAddress;
    const prevUploadState = prevProps.uploadBalanceWithdrawalAddress;
    const { pushNotification, intl } = this.props;

    if (uploadState.success && !(prevUploadState && prevUploadState.success)) {
      pushNotification({
        type: "SUCCESS",
        message: intl.formatMessage({
          defaultMessage: "Withdrawal Address saved successfully"
        })
      });
    }
    if (uploadState.error && !(prevUploadState && prevUploadState.error)) {
      pushNotification({
        type: "DANGER",
        title: intl.formatMessage({
          defaultMessage: "Error saving Withdrawal Address"
        }),
        message: uploadState.errorMessage
      });
    }
  }

  checkWithdrawalState(prevProps: TProps) {
    const withdrawState = this.props.balanceWithdraw;
    const prevWithdrawState = prevProps.balanceWithdraw;
    const { pushNotification, intl } = this.props;

    if (
      withdrawState.success &&
      !(prevWithdrawState && prevWithdrawState.success)
    ) {
      pushNotification({
        type: "SUCCESS",
        message: intl.formatMessage({
          defaultMessage: "Withdrawal successful"
        })
      });
    }
    if (
      withdrawState.error &&
      !(prevWithdrawState && prevWithdrawState.error)
    ) {
      pushNotification({
        type: "DANGER",
        title: intl.formatMessage({
          defaultMessage: "Withdrawal error"
        }),
        message: withdrawState.errorMessage
      });
    }
  }

  render() {
    return null;
  }
}

const mapStateToProps = (state: IAppState) => {
  return {
    balancesConversionsState: state.balancesConversionsState,
    assetsList: state.assetsList,
    uploadBalanceWithdrawalAddress: state.uploadBalanceWithdrawalAddress,
    balanceWithdraw: state.balanceWithdraw
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    pushNotification(data: INotification) {
      dispatch(pushNotification(data));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(NotificationManager);
