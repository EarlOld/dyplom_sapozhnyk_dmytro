export const suggestOptions = {
  allowCreate: false,
  closeOnSelect: true,
  createdItems: [],
  fill: true,
  items: [],
  minimal: true,
  openOnKeyDown: false,
  resetOnClose: false,
  resetOnQuery: true,
  resetOnSelect: false,
  large: true
};

export const dialogOptions = {
  autoFocus: true,
  canEscapeKeyClose: true,
  canOutsideClickClose: true,
  enforceFocus: true,
  usePortal: true
};
