import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import WithdrawalModal from "components/Dashboard/WithdrawalModal/WithdrawalModal";

describe("WithdrawalModal tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<WithdrawalModal isOpen />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
