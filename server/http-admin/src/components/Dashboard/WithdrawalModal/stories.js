import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import WithdrawalModal from "components/Dashboard/WithdrawalModal/WithdrawalModal";

export default {
  title: "WithdrawalModal",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <WithdrawalModal isOpen={true} assetId={63488} />;

Default.story = {
  name: "Open WithdrawalModal"
};
