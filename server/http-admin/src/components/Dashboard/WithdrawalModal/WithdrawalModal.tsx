import React, { useMemo, useState, useEffect } from "react";
import { dialogOptions } from "./data";
import _flowRight from "lodash/flowRight";
import _get from "lodash/get";
import { connect } from "react-redux";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import ControlsGroup from "components/Dashboard/ControlsGroup";
import { IDialogOptions } from "./types";
import {
  Dialog,
  Classes,
  Button,
  Elevation,
  Card,
  Intent,
  H5,
  InputGroup,
  Tabs,
  Tab,
  FormGroup
} from "@blueprintjs/core";
import {
  StyledContainer,
  StyledModalElement,
  StyledModalElementInline,
  StyledModalElementInlineTwo,
  StyledModalElementCard
} from "./styles";

import {
  uploadBalanceWithdrawalAddress,
  balanceWithdraw,
  getUserAssets
} from "store/actions";
import isNumeric from "utils/isNumeric";
import { SCALED } from "../Balances/data";
import { isValidAddress } from "../utils";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TOwnProps = {
  assetId: number;
  isOpen: boolean;
  onClose: () => void;
};
type TProps = TStateProps & TDispatchProps & WrappedComponentProps & TOwnProps;
const options: IDialogOptions = dialogOptions;
export const WithdrawalModal = ({
  userAssets,
  intl,
  assetId,
  assetsList,
  allBalances,
  isOpen,
  onClose = () => {},
  uploadWithdrawalAddress,
  balanceWithdraw,
  uploadBalanceWithdrawalAddressState,
  balanceWithdrawState,
  getUserAssets
}: TProps) => {
  const [editAddressMode, setEditAddressMode] = useState<boolean>(false);
  const asset: IAsset | undefined = useMemo(
    () => assetsList.find((asset: IAsset) => asset.id === assetId) || undefined,
    [assetsList, assetId]
  );
  const userAsset: IUserAsset | undefined = useMemo(
    () =>
      (userAssets &&
        userAssets.find(
          (userAsset: IUserAsset) => userAsset.asset.corecode === assetId
        )) ||
      undefined,
    [userAssets, assetId]
  );
  const balance: IBalance | undefined = useMemo(
    () =>
      allBalances.find((balance: IBalance) => balance.id === assetId) ||
      undefined,
    [allBalances, assetId]
  );

  const address: string | null = _get(userAsset, "withdrawal_address", "");

  const validAddress: boolean = useMemo(() => {
    return !!address ? isValidAddress(address, assetId) : false;
  }, [assetId, address]);

  const ready = !!asset && !!userAsset && !!balance;
  const acceptNewAddress = !validAddress || editAddressMode;
  const [newWithdrawalAddress, setNewWithdrawalAddress] = useState<string>("");
  const [newWithdrawalAddressError, setNewWithdrawalAddressError] = useState<
    string
  >("");
  const [tfaCode, setTfaCode] = useState<string>("");
  const [tfaCodeError, setTfaCodeError] = useState<string>("");
  const [amountLine, setAmountLine] = useState<string>("");
  const [amountLineError, setAmountLineError] = useState<string>("");

  useEffect(() => {
    setTfaCodeError("");
    setTfaCodeError("");
    setAmountLine("");
    setAmountLineError("");
  }, [isOpen]);

  useEffect(() => {
    uploadBalanceWithdrawalAddressState.success && getUserAssets();
  }, [uploadBalanceWithdrawalAddressState, getUserAssets]);

  const balanceTotal = balance
    ? (balance!.available + balance!.reserved) / SCALED
    : 0;

  const checkTfaCode = () => {
    const isError = !tfaCode.trim().length;

    setTfaCodeError(
      isError
        ? intl.formatMessage({
            defaultMessage: "Please enter Two Factor Authentication Code",
            id: "empty-tfa-code-error"
          })
        : ""
    );

    return !isError;
  };

  const checkNewWithdrawalAddress = () => {
    const isError = !newWithdrawalAddress.trim().length;

    setNewWithdrawalAddressError(
      isError
        ? intl.formatMessage({
            defaultMessage: "Please enter Withdrawal Address",
            id: "empty-withdrawal-address-error"
          })
        : ""
    );

    return !isError;
  };

  const checkAmount = () => {
    const amount = +amountLine;
    let error = "";

    if (!isNumeric(amountLine)) {
      error = intl.formatMessage({
        id: "not-a-number-error",
        defaultMessage: "Please enter an amount to withdraw"
      });
    } else if (amount > balanceTotal) {
      error = intl.formatMessage(
        {
          id: "max-value-error",
          defaultMessage: "Maximum value is {balanceTotal}"
        },
        { balanceTotal }
      );
    } else if (amount < 0) {
      error = intl.formatMessage({
        id: "min-value-error",
        defaultMessage: "Please enter a positive number"
      });
    }

    setAmountLineError(error);

    return !error;
  };

  const upload = () => {
    const isTfaChecked = checkTfaCode();
    const isAddressChecked = checkNewWithdrawalAddress();

    if (!(isTfaChecked && isAddressChecked)) {
      return;
    }

    uploadWithdrawalAddress({
      asset_iso: asset!.name,
      tfa_code: tfaCode.trim(),
      address: newWithdrawalAddress
    });
  };

  const withdraw = () => {
    const isTfaChecked = checkTfaCode();
    const isAmountChecked = checkAmount();

    if (
      !(isTfaChecked && isAmountChecked) ||
      !(userAsset && userAsset.withdrawal_address)
    ) {
      return;
    }

    balanceWithdraw({
      asset_iso: asset!.name,
      tfa_code: tfaCode.trim(),
      amount: +amountLine
    });
  };

  const handleNewAddressChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value);
    setNewWithdrawalAddress(e.target.value);
  };

  const handleTfaCodeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTfaCode(e.target.value);
  };

  const handleAmountLineChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAmountLine(e.target.value);
  };

  const handleAmountTabsChange = (tab: string) => {
    const amount = (balanceTotal * +tab) / 100;

    setAmountLine("" + amount);
  };

  const isUploadAddressButtonDisabled =
    uploadBalanceWithdrawalAddressState.pending;
  const isWithdrawalButtonDisabled =
    uploadBalanceWithdrawalAddressState.pending ||
    balanceWithdrawState.pending ||
    !(userAsset && userAsset.withdrawal_address);

  if (ready) {
    return (
      <>
        <Dialog
          className={Classes.DARK}
          onClose={onClose}
          title={`${asset && asset.name} Withdrawal`}
          {...options}
          isOpen={isOpen}
        >
          <div className={Classes.DIALOG_BODY}>
            <StyledContainer>
              <StyledModalElement>
                <H5 className="bp3-heading">
                  {asset && asset.name}
                  <FormattedMessage defaultMessage={" Withdrawal Address"} />
                </H5>
                <Card elevation={Elevation.ZERO}>
                  {acceptNewAddress ? (
                    <>
                      <Button
                        className={"back"}
                        onClick={() => setEditAddressMode(false)}
                        icon={"circle-arrow-left"}
                        intent={Intent.NONE}
                        disabled={!validAddress}
                      />
                      <FormGroup
                        helperText={newWithdrawalAddressError}
                        intent="danger"
                      >
                        <InputGroup
                          large
                          placeholder={intl.formatMessage({
                            defaultMessage: "Enter A New Withdrawal Address",
                            id: "address-input-placeholder"
                          })}
                          onChange={handleNewAddressChange}
                        />
                      </FormGroup>
                      <Button
                        onClick={upload}
                        icon={"upload"}
                        intent={Intent.NONE}
                        disabled={isUploadAddressButtonDisabled}
                      />
                    </>
                  ) : (
                    <>
                      <InputGroup
                        className={"predefined"}
                        large
                        placeholder={address || undefined}
                        readOnly
                      />
                      <Button
                        onClick={() => setEditAddressMode(true)}
                        intent={Intent[address ? "NONE" : "PRIMARY"]}
                      >
                        <FormattedMessage defaultMessage={"Edit"} />
                      </Button>
                    </>
                  )}
                </Card>
              </StyledModalElement>
              <StyledModalElement>
                <H5 className="bp3-heading">
                  <FormattedMessage
                    defaultMessage={"Two Factor Authentication Code"}
                  />
                </H5>
                <Card elevation={Elevation.ZERO} style={{ paddingTop: 0 }}>
                  <FormGroup helperText={tfaCodeError} intent="danger">
                    <InputGroup
                      style={{ paddingTop: "0" }}
                      large
                      placeholder={intl.formatMessage({
                        defaultMessage: "Enter Two Factor Authentication Code",
                        id: "tfa-placeholder"
                      })}
                      onChange={handleTfaCodeChange}
                    />
                  </FormGroup>
                </Card>
              </StyledModalElement>
              <StyledModalElementInline>
                <H5 className="bp3-heading">
                  <FormattedMessage
                    defaultMessage={"{coin} Amount"}
                    values={{ coin: asset && asset.name }}
                  />
                </H5>
              </StyledModalElementInline>
              <StyledModalElementInlineTwo>
                <ControlsGroup large={true}>
                  <FormGroup helperText={amountLineError} intent="danger">
                    <InputGroup
                      className={"amount-input"}
                      large
                      onChange={handleAmountLineChange}
                      value={amountLine}
                      placeholder={intl.formatMessage(
                        {
                          defaultMessage: `Enter amount`
                        },
                        { coin: asset && asset.name }
                      )}
                    />
                  </FormGroup>
                  <Tabs
                    animate={false}
                    onChange={handleAmountTabsChange}
                    defaultSelectedTabId={"undefined"}
                  >
                    <Tab title="25%" id="25" />
                    <Tab title="50%" id="50" />
                    <Tab title="75%" id="75" />
                    <Tab title="100%" id="100" />
                  </Tabs>
                </ControlsGroup>
              </StyledModalElementInlineTwo>
              <StyledModalElementInline>
                <H5 className="bp3-heading">
                  <FormattedMessage defaultMessage={"Min Withdrawal"} />
                </H5>
                <H5 className="bp3-heading">
                  <FormattedMessage defaultMessage={"Withdrawal Fee"} />
                </H5>
              </StyledModalElementInline>
              <StyledModalElementInlineTwo>
                <label className="bp3-text-muted">
                  {userAsset &&
                    userAsset.asset.min_withdrawal + " " + asset!.name}
                </label>
                <label className="bp3-text-muted">
                  {userAsset && userAsset.asset.withdrawal_fee}
                </label>
              </StyledModalElementInlineTwo>
              <StyledModalElementInline>
                <H5 className="bp3-heading">
                  <FormattedMessage defaultMessage={"Important Information"} />
                </H5>
              </StyledModalElementInline>
              <StyledModalElementCard>
                <Card interactive={false} elevation={Elevation.ONE}>
                  <p>
                    <label className="bp3-text-muted">
                      <FormattedMessage
                        defaultMessage={
                          "Please make sure to always review the below information before making a withdrawal, as it may change in the future."
                        }
                      />
                    </label>
                    <br />
                    <br />
                    <label className="bp3-text-muted">
                      <FormattedMessage defaultMessage={"Destination: "} />
                    </label>
                    <label className="bp3-text-muted light">
                      <FormattedMessage
                        defaultMessage={
                          "You are able to register one {coin} withdrawal address at a time. Please only withdraw to addresses you know and trust. CoinFLEX performs transaction monitoring and has a zero tolerance policy for activity associated with money laundering, terrorism, fraud, crime or darknet markets. Association with illicit activities will result in your accounts closure."
                        }
                        values={{ coin: asset && asset.name }}
                      />
                    </label>
                    <br />
                    <br />
                    <label className="bp3-text-muted">
                      <FormattedMessage
                        defaultMessage={"Destination address management: "}
                      />
                    </label>
                    <label className="bp3-text-muted light">
                      <FormattedMessage
                        defaultMessage={
                          "You can change your withdrawal address anytime, using your Authy token, Google Authenticator or YubiKey to authenticate the change from the website. CoinFLEX supports withdrawals to P2SH bitcoin addresses. Withdrawals are sent to the address that is your currently registered withdrawal address at the time of placing the withdrawal request and it is not possible to cancel a withdrawal or change the destination address for a withdrawal after placing the request. Registering an address from a non-matching network may result in loss of the withdrawal. "
                        }
                        values={{ coin: asset && asset.name }}
                      />
                    </label>
                    <br />
                    <br />
                    <label className="bp3-text-muted">
                      <FormattedMessage defaultMessage={"Clearing time: "} />
                    </label>
                    <label className="bp3-text-muted light">
                      <FormattedMessage
                        defaultMessage={
                          "Withdrawals are processed 24/7. You can track the transaction on the blockchain by monitoring your withdrawal address. The 24/7 support is not guaranteed during times of high volumes. In such cases 24h processing time applies."
                        }
                        values={{
                          coin: asset && asset.name
                        }}
                      />
                    </label>
                    <label className="bp3-text-muted light">
                      <FormattedMessage
                        defaultMessage={
                          "Withdrawals are processed 24/7. You can track the transaction on the blockchain by monitoring your withdrawal address. The 24/7 support is not guaranteed during times of high volumes. In such cases 24h processing time applies."
                        }
                        values={{
                          coin: asset && asset.name
                        }}
                      />
                    </label>
                  </p>
                </Card>
              </StyledModalElementCard>
            </StyledContainer>
          </div>
          <div className={Classes.DIALOG_FOOTER} style={{ marginTop: 0 }}>
            <div className={Classes.DIALOG_FOOTER_ACTIONS}>
              <Button
                large={true}
                onClick={withdraw}
                fill
                intent={Intent.PRIMARY}
                style={{ minHeight: "50px", margin: "0 20px" }}
                disabled={isWithdrawalButtonDisabled}
              >
                <FormattedMessage
                  defaultMessage={"Withdraw {coin}"}
                  values={{ coin: asset && asset.name }}
                />
              </Button>
            </div>
          </div>
        </Dialog>
      </>
    );
  } else {
    return null;
  }
};

const mapStateToProps = (state: IAppState) => {
  return {
    assetsList: state.assetsList,
    userAssets: state.userAssets,
    allBalances: state.allBalances,
    uploadBalanceWithdrawalAddressState: state.uploadBalanceWithdrawalAddress,
    balanceWithdrawState: state.balanceWithdraw
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    uploadWithdrawalAddress(data: IWithdrawalAddressData) {
      dispatch(uploadBalanceWithdrawalAddress(data));
    },
    balanceWithdraw(data: IWithdrawalData) {
      dispatch(balanceWithdraw(data));
    },
    getUserAssets() {
      dispatch(getUserAssets());
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(WithdrawalModal);
