import styled from "styled-components";
export const StyledContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  flex-direction: column;
`;

export const StyledModalElement = styled.div`
  margin-top: 16px;
  flex: 1;
  .bp3-card {
    display: flex;
    border: none;
    box-shadow: none !important;
    padding-left: 0;
    padding-right: 0;

    > div {
      flex: 1;
      &.predefined > input {
        box-shadow: none !important;
      }

      input.bp3-input {
        font-size: 14px;
      }
    }

    .bp3-form-group {
      margin-bottom: 0;
      .bp3-form-content {
        .bp3-form-helper-text {
          color: #f59a23;
        }
      }
    }
    button {
      margin-top: 1px;
      margin-left: 10px;
      height: 44px;
    }
    .back {
      margin-right: 10px;
      margin-left: 0;
    }
  }
`;

export const StyledModalElementInline = styled.div`
  display: flex;
  flex: 1;
  margin-top: 40px;
  h5 {
    flex: 1;
  }
`;

export const StyledModalElementInlineTwo = styled.div`
  width: 100%;
  display: flex;
  .controls-group {
    display: flex;
    width: 100%;

    .bp3-form-group {
      width: 60%;
      margin-bottom: 0;
      .bp3-form-content {
        .bp3-form-helper-text {
          color: #f59a23;
        }
      }
    }

    .bp3-input-group {
      &.amount-input > input {
        font-size: 14px;
      }
    }
    .bp3-tabs {
      flex: 1;
    }
  }
  label {
    flex: 1;
  }
`;

export const StyledModalElementCard = styled.div`
  width: 100%;
  display: flex;
  .bp3-card {
    overflow-y: scroll;
    height: 150px;
    .light {
      font-weight: 300;
    }
  }
`;
