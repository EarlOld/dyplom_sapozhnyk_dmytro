import React from "react";

import Pane from "../Pane";

import { tabs } from "./data";

export default () => <Pane title="Positions" tabs={tabs} />;
