import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Dashboard from "./index";

export default {
  title: "Dashboard",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <Dashboard />;

Default.story = {
  name: "default"
};
