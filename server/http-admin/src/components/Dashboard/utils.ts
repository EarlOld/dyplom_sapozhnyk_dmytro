import _isEmpty from "lodash/isEmpty";
import moment from "moment";
import { MONTHS_LIST } from "./data";

// export const isAlphanumeric = (testStr: string) => /^[a-z0-9]+$/i.test(testStr);
export const isValidAddress = (testStr: string, assetId: number) => {
  return !_isEmpty(testStr);
};

export const getCurrentAssetMonthIndex = () => {
  const currentData = moment(new Date());
  const day = Number(currentData.format("D"));
  const month = Number(currentData.format("M"));
  if (day >= 26) return month;
  return month - 1;
};

export const getAssetMonthIndexByName = (item: IAsset): number => {
  const assetSpotNameParts = item.name.split(String(item.spot_name));
  const assetMonthName = assetSpotNameParts.length > 1 && assetSpotNameParts[1];

  return MONTHS_LIST.findIndex(monthName => {
    return assetMonthName && assetMonthName.startsWith(monthName);
  });
};

export const getClosestActiveAsset = (
  assetsList: IAsset[]
): IAsset | undefined => {
  const currentMonthIndex = getCurrentAssetMonthIndex();

  const tempAssetsList = assetsList.map(item => ({
    index: getAssetMonthIndexByName(item),
    asset: item
  }));

  const sortedAssetsList = tempAssetsList.sort(
    (itemA, itemB) => itemA.index - itemB.index
  );

  for (const item of sortedAssetsList) {
    if (item.index >= currentMonthIndex) {
      return item.asset;
    }
  }
};

export const isMatchingMarketForAsset = (
  asset: IAsset,
  market: IMarket
): boolean =>
  market.base === asset.id ||
  (asset.spot_name === "USDT" && market.counter === asset.id);

export const compareAssetsByMarketsIndexes = (
  assetA: IAsset,
  assetB: IAsset,
  marketList: IMarket[]
) => {
  const marketIndexA = marketList.findIndex(market =>
    isMatchingMarketForAsset(assetA, market)
  );
  const marketIndexB = marketList.findIndex(market =>
    isMatchingMarketForAsset(assetB, market)
  );

  return marketIndexA === marketIndexB
    ? 0
    : marketIndexA > marketIndexB
    ? -1
    : 1;
};

export const sortAssetsByMarketsIndexes = (
  marketsList: IMarket[],
  assets: IAsset[]
) =>
  assets.sort((assetA, assetB) =>
    compareAssetsByMarketsIndexes(assetA, assetB, marketsList)
  );
