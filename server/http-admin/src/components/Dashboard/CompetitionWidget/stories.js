import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import CompetitionWidget from "./index";

export default {
  title: "Dashboard",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <CompetitionWidget />;

Default.story = {
  name: "default"
};
