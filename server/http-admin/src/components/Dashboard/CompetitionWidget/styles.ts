import styled from "styled-components";
import { Card } from "@blueprintjs/core";

export const Header = styled.div`
  margin: -15px -15px 15px -15px;
  padding: 12px 15px;
  border-bottom: 1px solid #353d42;
  .bp3-heading {
    font-size: 16px;
    font-weight: 700;
    margin-bottom: 0;
  }
  svg {
    cursor: pointer;
  }
`;

export const Title = styled.div`
  h5.bp3-heading {
    color: #fff;
    font-size: 14px;
    font-weight: 500;
    margin-bottom: 12px;

    &.prize {
      color: #ccc;
      font-weight: 700;
    }
  }
`;

export const CardWithBackgroundImage = styled(Card)<{ bg: any }>`
  margin: 0 0 15px 0;
  padding: 0 !important;
  background-image: url(${props => props.bg});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  .bracket-competitions-table {
    border-collapse: collapse;
    width: 100%;
    padding: 0;
    color: #fff;
    tr {
      padding: 0;
      margin-top: 60px;
      th {
        text-align: center;
        border: 1px solid rgba(255, 255, 255, 0.2);
        height: 57px;
        padding-top: 2px;
        border-bottom: 0;
        font-weight: 500;
        color: rgba(255, 255, 255, 0.8);
        &:first-child {
          border-left: 0;
        }
        &:last-child {
          border-right: 0;
        }
        .value {
          margin-top: 13px;
          font-size: 24px;
          font-weight: 200;
          color: #fff;
        }
      }
      &.top {
        th {
          border: 0;
        }
      }
    }
  }
`;

export const KeyValPair = styled.div`
  margin: 13px -5px;
  h5.bp3-heading {
    padding: 0 0 0 4px;
    margin: 2px 0;
    font-size: 14px;
    font-weight: 700;
  }
  .val {
    color: #ccc;
    font-weight: 400;
    padding: 0 4px;
  }
`;
