import React, { memo, useEffect, useMemo, useState } from "react";
import _flowRight from "lodash/flowRight";
import _filter from "lodash/filter";
import _sortBy from "lodash/sortBy";
import _first from "lodash/first";
import { connect } from "react-redux";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import { Card, Button, H4, H5, Icon } from "@blueprintjs/core";
import BracketWarsSVG from "../../../assets/images/svg/bracketWars.svg";
import { Header, Title, CardWithBackgroundImage, KeyValPair } from "./styles";
import "./style/index.scss";

import moment from "moment";
import { getCurrentUrl } from "../../../utils/helpers";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TProps = TStateProps & WrappedComponentProps;

const calculateTimeLeft = (startDate: string | undefined) => {
  let timeLeft = {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0
  };

  if (!startDate) {
    return timeLeft;
  }

  const difference =
    +new Date(startDate ? startDate : "2100-01-01") - +new Date();

  if (difference > 0) {
    timeLeft = {
      days: Math.floor(difference / (1000 * 60 * 60 * 24)) || 0,
      hours: Math.floor((difference / (1000 * 60 * 60)) % 24) || 0,
      minutes: Math.floor((difference / 1000 / 60) % 60) || 0,
      seconds: Math.floor((difference / 1000) % 60) || 0
    };
  }

  return timeLeft;
};

interface ITimeLeft {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
}
export const CompetitionWidget = memo(({ intl, competitions }: TProps) => {
  const [fire, setFire] = useState<boolean>(true);
  const [timeLeft, setTimeLeft] = useState<ITimeLeft>({
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0
  });

  const competition = useMemo(() => {
    const nowUTC = moment(new Date()).valueOf();
    return _first(
      _sortBy(
        _filter(competitions, comp => {
          const expirationDateUTC = moment.utc(comp.start_ts).valueOf();
          return expirationDateUTC > nowUTC;
        }),
        (comp: any) => moment.utc(comp.start_ts).valueOf()
      )
    );
    // eslint-disable-next-line
  }, [competitions, fire]);

  useEffect(() => {
    const timer = setTimeout(() => {
      const timeLeft = calculateTimeLeft(competition?.start_ts);

      const nothingLeft =
        timeLeft.days + timeLeft.hours + timeLeft.minutes + timeLeft.seconds <=
        0;

      if (nothingLeft) {
        setFire(!fire);
        return;
      }

      setTimeLeft(timeLeft);
    }, 1000);
    return () => clearTimeout(timer);
  });

  if (competition) {
    return (
      <Card className="competition-wrapper" tabIndex={1}>
        <Header>
          <H4>
            <FormattedMessage defaultMessage={"Next Competition"} />
            <Icon
              icon="circle-arrow-right"
              iconSize={23}
              className="right-icon"
              style={{ float: "right" }}
              onClick={() =>
                (window.location.href = getCurrentUrl(
                  "users_bracket_competitions"
                ))
              }
            />
          </H4>
        </Header>
        <Title>
          <H5>{competition.title}</H5>
          <H5 className="prize">
            {competition.prize
              .reduce((mem, prize) => (mem += +prize.amount), 0)
              .toFixed(2)}{" "}
            {competition.prize[0].asset_iso}{" "}
            <FormattedMessage defaultMessage={"Prize Pool"} />
          </H5>
        </Title>
        <CardWithBackgroundImage bg={BracketWarsSVG}>
          <table className={"bracket-competitions-table"}>
            <tbody>
              <tr className={"top"}>
                <th />
                <th />
                <th />
                <th />
              </tr>
              <tr className={"bottom"}>
                <th>
                  <FormattedMessage defaultMessage={"Days"} />
                  <div className={"value"}>{timeLeft?.days}</div>
                </th>
                <th>
                  <FormattedMessage defaultMessage={"Hrs"} />
                  <div className={"value"}>{timeLeft?.hours}</div>
                </th>
                <th>
                  <FormattedMessage defaultMessage={"Mins"} />
                  <div className={"value"}>{timeLeft?.minutes}</div>
                </th>
                <th>
                  <FormattedMessage defaultMessage={"Secs"} />
                  <div className={"value"}>{timeLeft?.seconds}</div>
                </th>
              </tr>
            </tbody>
          </table>
        </CardWithBackgroundImage>

        <KeyValPair>
          <H5>
            <FormattedMessage defaultMessage={"Organiser"} />
          </H5>
          <p className={"val"}>
            {competition.organiserName ? competition.organiserName : "None"}
          </p>
        </KeyValPair>

        <KeyValPair>
          <H5>
            <FormattedMessage defaultMessage={"Deposit"} />
          </H5>
          <p className={"val"}>
            {competition.deposit ? competition.deposit.toLocaleString() : 0}{" "}
            {competition.deposit ? competition.deposit_asset_iso : null}
          </p>
        </KeyValPair>

        <KeyValPair>
          <H5>
            <FormattedMessage defaultMessage={"Contribution"} />
          </H5>
          <p className={"val"}>
            {competition.contribution
              ? competition.contribution.toLocaleString()
              : 0}{" "}
            {competition.contribution
              ? competition.contribution_asset_iso
              : null}
          </p>
        </KeyValPair>

        <KeyValPair>
          <H5>
            <FormattedMessage defaultMessage={"Participants"} />
          </H5>
          <p className={"val"}>
            <FormattedMessage
              defaultMessage={"{value} Users"}
              values={{
                value: competition.participants ? competition.participants : 0
              }}
            />
          </p>
        </KeyValPair>

        <KeyValPair>
          <H5>
            <FormattedMessage defaultMessage={"Scoring"} />
          </H5>
          <p className={"val"}>
            {competition.strategy === "squared_roi"
              ? intl.formatMessage(
                  { defaultMessage: "Profit{squared} Blend" },
                  { squared: "²" }
                )
              : competition.strategy === "dash"
              ? intl.formatMessage({ defaultMessage: "Pure Volume" })
              : competition.strategy === "standard"
              ? intl.formatMessage({ defaultMessage: "Profit Blend" })
              : intl.formatMessage({ defaultMessage: "None" })}
          </p>
        </KeyValPair>

        <KeyValPair>
          <H5>
            <FormattedMessage defaultMessage={"Competition Start (UTC)"} />
          </H5>
          <p className={"val"}>
            {moment.utc(competition.start_ts).format("DD/MM/YY HH:mm")}
          </p>
        </KeyValPair>

        <Button
          className="bp3-intent-primary"
          style={{ width: "100%", margin: "8px 0" }}
          onClick={() =>
            window.open(
              "https://iot.coinflex.com/public/enter_competition/" +
                competition.id
            )
          }
        >
          {intl.formatMessage({ defaultMessage: "Join Competition" })}
        </Button>
      </Card>
    );
  } else {
    return null;
  }
});

const mapStateToProps = (state: IPublicState) => {
  return {
    competitions: state.competitions
  };
};

export default _flowRight(
  connect(mapStateToProps),
  injectIntl
)(CompetitionWidget);
