import React, { memo } from "react";
import { Navbar, Alignment } from "@blueprintjs/core";
import "./index.scss";
import coinflexPng from "../../../assets/images/icons/coinflex_logo_light.png";
import { localesList, localesMap } from "utils/i18n";
import { getI18n } from "store/actions";
import _flowRight from "lodash/flowRight";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import LanguageDropdown from "components/Header/LanguageDropdown";

interface IOwnProps {
  logoutHandler: () => void;
  i18n: any;
  isLogin: boolean;
  getI18n: (data: string) => void;
}

export const TopNavBar = memo((props: IOwnProps) => {
  const { i18n } = props;
  const selectedLocale = localesMap[i18n.locale] || localesList[0];

  const handleChangeLanguage = (newLocale: ILocale) => {
    props.getI18n(newLocale.code);
  };
  return (
    <Navbar className={"cf-navbar bp3-dark"}>
      <Navbar.Group align={Alignment.LEFT}>
        <Navbar.Heading>
          <img src={coinflexPng} alt="coinflexPng" />
        </Navbar.Heading>
      </Navbar.Group>
      <div className="navbar-buttons-wrapper">
        {props.logoutHandler && (
          <span onClick={props.logoutHandler}>Log-out</span>
        )}
        <Navbar.Divider />
        <a
          href="https://www.coinflex.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          www.coinflex.com
        </a>
        <Navbar.Divider />
        <LanguageDropdown
          value={selectedLocale}
          onChange={handleChangeLanguage}
          options={localesList}
          isTopNavBar={true}
        />
      </div>
    </Navbar>
  );
});

const mapStateToProps = (state: IPublicState) => {
  return {
    i18n: state.i18n
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    getI18n(data: string) {
      dispatch(getI18n(data));
    }
  };
};

export default _flowRight(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(TopNavBar);
