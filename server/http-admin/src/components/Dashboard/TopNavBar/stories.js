import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import TopNavBar from "./TopNavBar";

export default {
  title: "TopNavBar",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <TopNavBar />;

Default.story = {
  name: "default"
};
