import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import TopNavBar from "./TopNavBar";

describe("TopNavBar tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<TopNavBar />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
