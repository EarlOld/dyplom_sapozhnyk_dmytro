import React, { memo } from "react";
import { Select } from "@blueprintjs/select";
import { Button } from "@blueprintjs/core";
import clsx from "clsx";

import ControlsGroup from "../ControlsGroup";

import "./index.scss";

interface IItem {
  id: string;
  name: string;
}

interface IProps {
  onChange: (item: IItem) => void;
  selectedItem?: IItem;
  items: IItem[];
  disabled?: boolean;
  buttonClassName?: string;
}

const popoverProps = {
  popoverClassName: "items-dropper-popover"
};

const filterItem = (query: string, item: IItem) => {
  const normalizedTitle = (item.name || "").trim().toLowerCase();
  const normalizedQuery = query.trim().toLowerCase();

  if (!query) {
    return true;
  }

  return normalizedTitle.indexOf(normalizedQuery) >= 0;
};

const ItemRemdered = (item: IItem, { handleClick }: any) => (
  <div className="items-dropper-item" onClick={handleClick} key={item.id}>
    {item.name}
  </div>
);

export default memo(
  ({
    selectedItem,
    onChange,
    items,
    disabled = false,
    buttonClassName
  }: IProps) => {
    const RowSelect = Select.ofType<IItem>();

    return (
      <RowSelect
        disabled={disabled}
        items={items}
        onItemSelect={onChange}
        itemPredicate={filterItem}
        popoverProps={popoverProps}
        itemRenderer={ItemRemdered}
      >
        <ControlsGroup>
          <Button
            rightIcon="chevron-down"
            disabled={disabled}
            small={true}
            className={clsx("items-dropper-button", buttonClassName)}
          >
            {selectedItem && selectedItem.name}
          </Button>
        </ControlsGroup>
      </RowSelect>
    );
  }
);
