import React from "react";

import ItemsDropper from "./ItemsDropper";

const items = [
  { id: "all", name: "All Coins" },
  { id: "FLEX", name: "FLEX" },
  { id: "XBT", name: "XBT" },
  { id: "USDT", name: "USDT" }
];

const selectedItem = { id: "all", name: "All Coins" };

export default {
  title: "Items Dropper"
};

export const Default = () => (
  <div className="bp3-dark">
    <ItemsDropper
      items={items}
      onChange={() => {}}
      selectedItem={selectedItem}
    />
  </div>
);
