import React from "react";
import Footer from "./Footer";
import { render } from "utils/testUtils";

describe("Footer", () => {
  it("Should match snapshot", () => {
    const { container } = render(<Footer />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
