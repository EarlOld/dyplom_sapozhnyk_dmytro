import React from "react";
import { injectIntl } from "react-intl";

import coinflexPng from "assets/images/icons/coinflex_logo_light.png";
import telegramPng from "assets/images/icons/telegram.png";
import facebookPng from "assets/images/icons/facebook.png";
import linkedinPng from "assets/images/icons/linkedin.png";
import twitterPng from "assets/images/icons/twitter.png";
import mediumPng from "assets/images/icons/medium.png";
import blockfolioPng from "assets/images/icons/blockfolio.png";

import "./styles.scss";

interface ISocialLink {
  key: string;
  title: string;
  link: string;
  icon: string;
}

export default injectIntl(({ intl }) => {
  const socialLinks: ISocialLink[] = [
    {
      key: "telegram",
      title: intl.formatMessage({ defaultMessage: "Telegram" }),
      link: "https://t.me/coinflex_en",
      icon: telegramPng
    },
    {
      key: "twitter",
      title: intl.formatMessage({ defaultMessage: "Twitter" }),
      link: "https://twitter.com/coinflexdotcom",
      icon: twitterPng
    },
    {
      key: "facebook",
      title: intl.formatMessage({ defaultMessage: "Facebook" }),
      link: "https://www.facebook.com/coinflexdotcom/",
      icon: facebookPng
    },
    {
      key: "linkedIn",
      title: intl.formatMessage({ defaultMessage: "LinkedIn" }),
      link: "https://www.linkedin.com/company/coinflex/about/",
      icon: linkedinPng
    },
    {
      key: "medium",
      title: intl.formatMessage({ defaultMessage: "Medium" }),
      link: "https://medium.com/coinflex-official",
      icon: mediumPng
    },
    {
      key: "blockfolio",
      title: intl.formatMessage({ defaultMessage: "Blockfolio" }),
      link: "https://blockfolio.com/coin/FLEX",
      icon: blockfolioPng
    }
  ];

  return (
    <div className="page-footer">
      <img className="logo" src={coinflexPng} alt="Coinflex" />
      <div>
        {intl.formatMessage({
          defaultMessage: "Cryptocurrency Futures Exchange"
        })}
      </div>
      <div className="copyright">
        {intl.formatMessage({
          defaultMessage: "All Rights Reserved (C) 2020"
        })}
      </div>
      <div>
        <a className="coinflex-link" href="https://coinflex.com/">
          www.coinflex.com
        </a>
      </div>
      <div className="social-links">
        {socialLinks.map(link => (
          <a
            key={link.key}
            className={link.key}
            href={link.link}
            title={link.title}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={link.icon} alt={link.title} />
          </a>
        ))}
      </div>
    </div>
  );
});
