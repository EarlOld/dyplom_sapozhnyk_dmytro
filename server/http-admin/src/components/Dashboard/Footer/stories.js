import React from "react";
import Footer from "./Footer";

export default {
  title: "DashboardFooter"
};

export const Default = () => <Footer />;

Default.story = {
  name: "default"
};
