import React, { memo, useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import _flowRight from "lodash/flowRight";
import { injectIntl, WrappedComponentProps } from "react-intl";
import { CSVLink } from "react-csv";
import moment from "moment";
import { Icon, Tab, Tabs } from "@blueprintjs/core";

import { ReactComponent as DownloadSvg } from "assets/icons/download.svg";

import { getDepositWithdrawalsHistory } from "../../../store/actions";

import ControlsGroup from "../ControlsGroup";
import Pane from "../Pane";
import { getItemsByType, compareItemsByField, compareAssetsISO } from "./utils";
import ItemsDropper from "../ItemsDropper";

import {
  DepositWithdrawalsHistoryEntry,
  DepositWithdrawalsHistoryTable,
  DepositWithdrawalsLastItem
} from "./templates";

import {
  EDepositWithdrawalsHistoryTransactionState,
  EDepositWithdrawalsHistoryTransactionType,
  IDepositWithdrawalsHistoryStatusesInfo,
  IDepositWithdrawalsTypesDescriptions
} from "./types";

import "./styles.scss";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TProps = TStateProps & TDispatchProps & WrappedComponentProps;

interface ICoin {
  id: string;
  name: string;
}

const NO_EXPANDED_ITEMS_COUNT = 10;
const DATA_POLLING_INTERVAL = 2000;

let dataPollingIntervalId: number | undefined;

const DepositWithdrawalsHistory = memo(
  ({
    depositWithdrawalsHistory,
    getDepositWithdrawalsHistory,
    intl
  }: TProps) => {
    useEffect(() => {
      getDepositWithdrawalsHistory();

      if (!dataPollingIntervalId) {
        dataPollingIntervalId = setInterval(
          getDepositWithdrawalsHistory,
          DATA_POLLING_INTERVAL
        );
      }

      return () => {
        clearInterval(dataPollingIntervalId);
        dataPollingIntervalId = undefined;
      };
    }, [getDepositWithdrawalsHistory]);

    const lastDeposit:
      | IDepositWithdrawalsHistoryItem
      | undefined = useMemo(() => {
      const items = getItemsByType(
        depositWithdrawalsHistory,
        EDepositWithdrawalsHistoryTransactionType.Deposit
      ).sort((a, b) => compareItemsByField(a, b, "created_at", false));

      return items.length ? items[0] : undefined;
    }, [depositWithdrawalsHistory]);

    const lastWithdrawal:
      | IDepositWithdrawalsHistoryItem
      | undefined = useMemo(() => {
      const items = getItemsByType(
        depositWithdrawalsHistory,
        EDepositWithdrawalsHistoryTransactionType.Withdrawal
      ).sort((a, b) => compareItemsByField(a, b, "created_at", false));

      return items.length ? items[0] : undefined;
    }, [depositWithdrawalsHistory]);

    const coins: ICoin[] = useMemo(
      () =>
        [
          {
            id: "all",
            name: intl.formatMessage({
              id: "all-coins-tab",
              defaultMessage: "All Coins"
            })
          }
        ].concat(
          Array.from(
            depositWithdrawalsHistory.reduce((result, item) => {
              result.add(item.asset_iso);

              return result;
            }, new Set<string>())
          )
            .sort((a, b) => compareAssetsISO(a, b, true))
            .map(coin => ({ id: coin, name: coin }))
        ),
      [depositWithdrawalsHistory, intl]
    );

    const [currentItemsType, setCurrentItemsType] = useState<string | number>(
      "all"
    );

    const [currentCoin, setCurrentCoin] = useState<ICoin>(coins[0]);

    const [sortField, setSortField] = useState<
      keyof IDepositWithdrawalsHistoryItem
    >();
    const [sortDirection, setSortDirection] = useState<TSortDirection>("asc");

    const [isExpandedTable, setIsExpandedTable] = useState<boolean>(false);

    const toggleExpandedTableState = () => setIsExpandedTable(!isExpandedTable);

    const dataSource = useMemo(
      () =>
        depositWithdrawalsHistory
          .filter(item => {
            if (
              currentItemsType !== "all" &&
              item.transaction_type_id !== currentItemsType
            ) {
              return false;
            }

            if (currentCoin.id !== "all" && item.asset_iso !== currentCoin.id) {
              return false;
            }

            return true;
          })
          .map(item => ({
            ...item,
            amount: +item.amount,
            fee: +item.fee
          }))
          .sort((a, b) => {
            if (!sortField) {
              return 1;
            }
            return compareItemsByField(
              a,
              b,
              sortField,
              sortDirection === "asc"
            );
          }),
      [
        depositWithdrawalsHistory,
        currentItemsType,
        currentCoin,
        sortField,
        sortDirection
      ]
    );

    const visibleDataSource = useMemo(
      () =>
        dataSource.filter(
          (item, index) => isExpandedTable || index < NO_EXPANDED_ITEMS_COUNT
        ),
      [dataSource, isExpandedTable]
    );

    const pendingString = intl.formatMessage({
      defaultMessage: "Pending"
    });

    const rejectedString = intl.formatMessage({
      defaultMessage: "Rejected"
    });

    const processedString = intl.formatMessage({
      defaultMessage: "Processed"
    });

    const publishedString = intl.formatMessage({
      defaultMessage: "Published"
    });

    const onHoldString = intl.formatMessage({
      defaultMessage: "On Hold"
    });

    const statusesInfo: IDepositWithdrawalsHistoryStatusesInfo = {
      [EDepositWithdrawalsHistoryTransactionState.New]: {
        group: "pending",
        title: pendingString
      },
      [EDepositWithdrawalsHistoryTransactionState.AcceptedByTradeEngine]: {
        group: "pending",
        title: pendingString
      },
      [EDepositWithdrawalsHistoryTransactionState.RejectedByTradeEngine]: {
        group: "rejected",
        title: rejectedString
      },
      [EDepositWithdrawalsHistoryTransactionState.RejectedByMOS]: {
        group: "rejected",
        title: processedString
      },
      [EDepositWithdrawalsHistoryTransactionState.Processed]: {
        group: "processed",
        title: processedString
      },
      [EDepositWithdrawalsHistoryTransactionState.ProcessedByDepositsAndWithdrawalsProcessor]: {
        group: "published",
        title: publishedString
      },
      [EDepositWithdrawalsHistoryTransactionState.ConfirmedInTheBlockchain]: {
        group: "processed",
        title: processedString
      },
      [EDepositWithdrawalsHistoryTransactionState.LockedByDepositsAndWithdrawalsProcessor]: {
        group: "hold",
        title: onHoldString
      }
    };

    const typesDescriptions: IDepositWithdrawalsTypesDescriptions = {
      [EDepositWithdrawalsHistoryTransactionType.Deposit]: intl.formatMessage({
        defaultMessage: "Deposit"
      }),
      [EDepositWithdrawalsHistoryTransactionType.Withdrawal]: intl.formatMessage(
        { defaultMessage: "User Withdrawal" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.Rebate]: intl.formatMessage({
        defaultMessage: "Rebate"
      }),
      [EDepositWithdrawalsHistoryTransactionType.Commission]: intl.formatMessage(
        { defaultMessage: "Affiliate commission" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.CoinCredit]: intl.formatMessage(
        { defaultMessage: "Coin Credit" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.CoinRebate]: intl.formatMessage(
        { defaultMessage: "Coin Rebate" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.CoinRedemption]: intl.formatMessage(
        { defaultMessage: "Coin Redemption" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.Competition]: intl.formatMessage(
        { defaultMessage: "Competition (bracket win)" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.CommissionTier2]: intl.formatMessage(
        { defaultMessage: "Sub-affiliate commission" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.CommissionPrize]: intl.formatMessage(
        { defaultMessage: "Follower prize bonus" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.FollowerBonusStake]: intl.formatMessage(
        { defaultMessage: "Follower bonus stake" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.DmmReward]: intl.formatMessage(
        { defaultMessage: "DMM reward" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.StakeBonus]: intl.formatMessage(
        { defaultMessage: "Staking bonus" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.OrganiserContribution]: intl.formatMessage(
        { defaultMessage: "Organiser contribution" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.ParticipantContribution]: intl.formatMessage(
        { defaultMessage: "Participant contribution" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.OrganiserContributionReturn]: intl.formatMessage(
        { defaultMessage: "Organiser contribution return" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.OrganiserBonusCommission]: intl.formatMessage(
        { defaultMessage: "Organiser bonus commission" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.UserCompetitionPrize]: intl.formatMessage(
        { defaultMessage: "User competition prize" }
      ),
      [EDepositWithdrawalsHistoryTransactionType.CoinForFees]: intl.formatMessage(
        { defaultMessage: "Coin for fees" }
      )
    };

    const csvData: any = useMemo(() => {
      const result: any[] = [
        [
          intl.formatMessage({ defaultMessage: "Asset" }),
          intl.formatMessage({ defaultMessage: "Amount" }),
          intl.formatMessage({ defaultMessage: "Status" }),
          intl.formatMessage({ defaultMessage: "Date Time UTC" }),
          intl.formatMessage({ defaultMessage: "Description" }),
          intl.formatMessage({ defaultMessage: "Fees" }),
          intl.formatMessage({ defaultMessage: "Address" }),
          intl.formatMessage({ defaultMessage: "Transaction ID" })
        ]
      ];

      return result.concat(
        depositWithdrawalsHistory.map(item => {
          const statusInfo = statusesInfo[item.transaction_state_id];
          const description = typesDescriptions[item.transaction_type_id];

          return [
            item.asset_iso,
            item.amount.toLocaleString(),
            statusInfo ? statusInfo.title : "",
            moment(item.created_at).format("DD MMM YYYY, HH:mm"),
            description,
            item.fee.toLocaleString(),
            item.address,
            item.txid
          ];
        })
      );
    }, [depositWithdrawalsHistory, intl, statusesInfo, typesDescriptions]);

    const handleTypeFilterChange = (value: number) =>
      setCurrentItemsType(value);

    const typesFilterTabs = [
      {
        key: "all",
        title: intl.formatMessage({
          id: "all-types-tab",
          defaultMessage: "All"
        })
      },
      {
        key: EDepositWithdrawalsHistoryTransactionType.Deposit,
        title: intl.formatMessage({
          id: "deposit-type-tab",
          defaultMessage: "Deposit"
        })
      },
      {
        key: EDepositWithdrawalsHistoryTransactionType.Withdrawal,
        title: intl.formatMessage({
          id: "withdrawal-type-tab",
          defaultMessage: "Withdrawal"
        })
      }
    ];

    const handleSortableCellClick = (
      cellId: keyof IDepositWithdrawalsHistoryItem
    ) => {
      setSortDirection(
        cellId === sortField
          ? sortDirection === "asc"
            ? "desc"
            : "asc"
          : "asc"
      );
      setSortField(cellId);
    };

    return (
      <Pane title={intl.formatMessage({ defaultMessage: "History" })}>
        <div className="deposit-withdrawal-history">
          <div className="deposit-withdrawal-history-header">
            <div className="last-items">
              {lastDeposit && <DepositWithdrawalsLastItem {...lastDeposit} />}
              {lastWithdrawal && (
                <DepositWithdrawalsLastItem {...lastWithdrawal} />
              )}
            </div>
            {csvData.length > 1 && (
              <CSVLink
                data={csvData}
                filename={"deposit_withdrawals_history.csv"}
                className="download-csv-link"
              >
                {intl.formatMessage({
                  defaultMessage: "CSV"
                })}
                <Icon icon={<DownloadSvg />} />
              </CSVLink>
            )}
          </div>
          <div className="deposit-withdrawal-history-filter">
            <div className="filter-title">
              {intl.formatMessage({
                defaultMessage: "Filter"
              })}
            </div>
            <div className="filter-content">
              <ControlsGroup>
                <Tabs
                  id="types-tabs"
                  animate={false}
                  defaultSelectedTabId="all"
                  onChange={handleTypeFilterChange}
                >
                  {typesFilterTabs.map(tab => (
                    <Tab id={tab.key} key={tab.key} title={tab.title} />
                  ))}
                </Tabs>
              </ControlsGroup>
              <ItemsDropper
                items={coins}
                onChange={setCurrentCoin}
                selectedItem={currentCoin}
                buttonClassName="coin-dropper-button"
              />
            </div>
          </div>
          {!dataSource.length ? (
            <h2 className="no-data">
              {intl.formatMessage({
                defaultMessage: "No Data"
              })}
            </h2>
          ) : (
            <DepositWithdrawalsHistoryTable
              onSortableHeaderCellClick={handleSortableCellClick}
              sortDirection={sortDirection}
              sortField={sortField}
            >
              {visibleDataSource.map((item, i) => (
                <DepositWithdrawalsHistoryEntry
                  key={"transaction-" + i}
                  {...item}
                  statusesInfo={statusesInfo}
                  typesDescriptions={typesDescriptions}
                />
              ))}
            </DepositWithdrawalsHistoryTable>
          )}
          {dataSource.length > NO_EXPANDED_ITEMS_COUNT && (
            <div
              className="expand-button table-expand-button"
              onClick={toggleExpandedTableState}
            >
              <Icon
                icon={isExpandedTable ? "chevron-up" : "chevron-down"}
                iconSize={24}
              />
            </div>
          )}
        </div>
      </Pane>
    );
  }
);

const mapStateToProps = (state: IAppState) => ({
  depositWithdrawalsHistory: state.depositWithdrawalsHistory
});

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    getDepositWithdrawalsHistory() {
      dispatch(getDepositWithdrawalsHistory());
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(DepositWithdrawalsHistory);
