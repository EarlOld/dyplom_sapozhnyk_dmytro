import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import DepositWithdrawalsHistory from "./DepositWithdrawalsHistory";

export default {
  title: "DashboardDepositWithdrawalsHistory",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <div className="bp3-dark">
    <DepositWithdrawalsHistory />
  </div>
);

Default.story = {
  name: "default"
};
