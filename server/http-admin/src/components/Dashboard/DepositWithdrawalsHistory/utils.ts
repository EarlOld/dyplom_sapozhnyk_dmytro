import { MONTHS_LIST } from "../data";

import { EDepositWithdrawalsHistoryTransactionType } from "./types";

export const getItemsByType = (
  allItems: IDepositWithdrawalsHistoryItem[],
  typeId: EDepositWithdrawalsHistoryTransactionType
) => allItems.filter(item => item.transaction_type_id === typeId);

export const compareItemsByField = (
  a: IDepositWithdrawalsHistoryItem,
  b: IDepositWithdrawalsHistoryItem,
  fieldName: keyof IDepositWithdrawalsHistoryItem,
  isAsc: boolean
) => {
  if (fieldName === "asset_iso") {
    return compareAssetsISO(a.asset_iso, b.asset_iso, isAsc);
  }

  return compareValues(a[fieldName], b[fieldName], isAsc);
};

const assetsISOOrder = ["FLEX", "XBT", "ETH", "BCH", "USDC", "USDT"];

export const compareAssetsISO = (a: string, b: string, isAsc: boolean) => {
  const indexA = assetsISOOrder.findIndex(assetISO => a.startsWith(assetISO));
  const indexB = assetsISOOrder.findIndex(assetISO => b.startsWith(assetISO));

  if (indexA !== indexB) {
    return compareValues(indexA, indexB, isAsc);
  }

  const monthIndexA = MONTHS_LIST.findIndex(month => a.endsWith(month));
  const monthIndexB = MONTHS_LIST.findIndex(month => b.endsWith(month));

  if (monthIndexA !== monthIndexB) {
    return compareValues(monthIndexA, monthIndexB, isAsc);
  }

  return compareValues(a, b, isAsc);
};

const compareValues = (a: any, b: any, isAsc: boolean) =>
  a === b ? 0 : (a > b ? 1 : -1) * (isAsc ? 1 : -1);

export const getItemAddressLinks = (item: IDepositWithdrawalsHistoryItem) => {
  const coinName = assetsISOOrder.find(assetISO =>
    item.asset_iso.startsWith(assetISO)
  );

  const { txid, address } = item;

  switch (coinName) {
    case "ETH":
    case "USDT":
    case "USDC":
      return {
        transaction: `https://etherscan.io/tx/${txid}`,
        address: `https://etherscan.io/address/${address}`
      };

    case "XBT":
      return {
        transaction: `https://www.blockchain.com/btc/tx/${txid}`,
        address: `https://www.blockchain.com/btc/address/${address}`
      };

    case "BCH":
      return {
        transaction: `https://www.blockchain.com/bch/tx/${txid}`,
        address: `https://explorer.bitcoin.com/bch/address/${address}`
      };

    case "FLEX":
      return {
        transaction: `https://simpleledger.info/#tx/${txid}`,
        address: `https://simpleledger.info/#address/${address}`
      };

    default:
      return {
        transaction: "",
        address: ""
      };
  }
};

export const getItemTransactionDirectionType = (
  item: IDepositWithdrawalsHistoryItem
): "deposit" | "withdrawal" | undefined => {
  switch (+item.transaction_type_id) {
    case EDepositWithdrawalsHistoryTransactionType.Deposit:
    case EDepositWithdrawalsHistoryTransactionType.Rebate:
    case EDepositWithdrawalsHistoryTransactionType.Commission:
    case EDepositWithdrawalsHistoryTransactionType.CoinCredit:
    case EDepositWithdrawalsHistoryTransactionType.CoinRebate:
    case EDepositWithdrawalsHistoryTransactionType.Competition:
    case EDepositWithdrawalsHistoryTransactionType.CommissionTier2:
    case EDepositWithdrawalsHistoryTransactionType.CommissionPrize:
    case EDepositWithdrawalsHistoryTransactionType.FollowerBonusStake:
    case EDepositWithdrawalsHistoryTransactionType.DmmReward:
    case EDepositWithdrawalsHistoryTransactionType.StakeBonus:
    case EDepositWithdrawalsHistoryTransactionType.OrganiserContributionReturn:
    case EDepositWithdrawalsHistoryTransactionType.OrganiserBonusCommission:
    case EDepositWithdrawalsHistoryTransactionType.UserCompetitionPrize:
      return "deposit";

    case EDepositWithdrawalsHistoryTransactionType.Withdrawal:
    case EDepositWithdrawalsHistoryTransactionType.CoinRedemption:
    case EDepositWithdrawalsHistoryTransactionType.OrganiserContribution:
    case EDepositWithdrawalsHistoryTransactionType.ParticipantContribution:
    case EDepositWithdrawalsHistoryTransactionType.CoinForFees:
      return "withdrawal";

    default:
      return undefined;
  }
};
