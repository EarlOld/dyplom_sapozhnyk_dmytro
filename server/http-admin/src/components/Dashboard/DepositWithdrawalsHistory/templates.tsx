import React, { memo, ReactNode, useState } from "react";
import { injectIntl, WrappedComponentProps } from "react-intl";
import moment from "moment";
import clsx from "clsx";
import { Icon } from "@blueprintjs/core";

import {
  EDepositWithdrawalsHistoryTransactionType,
  IDepositWithdrawalsHistoryStatusesInfo,
  IDepositWithdrawalsTypesDescriptions
} from "./types";

import "./styles.scss";

import { getItemAddressLinks, getItemTransactionDirectionType } from "./utils";

interface ITableRow extends IDepositWithdrawalsHistoryItem {}

interface IDepositWithdrawalsHistoryTableProps {
  children: ReactNode;
  onSortableHeaderCellClick: (
    cellId: keyof IDepositWithdrawalsHistoryItem
  ) => void;
  sortDirection?: TSortDirection;
  sortField?: keyof IDepositWithdrawalsHistoryItem;
}

export const DepositWithdrawalsLastItem = injectIntl(
  memo(
    ({
      intl,
      amount,
      asset_iso,
      created_at,
      transaction_type_id
    }: IDepositWithdrawalsHistoryItem & WrappedComponentProps) => {
      return (
        <div className="last-item">
          <div className="title">
            {transaction_type_id ===
              EDepositWithdrawalsHistoryTransactionType.Deposit &&
              intl.formatMessage({ defaultMessage: "Last Deposit" })}
            {transaction_type_id ===
              EDepositWithdrawalsHistoryTransactionType.Withdrawal &&
              intl.formatMessage({ defaultMessage: "Last Withdrawal" })}
            <span className="date-time">
              {moment(created_at).format("DD MMM YYYY, HH:mm")}{" "}
              {intl.formatMessage({
                defaultMessage: "UTC"
              })}
            </span>
          </div>
          <div className="value">
            {amount.toLocaleString()}
            <span className="currency">{asset_iso}</span>
          </div>
        </div>
      );
    }
  )
);

export const DepositWithdrawalsHistoryTableSortableHeaderCell = memo(
  ({
    id,
    title,
    className,
    onClick,
    sortDirection,
    sortField
  }: {
    id: keyof IDepositWithdrawalsHistoryItem;
    title: ReactNode;
    className?: string;
    onClick: (cellId: keyof IDepositWithdrawalsHistoryItem) => void;
    sortDirection?: TSortDirection;
    sortField?: string;
  }) => (
    <th className={clsx("sortable", className)} onClick={() => onClick(id)}>
      {title}
      {sortField && sortField === id && (
        <span className="sort-icon">
          {sortDirection === "desc" ? (
            <Icon icon="caret-down" />
          ) : (
            <Icon icon="caret-up" />
          )}
        </span>
      )}
    </th>
  )
);

export const DepositWithdrawalsHistoryTable = injectIntl(
  memo(
    ({
      children,
      intl,
      onSortableHeaderCellClick,
      sortDirection,
      sortField
    }: IDepositWithdrawalsHistoryTableProps & WrappedComponentProps) => {
      return (
        <table className="bp3-html-table bp3-interactive history-table">
          <thead>
            <tr>
              <DepositWithdrawalsHistoryTableSortableHeaderCell
                className="coin-balance"
                id="asset_iso"
                title={intl.formatMessage({ defaultMessage: "Asset" })}
                onClick={onSortableHeaderCellClick}
                sortDirection={sortDirection}
                sortField={sortField}
              />
              <DepositWithdrawalsHistoryTableSortableHeaderCell
                className="amount"
                id="amount"
                title={intl.formatMessage({ defaultMessage: "Amount" })}
                onClick={onSortableHeaderCellClick}
                sortDirection={sortDirection}
                sortField={sortField}
              />
              <DepositWithdrawalsHistoryTableSortableHeaderCell
                className="status"
                id="transaction_state_id"
                title={intl.formatMessage({ defaultMessage: "Status" })}
                onClick={onSortableHeaderCellClick}
                sortDirection={sortDirection}
                sortField={sortField}
              />
              <DepositWithdrawalsHistoryTableSortableHeaderCell
                className="date-time"
                id="created_at"
                title={
                  <span>
                    {intl.formatMessage({
                      defaultMessage: "Date"
                    })}{" "}
                    <span className="time">
                      {intl.formatMessage({
                        defaultMessage: "Time UTC"
                      })}
                    </span>
                  </span>
                }
                onClick={onSortableHeaderCellClick}
                sortDirection={sortDirection}
                sortField={sortField}
              />
              <DepositWithdrawalsHistoryTableSortableHeaderCell
                className="description"
                id="transaction_type_id"
                title={intl.formatMessage({ defaultMessage: "Description" })}
                onClick={onSortableHeaderCellClick}
                sortDirection={sortDirection}
                sortField={sortField}
              />
              <DepositWithdrawalsHistoryTableSortableHeaderCell
                className="fee"
                id="fee"
                title={intl.formatMessage({ defaultMessage: "Fees" })}
                onClick={onSortableHeaderCellClick}
                sortDirection={sortDirection}
                sortField={sortField}
              />
              <th className="address">
                {intl.formatMessage({ defaultMessage: "Address" })}
              </th>
              <th className="actions">
                {intl.formatMessage({ defaultMessage: "Actions" })}
              </th>
            </tr>
          </thead>
          <tbody>{children}</tbody>
        </table>
      );
    }
  )
);

export const DepositWithdrawalsHistoryEntry = injectIntl(
  memo(
    (
      props: ITableRow &
        WrappedComponentProps & {
          statusesInfo: IDepositWithdrawalsHistoryStatusesInfo;
          typesDescriptions: IDepositWithdrawalsTypesDescriptions;
        }
    ) => {
      const { statusesInfo, typesDescriptions, intl } = props;

      const statusInfo = statusesInfo[props.transaction_state_id];
      const dateTime = moment(props.created_at);
      const description = typesDescriptions[props.transaction_type_id] || "";

      const [isExpanded, setIsExpanded] = useState<boolean>(false);

      const toggleExpandedState = () => setIsExpanded(!isExpanded);
      const addressLinks = getItemAddressLinks(props);
      const transationDirectionType = getItemTransactionDirectionType(props);

      return (
        <tr className={clsx(isExpanded && "expanded")}>
          <td className="primary title">{props.asset_iso}</td>
          <td className={clsx("amount", transationDirectionType)}>
            {props.amount.toLocaleString()}
          </td>
          <td className={clsx("status", statusInfo ? statusInfo.group : "")}>
            {statusInfo ? statusInfo.title : ""}
          </td>
          <td className="date-time">
            {dateTime.format("DD MMM YYYY")}{" "}
            <span className="time">{dateTime.format("HH:mm")}</span>
          </td>
          <td className="description">{description}</td>
          <td className="description">{props.fee}</td>
          <td className="address">
            <a
              href={addressLinks.address}
              target="_blank"
              rel="noreferrer noopener"
              className="address-value"
            >
              {props.address}
            </a>
            {isExpanded && (
              <a
                href={addressLinks.transaction}
                target="_blank"
                rel="noreferrer noopener"
              >
                <span className="transaction-id-title">
                  {intl.formatMessage({ defaultMessage: "Transaction ID" })}
                </span>
                <span className="transaction-value">{props.txid}</span>
              </a>
            )}
          </td>
          <td className="actions">
            <span className="expand-button" onClick={toggleExpandedState}>
              <Icon
                icon={isExpanded ? "chevron-up" : "chevron-down"}
                iconSize={24}
              />
            </span>
          </td>
        </tr>
      );
    }
  )
);
