export enum EDepositWithdrawalsHistoryTransactionType {
  Deposit = 1,
  Withdrawal = 2,
  Rebate = 11,
  Commission = 12,
  CoinCredit = 13,
  CoinRebate = 19,
  CoinRedemption = 21,
  Competition = 26,
  CommissionTier2 = 27,
  CommissionPrize = 28,
  FollowerBonusStake = 29,
  DmmReward = 30,
  StakeBonus = 31,
  OrganiserContribution = 32,
  ParticipantContribution = 33,
  OrganiserContributionReturn = 34,
  OrganiserBonusCommission = 35,
  UserCompetitionPrize = 36,
  CoinForFees = 37
}

export enum EDepositWithdrawalsHistoryTransactionState {
  New = 1,
  AcceptedByTradeEngine = 2,
  RejectedByTradeEngine = 3,
  RejectedByMOS = 4,
  Processed = 5,
  ProcessedByDepositsAndWithdrawalsProcessor = 6,
  ConfirmedInTheBlockchain = 7,
  LockedByDepositsAndWithdrawalsProcessor = 8
}

export interface IDepositWithdrawalsHistoryStatusesInfo {
  [status: number]: {
    group: string;
    title: string;
  };
}

export interface IDepositWithdrawalsTypesDescriptions {
  [status: number]: string;
}
