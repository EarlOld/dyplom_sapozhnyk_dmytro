import React from "react";
import DepositWithdrawalsHistory from "./DepositWithdrawalsHistory";
import { renderWithMockRedux } from "utils/testUtils";

describe("DepositWithdrawalsHistory", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<DepositWithdrawalsHistory />);
    expect(container.firstChild).toMatchSnapshot();
  });
});

it("Should have 'Deposit' description in the table", () => {
  const { getByText } = renderWithMockRedux(<DepositWithdrawalsHistory />);
  const el = getByText("Deposit");
  expect(el).toBeInTheDocument();
});
