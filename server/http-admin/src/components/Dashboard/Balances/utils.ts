import moment from "moment";
import _round from "lodash/round";
import { isSpotAsset } from "utils/isSpotAsset";
import { isUSDByName, isUSDT } from "utils/isUsdAsset";
import { contractCode } from "utils/reference";
import { IMarketProps } from "./Spot/types";
import { upperCaseFirstLetter } from "utils/helpers";
import { MONTHS_LIST } from "../data";
import { SCALED } from "./data";
import _get from "lodash/get";

interface IPrices {
  [balanceName: string]: number;
}

const getBalancesPrices = (tickersList: ITicker[]): IPrices => {
  const prices = tickersList.reduce((result: IPrices, ticker) => {
    const baseCounterNames = ticker.spot_name.split("/");

    if (baseCounterNames[1] && baseCounterNames[1] === "USDT") {
      result[baseCounterNames[0]] = ticker.last || 0;
    }
    return result;
  }, {});

  prices["USDT"] = 10000;

  return prices;
};

export const getFuturesBalances = (
  allBalances: IBalance[],
  assetsList: IAsset[],
  marketsList: IMarket[],
  tickersList: ITicker[]
): IFuturesBalance[] => {
  let currentMonth: number = new Date().getMonth();
  const currentMonthName = upperCaseFirstLetter(
    MONTHS_LIST[currentMonth].toLowerCase()
  );

  const currentYear = +moment().format("YY");
  const prices = getBalancesPrices(tickersList);

  return allBalances
    .filter((balance: IBalance): boolean => {
      const matchingAsset: IAsset | undefined = assetsList.find(
        asset => asset.id === balance.id
      );

      return !!(matchingAsset && !isSpotAsset(matchingAsset));
    })
    .map(balance => {
      const matchingAsset = assetsList.find(
        asset => asset.id === balance.id
      ) as IAsset;
      const spotName = matchingAsset.spot_name || "";
      const matchingMarket = marketsList.find(
        market => market.base === balance.id
      );
      const matchingTicker = tickersList.find(
        ticker => ticker.base === balance.id
      );

      let price = 0;
      let monthName = "";
      let year = 0;
      let expires = null;

      const isUsdFuturesAsset = isUSDByName(matchingAsset.name!);
      const isAssetWithMonth = MONTHS_LIST.some(month =>
        matchingAsset.name.toUpperCase().endsWith(month + currentYear)
      );

      const isAddVisibleBalance: boolean =
        !!balance.available ||
        !!balance.reserved ||
        (isUsdFuturesAsset && isAssetWithMonth) ||
        (isUSDT(matchingAsset.spot_id!) &&
          matchingAsset.name.endsWith(currentMonthName.toUpperCase()));

      if (matchingTicker) {
        price = matchingTicker!.last || 0;
      } else if (isAddVisibleBalance) {
        const { spot_name } = matchingAsset;

        price = prices[spot_name!] || 0;
      }

      if (matchingMarket) {
        expires = matchingMarket!.expires || null;
        const tenor = matchingMarket!.tenor;

        if (tenor) {
          monthName = contractCode[tenor.substring(0, 1)] || "";
          year = +tenor.substring(1);
        }
      } else if (isAddVisibleBalance) {
        const { name, spot_name } = matchingAsset;

        if (spot_name && name.startsWith(spot_name)) {
          let _monthName = name.substr(spot_name.length);

          if (
            _monthName.length === 5 &&
            _monthName.endsWith(currentYear.toString())
          ) {
            _monthName = _monthName.substring(0, 3);
          }

          const monthIndex = MONTHS_LIST.indexOf(_monthName);

          if (monthIndex >= 0) {
            monthName = upperCaseFirstLetter(_monthName.toLowerCase());
            expires = null;
          }
          year = currentYear;
        }
      }

      const hasData =
        !!(matchingMarket && matchingTicker) || isAddVisibleBalance;

      return {
        ...balance,
        hasData,
        name: spotName,
        price,
        monthName,
        year,
        expires
      };
    })
    .filter(balanceData => balanceData.hasData);
};

export const expandFuturesBalance = (
  futuresBalance: IFuturesBalance,
  borrowLoans: IBorrowLoan[],
  assetsList: IAsset[],
  transfers: IConvertedTotal[],
  collaterals: IBorrowCollateral[]
) => {
  const loans = borrowLoans
    ? borrowLoans.filter(loan => loan.asset_id === futuresBalance.id)
    : [];

  const loanQuantity = loans.reduce((sum, loan) => {
    sum += loan.principal;
    return sum;
  }, 0);

  const matchingAsset = assetsList.find(
    asset => asset.id === futuresBalance.id
  ) as IAsset;

  const convertedTotal = transfers.find(
    total =>
      total.asset_to === futuresBalance.id &&
      total.asset_from === matchingAsset.spot_id
  );

  const negativeConvertedTotal = transfers.find(
    total =>
      total.asset_from === futuresBalance.id &&
      total.asset_to === matchingAsset.spot_id
  );

  const collateral =
    collaterals &&
    collaterals.find(collateral => collateral.asset_id === futuresBalance.id);

  let transferred = (convertedTotal && convertedTotal.total) || 0;
  transferred -= _get(negativeConvertedTotal, "total", 0);

  let availableToWithdraw = 0;

  if (transferred > 0 && futuresBalance.available > 0) {
    availableToWithdraw = Math.min(transferred, futuresBalance.available);
  }

  if (collateral) {
    availableToWithdraw = Math.min(availableToWithdraw, collateral.available);
  }

  if (
    futuresBalance.expires &&
    futuresBalance.available > 0 &&
    moment.utc(futuresBalance.expires).isBefore(new Date())
  ) {
    availableToWithdraw = futuresBalance.available;
  }

  return {
    ...futuresBalance,
    transferred: transferred / SCALED,
    transferredInUSDT:
      (transferred *
        (isUSDByName(futuresBalance.name)
          ? 1
          : futuresBalance.price / SCALED)) /
      SCALED,
    collateralTotal: _round(
      ((collateral && collateral.total) || 0) / SCALED,
      2
    ),
    collateralAvailable: _round(
      ((collateral && collateral.available) || 0) / SCALED,
      2
    ),
    available: futuresBalance.available / SCALED,
    availableToWithdraw: availableToWithdraw / SCALED,
    borrowed: loanQuantity / SCALED,
    required: futuresBalance.reserved / SCALED
  };
};

interface ICurenciesOrder {
  [key: string]: number;
}
export const sortCurrencies = (currencies: Array<IMarketProps>) => {
  let tempCurrencies = [...currencies];
  const currenciesOrder: ICurenciesOrder = {
    FLEX: 1,
    XBT: 2,
    ETH: 3,
    BCH: 4,
    USDC: 5,
    USDT: 6,
    default: 10000
  };

  tempCurrencies.sort((a: IMarketProps, b: IMarketProps) => {
    return (
      (currenciesOrder[a.name] || currenciesOrder.default) -
      (currenciesOrder[b.name] || currenciesOrder.default)
    );
  });

  return tempCurrencies;
};
