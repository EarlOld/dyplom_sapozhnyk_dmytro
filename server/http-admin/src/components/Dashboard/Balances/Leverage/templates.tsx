import React, { memo, ReactNode } from "react";
import { injectIntl, WrappedComponentProps } from "react-intl";
import { Slider, FormGroup, InputGroup } from "@blueprintjs/core";

import "./styles.scss";
import { ILeverageSliderViewProps } from "../../../LeverageSlider/types";

import IconButton from "../../IconButton";

interface ISpotBalancesTableProps {
  children: ReactNode;
}

export const LeverageBalancesTable = injectIntl(
  memo(
    ({ children, intl }: ISpotBalancesTableProps & WrappedComponentProps) => (
      <div className="balances-table-wrapper">
        <table className="bp3-html-table balances-table">
          <thead>
            <tr>
              <th className="title">
                {intl.formatMessage({ defaultMessage: "Futures Asset" })}
              </th>
              <th className="borrowed">
                {intl.formatMessage({ defaultMessage: "Borrowed" })}
              </th>
              <th className="set-borrowed">&nbsp;</th>
              <th className="slider">
                {intl.formatMessage({
                  defaultMessage: "Slide to adjust leverage"
                })}
              </th>
              <th className="repay-all">&nbsp;</th>
              <th className="availabe-max">
                {intl.formatMessage({ defaultMessage: "Available" })}
                <span className="max">
                  {"/"}
                  {intl.formatMessage({ defaultMessage: "Max" })}
                </span>
              </th>
            </tr>
          </thead>
          <tbody>{children}</tbody>
        </table>
      </div>
    )
  )
);

const renderSliderLabel = (val: number) => `${val || 0}x`;

export const LeverageBalanceEntry = memo((props: ILeverageSliderViewProps) => {
  return (
    <tr>
      <td className="primary title">{props.name}</td>
      <td className="borrowed">
        <FormGroup helperText={props.inputBorrowedError} intent="danger">
          <InputGroup
            value={props.inputBorrowedValue}
            disabled={props.disabled}
            autoFocus={props.isInputBorrowedFocused}
            onChange={props.onInputBorrowedChange}
            onFocus={props.onInputBorrowedFocus}
            onBlur={props.onInputBorrowedBlur}
            rightElement={
              <span className="balance-name">
                &nbsp;&nbsp;{props.balanceName}&nbsp;&nbsp;
              </span>
            }
          />
        </FormGroup>
      </td>
      <td className="set-borrowed">
        <IconButton
          icon="tick-circle"
          iconSize={23}
          disabled={props.disabled}
          onClick={props.onInputBorrowedRelease}
        />
      </td>
      <td className="slider">
        <Slider
          className="slider"
          min={props.minValue}
          max={props.maxValue}
          stepSize={1}
          value={props.value}
          onChange={props.onSliderChange}
          onRelease={props.onSliderRelease}
          disabled={props.disabled}
          labelRenderer={renderSliderLabel}
        />
      </td>
      <td className="repay-all">
        <IconButton
          className="repay-button"
          icon="delete"
          iconSize={23}
          disabled={props.disabled}
          onClick={props.repayAll}
        />
      </td>
      <td className="availabe-max">
        <span className="availabe-value">{props.viewValues.available}</span>
        <span className="max-value">/{props.viewValues.max}</span>{" "}
        <span className="balane-name">{props.balanceName}</span>
      </td>
    </tr>
  );
});
