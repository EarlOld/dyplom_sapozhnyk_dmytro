import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Leverage from "./Leverage";

export default {
  title: "Dashboard Leverage Balances",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <div className="bp3-dark">
    <Leverage />
  </div>
);

Default.story = {
  name: "default"
};
