import React from "react";
import Leverage from "./Leverage";
import { renderWithMockRedux } from "utils/testUtils";

describe("Leverage Balances table", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Leverage />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
