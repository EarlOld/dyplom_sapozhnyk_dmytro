import React, { memo, useMemo, useState } from "react";
import { connect } from "react-redux";
import { Button } from "@blueprintjs/core";
import { injectIntl, WrappedComponentProps } from "react-intl";
import _flowRight from "lodash/flowRight";
import LeverageSlider from "../../../LeverageSlider";
import { getFuturesBalances } from "../utils";
import FuturesMonthsTabs from "../FuturesMonthsTabs";
import { LeverageBalancesTable, LeverageBalanceEntry } from "./templates";
import "./styles.scss";
import { ITransferModalData } from "../../index";

type TStateProps = ReturnType<typeof mapStateToProps>;

interface IOpenTransferModal {
  openTransferModal: (data: ITransferModalData) => void;
  openDepositModal: (assetId: number) => void;
}
type TProps = TStateProps & WrappedComponentProps & IOpenTransferModal;

const LeverageBalances = memo(
  ({
    assetsList,
    allBalances,
    marketList,
    tickersList,
    intl,
    openTransferModal,
    openDepositModal
  }: TProps) => {
    const allBalancesList = useMemo(
      () =>
        getFuturesBalances(allBalances, assetsList, marketList, tickersList),
      [allBalances, assetsList, marketList, tickersList]
    );

    const [currentMonth, setCurrentMonth] = useState<string>("");

    const balancesList = useMemo(
      () =>
        currentMonth && currentMonth !== "all"
          ? allBalancesList.filter(
              balance => `${balance.monthName}_${balance.year}` === currentMonth
            )
          : allBalancesList,
      [allBalancesList, currentMonth]
    );

    return (
      <div>
        <div className="controls">
          <FuturesMonthsTabs
            balances={allBalancesList}
            onMonthChange={setCurrentMonth}
          />
          <div className="add">
            <Button onClick={() => openDepositModal(63488)}>
              {intl.formatMessage({ defaultMessage: "Deposit" })}
            </Button>
            <Button
              className="bp3-intent-primary"
              onClick={() =>
                openTransferModal({ fromAssetId: 63488, toAssetId: 0 })
              }
            >
              {intl.formatMessage({ defaultMessage: "Transfer" })}
            </Button>
          </div>
        </div>
        <div className="leverage-balances">
          <LeverageBalancesTable>
            {balancesList.map(balance => (
              <LeverageSlider
                key={balance.id}
                assetId={balance.id}
                name={`${balance.name} ${balance.monthName.toUpperCase()} ${
                  balance.year
                }`}
                balanceName={balance.name}
                isLocked={false}
                isDisabled={false}
                viewComponent={LeverageBalanceEntry}
              />
            ))}
          </LeverageBalancesTable>
        </div>
        {/*<div className="spot-balances-footer">*/}
        {/*  <Button className="history-button">*/}
        {/*    {intl.formatMessage({ defaultMessage: "View History" })}*/}
        {/*  </Button>*/}
        {/*</div>*/}
      </div>
    );
  }
);

const mapStateToProps = (state: IAppState) => ({
  allBalances: state.allBalances,
  assetsList: state.assetsList,
  marketList: state.marketList,
  tickersList: state.tickersList
});

export default _flowRight(
  connect(mapStateToProps),
  injectIntl
)(LeverageBalances);
