import React from "react";
import Futures from "./Futures";
import { renderWithMockRedux } from "utils/testUtils";

describe("Futures Balances table", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Futures />);
    expect(container.firstChild).toMatchSnapshot();
  });
});

it("Should have Collateral Required comumn in the table", () => {
  const { getByText } = renderWithMockRedux(<Futures />);
  const el = getByText("Collateral Required");
  expect(el).toBeInTheDocument();
});
