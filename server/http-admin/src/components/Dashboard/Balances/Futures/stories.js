import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Futures from "./Futures";

export default {
  title: "Dashboard Futures Balances",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <div className="bp3-dark">
    <Futures />
  </div>
);

Default.story = {
  name: "default"
};
