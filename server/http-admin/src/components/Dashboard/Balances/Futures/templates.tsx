import React, { memo, ReactNode } from "react";
import { injectIntl, WrappedComponentProps } from "react-intl";
import moment from "moment";
import _round from "lodash/round";
import { Button } from "@blueprintjs/core";
import { decimals } from "utils/reference";
import "./styles.scss";
import { IOpenTransferModal } from "../../index";

interface IFuturesBalancesHeaderProps {
  collateralTotal: number;
  required: number;
  PnL: number;
}

interface IFuturesBalancesTableProps {
  children: ReactNode;
}

interface ITableRow {
  id: number;
  name: string;
  monthName: string;
  year: number;
  transferred: number;
  transferredInUSDT: number;
  availableToWithdraw: number;
  borrowed: number;
  expires: number | null;
}

export const FuturesBalancesHeader = injectIntl(
  memo(
    ({
      collateralTotal,
      required,
      PnL
    }: IFuturesBalancesHeaderProps & WrappedComponentProps) => (
      <table className="balances-header-table">
        <thead>
          <tr>
            <th>Total collateral</th>
            <th>Collateral Required</th>
            <th>PnL</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              {collateralTotal.toLocaleString()}
              <span className="currency">USDT</span>
            </td>
            <td>
              {required.toLocaleString()}
              <span className="currency">USDT</span>
            </td>
            <td>
              {PnL.toLocaleString()}
              <span className="currency">USDT</span>
            </td>
          </tr>
        </tbody>
      </table>
    )
  )
);

export const FuturesBalancesTable = injectIntl(
  memo(
    ({
      children,
      intl
    }: IFuturesBalancesTableProps & WrappedComponentProps) => (
      <div className="balances-table-wrapper">
        <table className="bp3-html-table balances-table">
          <thead>
            <tr>
              <th className="title">
                {intl.formatMessage({ defaultMessage: "Futures Asset" })}
              </th>
              <th className="transferred">
                {intl.formatMessage({ defaultMessage: "Transferred Amount" })}
              </th>
              <th className="available">
                {intl.formatMessage({ defaultMessage: "Max Withdrawal" })}
              </th>
              <th className="borrowed">
                {intl.formatMessage({ defaultMessage: "Borrowed" })}
              </th>
              <th className="actions">
                {intl.formatMessage({ defaultMessage: "Actions" })}
              </th>
            </tr>
          </thead>
          <tbody>{children}</tbody>
        </table>
      </div>
    )
  )
);

export const FuturesBalanceEntry = injectIntl(
  memo((props: ITableRow & WrappedComponentProps & IOpenTransferModal) => {
    const _decimals = 4;
    return (
      <tr>
        <td className="primary title">
          {props.name} {props.monthName.toUpperCase()} {props.year}
          <span className="expiry">
            {props.expires
              ? moment(props.expires).format("DD-MM-YYYY") +
                " " +
                props.intl.formatMessage({
                  id: "expiry",
                  defaultMessage: "Expiry"
                })
              : " "}
          </span>
        </td>
        <td className="transferred">
          <span className="balance-name">{props.name}</span>
          {props.transferred.toFixed(_decimals).toLocaleString()}
          <span className="usdt-value">
            USDT{" "}
            {_round(
              props.transferredInUSDT,
              decimals["USDT"].balance
            ).toLocaleString()}{" "}
          </span>
        </td>
        <td className="available">
          <span className="balance-name">{props.name}</span>
          {props.availableToWithdraw.toFixed(_decimals).toLocaleString()}
        </td>
        <td className="borrowed">
          <span className="balance-name">{props.name}</span>
          {_round(props.borrowed, _decimals).toLocaleString()}
        </td>
        <td className="actions">
          <Button
            className="bp3-intent-primary"
            onClick={() =>
              props.openTransferModal({
                fromAssetId: props.id,
                toAssetId: props.id
              })
            }
          >
            {props.intl.formatMessage({
              id: "transfer",
              defaultMessage: "Transfer"
            })}
          </Button>
        </td>
      </tr>
    );
  })
);
