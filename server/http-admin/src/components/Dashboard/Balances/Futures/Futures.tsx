import React, { memo, useMemo, useState } from "react";
import { connect } from "react-redux";
import { injectIntl, WrappedComponentProps } from "react-intl";
import _flowRight from "lodash/flowRight";
import _sortBy from "lodash/sortBy";
import _first from "lodash/first";
import { SCALED } from "../data";
import { expandFuturesBalance, getFuturesBalances } from "../utils";
import FuturesMonthsTabs from "../FuturesMonthsTabs";

import {
  FuturesBalanceEntry,
  FuturesBalancesHeader,
  FuturesBalancesTable
} from "./templates";

import "./styles.scss";
import { IOpenTransferModal } from "../../index";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TProps = TStateProps & WrappedComponentProps & IOpenTransferModal;

const FuturesBalances = memo(
  ({
    assetsList,
    allBalances,
    borrowLoans,
    borrowConvertedTotals,
    borrowCollaterals,
    marketList,
    tickersList,
    openTransferModal
  }: TProps) => {
    const expandedFuturesBalance: IExpandedFuturesBalance[] = useMemo(() => {
      const futuresBalances: IFuturesBalance[] = getFuturesBalances(
        allBalances,
        assetsList,
        marketList,
        tickersList
      );
      return futuresBalances.map(futuresBalance =>
        expandFuturesBalance(
          futuresBalance,
          borrowLoans,
          assetsList,
          borrowConvertedTotals,
          borrowCollaterals
        )
      );
    }, [
      allBalances,
      assetsList,
      tickersList,
      marketList,
      borrowCollaterals,
      borrowConvertedTotals,
      borrowLoans
    ]);

    const lastMonthString = _first(expandedFuturesBalance)
      ? [
          _first(expandedFuturesBalance)!.monthName,
          _first(expandedFuturesBalance)!.year
        ].join("_")
      : "all";

    const [currentMonth, setCurrentMonth] = useState<string>(
      lastMonthString || "all"
    );

    const balancesList = useMemo(
      () =>
        currentMonth && currentMonth !== "all"
          ? expandedFuturesBalance.filter((balance: IFuturesBalance) => {
              return `${balance.monthName}_${balance.year}` === currentMonth;
            })
          : expandedFuturesBalance,
      [expandedFuturesBalance, currentMonth]
    );

    const headerData = useMemo(() => {
      return balancesList.reduce(
        (result, balance) => {
          if (balance.price) {
            if (balance.name === "USDT") {
              result.collateralTotal += balance.collateralTotal;
              result.required +=
                balance.collateralTotal - balance.collateralAvailable;
            }

            const pnl =
              ((balance.required +
                balance.available -
                balance.borrowed -
                balance.transferred) *
                balance.price) /
              SCALED;

            result.PnL += pnl;
          }
          return result;
        },
        {
          collateralTotal: 0,
          required: 0,
          PnL: 0
        }
      );
    }, [balancesList]);

    return (
      <div>
        <div className="controls">
          <FuturesMonthsTabs
            balances={expandedFuturesBalance}
            onMonthChange={setCurrentMonth}
          />
        </div>
        <div className="futures-balances">
          <FuturesBalancesHeader {...headerData} />
          <FuturesBalancesTable>
            {_sortBy(balancesList, "availableToWithdraw")
              .reverse()
              .map(balance => (
                <FuturesBalanceEntry
                  key={balance.id}
                  {...balance}
                  openTransferModal={openTransferModal}
                />
              ))}
          </FuturesBalancesTable>
        </div>
      </div>
    );
  }
);

const mapStateToProps = (state: IAppState) => ({
  allBalances: state.allBalances,
  assetsList: state.assetsList,
  borrowLoans: state.borrowLoans,
  borrowConvertedTotals: state.borrowConvertedTotals,
  borrowCollaterals: state.borrowCollaterals,
  marketList: state.marketList,
  tickersList: state.tickersList
});

export default _flowRight(
  connect(mapStateToProps),
  injectIntl
)(FuturesBalances);
