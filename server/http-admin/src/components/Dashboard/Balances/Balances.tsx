import React, { useState } from "react";
import Pane from "../Pane";

import Futures from "./Futures/";
import Leverage from "./Leverage/";
import Spot from "./Spot/";

import { injectIntl, WrappedComponentProps } from "react-intl";
import { ITransferModalData } from "../index";

interface IProps extends WrappedComponentProps {
  openTransferModal: (data: ITransferModalData) => void;
  openWithdrawalModal: (assetId: number) => void;
  openDepositModal: (assetId: number) => void;
}

export const Balances = ({
  intl,
  openTransferModal,
  openWithdrawalModal,
  openDepositModal
}: IProps) => {
  const [currentTab, setCurrentTab] = useState<string>("spot");

  const tabs = [
    {
      id: "spot",
      title: intl.formatMessage({ defaultMessage: "Spot" })
    },
    {
      id: "futures",
      title: intl.formatMessage({ defaultMessage: "Futures" })
    },
    {
      id: "leverage",
      title: intl.formatMessage({ defaultMessage: "Leverage" })
    }
  ];

  return (
    <div className="bp3-dark">
      <Pane
        title={intl.formatMessage({
          defaultMessage: "Balances",
          id: "dashboard-balances-spot"
        })}
        tabs={tabs}
        tabsId="balances_tabs"
        onTabsChange={setCurrentTab}
        link={"/dashboard/balances"}
      >
        {currentTab === "spot" && (
          <Spot
            openTransferModal={openTransferModal}
            openWithdrawalModal={openWithdrawalModal}
            openDepositModal={openDepositModal}
          />
        )}
        {currentTab === "leverage" && (
          <Leverage
            openTransferModal={openTransferModal}
            openDepositModal={openDepositModal}
          />
        )}
        {currentTab === "futures" && (
          <Futures openTransferModal={openTransferModal} />
        )}
      </Pane>
    </div>
  );
};

export default injectIntl(Balances);
