import React, { memo, useMemo, useEffect } from "react";
import { Tabs, Tab } from "@blueprintjs/core";
import { injectIntl, WrappedComponentProps } from "react-intl";

import ControlsGroup from "../../ControlsGroup";
import { MONTHS_LIST } from "../../data";

import "./styles.scss";

interface ITabBalance extends IBalance {
  name: string;
  monthName: string;
  year: number;
}

interface IOwnProps {
  defaultTabId?: string;
  onMonthChange: (month: string) => void;
  balances: ITabBalance[];
}

let isInit = false;

export default injectIntl(
  memo((props: IOwnProps & WrappedComponentProps) => {
    const monthsList = useMemo(
      () =>
        Array.from(
          props.balances.reduce((result, balance) => {
            result.add(`${balance.monthName}_${balance.year}`);

            return result;
          }, new Set<string>())
        ).sort((a, b) => {
          const [monthA, yearA] = a.split("_");
          const [monthB, yearB] = b.split("_");
          if (yearA !== yearB) {
            return +yearA > +yearB ? -1 : 1;
          }
          const monthNumA = MONTHS_LIST.indexOf(monthA.toUpperCase());
          const monthNumB = MONTHS_LIST.indexOf(monthB.toUpperCase());

          return monthNumA !== monthNumB ? (monthNumA > monthNumB ? 1 : -1) : 0;
        }),
      [props.balances]
    );

    useEffect(() => {
      if (isInit) {
        return;
      }

      if (monthsList && monthsList.length) {
        isInit = true;
        props.onMonthChange(monthsList[0]);
      }
    }, [monthsList, props]);

    return (
      <div>
        <div className="title">
          {props.intl.formatMessage({
            id: "contract-month-title",
            defaultMessage: "Contract Month"
          })}
        </div>
        <ControlsGroup>
          <Tabs
            id="months_tabs"
            animate={false}
            defaultSelectedTabId={props.defaultTabId}
            onChange={props.onMonthChange}
          >
            {monthsList.map(month => (
              <Tab id={month} key={month} title={month.replace("_", " ")} />
            ))}
            <Tab
              id="all"
              key="all"
              title={props.intl.formatMessage({
                id: "all-months-tab",
                defaultMessage: "All"
              })}
            />
          </Tabs>
        </ControlsGroup>
      </div>
    );
  })
);
