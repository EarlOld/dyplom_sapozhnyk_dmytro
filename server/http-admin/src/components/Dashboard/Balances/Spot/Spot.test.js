import React from "react";
import Spot from "./Spot";
import { renderWithMockRedux } from "utils/testUtils";

describe("Spot Balances table", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Spot />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should have FLEX in list of assets", () => {
    const { getByText } = renderWithMockRedux(<Spot />);
    const asset = getByText("FLEX");
    expect(asset).toBeInTheDocument();
  });

  it("Should have 96.49 value in the table", () => {
    const { getByText } = renderWithMockRedux(<Spot />);
    const valueApprox = getByText("96.49");
    expect(valueApprox).toBeInTheDocument();
  });
});
