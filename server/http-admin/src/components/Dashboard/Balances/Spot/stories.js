import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Spot from "./Spot";

export default {
  title: "Dashboard Spot Balances",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <div className="bp3-dark">
    <Spot />
  </div>
);

Default.story = {
  name: "default"
};
