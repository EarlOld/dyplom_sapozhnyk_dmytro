export interface IMarketProps {
  id: number;
  name: string;
  total: number;
  dollarWorth: number;
}
