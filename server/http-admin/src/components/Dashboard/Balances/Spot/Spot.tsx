import React, { memo } from "react";
import { connect } from "react-redux";
import _flowRight from "lodash/flowRight";
import _sortBy from "lodash/sortBy";
import { injectIntl, WrappedComponentProps } from "react-intl";

import { isSpotAsset } from "utils/isSpotAsset";
import { isUsdAsset } from "utils/isUsdAsset";

import { SCALED } from "../data";

import { SpotBalancesHeader, SpotBalancesTable } from "./templates";
import "./styles.scss";
import { IOpenTransferModal } from "../../index";

interface IOpenWithdrawalModal {
  openWithdrawalModal: (assetId: number) => void;
  openDepositModal: (assetName: number) => void;
  openTransferModal: (transferModalData: IOpenTransferModal) => void;
}

type TStateProps = ReturnType<typeof mapStateToProps>;

type TProps = TStateProps &
  WrappedComponentProps &
  IOpenWithdrawalModal &
  IOpenTransferModal;

const SpotBalances = memo(
  ({
    assetsList,
    allBalances,
    changeList,
    openWithdrawalModal,
    openDepositModal,
    intl,
    openTransferModal,
    userAssets
  }: TProps) => {
    let usdtTotal = 0;
    let dataSource: any[] = [];

    if (userAssets && allBalances) {
      const mergedBalances = userAssets.map(uA => {
        const matchingBalance = allBalances.find(
          (balance: IBalance) => balance.id === uA.asset.corecode
        );
        return {
          id: uA.asset.corecode,
          available: 0,
          reserved: 0,
          ...matchingBalance
        };
      });

      dataSource = mergedBalances
        .filter((balance: IBalance): boolean => {
          const matchingAsset: IAsset | undefined = assetsList.find(
            asset => asset.id === balance.id
          );
          return !!(matchingAsset && isSpotAsset(matchingAsset));
        })
        .map((balance: IBalance) => {
          const total = (balance.available + balance.reserved) / SCALED;
          const isUsd = isUsdAsset(balance.id);
          const matchingAsset: IAsset | undefined = assetsList.find(
            asset => asset.id === balance.id
          );
          const matchingChange = changeList.find(
            change => change.base === balance.id && isUsdAsset(change.counter)
          );

          const price = isUsd ? 1 : matchingChange ? matchingChange.price : 0;
          const name =
            (matchingAsset && matchingAsset.name) ||
            intl.formatMessage({ defaultMessage: "Unknown Asset" });

          const dollarWorth = price * total;
          usdtTotal += dollarWorth;
          return {
            id: balance.id,
            name: name,
            total,
            dollarWorth
          };
        });
    }

    return (
      <div className="spot-balances">
        <SpotBalancesHeader
          usdtTotal={usdtTotal}
          openTransferModal={openTransferModal}
        />
        <SpotBalancesTable
          userAssets={userAssets}
          rows={_sortBy(dataSource, "total").reverse()}
          usdtTotal={usdtTotal}
          openDepositModal={openDepositModal}
          openWithdrawalModal={openWithdrawalModal}
          openTransferModal={openTransferModal}
        />
      </div>
    );
  }
);

const mapStateToProps = (state: IAppState) => ({
  userAssets: state.userAssets,
  allBalances: state.allBalances,
  assetsList: state.assetsList,
  bucketList: state.bucketList,
  changeList: state.changeList
});

export default _flowRight(connect(mapStateToProps), injectIntl)(SpotBalances);
