import React, { memo, useState, useEffect } from "react";
import { Button } from "@blueprintjs/core";
import { injectIntl, WrappedComponentProps } from "react-intl";
import _round from "lodash/round";
import _orderBy from "lodash/orderBy";
import { decimals } from "utils/reference";
import { sortCurrencies } from "../utils";
import { Icon, Intent } from "@blueprintjs/core";
import { IMarketProps } from "./types";
import "./styles.scss";
import { IOpenTransferModal, ITransferModalData } from "../../index";

interface ITableRow {
  id: number;
  name: string;
  total: number;
  dollarWorth: number;
}

interface ISpotBalancesHeaderProps {
  usdtTotal: number;
}

interface ISpotBalancesTableProps {
  rows: ITableRow[];
  usdtTotal: number;
  openWithdrawalModal: (balanceId: number) => void;
  openDepositModal: (assetName: number) => void;
  openTransferModal: (data: ITransferModalData) => void;
  userAssets: IUserAsset[];
}

export const SpotBalancesHeader = injectIntl(
  memo(
    ({
      usdtTotal,
      intl,
      openTransferModal
    }: ISpotBalancesHeaderProps &
      WrappedComponentProps &
      IOpenTransferModal) => (
      <div className={"spot-header"}>
        <section>
          <div className="title">
            {intl.formatMessage({ defaultMessage: "Total Value" })}
          </div>
          <div className="total-value">
            {_round(usdtTotal, decimals["USDT"].balance).toLocaleString()}
            <span className="currency">USDT</span>
          </div>
        </section>
        <section>
          <div className="info">
            {intl.formatMessage({
              defaultMessage: "Transfer a spot balance to a Futures Asset"
            })}
          </div>
          <Button
            onClick={() =>
              openTransferModal({ fromAssetId: 63488, toAssetId: 51202 })
            }
          >
            {intl.formatMessage({ defaultMessage: "Transfer" })}
          </Button>
        </section>
      </div>
    )
  )
);

export const SpotBalancesTable = injectIntl(
  memo(
    ({
      rows,
      openWithdrawalModal,
      usdtTotal,
      openDepositModal,
      openTransferModal,
      userAssets,
      intl
    }: ISpotBalancesTableProps & WrappedComponentProps) => {
      const [tableRows, setTableRows] = useState<Array<IMarketProps> | []>([]);
      const [sortOrder, setSortOrder] = useState<"desc" | "asc">("desc");
      useEffect(() => {
        const sortedRows = sortCurrencies(rows);
        setTableRows(sortedRows);
      }, [rows]);

      const handleSortByCoin = () => {
        setTableRows(_orderBy(tableRows, ["name"], sortOrder));
        setSortOrder(sortOrder === "desc" ? "asc" : "desc");
      };

      const handleSortByUSDT = () => {
        setTableRows(_orderBy(tableRows, ["dollarWorth"], sortOrder));
        setSortOrder(sortOrder === "desc" ? "asc" : "desc");
      };
      return (
        <div className="balances-table-wrapper">
          <table className="bp3-html-table balances-table">
            <thead>
              <tr>
                <th className="coin">
                  <span>{intl.formatMessage({ defaultMessage: "Asset" })}</span>
                </th>
                <th className="coin-balance">
                  <span>
                    {intl.formatMessage({ defaultMessage: "Balance" })}
                  </span>
                  <span className="sort-icon" onClick={handleSortByCoin}>
                    {sortOrder === "desc" ? (
                      <Icon icon="caret-down" />
                    ) : (
                      <Icon icon="caret-up" />
                    )}
                  </span>
                </th>
                <th className="usdt">
                  <span>{intl.formatMessage({ defaultMessage: "USDT" })}</span>
                  <span className="sort-icon" onClick={handleSortByUSDT}>
                    {sortOrder === "desc" ? (
                      <Icon icon="caret-down" />
                    ) : (
                      <Icon icon="caret-up" />
                    )}
                  </span>
                </th>
                <th className="actions" colSpan={3}>
                  {intl.formatMessage({ defaultMessage: "Actions" })}
                </th>
              </tr>
            </thead>
            <tbody>
              {(tableRows as Array<IMarketProps>).map(item => {
                const userAsset: IUserAsset | undefined =
                  (userAssets &&
                    userAssets.find(
                      (userAsset: IUserAsset) =>
                        userAsset.asset.corecode === item.id
                    )) ||
                  undefined;

                const pendingDeposit =
                  userAsset &&
                  userAsset.deposit_address_requested &&
                  !userAsset.deposit_address;

                return (
                  <tr key={item.id}>
                    <td className="primary title">{item.name}</td>
                    <td className="row-total">
                      {_round(
                        item.total,
                        (decimals[item.name] && decimals[item.name].balance) ||
                          2
                      ).toFixed(4)}
                    </td>
                    <td className="usdt">
                      {" "}
                      {item.dollarWorth
                        ? `${_round(
                            item.dollarWorth,
                            decimals["USDT"].balance
                          ).toLocaleString()}`
                        : "0"}
                    </td>
                    <td className="actions" colSpan={3}>
                      <Button
                        className={"transfer-action"}
                        onClick={() =>
                          openTransferModal({
                            fromAssetId: item.id,
                            toAssetId: 0
                          })
                        }
                        intent={Intent.NONE}
                      >
                        {intl.formatMessage({ defaultMessage: "Transfer" })}
                      </Button>
                      <Button
                        disabled={!item.total}
                        onClick={() => openWithdrawalModal(item.id)}
                      >
                        {intl.formatMessage({ defaultMessage: "Withdraw" })}
                      </Button>
                      <Button
                        className="bp3-intent-primary"
                        onClick={() => openDepositModal(item.id)}
                        loading={pendingDeposit}
                      >
                        {intl.formatMessage({ defaultMessage: "Deposit" })}
                      </Button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <tfoot>
              <tr>
                <td>{intl.formatMessage({ defaultMessage: "Total" })}</td>
                <td className="usdt">&nbsp;</td>
                <td className="total"> {usdtTotal.toLocaleString()}</td>
                <td className="actions" colSpan={3}>
                  &nbsp;
                </td>
              </tr>
            </tfoot>
          </table>
        </div>
      );
    }
  )
);
