import styled from "styled-components";
export const StyledVideosContainer = styled.div`
  .bp3-card {
    padding: 4px;
    margin: 16px 4px;
    h5 {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      text-align: center;
      margin-top: 4px;
    }
    iframe {
      width: 100%;
    }
  }
`;
