import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import Videos from "./Videos";

export default {
  title: "Videos",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <Videos />;

Default.story = {
  name: "default"
};
