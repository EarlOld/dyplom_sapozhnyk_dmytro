import React, { memo, useEffect } from "react";
import { PLAYER_OPTIONS } from "./data";
import _flowRight from "lodash/flowRight";
import { connect } from "react-redux";
import { injectIntl, WrappedComponentProps } from "react-intl";
import { setTVVideosList } from "store/actions";
import YouTube from "react-youtube";
import { loadVideosList } from "../../TVWidget/utils";
import { Card, Elevation, H5 } from "@blueprintjs/core";
import { StyledVideosContainer } from "./styles";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
type TProps = TStateProps & TDispatchProps & WrappedComponentProps;

export const Videos = memo(({ videosList, setTVVideosList }: TProps) => {
  useEffect(() => loadVideosList(result => setTVVideosList(result)), [
    setTVVideosList
  ]);

  return videosList && videosList.length ? (
    <StyledVideosContainer>
      <Card elevation={Elevation.ONE}>
        <H5>{videosList[videosList.length - 1].title}</H5>
        <YouTube
          videoId={videosList[videosList.length - 1].id}
          opts={PLAYER_OPTIONS}
        />
      </Card>
      <Card elevation={Elevation.ONE}>
        <H5>{videosList[videosList.length - 2].title}</H5>
        <YouTube
          videoId={videosList[videosList.length - 2].id}
          opts={PLAYER_OPTIONS}
        />
      </Card>
      <Card elevation={Elevation.ONE}>
        <H5>{videosList[videosList.length - 3].title}</H5>
        <YouTube
          videoId={videosList[videosList.length - 3].id}
          opts={PLAYER_OPTIONS}
        />
      </Card>
    </StyledVideosContainer>
  ) : null;
});

const mapStateToProps = (state: IPublicState) => {
  return {
    videosList: state.tvVideosList
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setTVVideosList(params: ILoadVideosListResult) {
      dispatch(setTVVideosList(params));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(Videos);
