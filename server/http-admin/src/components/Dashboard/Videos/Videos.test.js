import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import Videos from "./Videos";

describe("Videos tables", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<Videos />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
