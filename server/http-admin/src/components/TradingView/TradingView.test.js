import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import iotView from "./iotView";

describe("iotView", () => {
  it("Should be rendered in the DOM", () => {
    const { container } = renderWithMockRedux(<iotView />);
    expect(container.firstChild).toBeInTheDocument();
  });
});
