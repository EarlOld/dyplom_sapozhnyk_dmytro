import React from "react";
import { Dispatch } from "react";
import { getChartHistory } from "services/http/http";
import { connect } from "react-redux";
import { setPositionList } from "store/actions";
import {
  widget,
  Bar,
  ChartingLibraryWidgetOptions,
  StudyOverrides,
  TimeFrameItem,
  LanguageCode,
  IChartingLibraryWidget,
  Timezone,
  ThemeName
} from "iotview/charting_library/charting_library.min";
import {
  ErrorCallback,
  LibrarySymbolInfo,
  ResolutionString
} from "iotview/charting_library/datafeed-api";
import { IBasicDataFeed } from "iotview/charting_library/charting_library.min";
import "./style/index.scss";
import { localesMap } from "utils/i18n";
import Datafeeds from "./datafeeds/datafeeds";
export interface IDataFeeds extends IBasicDataFeed {
  barsUpdater: any;
}
export type TGetBars = (
  symbolInfo: LibrarySymbolInfo,
  resolution: ResolutionString,
  rangeStartDate: number,
  rangeEndDate: number,
  onDataCallback: (bars: Bar[]) => void,
  onErrorCallback: ErrorCallback
) => void;

type TGetBarsRequestParams = [
  LibrarySymbolInfo,
  ResolutionString,
  number,
  number,
  (bars: Bar[]) => void,
  ErrorCallback
];

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

type TWidgetProps = Pick<
  ChartingLibraryWidgetOptions,
  | "interval"
  | "custom_css_url"
  | "enabled_features"
  | "disabled_features"
  | "autosize"
  | "timezone"
  | "theme"
  | "debug"
  | "overrides"
  | "toolbar_bg"
  | "locale"
  | "preset"
> & {
  containerId: string;
  libraryPath: string;
  studiesOverrides: StudyOverrides;
  timeFrames: TimeFrameItem[];
};

interface IOwnProps extends TWidgetProps {
  symbol: string;
}

type TProps = TStateProps & TDispatchProps & IOwnProps;

interface ICacheData {
  [ticker: string]: IChartHistoryItem[];
}

export class iotView extends React.PureComponent<TProps> {
  static defaultProps = {
    symbol: "xbt_usdt",
    interval: "60",
    containerId: "tv_chart_container",
    libraryPath: "/charting_library/",
    custom_css_url: "/charting_library_style.css",
    autosize: true,
    timezone: "Asia/Shanghai" as Timezone,
    locale: "en" as LanguageCode,
    theme: "Dark" as ThemeName,
    debug: false,
    preset: "mobile" as "mobile",
    disabled_features: [
      "remove_library_container_border",
      "hide_last_na_study_output",
      "property_pages",
      "use_localstorage_for_settings",
      "header_symbol_search",
      "header_compare",
      "header_compare",
      "header_interval_dialog_button",
      "show_interval_dialog_on_key_press",
      "header_saveload",
      "remove_library_container_border"
    ],
    studiesOverrides: {
      "bollinger bands.median.color": "#33FF88",
      "bollinger bands.upper.linewidth": 7,
      "volume.volume.color.0": "#376691",
      "volume.volume.color.1": "#376691"
    },
    overrides: {
      "paneProperties.background": "#191919",
      "symbolWatermarkProperties.color": "rgba(0, 0, 0, 0)",
      "mainSeriesProperties.candleStyle.upColor": "rgb(0, 211, 184)",
      "mainSeriesProperties.candleStyle.downColor": "rgb(255, 35, 102)",
      "paneProperties.vertGridProperties.color": "rgba(43, 80, 114, 0)",
      "paneProperties.horzGridProperties.color": "rgba(43, 80, 114, 0)",
      "scalesProperties.lineColor": "rgba(255, 255, 255, 0.3)",
      "scalesProperties.textColor": "rgba(255, 255, 255, 0.3)",
      "scalesProperties.backgroundColor": "#0D0F10",
      "paneProperties.legendProperties.showLegend": false
    },
    timeFrames: [],
    toolbar_bg: "rgba(43, 80, 114, 0)"
  };

  public symbol: string;
  public symbolTitle = "-";

  private baseId = 0;
  private quoteId = 0;
  private interval: string;
  private getBarTimer?: ReturnType<typeof setTimeout>;
  private tvWidget?: IChartingLibraryWidget;
  private cacheData: ICacheData = {};
  private datafeeds: IDataFeeds;
  private lastTime = 0;
  private timer?: ReturnType<typeof setTimeout>;
  private intervalList = [
    {
      text: "1m",
      type: "1",
      param: "1m"
    },
    {
      text: "5m",
      type: "5",
      param: "5m"
    },
    {
      text: "30m",
      type: "30",
      param: "30m"
    },
    {
      text: "1h",
      type: "60",
      param: "1h"
    },
    {
      text: "2h",
      type: "120",
      param: "2h"
    },
    {
      text: "4h",
      type: "240",
      param: "4h"
    },
    {
      text: "1d",
      type: "1440",
      param: "1d"
    }
  ];
  private getBarsRequestParams?: TGetBarsRequestParams;

  constructor(props: TProps) {
    super(props);
    this.symbol = props.symbol;
    this.interval = props.interval;
    this.datafeeds = new Datafeeds(this);
  }

  componentDidMount() {
    if (this.props.currentSymbol.symbolTitle) {
      this.initMarket();
    }
  }

  componentDidUpdate(prevProps: TProps) {
    const prevCurrentSymbol = prevProps.currentSymbol;
    const currentSymbol = this.props.currentSymbol;

    if (
      currentSymbol.symbolTitle &&
      !(prevCurrentSymbol && prevCurrentSymbol.symbolTitle)
    ) {
      this.initMarket();
    }

    if (this.props.selectedMarket !== prevProps.selectedMarket) {
      this.initMarket(this.props.selectedMarket);
    }
  }

  componentWillUnmount() {
    if (this.tvWidget) {
      this.tvWidget.remove();
      this.tvWidget = undefined;
    }
  }

  initMarket = (event?: IMarket | IPositionListItem) => {
    if (event) {
      this.baseId = event.base;
      this.quoteId = event.counter;
      this.symbolTitle = event.name;
    } else {
      this.baseId = this.props.currentSymbol.base.id;
      this.quoteId = this.props.currentSymbol.quote.id;
      this.symbolTitle = this.props.currentSymbol.symbolTitle;
    }

    let symbol = this.baseId.toString() + "_" + this.quoteId.toString();

    const symbolsMatch = symbol === this.symbol;
    const widgetExists = !!this.tvWidget;

    if (widgetExists) {
      if (!symbolsMatch) {
        this.symbol = symbol;
        let since = 0;
        this.datafeeds.barsUpdater.requestsPending = 0;
        this.tvWidget!.setSymbol(this.symbolTitle, this.interval, () => {});
        this._getKlineData(this.baseId, this.quoteId, this.interval, since);
      }
      return;
    } else {
      this.symbol = symbol;
      if (!widgetExists) {
        this.init();
        if (this.tvWidget) {
          let since = 0;
          this._getKlineData(this.baseId, this.quoteId, this.interval, since);
        }
      }
    }
  };

  init = () => {
    const { props } = this;
    const widgetOptions = {
      symbol: props.currentSymbol.symbolTitle,
      datafeed: this.datafeeds,
      interval: props.interval,
      container_id: props.containerId,
      library_path: props.libraryPath,
      custom_css_url: props.custom_css_url,
      locale: localesMap[props.i18n.locale].chartCode as LanguageCode,
      enabled_features: props.enabled_features,
      disabled_features: props.disabled_features,
      autosize: props.autosize,
      studies_overrides: props.studiesOverrides,
      timezone: props.timezone,
      theme: props.theme,
      debug: props.debug,
      // preset: this.props.preset,
      time_frames: props.timeFrames,
      overrides: props.overrides,
      toolbar_bg: props.toolbar_bg
    };

    // this.tvWidget = new widget(widgetOptions) as IChartingLibraryWidget;

    this.tvWidget = new widget(widgetOptions) as IChartingLibraryWidget;
    const thatWidget = this.tvWidget;
    this.tvWidget.onChartReady(() => {
      thatWidget
        .chart()
        .onIntervalChanged()
        .subscribe(null, (interval, obj) => {
          this.resetInterval(interval);
        });
      // this._getKlineData(this.baseId, this.quoteId, this.interval, 1572004800000)
    });

    // this.tvWidget.onChartReady(() => {
    //   let thats = this.tvWidget as IChartingLibraryWidget;
    //   //设置均线种类 均线样式
    //   // thats.chart().createStudy("Moving Average", false, false, [5], null, {
    //   //   "Plot.color": "rgb(150, 95, 196)"
    //   // });
    //   // thats.chart().createStudy("Moving Average", false, false, [10], null, {
    //   //   "Plot.color": "rgb(116,149,187)"
    //   // });
    //   // thats.chart().createStudy("Moving Average", false, false, [20], null, {
    //   //   "plot.color": "rgb(58,113,74)"
    //   // });
    //   // thats.chart().createStudy("Moving Average", false, false, [30], null, {
    //   //   "plot.color": "rgb(118,32,99)"
    //   // });
    //   thats
    //     .chart()
    //     .onIntervalChanged()
    //     .subscribe(null, (interval, obj) => {
    //       this.resetInterval(interval);
    //     });
    //   // this._getKlineData(this.baseId, this.quoteId, this.interval, 1572004800000)
    // });
  };

  getBars: TGetBars = (
    symbolInfo,
    resolution,
    rangeStartDate,
    rangeEndDate,
    onDataCallback,
    onErrorCallback
  ) => {
    // if (this.interval !== resolution) {
    // 	this.ws.onunsub(this.interval)
    // 	this.interval = resolution;
    // 	this.ws.onsub();
    // }
    var ticker = this.symbol + "-" + this.interval;
    if (this.cacheData[ticker] && this.cacheData[ticker].length) {
      let newBars: Bar[] = [];
      this.cacheData[ticker].forEach(item => {
        if (
          item.time >= rangeStartDate * 1000 &&
          item.time <= rangeEndDate * 1000
        ) {
          newBars.push(item);
        }
      });
      onDataCallback(newBars);
      this.getBarsRequestParams = undefined;
    } else {
      this.getBarsRequestParams = [
        symbolInfo,
        resolution,
        rangeStartDate,
        rangeEndDate,
        onDataCallback,
        onErrorCallback
      ];
    }
  };

  onDataUpdated() {
    if (this.getBarsRequestParams) {
      this.getBars(...this.getBarsRequestParams);
    }
  }

  resetInterval = (interval: string) => {
    if (interval === this.interval) return;
    this.interval = interval;
    let since = 0;
    this._getKlineData(this.baseId, this.quoteId, this.interval, since);
    // this.tvWidget.chart().setResolution(interval, function onReadyCallback() { });
  };

  _getKlineData = (
    base: number,
    counter: number,
    interval: string,
    since: number
  ) => {
    let data = {
      base,
      counter,
      interval: this.intervalList.filter(item => item.type === interval)[0]
        .param,
      since
    };
    getChartHistory(data).then((res: IChartHistoryItem[]) => {
      let data = res;
      var list: IChartHistoryItem[] = [];
      var ticker = this.symbol + "-" + this.interval;
      if (data && data.length) {
        data.forEach(element => {
          list.push({
            time: element.time,
            open: element.open,
            high: element.high,
            low: element.low,
            close: element.close,
            volume: element.volume
          });
        });
        list.sort((a, b) => a.time - b.time);
        if (list.length > 1 || !this.cacheData[ticker]) {
          this.cacheData[ticker] = list;
        } else if (list.length === 1) {
          if (this.lastTime === list[0].time) {
            this.cacheData[ticker].pop();
          }
          this.cacheData[ticker].push(list[0]);
        }
        this.lastTime = list[list.length - 1].time;
        this.datafeeds.barsUpdater.updateData();
        // this.subscribe()
      } else {
        this.lastTime = 0;
        this.cacheData[ticker] = [];
        this.datafeeds.barsUpdater.updateData();
      }
      this.onDataUpdated();
    });
  };

  updateLine = (e: IOrdersMatched) => {
    if (e.time <= this.lastTime) {
      return;
    }
    var ticker =
      e.base.toString() + "_" + e.counter.toString() + "-" + this.interval;
    let nextTime = this.lastTime + +this.interval * 60 * 1000;
    if (!this.cacheData[ticker]) return;
    if (parseInt("" + e.time / 1000) < nextTime) {
      let lastData = this.cacheData[ticker].pop();
      if (lastData) {
        let lastPrice = e.price / 10000;
        lastData.last = lastPrice;
        lastData.close = lastPrice;
        if (lastPrice < lastData.low) {
          lastData.low = lastPrice;
        }
        if (lastPrice > lastData.high) {
          lastData.high = lastPrice;
        }
        lastData.volume += e.quantity / 10000;
        this.cacheData[ticker].push(lastData);
      }
    } else {
      this.lastTime = nextTime;
      let lastPrice = e.price / 10000;
      let lastData: IChartHistoryItem = {
        close: lastPrice,
        high: lastPrice,
        low: lastPrice,
        open: lastPrice,
        last: lastPrice,
        time: this.lastTime,
        volume: e.quantity / 10000
      };
      this.cacheData[ticker].push(lastData);
    }
    if (!!this.tvWidget) {
      this.datafeeds.barsUpdater.updateData();
    }
    this.onDataUpdated();
  };

  render() {
    let { lastTrade, currentSymbol } = this.props;
    if (
      lastTrade[currentSymbol.base.id] &&
      lastTrade[currentSymbol.base.id].price
    ) {
      this.updateLine(lastTrade[currentSymbol.base.id] as IOrdersMatched);
    }
    return (
      <div className="chart">
        <div className="tv-wrapper">
          {/* <ul className="interval-btns" className="flex">
                    <li>5M</li>
                    <li>30M</li>
                    <li>1H</li>
                    <li>2H</li>
                    <li>4H</li>
                </ul> */}
          <div id={this.props.containerId} className="TVChartContainer" />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    currentSymbol: state.currentSymbol,
    lastTrade: state.lastTrade,
    i18n: state.i18n,
    selectedMarket: state.selectedMarket
  };
};

const mapDispatchToProps = (dispatch: Dispatch<TDispatch>) => {
  return {
    setPositionList(data: IPositionListItem[]) {
      dispatch(setPositionList(data));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(iotView);
