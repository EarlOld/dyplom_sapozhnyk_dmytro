import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import iotView from "./iotView";

export default {
  title: "iotView",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <iotView />;

Default.story = {
  name: "default"
};
