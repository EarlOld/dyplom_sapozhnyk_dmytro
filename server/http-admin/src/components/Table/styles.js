import { Table } from "antd";
import styled from "styled-components";

export const StyledTable = styled(Table)`
  .ant-table {
    font-weight: 400;
    .ant-table-header {
      background: #252526;
      overflow: hidden !important;
      margin-bottom: 0 !important;
    }
    .ant-table-fixed-left,
    .ant-table-fixed-right,
    .ant-table-fixed-left table,
    table {
      border-radius: 0;
    }
    .ant-table-thead {
      & > tr {
        & > th {
          background: #252526;
          color: #ffffff;
          vertical-align: middle;
          line-height: 16px;
          border-bottom-color: #282829;
          padding: 10px 10px 8px 10px;
          font-size: 14px;
          font-weight: 500;
          height: 70px;
          &:first-child {
            border-top-left-radius: 0;
          }
          &:last-child {
            border-top-right-radius: 0;
          }
          &:hover {
            background: #212121;
          }
          .head-title {
            // white-space: nowrap;
            .sub {
              color: rgba(255, 255, 255, 0.3);
              font-size: 12px;
              font-weight: 400;
            }
          }
        }
      }
    }
    .ant-table-tbody {
      & > tr {
        & > td {
          padding: 6px 10px;
          border-bottom-color: #282829;
          background: #19191a;
          vertical-align: middle;
          line-height: 18px;
          color: rgba(255, 255, 255, 0.7);
          .sub {
            white-space: nowrap;
            color: rgba(255, 255, 255, 0.3);
            font-size: 12px;
            font-weight: 400;
          }
          .ant-progress-line {
            width: 64px;
            line-height: 16px;
            .ant-progress-inner {
              background: #2f2f30;
              border-radius: 0;
              .ant-progress-bg {
                border-radius: 0;
                opacity: 0.7;
              }
            }
          }
          .progress-text {
            line-height: 16px;
            label {
              margin-left: 1px;
              opacity: 0.3;
              font-size: 12px;
            }
          }
          .btn {
            display: flex;
            flex: 0 0 32px;
            height: 32px;
            justify-content: center;
            align-items: center;
            border-radius: 4px;
            background: #252526;
            margin-right: 8px;
            color: #ffffff;
            &:hover {
              color: #00ccff;
            }
            .icon-close {
              font-size: 22px;
              text-align: center;
              opacity: 0.5;
            }
            .icon-edit {
              display: inline-block;
              width: 20px;
              height: 19px;
              opacity: 0.7;
              background: url(../../assets/images/icons/icon-edit.png) no-repeat;
            }
          }
          .btn-more {
            flex: 0 0 10px;
            height: 32px;
            .icon-more {
              display: block;
              position: relative;
              top: 12px;
              left: 3px;
              width: 7px;
              height: 7px;
              border: 1px solid #ffffff;
              border-radius: 50%;
              opacity: 0.3;
              &:before,
              &:after {
                display: block;
                content: " ";
                position: absolute;
                left: -1px;
                width: 7px;
                height: 7px;
                border: 1px solid #ffffff;
                border-radius: 50%;
              }
              &:before {
                bottom: 8px;
              }
              &:after {
                top: 8px;
              }
            }
          }
          a {
            color: rgba(255, 255, 255, 0.7);
            .sub {
              transition: color 0.2s;
            }
            &:hover {
              color: rgb(76, 140, 184);
              .sub {
                color: rgb(76, 140, 184);
              }
            }
          }
        }
        &:last-child {
          & > td {
            border-bottom: none;
          }
        }
        &.ant-table-row-hover,
        &:hover {
          &:not(.ant-table-expanded-row):not(.ant-table-row-selected) {
            & > td {
              background: #000000;
              .icon-more {
                opacity: 1;
              }
            }
          }
        }
      }
    }
  }
  .ant-table-placeholder {
    background: none;
    border: none;
    opacity: 0.2;
    .ant-empty-normal {
      color: #ffffff;
    }
  }
  .ant-table-fixed-header {
    & > .ant-table-content {
      & > .ant-table-scroll {
        & > .ant-table-body {
          background: none;
        }
      }
    }
  }
`;

export default StyledTable;
