import React from "react";
import { render } from "utils/testUtils";
import { Table } from "./Table";

describe("Table", () => {
  it("Should match snapshot", () => {
    const { container } = render(<Table />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
