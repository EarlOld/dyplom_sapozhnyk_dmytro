import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import TVWidget from "./TVWidget";

describe("TVWidget", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<TVWidget visible />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should not present in the DOM when unvisible", () => {
    const { container } = renderWithMockRedux(<TVWidget visible={false} />);
    expect(container.firstChild).not.toBeInTheDocument();
  });
});
