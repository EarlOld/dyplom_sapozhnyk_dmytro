import React, { Component } from "react";
import { connect } from "react-redux";
import YouTube from "react-youtube";
import { Tabs } from "antd-mobile";
import { Switch } from "antd";
import DraggableModal from "components/DraggableModal";
import { YouTubePlayer } from "youtube-player/dist/types";
import {
  FormattedMessage,
  injectIntl,
  WrappedComponentProps
} from "react-intl";
import _flowRight from "lodash/flowRight";

import { setTVVideosList } from "store/actions";
import PlaylistItem from "./PlaylistItem";

import { loadVideosList, selectVideos } from "./utils";
import "./style/index.scss";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;
interface IOwnProps {
  visible?: boolean;
  onClose: () => void;
}

type TProps = TStateProps & TDispatchProps & IOwnProps & WrappedComponentProps;

type TTabKey = "all" | "education" | "media";

type TActiveVideosIndexesByTabs = {
  [key in TTabKey]: number;
};

interface IState {
  isAutoplay: boolean;
  activeTab: TTabKey;
  activeVideosIndexesByTabs: TActiveVideosIndexesByTabs;
  playlist: ITVVideo[];
  isPlaylistCollapsed: boolean;
}

interface IVideoTab {
  key: TTabKey;
  title: string;
}

let isInit = false;

export class TVWidget extends Component<TProps, IState> {
  private player?: YouTubePlayer;

  get tabs(): IVideoTab[] {
    const { intl } = this.props;
    return [
      { title: intl.formatMessage({ defaultMessage: "All" }), key: "all" },
      {
        title: intl.formatMessage({ defaultMessage: "Education" }),
        key: "education"
      },
      { title: intl.formatMessage({ defaultMessage: "Media" }), key: "media" }
    ];
  }

  constructor(props: TProps) {
    super(props);

    const activeVideosIndexesByTabs = this.tabs.reduce(
      (indexes, tab) => {
        indexes[tab.key] = 0;
        return indexes;
      },
      {} as {
        [key in TTabKey]?: number;
      }
    ) as TActiveVideosIndexesByTabs;

    this.state = {
      isAutoplay: true,
      activeTab: "education",
      activeVideosIndexesByTabs,
      playlist: [],
      isPlaylistCollapsed: false
    };

    this.togglePlaylist = this.togglePlaylist.bind(this);
    this.onWindowClose = this.onWindowClose.bind(this);
    this.onPlayerReady = this.onPlayerReady.bind(this);
    this.onVideoEnd = this.onVideoEnd.bind(this);
    this.upNextVideo = this.upNextVideo.bind(this);
    this.onPlaylistItemClick = this.onPlaylistItemClick.bind(this);
  }

  componentDidUpdate(prevProps: TProps, prevState: IState) {
    const { props } = this;

    if (!prevProps.visible && props.visible) {
      if (!isInit) {
        isInit = true;
        loadVideosList(result => {
          props.setTVVideosList(result);
        });
      } else if (
        this.getActiveVideo() &&
        this.state.isAutoplay &&
        this.player
      ) {
        this.player.playVideo();
      }
    }

    if (!prevProps.videosList.length && props.videosList.length) {
      this.setState({
        playlist: selectVideos(props.videosList, this.state.activeTab)
      });
    }
  }

  onChangeTabs = (tab: { key?: string }) => {
    const key = tab.key as TTabKey;

    this.setState({
      activeTab: key,
      playlist: selectVideos(this.props.videosList, key)
    });
  };

  onPlaylistItemClick = (itemIndex: number) => {
    const { activeTab, activeVideosIndexesByTabs } = this.state;
    this.setState({
      activeVideosIndexesByTabs: {
        ...activeVideosIndexesByTabs,
        [activeTab]: itemIndex
      }
    });
  };

  upNextVideo() {
    const { activeVideosIndexesByTabs, playlist, activeTab } = this.state;
    const activeVideoIndex = activeVideosIndexesByTabs[activeTab] as number;

    if (activeVideoIndex < playlist.length - 1) {
      this.setState({
        activeVideosIndexesByTabs: {
          ...activeVideosIndexesByTabs,
          [activeTab]: activeVideoIndex + 1
        }
      });
    }
  }

  toggleAutoplay(isOn: boolean) {
    this.setState({
      isAutoplay: isOn
    });
  }

  togglePlaylist() {
    this.setState({
      isPlaylistCollapsed: !this.state.isPlaylistCollapsed
    });
  }

  getActiveVideo() {
    const { state } = this;
    const activeVideoIndex = state.activeVideosIndexesByTabs[state.activeTab];

    return state.playlist[activeVideoIndex];
  }

  onPlayerReady(event: { target: YouTubePlayer }) {
    this.player = event.target;
  }

  onVideoEnd() {
    if (this.state.isAutoplay) {
      this.upNextVideo();
    }
  }

  onWindowClose() {
    const { player } = this;

    if (player) {
      player.pauseVideo();
    }

    this.props.onClose();
  }

  render() {
    const { visible, intl } = this.props;
    const { activeTab, playlist, isPlaylistCollapsed, isAutoplay } = this.state;
    const activeVideo = this.getActiveVideo();

    const playerOptions = {
      width: "294",
      height: "165",
      playerVars: {
        autoplay: (isAutoplay ? 1 : 0) as 0 | 1
      }
    };

    const footer = (
      <div
        className={`footer ${isPlaylistCollapsed ? "collapsed" : ""}`}
        onClick={this.togglePlaylist}
      />
    );

    return (
      <DraggableModal
        visible={visible}
        width={294}
        title={intl.formatMessage({ defaultMessage: "CoinFLEX TV" })}
        onOk={() => {}}
        onCancel={this.onWindowClose}
        wrapClassName="tv-widget-wrap"
        className="tv-widget"
        mask={false}
        maskClosable={false}
        footer={footer}
      >
        <div className="tv-widget-content">
          <Tabs
            tabs={this.tabs}
            tabBarBackgroundColor="none"
            initialPage={activeTab}
            onChange={this.onChangeTabs}
          />
          <div className="player-wrap">
            {activeVideo && (
              <YouTube
                videoId={activeVideo.id}
                opts={playerOptions}
                onReady={this.onPlayerReady}
                onEnd={this.onVideoEnd}
              />
            )}
          </div>
          {activeVideo && (
            <div className="video-title">{activeVideo.title}</div>
          )}
          <div className={`playlist ${isPlaylistCollapsed ? "collapsed" : ""}`}>
            <div className="controls">
              <span onClick={this.upNextVideo}>Up next</span>
              <span className="autoplay">
                <FormattedMessage defaultMessage="AUTOPLAY" />
                <Switch
                  size="small"
                  checked={isAutoplay}
                  onChange={this.toggleAutoplay.bind(this)}
                />
              </span>
            </div>
            {playlist.map((video, i) => (
              <PlaylistItem
                key={video.id}
                data={video}
                onClick={() => this.onPlaylistItemClick(i)}
              />
            ))}
          </div>
        </div>
      </DraggableModal>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    videosList: state.tvVideosList
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setTVVideosList(params: ILoadVideosListResult) {
      dispatch(setTVVideosList(params));
    }
  };
};

export default _flowRight(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(TVWidget);
