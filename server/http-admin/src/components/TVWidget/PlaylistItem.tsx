import React from "react";
import moment from "moment";

import "./style/index.scss";

interface IOwnProps {
  data: ITVVideo;
  onClick: (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

const PlaylistItem = ({ data, onClick }: IOwnProps) => {
  return (
    <div onClick={onClick} className="playlist-item">
      <div>
        <div className="title">{data.title}</div>
        <div className="viewsCount">{data.viewCount} views</div>
      </div>
      <div className="thumbnail">
        <img src={data.thumbnail} alt={"alt"} />
        <span className="duration">
          {moment
            .utc(moment.duration(data.duration).as("milliseconds"))
            .format("mm:ss")}
        </span>
      </div>
    </div>
  );
};

export default PlaylistItem;
