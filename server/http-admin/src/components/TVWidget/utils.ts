import {
  getYoutubeChannelVideos,
  getYoutubePlaylistVideos,
  getYoutubeVideos
} from "services/http/http";

import {
  YOUTUBE_CHANNEL_ID,
  YOUTUBE_MEDIA_PLAYLIST_ID,
  YOUTUBE_DEVELOPER_KEY
} from "config";

export const loadVideosList = (
  onResult: (result: ILoadVideosListResult) => void
) => {
  const videosIds: string[] = [];

  Promise.all([
    getYoutubeChannelVideos({
      channelId: YOUTUBE_CHANNEL_ID,
      part: "snippet,id",
      key: YOUTUBE_DEVELOPER_KEY,
      maxResults: 50,
      order: "date"
    }),
    getYoutubePlaylistVideos({
      playlistId: YOUTUBE_MEDIA_PLAYLIST_ID,
      part: "snippet,id",
      key: YOUTUBE_DEVELOPER_KEY,
      maxResults: 50,
      order: "date"
    })
  ]).then(
    (
      responses: [TYoutubeChannelItemsResponse, TYoutubePlaylistItemsResponse]
    ) => {
      const educationResponse = (responses[0].items || []).filter(
        item => item.id && item.id.kind === "youtube#video"
      );
      const mediaResponse = (responses[1].items || []).filter(
        item => item.id && item.kind === "youtube#playlistItem"
      );

      educationResponse.forEach(item => videosIds.push(item.id.videoId));
      mediaResponse.forEach(item =>
        videosIds.push(item.snippet.resourceId.videoId)
      );

      getYoutubeVideos({
        id: videosIds.join(","),
        part: "statistics,contentDetails",
        key: YOUTUBE_DEVELOPER_KEY,
        maxResults: 50
      }).then((response: TYoutubeVideosResponse) => {
        onResult({
          educationItems: educationResponse,
          mediaItems: mediaResponse,
          itemsInfo: response
        });
      });
    }
  );
};

export const selectVideos = (playlist: ITVVideo[], groupId: string) => {
  return groupId === "all"
    ? playlist
    : playlist.filter(video => video.group === groupId);
};
