import React from "react";
import { action } from "@storybook/addon-actions";
import { wrapWithMockRedux } from "utils/testUtils";
import TVWidget from "./TVWidget";

export default {
  title: "TVWidget",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <TVWidget visible onClose={action("Close!")} />;

Default.story = {
  name: "default"
};
