import React from "react";
import * as SWRTC from "@andyet/simplewebrtc";
import { Provider } from "react-redux";
import Pit from "./Pit";

const store = SWRTC.createStore();

export default {
  title: "Pit"
};

export const Default = () => (
  <Provider store={store}>
    <Pit off={false} />
  </Provider>
);

Default.story = {
  name: "default"
};
