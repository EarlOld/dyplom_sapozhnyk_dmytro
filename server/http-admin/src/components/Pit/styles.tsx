import styled, { css } from "styled-components";

type TOnOrOff = "on" | "off";
interface IPitButton {
  isOnOrOff: TOnOrOff;
}

export const StyledContainer = styled.div`
  height: 100%;
  padding: 0;
  margin: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #212528;
`;

export const StyledPitContentWrapper = styled.div`
  height: 200px;
  display: flex;
  flex-direction: column;
`;

export const StyledTitle = styled.div`
  height: auto;
  text-align: center;
  margin-bottom: 50px;

  h1 {
    color: white;
  }
`;

export const StyledButtonContainer = styled.div`
  flex: 1;
  display: flex;
`;

const BothButtonCSS = css<IPitButton>`
  height: 200px;
  width: 200px;
  border-radius: 50%;
  font-weight: 500;
  outline: 0;
  user-select: none;
  text-decoration: none;
  text-align: center;
  cursor: pointer;
  color: #7048bb;
  border-color: #7048bb;
  font-size: 100px;
  background: transparent;
  &:hover {
    opacity: 0.8;
  }
  ${props =>
    props.isOnOrOff === "off" &&
    css`
      background: #ec4068;
      color: white;
      border: 0;
    `};
  ${props =>
    props.isOnOrOff === "on" &&
    css`
      box-shadow: 0 2.8px 2.2px rgba(0, 0, 0, 0.034),
        0 6.7px 5.3px rgba(0, 0, 0, 0.048), 0 12.5px 10px rgba(0, 0, 0, 0.06),
        0 22.3px 17.9px rgba(0, 0, 0, 0.072),
        0 41.8px 33.4px rgba(0, 0, 0, 0.086), 0 100px 80px rgba(0, 0, 0, 0.12);
    `};
  > div {
    margin-top: 10px;
    font-size: 12px;
  }
`;

export const StyledDeafButton = styled.button<IPitButton>`
  ${BothButtonCSS}
`;

export const StyledMuteButton = styled.button<IPitButton>`
  ${BothButtonCSS}
`;

export const StyledDivider = styled.div`
  display: inline;
  width: 1px;
  height: 200px;
  margin: 0 50px;
  border-right: #ec4068;
`;

export const StyledIcon = styled.div`
  margin-top: 25px;
  font-size: 100px;
  display: flex;
  flex: 1;
`;
