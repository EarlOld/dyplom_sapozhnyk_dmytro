import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import { Pit } from "./Pit";

describe("Pit widget", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(
      <Pit match={{ params: { chatId: 2 } }} />
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});
