import React, { memo, useState } from "react";
import * as SWRTC from "@andyet/simplewebrtc";
import {
  StyledContainer,
  StyledTitle,
  StyledButtonContainer,
  StyledPitContentWrapper,
  StyledDeafButton,
  StyledMuteButton,
  StyledDivider,
  StyledIcon
} from "./styles";
import { UserControlsProps } from "@andyet/simplewebrtc/components/UserControls";
import { Icon } from "antd";
import { withRouter, RouteComponentProps } from "react-router-dom";
import _flowRight from "lodash/flowRight";
import {
  injectIntl,
  FormattedMessage,
  WrappedComponentProps
} from "react-intl";
import { CHAT_KEY, CHAT_PASSWORD, SIMPLEWEBRTC_API_KEY } from "config";
import { ReactComponent as Sound } from "assets/icons/sound.svg";
import { ReactComponent as SoundOff } from "assets/icons/soundOff.svg";
import { ReactComponent as Mic } from "assets/icons/mic.svg";
import { ReactComponent as MicOff } from "assets/icons/micOff.svg";
import { ReactComponent as Spinner } from "assets/icons/spinner.svg";
import { Provider as ReduxProvider } from "react-redux";

const store = SWRTC.createStore();

const CONFIG_URL = `https://api.simplewebrtc.com/config/guest/${SIMPLEWEBRTC_API_KEY}`;

type TProps = IPitSettings &
  WrappedComponentProps &
  RouteComponentProps<RouteInfo>;

interface RouteInfo {
  chatId?: string;
}

export const Pit = ({
  match: {
    params: { chatId }
  }
}: TProps) => {
  const [on, setOn] = useState<boolean>(false);

  return (
    <ReduxProvider store={store}>
      <StyledContainer>
        <StyledPitContentWrapper>
          <StyledTitle>
            <h1>
              <FormattedMessage defaultMessage={"The Pit"} />
            </h1>
          </StyledTitle>
          <StyledButtonContainer>
            {!on ? (
              <>
                <StyledDeafButton
                  onClick={() => setOn(!on)}
                  isOnOrOff={!on ? "off" : "on"}
                >
                  {!on ? (
                    <Icon component={SoundOff} />
                  ) : (
                    <Icon component={Sound} />
                  )}
                </StyledDeafButton>
                <StyledDivider />
                <StyledDeafButton
                  onClick={() => setOn(!on)}
                  isOnOrOff={!on ? "off" : "on"}
                >
                  {!on ? <Icon component={MicOff} /> : <Icon component={Mic} />}
                </StyledDeafButton>
              </>
            ) : (
              <SWRTC.Provider configUrl={CONFIG_URL}>
                <SWRTC.Connecting>
                  <StyledIcon>
                    <Icon component={Spinner} />
                  </StyledIcon>
                </SWRTC.Connecting>
                <SWRTC.Connected>
                  <SWRTC.Room
                    name={CHAT_KEY! + (chatId || "")}
                    password={CHAT_PASSWORD}
                  >
                    {() => {
                      return (
                        <>
                          <SWRTC.RequestUserMedia audio auto />
                          <SWRTC.RemoteAudioPlayer />
                          <SWRTC.UserControls>
                            {({
                              isMuted,
                              unmute,
                              mute,
                              deafen,
                              undeafen,
                              isDeafened
                            }: UserControlsProps) => {
                              return (
                                <>
                                  <StyledDeafButton
                                    onClick={isDeafened ? undeafen! : deafen!}
                                    isOnOrOff={isDeafened ? "off" : "on"}
                                  >
                                    {isDeafened ? (
                                      <Icon component={SoundOff} />
                                    ) : (
                                      <Icon component={Sound} />
                                    )}
                                  </StyledDeafButton>
                                  <StyledDivider />
                                  <StyledMuteButton
                                    onClick={isMuted ? unmute! : mute!}
                                    isOnOrOff={isMuted ? "off" : "on"}
                                  >
                                    {isMuted ? (
                                      <Icon component={MicOff} />
                                    ) : (
                                      <Icon component={Mic} />
                                    )}
                                  </StyledMuteButton>
                                </>
                              );
                            }}
                          </SWRTC.UserControls>
                        </>
                      );
                    }}
                  </SWRTC.Room>
                </SWRTC.Connected>
              </SWRTC.Provider>
            )}
          </StyledButtonContainer>
        </StyledPitContentWrapper>
      </StyledContainer>
    </ReduxProvider>
  );
};

export default _flowRight(withRouter, injectIntl, memo)(Pit);
