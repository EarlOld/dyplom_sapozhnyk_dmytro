import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

type TStateProps = ReturnType<typeof mapStateToProps> & {
  children: React.ReactElement;
};

const AuthGuardProvider = (props: TStateProps) => {
  const { children, authToken } = props;

  const validAuthToken =
    !!authToken && !!authToken.token && !!authToken.expiration_date;

  return validAuthToken ? children : <Redirect to={"/login"} />;
};

const mapStateToProps = (state: IAppState) => {
  return {
    authToken: state.authToken
  };
};

export default connect(mapStateToProps)(AuthGuardProvider);
