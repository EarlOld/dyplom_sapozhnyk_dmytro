import React, { useState, useEffect, useCallback } from "react";
import { decimals } from "utils/reference";
import BScroll from "better-scroll";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import "./style/index.scss";
import TradeList from "./TradeList";
import _throttle from "lodash/throttle";
type TIRecentTradesProps = ReturnType<typeof mapStateToProps>;

const data = {
  baseId: 0,
  decimal: 4,
  quoteId: 0,
  lastTime: 0,
  dataTrades: []
};

let { baseId, decimal, quoteId, lastTime, dataTrades = [] as ITrade[] } = data;

export const RecentTrades = React.memo((props: TIRecentTradesProps) => {
  const [baseName, setBaseName] = useState<string>("");
  const [quoteName, setQuoteName] = useState<string>("");
  const [maxAmount, setMaxAmount] = useState<number>(1);
  const [tradeList, setTradeList] = useState<ITrade[]>([]);
  const wrapper = document.querySelector("#recentScroll") as Element;
  const getMaxAmount = (list: ITrade[]) => {
    let max = 0;
    list.forEach(item => {
      if (item.amount! > max) {
        max = item.amount!;
      }
    });
    setMaxAmount(max);
  };
  const updateList = useCallback(
    _throttle((newList: any) => {
      setTradeList(newList);
    }, 250),
    [setTradeList]
  );

  const updateHistory = useCallback(
    (data: IOrdersMatched) => {
      const { currentSymbol } = props;
      if (data.time <= lastTime) {
        return;
      }
      lastTime = data.time;
      const list: ITrade[] = dataTrades;
      list.unshift({
        price: data.price / currentSymbol.quote.scale,
        amount: data.quantity / currentSymbol.base.scale,
        side: data.bid_rem === 0 ? 1 : 2,
        time: parseInt("" + data.time / 1000),
        base: data.base,
        counter: data.counter,
        quantity: data.quantity
      });
      dataTrades = list.slice(0, 30);
      updateList(list.slice(0, 30));
    },

    [props, updateList]
  );

  const checkTabTitle = useCallback(() => {
    const { currentSymbol } = props;
    setBaseName(
      currentSymbol.base.spot_name
        ? currentSymbol.base.spot_name
        : currentSymbol.base.name
    );
    setQuoteName(
      currentSymbol.quote.spot_name
        ? currentSymbol.quote.spot_name
        : currentSymbol.quote.name
    );
    decimal = decimals[currentSymbol.base.spot_name!]
      ? decimals[currentSymbol.base.spot_name!].price
      : decimal;
    baseId = currentSymbol.base.id;
    quoteId = currentSymbol.quote.id;
  }, [props]);
  useEffect(() => {
    if (wrapper) {
      new BScroll(wrapper, {
        click: true, // better-scroll 默认会阻止浏览器的原生 click 事件
        mouseWheel: true
      });
    }
  }, [wrapper]);
  useEffect(() => {
    checkTabTitle();
    const { lastTrade, currentSymbol } = props;
    if (
      currentSymbol.symbolTitle &&
      lastTrade[currentSymbol.base.id] &&
      lastTrade[currentSymbol.base.id].price
    ) {
      if (
        currentSymbol.base.id !== baseId ||
        currentSymbol.quote.id !== quoteId
      ) {
      } else {
        //TODO: typesript: to clarify the lastTrade state field type
        updateHistory(
          (lastTrade[currentSymbol.base.id] as unknown) as IOrdersMatched
        );
      }
    } else {
      setTradeList([]);
    }
  }, [checkTabTitle, props, updateHistory]);
  useEffect(() => {
    getMaxAmount(tradeList);
  }, [tradeList]);
  return (
    <div className="recent-trade">
      <div className="header">
        <FormattedMessage defaultMessage="Recent Trades" />
      </div>
      <ul className="flex list-head">
        <li>
          <FormattedMessage defaultMessage="Price" /> <span>{quoteName}</span>
        </li>
        <li>
          <FormattedMessage defaultMessage="Amount" /> <span>{baseName}</span>
        </li>
        <li>
          <FormattedMessage defaultMessage="Time" />
        </li>
      </ul>
      <div id="recentScroll" className="trade-list">
        <TradeList
          tradeList={tradeList}
          decimal={decimal}
          maxAmount={maxAmount}
        />
      </div>
    </div>
  );
});
const mapStateToProps = (state: IPublicState) => {
  return {
    lastTrade: state.lastTrade,
    currentSymbol: state.currentSymbol
  };
};

export default connect(mapStateToProps)(RecentTrades);
