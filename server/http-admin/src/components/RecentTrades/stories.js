import React from "react";
import { wrapWithMockRedux } from "utils/testUtils";
import RecentTrades from "./RecentTrades";

export default {
  title: "RecentTrades",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => <RecentTrades />;

Default.story = {
  name: "default"
};
