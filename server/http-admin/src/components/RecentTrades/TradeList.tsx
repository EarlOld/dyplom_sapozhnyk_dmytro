import React, { memo } from "react";
import moment from "moment";
import Calc from "utils/math.calc";
interface IRecentTradeListProps {
  tradeList: ITrade[];
  decimal: number;
  maxAmount: number;
}
const TradeList = memo((props: IRecentTradeListProps) => {
  const { tradeList, decimal, maxAmount } = props;
  return (
    <ul>
      {tradeList.map((item, index) => (
        <li className="list-item flex" key={index}>
          <span className={item.side === 1 ? "price bid" : "price ask"}>
            {item.price.toFixed(decimal)}
          </span>
          <span className="amount">{Calc.toThousands(item.amount)}</span>
          <span className="time">{moment(item.time).format("HH:mm:ss")}</span>
          <label
            className={item.side === 1 ? "depth bid" : "depth ask"}
            style={{ width: (item.amount! * 100) / maxAmount + "%" }}
          />
        </li>
      ))}
    </ul>
  );
});
export default TradeList;
