import React from "react";
import { renderWithMockRedux } from "utils/testUtils";
import RecentTrades from "./RecentTrades";

describe("RecentTrades", () => {
  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<RecentTrades />);
    expect(container.firstChild).toMatchSnapshot();
  });
});
