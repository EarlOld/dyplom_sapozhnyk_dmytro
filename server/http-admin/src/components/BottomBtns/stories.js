import React from "react";
import { action } from "@storybook/addon-actions";
import { wrapWithMockRedux } from "utils/testUtils";
import BottomBtns from "./BottomBtns";

export default {
  title: "BottomBtns",
  decorators: [wrapWithMockRedux()]
};

export const Default = () => (
  <BottomBtns jumpToTrade={action("Jumping to trade!")} />
);

Default.story = {
  name: "default"
};
