import React, { Component } from "react";
import { Button } from "antd";
import { connect } from "react-redux";
import Calc from "utils/math.calc";
import { decimals } from "utils/reference";
import { setBidPrice, setAskPrice, setTopTabs } from "store/actions";
import "./style/index.scss";
import { FormattedMessage } from "react-intl";

type TStateProps = ReturnType<typeof mapStateToProps>;
type TDispatchProps = ReturnType<typeof mapDispatchToProps>;

interface IOwnProps {
  jumpToTrade: (e: TDirection) => void;
}

type TProps = TStateProps & TDispatchProps & IOwnProps;

export class BottomBtns extends Component<TProps> {
  private buyPrice = 0;
  private sellPrice = 0;

  onChangeDirection = (e: TDirection) => {
    this.props.jumpToTrade(e);
  };
  updateLastPrice = (e: ILastTradeData) => {
    const { currentSymbol } = this.props;
    let lastPrice = 0;
    if (e.last)
      lastPrice =
        e.last / (currentSymbol.quote ? currentSymbol.quote.scale : 10000);
    if (e.price)
      lastPrice =
        e.price / (currentSymbol.quote ? currentSymbol.quote.scale : 10000);
    this.buyPrice = lastPrice;
    this.sellPrice = lastPrice;
  };
  handleAskData = (data: TDepthData) => {
    const { isMobile } = this.props;
    if (!isMobile) return;
    data.sort((a, b) => a[0] - b[0]);
    data.length > 0 && this.props.setAskPrice(+(data[0][0] / 10000).toFixed(4));
  };
  handleBidData = (data: TDepthData) => {
    const { isMobile } = this.props;
    if (!isMobile) return;
    data.sort((a, b) => b[0] - a[0]);
    data.length > 0 && this.props.setBidPrice(+(data[0][0] / 10000).toFixed(4));
  };
  componentDidUpdate(prevProps: TProps) {
    const { askData, bidData } = this.props;
    if (askData && askData !== prevProps.askData) {
      this.handleAskData(askData);
    }
    if (bidData && bidData !== prevProps.bidData) {
      this.handleBidData(bidData);
    }
  }
  render() {
    const { askPrice, bidPrice, currentSymbol } = this.props;
    let decimalPrice = 0;
    if (currentSymbol && currentSymbol.base && currentSymbol.base.spot_name) {
      decimalPrice = decimals[currentSymbol.base.spot_name]
        ? decimals[currentSymbol.base.spot_name].price
        : decimalPrice;
    }
    return (
      <div className="bottom-btns">
        <Button.Group className="trade-type-radio">
          <Button onClick={() => this.onChangeDirection("1")}>
            <span className="type">
              <FormattedMessage defaultMessage="BID" />
            </span>
            <span className="price">
              {Calc.toFixeds(bidPrice, decimalPrice)}
            </span>
          </Button>
          <Button onClick={() => this.onChangeDirection("2")}>
            <span className="price">
              {Calc.toFixeds(askPrice, decimalPrice)}
            </span>
            <span className="type">
              <FormattedMessage defaultMessage="ASK" />
            </span>
          </Button>
        </Button.Group>
      </div>
    );
  }
}

const mapStateToProps = (state: IPublicState) => {
  return {
    lastTrade: state.lastTrade,
    currentSymbol: state.currentSymbol,
    bidPrice: state.bidPrice,
    askPrice: state.askPrice,
    isMobile: state.isMobile,
    askData: state.askData,
    bidData: state.bidData
  };
};

const mapDispatchToProps = (dispatch: TDispatch) => {
  return {
    setBidPrice(data: number) {
      dispatch(setBidPrice(data));
    },
    setAskPrice(data: number) {
      dispatch(setAskPrice(data));
    },
    setTopTabs(data: TTopTabType) {
      dispatch(setTopTabs(data));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BottomBtns);
