import React from "react";
import { fireEvent } from "@testing-library/react";
import { renderWithMockRedux } from "utils/testUtils";
import BottomBtns from "./BottomBtns";

describe("Bottom buttons", () => {
  const handleJumpToTrade = jest.fn();

  const TestBottomBtns = () => <BottomBtns jumpToTrade={handleJumpToTrade} />;

  it("Should match snapshot", () => {
    const { container } = renderWithMockRedux(<TestBottomBtns />);
    expect(container.firstChild).toMatchSnapshot();
  });

  it("Should fire jumping to trade when any Button is clicked ", () => {
    const { getByText } = renderWithMockRedux(<TestBottomBtns />);

    const buyButton = getByText("BID");
    expect(buyButton).toBeInTheDocument();
    fireEvent.click(buyButton);
    expect(handleJumpToTrade).toHaveBeenCalledWith("1");

    const sellButton = getByText("ASK");
    expect(sellButton).toBeInTheDocument();
    fireEvent.click(sellButton);
    expect(handleJumpToTrade).toHaveBeenCalledWith("2");
  });
});
