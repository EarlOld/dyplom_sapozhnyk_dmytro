import React from "react";
import { Provider } from "react-redux";
import store from "./store";
import { CookiesProvider } from "react-cookie";
import Theme from "style/index";
import MainRouter from "./routers/MainRouter";
import ConnectedIntlProvider from "components/ConnectedIntlProvider";

const App: React.FC = () => {
  return (
    <div className="App">
      <Provider store={store}>
        <Theme />
        <CookiesProvider>
          <Provider store={store}>
            <Theme />
            <ConnectedIntlProvider>
              <MainRouter />
            </ConnectedIntlProvider>
          </Provider>
        </CookiesProvider>
      </Provider>
    </div>
  );
};

export default App;
