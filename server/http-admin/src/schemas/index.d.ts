declare namespace NodeJS {
  export interface ProcessEnv {
    YOUTUBE_CHANNEL_ID: string;
    YOUTUBE_MEDIA_PLAYLIST_ID: string;
    YOUTUBE_DEVELOPER_KEY: string;
  }
}

declare module "react-clipboard.js";
declare module "react-lazy-load";

interface IAsset {
  id: number;
  name: string;
  spot_id?: number;
  spot_name?: string;
  scale: number;
}

interface IMarket {
  base: number;
  counter: number;
  name: string;
  spot_name: string;
  start: number;
  expires: number;
  tenor: string;
  tick: number;
}

interface ICurrentSymbol {
  base: IAsset;
  quote: IAsset;
  name?: string;
  scale?: number;
  spot_id?: number;
  spot_name?: string;
  min: number;
  symbolTitle: string;
}

interface IPosition {
  asset: number;
  position: number;
  through?: number;
  asset_id?: number;
}

interface IPositionListItem {
  base: number;
  counter: number;
  title: string;
  name: string;
  priceAgo: number;
  decimal: number;
  price: string;
  change: number;
}

interface IPositions {
  [id: number]: IPosition;
}

interface IBalance {
  id: number;
  available: number;
  reserved: number;
}

interface IFuturesBalance extends IBalance {
  hasData: boolean;
  name: string;
  price: number;
  monthName: string;
  year: number;
  expires: number | null;
}

interface IExpandedFuturesBalance extends IFuturesBalance {
  transferred: number;
  transferredInUSDT: number;
  collateralTotal: number;
  collateralAvailable: number;
  available: number;
  availableToWithdraw: number;
  borrowed: number;
  required: number;
}

interface ITicker {
  base: number;
  counter: number;
  name: string;
  spot_name: string;
  last?: number;
  bid?: number;
  ask?: number;
  low?: number;
  high?: number;
  volume: number;
  time: number;
}

interface ITickerChange {
  base: number;
  counter: number;
  last: number;
  bid: number;
  ask: number;
  low: number;
  high: number;
  volume: number;
  price: number;
  decimal: number;
}

interface IOrder {
  id: number;
  base: number;
  counter: number;
  quantity: number;
  price: number;
  time: number;
}

interface IOrdersMatched {
  base: number;
  counter: number;
  bid: number;
  bid_tonce: number;
  ask: number;
  ask_tonce: number;
  quantity: number;
  price: number;
  total: number;
  bid_rem: number;
  ask_rem: number;
  time: number;
  bid_base_fee: number;
  bid_counter_fee: number;
  ask_base_fee: number;
  ask_counter_fee: number;
  notice?: string;
}

interface ITrade {
  time: number;
  base: number;
  counter: number;
  quantity: number;
  price: number;
  total?: number;
  base_fee?: number;
  counter_fee?: number;
  order_id?: number | null;
  side?: number;
  amount?: number;
}

interface IOrderBook {
  id: number;
  price: number;
  quantity: number;
  time: number;
}

interface IBorrowOffer {
  id: number;
  created: number;
  asset_id: number;
  amount: number;
  min_amount: number;
  max_amount: number;
  term: number;
  initial_margin: number;
  maintenance_margin: number;
  apr: number;
  asset_name: string;
  max_or_available: number;
}

interface IBorrowLoan {
  id: number;
  asset_id: number;
  principal: number;
}

interface IBorrowCollateral {
  asset_id: number;
  total: number;
  available: number;
}

interface IPlaceOrderData {
  base?: number;
  method?: string;
  counter?: number;
  total?: number;
  quantity?: number;
  amount?: number;
  price?: number;
  offer_id?: number;
}

interface ISocketResponse {
  error_code: number;
  error_msg?: string;
  tag: number;
}

interface ISocketResponse extends IRequestError {
  tag: number;
}

interface IOrderBookList extends ISocketResponse {
  orders: IOrderBook[];
  base?: number;
}

interface ILastTradeData extends ISocketResponse {
  base: number;
  last: number;
  bid: number;
  ask: number;
  low?: number;
  high?: number;
  price?: number;
  volume: number;
}

interface ILastTrade {
  [id: number]: ILastTradeData | IOrdersMatched;
}

type TDepthData = number[][];

interface IPlaceOrder extends ISocketResponse {
  method: string;
  tonce: number;
  base: number;
  counter: number;
  quantity: number;
  price: number;
  total: number;
  persist: boolean | "fill_or_kill";
}

interface IYoutubeBaseRequest {
  part: string;
  key: string;
  maxResults?: number;
  order?:
    | "date"
    | "rating"
    | "relevance"
    | "title"
    | "videoCount"
    | "viewCount";
}

interface IYoutubeChannelItemsRequest extends IYoutubeBaseRequest {
  channelId: string;
}

interface IYoutubePlaylistItemsRequest extends IYoutubeBaseRequest {
  playlistId: string;
}

interface IYoutubeVideosRequest extends IYoutubeBaseRequest {
  id: string;
}

interface IYoutubeSearchListResponse<T> {
  kind: string;
  etag: string;
  regionCode?: string;
  pageInfo: {
    totalResults: number;
    resultsPerPage: number;
  };
  id: {
    kind: string;
    videoId: string;
  };
  items: T[];
}

interface IYoutubeVideoThumbnail {
  url: string;
  width: number;
  height: number;
}

interface IYoutubeChannelResponseItem {
  kind: string;
  etag: string;
  id: {
    kind: string;
    videoId: string;
  };
  snippet: {
    publishedAt: string;
    channelId: string;
    title: string;
    description: string;
    thumbnails: {
      default: IYoutubeVideoThumbnail;
      medium: IYoutubeVideoThumbnail;
      high: IYoutubeVideoThumbnail;
    };
  };
  channelTitle: string;
  liveBroadcastContent: string;
}

interface IYoutubePlaylistResponseItem {
  kind: string;
  etag: string;
  id: string;
  snippet: {
    publishedAt: string;
    channelId: string;
    title: string;
    description: string;
    thumbnails: {
      default: IYoutubeVideoThumbnail;
      medium: IYoutubeVideoThumbnail;
      high: IYoutubeVideoThumbnail;
      standard: IYoutubeVideoThumbnail;
      maxres: IYoutubeVideoThumbnail;
    };
    channelTitle: string;
    playlistId: string;
    position: number;
    resourceId: {
      kind: string;
      videoId: string;
    };
  };
}

interface TYoutubeVideosResponseItem {
  kind: string;
  etag: string;
  id: string;
  contentDetails: {
    duration: string;
    dimension: string;
    definition: string;
    caption: string;
    licensedContent: boolean;
    projection: string;
  };
  statistics: {
    viewCount: string;
    likeCount: string;
    dislikeCount: string;
    favoriteCount: string;
    commentCount: string;
  };
}

type TYoutubeChannelItemsResponse = IYoutubeSearchListResponse<
  IYoutubeChannelResponseItem
>;
type TYoutubePlaylistItemsResponse = IYoutubeSearchListResponse<
  IYoutubePlaylistResponseItem
>;
type TYoutubeVideosResponse = IYoutubeSearchListResponse<
  TYoutubeVideosResponseItem
>;

interface ITVVideo {
  id: string;
  group: "education" | "media";
  title: "string";
  thumbnail: "string";
  duration: "string";
  viewCount: number;
}

interface ILoadVideosListResult {
  educationItems: IYoutubeChannelResponseItem[];
  mediaItems: IYoutubePlaylistResponseItem[];
  itemsInfo: TYoutubeVideosResponse;
}

interface IChartHistoryRequest {
  base: number;
  counter: number;
  interval: string;
  since: number;
  limit?: number;
}

interface IChartHistoryItem {
  close: number;
  high: number;
  low: number;
  open: number;
  time: number;
  volume: number;
  last?: number;
}

type TChartData = number[];

interface IChartsData {
  [id: number]: TChartData;
}

interface ICancelOrderResponse extends ISocketResponse {
  id: number;
  tonce: number;
  base: number;
  counter: number;
  quantity: number;
  price: number;
  time: number;
}

type TTopTabType = "CHART" | "TRADE" | "POSITIONS" | "ORDERS" | "RECENT";

type TPanelType =
  | "panel_trade"
  | "panel_trade_confirm"
  | "panel_trade_confirm_result"
  | "login";

type TDirection = "1" | "2";

type TAskBidSide = "ask" | "bid";
type TBuySellSide = "BUY" | "SELL";

type TSortDirection = "asc" | "desc";

type TTableColumnAlign = "left" | "right" | "center" | undefined;

type TTableColumnFixed = boolean | "left" | "right" | undefined;

interface ITab {
  title: string;
  key?: string;
}

interface ITradeData {
  base: number;
  quantity: number;
  quote: number;
  position: number;
  confirmAmount: number;
  entryPrice: number;
  side: TAskBidSide;
}

interface IPanelConfirm {
  panelType: TPanelType;
  tradeData?: ITradeData | {};
  tradeResultData?: Array; //TODO: typescript: not used?
}

interface II18n {
  locale: string;
  languages: string[];
  messages: { [key: string]: string };
}

interface ILoginRequest {
  "user[email]": string;
  "user[password]": string;
}

interface ILoginRequestV2 {
  email: string;
  password: string;
}

interface I2FARequest {
  twofactor_token: string;
}

interface I2FARequestV2 {
  tfa_code: string;
}

interface ILockedLeverageSlidersValues {
  [assetId: number]: number;
}

type TDispatch = any;

interface IPublicState {
  bucketList: {
    assetPair: { counter: number; base: number };
    ohlcList: IChartHistoryItem[];
  }[];
  topTabsType: TTopTabType;
  isMobile: boolean;
  currentSymbol: ICurrentSymbol;
  quoteInfo: Object; //TODO: typescript: not used?
  changeList: ITickerChange[];
  marketList: IMarket[];
  assetsList: IAsset[];
  tickersList: ITicker[];
  tvVideosList: ITVVideo[];
  orderbook_orders: Object; //TODO: typescript: not used?
  orderbook_bids: Object; //TODO: typescript: not used?
  orderbook_asks: Object; //TODO: typescript: not used?
  orderBookList: IOrderBookList;
  lastTrade: ILastTrade;
  lastTicker: ITickerChange; //TODO: typescript: not used?
  positionList: IPositionListItem[];
  bidPrice: number;
  askPrice: number;
  isLogin: boolean;
  recentTrade: Object; //TODO: typescript: not used?
  panelConfirm: IPanelConfirm;
  i18n: II18n;
  serverNonce: string | null;
  historyTrade: ITrade[];
  lastPosition: IOrdersMatched;
  askData: TDepthData;
  bidData: TDepthData;
  selectedMarket: IPositionListItem | IMarket;
  orderConfirmResult: IOrdersMatched;
  websocketAuthParams: IUserAuthentificateParams;
  competitions: ICompetition[];
  notifications: INotification[];
  amountFromOrderBook: number;
}

interface IConvertedTotal {
  asset_from: number;
  asset_to: number;
  total: number;
  total_from: number;
  total_to: number;
}

interface IConvertData {
  asset_from: number;
  asset_to: number;
  amount: number;
}

interface IDoBorrowData {
  offer_id: number;
  amount: number;
}

interface IConvertAndDoBorrowData extends IConvertData {
  borrow_offer_id?: number;
  borrow_amount?: number;
}

interface IConvertAndDoBorrowTwiceData extends IConvertAndDoBorrowData {
  counter_borrow_offer_id?: number;
  counter_borrow_amount?: number;
}

enum EDepositWithdrawalsHistoryTransactionType {
  Deposit = 1,
  Withdrawal = 2
}

enum EDepositWithdrawalsHistoryTransactionState {
  New = 1,
  AcceptedByTradeEngine = 2,
  RejectedByTradeEngine = 3,
  RejectedByMOS = 4,
  Processed = 5,
  ProcessedByDepositsAndWithdrawalsProcessor = 6,
  ConfirmedInTheBlockchain = 7,
  LockedByDepositsAndWithdrawalsProcessor = 8
}

interface IDepositWithdrawalsHistoryItem {
  asset_iso: string;
  amount: string | number;
  transaction_type_id: EDepositWithdrawalsHistoryTransactionType;
  transaction_state_id: EDepositWithdrawalsHistoryTransactionState;
  txid: string;
  address: string | null;
  fee: string | number;
  created_at: string;
}

interface IRequestState {
  pending: boolean;
  error: boolean;
  success: boolean;
  errorCode?: number | string;
  errorMessage?: string;
}

type TBalanceConvertionState = IRequestState & IConvertData;

interface IBalancesConvertionsState {
  [fromBalanceId: number]: TBalanceConvertionState;
}

type TUploadWithdrawalAddress = IRequestState & IWithdrawalAddressData;
type TAddressState = IRequestState & { name: string };

type TBalanceWithdraw = IRequestState & IWithdrawalData;

interface IUserState {
  allBalances: IBalance[];
  borrowOffers: IBorrowOffer[];
  borrowLoans: IBorrowLoan[];
  borrowConversions: IBorrowConversion[];
  positions: IPositions;
  orderList: IOrder[] | null;
  currentBalance: number;
  borrowCollaterals: IBorrowCollateral[];
  lockedLeverageSlidersValues: ILockedLeverageSlidersValues;
  user: IUser;
  borrowConvertedTotals: IConvertedTotal[];
  balancesConversionsState: IBalancesConvertionsState;
  uploadBalanceWithdrawalAddress: TUploadWithdrawalAddress;
  getAddressState: TAddressState;
  balanceWithdraw: TBalanceWithdraw;
  authToken: IAuthToken;
  depositAddress: object;
  withdrawalAddress: object;
  depositWithdrawalsHistory: IDepositWithdrawalsHistoryItem[];
  userAssets: IUserAsset[];
}

interface IUser extends IUserAuthentificateParams {
  first_name: string;
  last_name: string;
  email: string;
  ui_settings: any;
  affiliate_level: number;
  vip_level?: number;
  locale: string;
  entered_competitions: number[] | null;
}

interface IAppState extends IPublicState, IUserState {}

interface IUserAssetAsset {
  name: string;
  corecode: number;
  scale: number;
  convertible_to: string[];
  asset_type: string;
  is_available: boolean;
  min_deposit: number;
  min_withdrawal: number;
  deposit_fee: number;
  withdrawal_fee: number;
}

interface IUserAsset {
  asset: IUserAssetAsset;
  deposit_address: string | null;
  withdrawal_address: string | null;
  deposit_address_requested: boolean;
  withdrawal_address_submitted: boolean;
}

interface ILocale {
  code: string;
  momentCode: string;
  chartCode: string;
  title: string;
  nativeTitle: string;
}

interface IBucketListParam {
  interval: string;
  since: number;
  assets: IBaseCounter[];
}

interface IBaseCounter {
  base: number;
  counter: number;
}

interface IPitSettings {
  off: boolean;
}

interface IUserAuthentificateParams {
  core_id: number;
  api_key: string;
  priv_key: string;
}

interface IAuthToken {
  token: string;
  refresh_token: string;
  expiration_date: string;
  tfa_verified: boolean;
}

interface IAuthParams {
  auth: {
    username: string;
    password: string;
  };
}

interface IBorrowConversion {
  asset_from: number;
  asset_to: number;
}

interface IPrize {
  amount: number;
  asset_iso: string;
}

interface INotification {
  type?: "NONE" | "PRIMARY" | "SUCCESS" | "WARNING" | "DANGER";
  title?: string;
  message?: string;
}

type TStrategy = "dash" | "standard" | "squared_roi";

interface ICompetition {
  id: string;
  title: string;
  organiserName: string;
  deposit: number;
  deposit_asset_iso: string;
  contribution: string;
  contribution_asset_iso: string;
  participants: number;
  strategy: TStrategy;
  prize: IPrize[];
  start_ts: string;
  end_ts: string;
}

interface IWithdrawalData {
  asset_iso: string;
  tfa_code: string;
  amount: number;
}

interface IWithdrawalAddressData {
  asset_iso: string;
  tfa_code: string;
  address: string;
}
