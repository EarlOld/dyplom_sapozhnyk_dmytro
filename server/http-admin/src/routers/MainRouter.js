import React, { Component, Suspense } from "react";
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import AuthGuardProvider from "components/AuthGuardProvider";
import Home from "components/Home";
import Pit from "components/Pit";
import Dashboard from "components/Dashboard";
import Login from "components/Login";
import { PUBLIC_URL } from "config";

export const authenticated = Component => props => (
  <AuthGuardProvider>
    <Component {...props} />
  </AuthGuardProvider>
);

class Routers extends Component {
  render() {
    return (
      <Suspense className="routers" fallback="">
        <BrowserRouter>
          <Switch>
            <Route path={PUBLIC_URL + "/markets"} exact component={Home} />
            <Route path={PUBLIC_URL + "/markets/:id"} exact component={Home} />
            <Route path={PUBLIC_URL + "/login"} exact component={Login} />
            <Route path={PUBLIC_URL + "/pit"} exact component={Pit} />
            <Route path={PUBLIC_URL + "/pit/:chatId"} exact component={Pit} />
            <Route path={PUBLIC_URL || "/"} exact>
              <Redirect to={"/dashboard"} />
            </Route>
            <Route
              path={PUBLIC_URL + "/dashboard"}
              component={authenticated(Dashboard)}
            />
          </Switch>
        </BrowserRouter>
      </Suspense>
    );
  }
}

export default Routers;
